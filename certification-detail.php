<? include 'partials/header-cloud.php'; ?>

<main>
    <section class="course-header">
        <div class="row">
            <div class="column small-12 medium-7 large-8">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Учебный центр</a></li>
                    <li><a href="#">О нас</a></li>
                </ul>
                <div class="course-header__wrapper">
                    <div class="course-header__title">
                        <h1>Сертификация "MCSA: Windows Server 2008"</h1>
                        <p>
                            Статус после получения: Microsoft Certified Solutions Associate: Windows Server 2008
                        </p>
                    </div>
                </div>
            </div>
            <div class="column small-12 medium-5 large-3 large-offset-1">
                <div class="feedback-header">
                    <div class="feedback-header__name">
                        <span>Светлана Жученко</span>
                    </div>
                    <div class="feedback-header__photo">
                        <img src="dist/images/feedback__header/feedback-header__photo/photo-1.png" alt="">
                    </div>
                    <div class="feedback-header__position">
                        <span>менеджер</span>
                        <span>интернет-магазина</span>
                    </div>
                    <div class="feedback-header__contacts">
                        <span>8 (800) 200-08-60 доб. 6011</span>
                        <a href="Svetlana.Zhuchenko@softlinegroup.com">Svetlana.Zhuchenko@softlinegroup.com</a>
                    </div>

                    <a href="#" class="button expanded">Связаться сейчас</a>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div style="padding-bottom: 8rem" class="column small-12 large-8">
                <div class="row">
                    <div class="column small-12 large-3">
                        <a href="#"><img src="content/certification/layer-306.png" alt=""></a>
                    </div>
                    <div class="column small-12 large-9">
                        <div class="b-editor">
                            <p>
                                Данная сертификация представляет собой первую ступень для сертифицированных специалистов
                                Microsoft, специализирующихся на продуктах Windows Server. Получение статуса MCSA:
                                Windows Server 2008 подтверждает наличие у кандидатов основных навыков по работе с
                                Windows Server 2008, умение внедрять различные решения по использованию данного продукта
                                в корпоративных средах, знания установки, настройки, поддержки, администрирования
                                серверов, сетевой инфраструктуры и службы каталогов Active Directory.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="column small-12 large-3">
                        <p style="margin-bottom: 3rem;"><strong>Центр тестирования:</strong></p>
                        <a href="#"><img src="content/certification/layer-305.png" alt=""></a>
                    </div>
                    <div class="column small-12 large-9">
                        <div class="b-editor">
                            <p>
                                Сертификация MCSA это основа вашей IT карьеры. Работодатели заинтересованы в
                                специалисте, компетентность которого подтверждается международными сертификатами
                                Microsoft. Получение статуса MCSA - это первый шаг к статусу MCSE - стандарту IT
                                профессионализма, признанному во всем мире.
                            </p>
                        </div>
                        <div>
                            <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                            <script src="//yastatic.net/share2/share.js"></script>
                            <div class="ya-share2" data-services="facebook,gplus,twitter" data-counter=""></div>
                        </div>
                    </div>
                </div>
                <div class="spacer spacer-60"></div>
                <div class="tab-box">
                    <ul class="tabs" data-tabs id="example-tabs">
                        <li class="tabs-title is-active">
                            <a href="#panel1" aria-selected="true">Схема получения сертификации</a>
                        </li>
                    </ul>

                    <div class="tabs-content" data-tabs-content="example-tabs">
                        <div class="tabs-panel is-active" id="panel1">
                            <div class="certificate-version">
                                <div class="certificate-version__title"
                                     onclick="$(this).closest('.certificate-version').toggleClass('is-opened').find('.certificate-version__content').stop().slideToggle(); return false;">
                                    <h4>Вариант 1</h4>
                                    <svg class="icon arrow">
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </div>
                                <div class="certificate-version__content">
                                    <div class="certificate-version__table-container">
                                        <table class="hide-for-medium">
                                            <thead>
                                            <tr>
                                                <td></td>
                                                <td>Рекомендуемые курсы
                                                    для подготовки к экзамену:
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" id="input_1">
                                                    <label for="input_1"></label>
                                                </td>
                                                <td>
                                                    <strong>Планирование и внедрение Windows Server 2008</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td width="60">Уровень:</td>
                                                            <td width="140">продвинутый</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="70">Цена: <i class="icon icon-help">?</i></td>
                                                            <td width="140">29 000 руб. </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table class="show-for-medium">
                                            <thead>
                                            <tr>
                                                <td width="36"></td>
                                                <td>Рекомендуемые курсы для подготовки к экзамену:</td>
                                                <td width="150">Уровень:</td>
                                                <td width="130">Цена: <i class="icon icon-help">?</i></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" id="input_1">
                                                    <label for="input_1"></label>
                                                </td>
                                                <td><strong>Планирование и внедрение Windows Server 2008</strong></td>
                                                <td>продвинутый</td>
                                                <td>29 000 руб.</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="certificate-version__table-container">
                                        <table class="hide-for-medium">
                                            <thead>
                                            <tr>
                                                <td></td>
                                                <td>Экзамен:
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" id="input_1">
                                                    <label for="input_1"></label>
                                                </td>
                                                <td>
                                                    <strong>Планирование и внедрение Windows Server 2008</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td width="60">Города:</td>
                                                            <td width="140">Москва<br>Санкт-Петербург</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="70">Цена: <i class="icon icon-help">?</i></td>
                                                            <td width="140">6 500 руб.</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table class="show-for-medium">
                                            <thead>
                                            <tr>
                                                <td width="36"></td>
                                                <td>Экзамен:</td>
                                                <td width="150">Города:</td>
                                                <td width="130">Цена экзамена: <i class="icon icon-help">?</i></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" id="input_2">
                                                    <label for="input_2"></label>
                                                </td>
                                                <td><strong>Windows Server 2008, Server Administrator</strong></td>
                                                <td>Москва<br>Санкт-Петербург</td>
                                                <td>6 500 руб.</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="certificate-version">
                                <div class="certificate-version__title"
                                     onclick="$(this).closest('.certificate-version').toggleClass('is-opened').find('.certificate-version__content').stop().slideToggle(); return false;">
                                    <h4>Вариант 2</h4>
                                    <svg class="icon arrow">
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </div>
                                <div class="certificate-version__content">
                                    <div class="certificate-version__table-container">
                                        <table class="hide-for-medium">
                                            <thead>
                                            <tr>
                                                <td></td>
                                                <td>Рекомендуемые курсы для подготовки к экзамену:</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" id="input_1">
                                                    <label for="input_1"></label>
                                                </td>
                                                <td>
                                                    <strong>Конфигурирование решений по защищенному доступу на базе
                                                        Windows Server 2008 Active Directory</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td width="60">Уровень:</td>
                                                            <td width="140">продвинутыйг</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="70">Цена: <i class="icon icon-help">?</i></td>
                                                            <td width="140">19 000 руб.</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" id="input_1">
                                                    <label for="input_1"></label>
                                                </td>
                                                <td>
                                                    <strong>Конфигурирование службы каталогов Windows Server 2008 Active
                                                        Directory (R2)</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td width="60">Уровень:</td>
                                                            <td width="140">продвинутыйг</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="70">Цена: <i class="icon icon-help">?</i></td>
                                                            <td width="140">19 000 руб.</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table class="show-for-medium">
                                            <thead>
                                            <tr>
                                                <td width="36"></td>
                                                <td>Рекомендуемые курсы для подготовки к экзамену:</td>
                                                <td width="150">Уровень:</td>
                                                <td width="130">Цена: <i class="icon icon-help">?</i></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" id="input_3">
                                                    <label for="input_3"></label>
                                                </td>
                                                <td><strong>Конфигурирование решений по защищенному доступу на базе
                                                        Windows Server 2008 Active Directory</strong></td>
                                                <td>продвинутый</td>
                                                <td>19 000 руб.</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" id="input_4">
                                                    <label for="input_4"></label>
                                                </td>
                                                <td><strong>Конфигурирование службы каталогов Windows Server 2008 Active
                                                        Directory (R2)</strong></td>
                                                <td>продвинутый</td>
                                                <td>19 000 руб.</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="certificate-version__table-container">
                                        <table class="hide-for-medium">
                                            <thead>
                                            <tr>
                                                <td></td>
                                                <td>Экзамен:
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" id="input_1">
                                                    <label for="input_1"></label>
                                                </td>
                                                <td>
                                                    <strong>Планирование и внедрение Windows Server 2008</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td width="60">Города:</td>
                                                            <td width="140">Москва<br>Санкт-Петербург</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="70">Цена: <i class="icon icon-help">?</i></td>
                                                            <td width="140">6 500 руб.</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table class="show-for-medium">
                                            <thead>
                                            <tr>
                                                <td width="36"></td>
                                                <td>Экзамен:</td>
                                                <td width="150">Города:</td>
                                                <td width="130">Цена экзамена: <i class="icon icon-help">?</i></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" id="input_2">
                                                    <label for="input_2"></label>
                                                </td>
                                                <td><strong>Windows Server 2008, Server Administrator</strong></td>
                                                <td>Москва<br>Санкт-Петербург</td>
                                                <td>6 500 руб.</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="certificate-amount">
                                <table>
                                    <tr>
                                        <td>
                                            Общая цена подготовки (4 курсa) <i class="icon icon-help">?</i>
                                        </td>
                                        <td width="60"></td>
                                        <td width="160">106 000 руб.</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Общая цена сертификации (3 экзаменa) <i class="icon icon-help">?</i>
                                        </td>
                                        <td width="60"></td>
                                        <td width="160">19 500 руб.</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="certificate-amount__result">
                                                <div class="certificate-amount__result-icon">
                                                    <svg class="icon check">
                                                        <use xlink:href="#images--svg--checked"></use>
                                                    </svg>
                                                </div>
                                                <div>
                                                    <small>Сертификат:</small>
                                                    <div>MCSA: Windows Server 2008</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td width="60">Итого:</td>
                                        <td width="160">
                                            125 500 руб.
                                            <br>
                                            <div class="additional">
                                                Скидки распространяются на клиентов РФ
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                                <div class="b-editor">
                                    <p>
                                        Отправьте заявку на получение сертификата и мы сформируем для вас удобный
                                        календарь для прохождения нужных курсов и сдачи экзаменов. Если вы уже прошли
                                        обучение по каким-то курсам или сдали экзамены, то, пожалуйста, снимите галочку
                                        в схеме.
                                    </p>
                                </div>

                                <button class="button small">Отправить заявку</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column small-12 large-offset-1 large-3">
                <aside>
                    <div class="aside-widget">
                        <div class="aside-widget__title">О центре</div>
                        <div class="aside-widget__content">
                            <ul class="aside-menu">


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/mission">

                                        <span class="b-link__text">Программирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/training-conditions">

                                        <span class="b-link__text">Системное администрирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class=""
                                       href="http://edu.softline.ru/about/svedeniya-ob-obrazovatelnoy-organizatsii">

                                        <span class="b-link__text">Пользовательское ПО</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/accreditation">

                                        <span class="b-link__text">Операционные системы (ОС)</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/status">

                                        <span class="b-link__text">Сетевые технологии</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-klientov">

                                        <span class="b-link__text">Информационная безопасность</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-partnerov">

                                        <span class="b-link__text">Виртуализация</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/razrabotka-kursov">

                                        <span class="b-link__text">Базы данных и СУБД</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/delivery-info">

                                        <span class="b-link__text">Моделирование и САПР</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/requisites">

                                        <span class="b-link__text">Коммуникации</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/tech-support">

                                        <span class="b-link__text">Резервное копирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">CRM</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Бизнес-аналитика</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">IT-сервис менеджмент</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Импортозамещение ПО</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Бизнес-тренинги</span>
                                    </a>

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__title">Подбор курса</div>
                        <div class="aside-widget__content">
                            <form action="#">
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Направление:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все направления</option>
                                            <option value="">Все направления 2</option>
                                            <option value="">Все направления 3</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Специальность:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все специальности</option>
                                            <option value="">Все специальности 2</option>
                                            <option value="">Все специальности 3</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Вендор:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все Вендоры</option>
                                            <option value="">Все Вендоры 2</option>
                                            <option value="">Все Вендоры 3</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Вендор:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все Вендоры</option>
                                            <option value="">Все Вендоры 2</option>
                                            <option value="">Все Вендоры 3</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="radio-box">
                                    <label class="b-radiobox">
                                        <input name="event-filter" type="radio">
                                        <span>Очно (в аудитории)</span>
                                    </label>
                                    <label class="b-radiobox">
                                        <input name="event-filter" type="radio">
                                        <span>Дистанционно</span>
                                    </label>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Место проведения:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Москва</option>
                                            <option value="">Москва 2</option>
                                            <option value="">Москва 3</option>
                                        </select>
                                    </label>
                                </div>
                                <button class="button small expanded">Подобрать курс</button>
                            </form>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__title">Будьте в курсе</div>
                        <div class="aside-widget__content">
                            <p>Подпишитесь на рассылку новостей Softline</p>
                            <form action="#">
                                <label for="">
                                    <input type="text" placeholder="Ваш e-mail">
                                </label>
                                <label for="">
                                    <input type="text" placeholder="Ваше имя">
                                </label>
                                <button class="button small expanded">Подписаться</button>
                            </form>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>
    <section class="subscribe-banner">
        <div class="subscribe-banner__wrapper">
            <div class="row">
                <div class="column small-12 large-9 large-offset-3">
                    <div class="subscribe-banner__title">
                        <span>Будьте в курсе</span>
                    </div>
                    <form class="subscribe-banner__form">
                        <div class="subscribe-banner__input subscribe-banner__input--name">
                            <input type="text" placeholder="Ваше имя">
                        </div>
                        <div class="subscribe-banner__input subscribe-banner__input--email">
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="subscribe-banner__button">
                            <button class="button">Подписаться</button>
                        </div>
                    </form>
                    <div class="subscribe-banner__desc">
                        <span>Подпишитесь на информацию о новинках, скидках и акциях. <br> Уже более 36 000 подписчиков!</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<footer class="footer">
    <div class="row footer__middle">
        <div class="column small-12 large-3">
            <div class="footer__contacts">
                <div class="row">
                    <div class="column small-12 medium-6 large-12">
                        <div class="footer__contacts-item">
                            <span><a href="mailto:services@softlinegroup.com">services@softlinegroup.com</a></span>
                        </div>
                        <div class="footer__contacts-item">
                            <span><a href="tel:+74952320023">+7(495) 232-0023</a></span> <br>
                            <span><a href="tel:88002320023">8 800 2320023</a> </span>
                        </div>
                    </div>
                    <div class="column small-12 medium-6 large-12">
                        <div class="footer__contacts-item footer__contacts-item--address">
                            <span>115114, Москва, <br class="show-for-medium">Дербеневская наб., <br
                                        class="show-for-medium"> дом 7, стр. 8</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column small-12 medium-2">
            <ul class="footer__link">
                <li><a href="#">Программное обеспечение</a></li>
                <li><a href="#">Услуги и решения</a></li>
                <li><a href="#">IT-обучение</a></li>
                <li><a href="#">Специальные предложения</a></li>
                <li><a href="#">IT-события</a></li>
            </ul>
        </div>
        <div class="column small-12 medium-2">
            <ul class="footer__link">
                <li><a href="#">Новости компании </a></li>
                <li><a href="#">Мобильные <br class="show-for-medium"> приложения</a></li>
                <li><a href="#">Печатный <br class="show-for-medium"> каталог ПО</a></li>
                <li><a href="#">Облачные решения</a></li>
                <li><a href="#">Качество <br class="show-for-medium"> и ISO 9001</a></li>
            </ul>
        </div>
        <div class="column small-12 medium-2">
            <ul class="footer__link">
                <li><a href="#">Техподдержка </a></li>
                <li><a href="#">О компании</a></li>
                <li><a href="#">Вакансии</a></li>
                <li><a href="#">Контакты</a></li>
                <li><a href="#">Приемная Softline</a></li>
                <li><a href="#">Группа компаний Softline</a></li>
            </ul>
        </div>
        <div class="column small-12 medium-6 large-2">

            <div class="footer__social">
                <p style="margin: 0 -30px 0 0">Свяжитесь с нами:</p>
            </div>
            <div class="footer__social">
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon facebook">
                            <use xlink:href="#images--svg--facebook"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon twitter">
                            <use xlink:href="#images--svg--twitter"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon vk">
                            <use xlink:href="#images--svg--vk"></use>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="footer__social">
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon youtube">
                            <use xlink:href="#images--svg--youtube"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon linkedin">
                            <use xlink:href="#images--svg--linkedin"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon habr">
                            <use xlink:href="#images--svg--habr"></use>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="footer__social">
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon google-plus">
                            <use xlink:href="#images--svg--google+"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon instagram">
                            <use xlink:href="#images--svg--instagram"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon telegram">
                            <use xlink:href="#images--svg--telegram"></use>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="column small-12 show-for-medium">
            <hr class="footer__divider">
        </div>
        <div class="column small-12 medium-3">
            <ul class="nav-area show-for-medium">
                <li>
                    <button>RU</button>
                </li>
            </ul>
        </div>
        <div class="column small-12 medium-2">
            <div class="footer__company">© 1993—2017 Softline</div>
        </div>
        <div class="column small-12 medium-2">
            <div class="footer__terms"><a href="#">Условия использования</a></div>
        </div>
        <div class="column small-12 medium-2"><span class="footer__age-limit">14+</span></div>
    </div>
</footer>

<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        $(document).foundation();
        //$('.b-select').select2();
    });
</script>
</body>
</html>

