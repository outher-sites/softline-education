<? include 'partials/header.php'; ?>

<main>
    <section class="banner">
            <div class="banner__slide" style="background-image:url(dist/images/standard-banner/bg-1.jpg);">
                <div class="row">
                    <div class="column small-12 medium-6 xlarge-7">
                        <div class="banner__title text-center medium-text-left">
                            <span class="h1">Новые возможности<br
                                        class="show-for-large">с программой Software Assurance</span>
                        </div>
                        <div class="banner__desc banner__desc--padding text-center medium-text-left ">

                        </div>
                        <div class="banner__link">

                        </div>
                    </div>
                </div>
            </div>
    </section>
    <section>
        <div class="row">
            <div style="padding-bottom: 8rem" class="column small-12">
                <div style="padding-top: 25px" class="b-editor">

                    <h2>Теперь тренинг-дни SATV можно использовать для заказа: </h2>
                    <ul>
                        <li>MCP экзаменов
                        </li>

                        <li>Сертификатов о завершении курсов MOOC по программе LaaS (Microsoft Certificate of
                            Completion)
                        </li>
                    </ul>

                    <ul id="examinations-tabs">
                        <li class="tabs-title"><button class="button" onclick="$('#examinations-1').slideDown();">Узнать больше о возможности бесплатной сертификации</button></li>
                    </ul>
                    <div>
                        <div id="examinations-1" style="display: none;">
                            <p>
                                Теперь благодаря преимуществу программы <strong>Software Assurance</strong> в рамках
                                лицензионного соглашения с
                                <strong>Microsoft</strong> обучение и сертификация сотрудников могут стать абсолютно
                                бесплатными* </p>
                            <p>Используйте SATV-дни на очное обучение в нашем учебном центре <strong>Softline</strong> и
                                заказ
                                ваучеров на
                                экзамены. <br class="show-for-large">
                                или
                            </p>

                            <p>Обучайтесь самостоятельно с помощью <strong>Massive Open Online Courses</strong> от
                                <strong>Microsoft</strong>
                                на нашей платформе,
                                получайте сертификаты и заказывайте экзамены:</p>

                            <div class="examinations">
                                <div class="examinations__wrapper">
                                    <div class="examinations__title">
                                        <p>MCP Экзамен
                                            <br class="show-for-large"> (1 SATV День)</p>
                                    </div>

                                    <ul class="examinations__note">
                                        <li>Все экзамены Microsoft c префиксами 70- и 74-, доступные через PearsonVUE
                                        </li>
                                        <li>Код ваучера активен в течение 12 месяцев с момента получения</li>
                                    </ul>
                                </div>
                                <div class="examinations__wrapper">
                                    <div class="examinations__title">
                                        <p>
                                            Microsoft Certificate of Completion (LaaS) -
                                            (1 SATV День)
                                        </p>
                                    </div>
                                    <ul class="examinations__note">
                                        <li>Сертификат можно заказать по окончании MOOCs на нашей платформе</li>
                                        <li>Код ваучера активен в течение 12 месяцев с момента получения</li>
                                    </ul>
                                </div>
                            </div>

                            <p>
                                *Программа Software Assurance Training Vouchers доступна только для организаций. Ваша
                                компания
                                получает некоторое количество учебных дней с учетом количества квалификационных лицензий
                                на
                                программное обеспечение Microsoft Office и/или Windows, подпадающих под условия
                                программы
                                Software Assurance.
                            </p>
                        </div>
                    </div>
                    <h2>Как проверить количество SATV Days, закреплённых за Вашей организацией:</h2>

                    <p>Вы можете проверить количество учебных дней Software Assurance в рамках текущего лицензионного
                        соглашения с Майкрософт самостоятельно на сайте <a href="https://licensing.microsoft.com/Customer/">Volume Licensing Service Center</a> или заполнив
                        письмо согласия (Letter of Authority).</p>
                    <a href="#" class="button">Скачать Letter of Authority</a>
                    <div style="padding-bottom: 25px" class="medium-flex-container">
                        <div style="padding-bottom: 20px">
                            <p><strong>Необходимо заполнить:</strong></p>
                            <ul>
                                <li>Номер лицензионного соглашения</li>
                                <li>Название компании (не обязательно, но приветствуется)</li>
                                <li>Имя, фамилия и электронный адрес контактного лица.</li>
                            </ul>
                        </div>
                        <div style="padding-bottom: 20px">
                            <p><strong>Вы получите:</strong></p>
                            <ul>
                                <li>Количество оставшихся дней обучения, если есть</li>
                                <li>Дату начала и окончания соглашения</li>
                                <li>Имя администратора SATV</li>
                            </ul>
                        </div>
                    </div>

                </div>

                <div>
                    <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                    <script src="//yastatic.net/share2/share.js"></script>
                    <div class="ya-share2" data-services="facebook,gplus,twitter" data-counter=""></div>
                </div>
            </div>
            <div class="column small-12 large-offset-1 large-3">

            </div>
        </div>
    </section>
    <section class="subscribe-banner">
        <div class="subscribe-banner__wrapper">
            <div class="row">
                <div class="column small-12 large-9 large-offset-3">
                    <div class="subscribe-banner__title">
                        <span>Будьте в курсе</span>
                    </div>
                    <form class="subscribe-banner__form">
                        <div class="subscribe-banner__input subscribe-banner__input--name">
                            <input type="text" placeholder="Ваше имя">
                        </div>
                        <div class="subscribe-banner__input subscribe-banner__input--email">
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="subscribe-banner__button">
                            <button class="button">Подписаться</button>
                        </div>
                    </form>
                    <div class="subscribe-banner__desc">
                        <span>Подпишитесь на информацию о новинках, скидках и акциях. <br> Уже более 36 000 подписчиков!</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
<script>
        $(document).foundation();
</script>

</body>
</html>

