<? include 'partials/header.php'; ?>
<main>
    <section>
        <div class="row">
            <div class="column small-12 medium-9">
                <div class="b-title">
                    <h5>Мои курсы</h5>
                </div>
            </div>
            <div class="column small-12 medium-9">
                <div class="course-card">
                    <div class="course-card__photo">
                        <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                    </div>
                    <div class="course-card__cell">
                        <div class="course-card__title">
                            <span>Java Standard Edition (Java SE). Программирование. Базовые технологии</span>
                        </div>
                        <div class="course-card__details">
                            <div class="row small-up-1 large-up-2">
                                <div class="column">
                                    <div class="course-info">
                                        <div class="course-info__cell">
                                            <div class="course-info__name"><span>Производитель:</span></div>
                                            <div class="course-info__value"><a href="#">Java</a></div>
                                        </div>
                                        <div class="course-info__cell">
                                            <div class="course-info__name">Формат обучения:</div>
                                            <div class="course-info__value">Производит</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="column">
                                    <div class="course-info course-info--basis-10">
                                        <div class="course-info__cell">
                                            <div class="course-info__name"><span>Уровень:</span></div>
                                            <div class="course-info__value"><span>Начальный</span></div>
                                        </div>
                                        <div class="course-info__cell">
                                            <div  class="course-info__name"><span>Цена (от):</span></div>
                                            <div class="course-info__value">
                                                <span>35 100,00 руб. </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="course-card__desc">
                            <p>
                                В предлагаемой программе даётся обзор платформ: Java Standard Edition (J2SE/Java SE 8) и
                                Java Enterprise Edition (J2EE / Java EE 7)
                            </p>
                        </div>
                        <div class="course-card__button">
                            <a href="#" class="button">расписание и цены</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
</main>
<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
</body>
</html>
