<? include 'partials/header.php'; ?>

<section class="service">
    <div class="row">
        <div class="column small-12 medium-3">
            <div class="service__title">
                <span>SAP хостинг</span>
            </div>
        </div>
        <div class="column small-12 medium-9">
            <div class="service__desc">
                <p>
                    Облачный SAP-хостинг стал отличной альтернативой закупке дорогого железа
                    и размещению систем внутри компании. Теперь не нужно заказывать дополнительное оборудование для
                    бизнес-приложений. Хостинг на сторонней площадке облегчает работу внутренней локальной
                    инфраструктуры организации, избавляя ее от лишней нагрузки.
                </p>
                <p>
                    В рамках услуги SAP-хостинг компания Softline берет на себя вопросы миграции, администрирования,
                    технического обслуживания и сопровождения приложений SAP, гарантируя работоспособность систем SAP в
                    облаке.
                </p>
            </div>
            <div class="feature">
                <div class="row small-up-1 medium-up-3">
                    <div class="column">
                        <div class="feature__card">
                            <div class="feature__card-icon">
                                <img src="/dist/images/feature/feature__card-icon/elastic.png" alt="">
                            </div>
                            <div class="feature__card-title"><span>Эластичность</span></div>
                            <div class="feature__card-desc">
                                <p>
                                    Возможность быстро увеличивать или у меньшать
                                    объем используемых услуг в соответствии с потребностями
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="feature__card">
                            <div class="feature__card-icon">
                                <img src="/dist/images/feature/feature__card-icon/economy.png" alt="">
                            </div>
                            <div class="feature__card-title"><span>Экономичность</span></div>
                            <div class="feature__card-desc">
                                <p>
                                    Оплата только за то, чем пользуетесь, снижая до минимума затраты на оборудование
                                    и ИТ
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="feature__card">
                            <div class="feature__card-icon">
                                <img src="/dist/images/feature/feature__card-icon/accessibility.png" alt="">
                            </div>
                            <div class="feature__card-title"><span>Доступность</span></div>
                            <div class="feature__card-desc">
                                <p>
                                    К облачным системам можно обращаться круглосуточно,
                                    из любого места и с любого устройства
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="feature__card">
                            <div class="feature__card-icon">
                                <img src="/dist/images/feature/feature__card-icon/Simplicity.png" alt="">
                            </div>
                            <div class="feature__card-title"><span>Простота</span></div>
                            <div class="feature__card-desc">
                                <p>
                                    Ваши ИТ-службы будут освобождены от обязанностей
                                    поддержки серверов и обновления приложений
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="feature__card">
                            <div class="feature__card-icon">
                                <img src="/dist/images/feature/feature__card-icon/fastest.png" alt="">
                            </div>
                            <div class="feature__card-title"><span>Быстрота</span></div>
                            <div class="feature__card-desc">
                                <p>
                                    Более быстрый процесс внедрения с меньшими предварительными затратами.
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<section class="callback-banner">
    <div class="callback-banner__wrapper">
        <div class="row">
            <div class="column small-12">
                <div class="callback-banner__title">
                    <span>Запросить консультацию</span>
                </div>
                <div class="callback-banner__desc">
                    <p>Заполните форму, и наш специалист свяжется с вами,
                        чтобы проконсультировать вас по развертыванию систем SAP в облаке.</p>
                </div>
                <form class="callback-banner__form">
                    <div class="callback-banner__input">
                        <input type="text" placeholder="Имя">
                    </div>
                    <div class="callback-banner__input">
                        <input type="text" placeholder="Телефон">
                    </div>
                    <div class="callback-banner__input" >
                        <input type="text" placeholder="E-mail">
                    </div>
                    <button class="callback-banner__button"></button>
                </form>
                <div class="callback-banner__desc">
                    <p><span>Это абсолютно бесплатно, и очень оперативно.</span></p>
                </div>
            </div>
        </div>
    </div>
</section>
<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
</body>
</html>

