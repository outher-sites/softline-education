

<section class="subscribe-banner">
    <div class="subscribe-banner__wrapper">
        <div class="row">
            <div class="column small-12 large-9 large-offset-3">
                <div class="subscribe-banner__title">
                    <span>Будьте в курсе</span>
                </div>
                <form class="subscribe-banner__form">
                    <div class="subscribe-banner__input subscribe-banner__input--name"> <!--доп модификатор: error-->
                        <input type="text" placeholder="Ваше имя">
                    </div>
                    <div class="subscribe-banner__input subscribe-banner__input--email">
                        <input type="text" placeholder="Ваш e-mail">
                    </div>
                    <div class="subscribe-banner__button">
                        <button class="button">Подписаться</button>
                    </div>
                </form>
                <div class="subscribe-banner__desc">
                    <span>Подпишитесь на информацию о новинках, скидках и акциях. <br> Уже более 36 000 подписчиков!</span>
                </div>
            </div>
        </div>
    </div>
</section>