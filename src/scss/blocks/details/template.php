<section class="details">
    <div class="row">
        <div class="column medium-offset-6 medium-6 large-offset-7 large-offset-5  small-12 ">
            <div class="details__wrapper">
                <div class="details__title h1"><span>Нам можно доверять</span></div>
                <div class="details__desc">
                    <p>
                        Мы выполнили более 2000 проектов. От серверного оборудования до бытовых мелочей: мы
                        поставляем
                        широчайший список IT-оборудования для нужд бизнеса
                        и госкомпаний.
                    </p>
                </div>
                <div class="details__button">
                    <a href="#" class="button">Подробнее о Softline</a>
                </div>
            </div>
        </div>
    </div>
</section>