<section style="background-image: url(dist/images/standard-banner/bg-1.jpg);" class="standard-banner">
    <div class="standard-banner__wrapper">
        <div class="row">
            <div class="column small-12 medium-8">
                <div class="standard-banner__title">
                    <h2>Новые возможности
                        <br class="show-for-medium">  с программой Software Assurance</h2>
                </div>
                <a href="#" class="button">Подробнее</a>
            </div>
        </div>
    </div>
</section>