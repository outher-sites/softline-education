
<section class="partners">
    <div class="row">
        <div class="column small-12">
            <h2>Наши партнёры</h2>
        </div>
    </div>
    <div class="partners__card-wrapper row small-up-1 medium-up-3 large-up-4">
        <div class="column">
            <div class="flex-container">
                <a href="http://edu.softline.ru/vendors/microsoft" class="b-card">
                    <img src="dist/images/partners/partners__card/microsoft.png" alt="">
                </a>
            </div>
        </div>
        <div class="column">
            <div class="flex-container">
                <a href="http://edu.softline.ru/vendors/cisco-systems" class="b-card">
                    <img src="content/partners/cisco.png" alt="">
                </a>
            </div>
        </div>
        <div class="column">
            <div class="flex-container">
                <a href="http://edu.softline.ru/vendors/veeam-vmce" class="b-card">
                    <img src="content/partners/veeam.png" alt="">
                </a>
            </div>
        </div>
        <div class="column">
            <div class="flex-container">
                <a href="http://edu.softline.ru/vendors/vmware" class="b-card">
                    <img src="content/partners/vmware.png" alt="">
                </a>
            </div>
        </div>
        <div class="column">
            <div class="flex-container">
                <a href="http://edu.softline.ru/vendors/kaspersky" class="b-card">
                    <img src="content/partners/kaspersky.jpg" alt="">
                </a>
            </div>
        </div>
        <div class="column">
            <div class="flex-container">
                <a href="http://edu.softline.ru/vendors/oracle" class="b-card">
                    <img src="content/partners/oracle.png" alt="">
                </a>
            </div>
        </div>
        <div class="column">
            <div class="flex-container">
                <a href="http://edu.softline.ru/vendors/ibm" class="b-card">
                    <img src="content/partners/ibm.png" alt="">
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="small-12 column">
            <hr>
        </div>
    </div>
</section>