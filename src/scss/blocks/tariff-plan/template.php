<section class="tariff-plan">
    <div class="row">
        <div class="column small-12">
            <table class="tariff-plan__table" width="100%" cellpadding="0" cellspacing="0">
                <tr class="tariff-plan__top">
                    <td width="40%"></td>
                    <td width="20%"><span>Office 365 Бизнес</span></td>
                    <td width="20%"><span>Office 365 Бизнес базовый</span></td>
                    <td width="20%"><span>Office 365 Бизнес премиум</span></td>
                </tr>
                <tr class="tariff-plan__row">
                    <td><span>Версия Office в браузере, доступно только в браузере</span></td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                </tr>
                <tr class="tariff-plan__row">
                    <td><span>Файловое хранилище с функциями общего доступа</span></td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                </tr>
                <tr class="tariff-plan__row">
                    <td><span>Интерактивная презентация документов в цифровом формате</span></td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                </tr>
                <tr class="tariff-plan__row">
                    <td><span>Набор приложений Office для компьютеров, планшетов и телефонов</span></td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                </tr>
                <tr class="tariff-plan__row">
                    <td><span>Корпоративная социальная сеть</span></td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon unchecked"><use xlink:href="#images--svg--unchecked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                </tr>
                <tr class="tariff-plan__row">
                    <td><span>Групповые и неограниченные видеоконференции в формате HD</span></td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon unchecked"><use xlink:href="#images--svg--unchecked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                </tr>
                <tr class="tariff-plan__row">
                    <td><span>Электронная почта бизнес-класса, календарь и контакты</span></td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon unchecked"><use xlink:href="#images--svg--unchecked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                </tr>
                <tr class="tariff-plan__row">
                    <td><span>Анализ и сбор данных о совместной работе</span></td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon unchecked"><use xlink:href="#images--svg--unchecked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                </tr>
                <tr class="tariff-plan__row">
                    <td><span>Набор приложений Office для компьютеров, планшетов и телефонов</span></td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon unchecked"><use xlink:href="#images--svg--unchecked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon unchecked"><use xlink:href="#images--svg--unchecked"></use></svg>
                                </span>
                    </td>
                    <td width="20%">
                                <span class="checked">
                                    <svg class="icon checked"><use xlink:href="#images--svg--checked"></use></svg>
                                </span>
                    </td>
                </tr>
                <tr class="tariff-plan__row">
                    <td><strong>Стоимость, руб</strong></td>
                    <td width="20%">
                        <strong>5 671,00</strong>
                    </td>
                    <td width="20%">
                        <strong>312,00</strong>
                    </td>
                    <td width="20%">
                        <strong>8 591,00</strong>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</section>