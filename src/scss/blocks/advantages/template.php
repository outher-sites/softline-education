<section class="advantages">
    <div class="row small-up-1 medium-up-3">
        <div class="column">
            <div class="advantages__card">
                <div class="advantages__icon"></div>
                <div class="advantages__desc">
                    <span>ЦОДы уровня <br> Tier-3</span>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="advantages__card">
                <div class="advantages__icon"></div>
                <div class="advantages__desc">
                    <span>SLA с финансовыми гарантиями</span>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="advantages__card">
                <div class="advantages__icon"></div>
                <div class="advantages__desc">
                    <span>Шифрованные каналы связи</span>
                </div>
            </div>
        </div>
    </div>
</section>