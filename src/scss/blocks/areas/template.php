<section class="areas">
    <div class="row">
        <div class="column small-12">
            <div class="text-center  h1">
                   <span>Предлагаем программы <br class="show-for-large">
                        для специалистов по направлениям:
                   </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="column small-12">
            <div class="areas__wrap">
                <div class="row small-up-1 medium-up-3 large-up-6 medium-collapse">
                    <div class="column">
                        <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/finance.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/finance.png)"
                       class="areas__icon"></span>
            </span>
                            <span class="areas__title">Финансы</span>
                        </a>
                    </div>
                    <div class="column">
                        <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/retail.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/retail.png)"
                       class="areas__icon"></span>
            </span>
                            <span class="areas__title">Ритейл</span>
                        </a>
                    </div>
                    <div class="column">
                        <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/tek.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/tek.png)" class="areas__icon"></span>
            </span>
                            <span class="areas__title">ТЭК</span>
                        </a>
                    </div>
                    <div class="column">
                        <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/industry.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/industry.png)" class="areas__icon"></span>
            </span>
                            <span class="areas__title">Промышленность</span>
                        </a>
                    </div>
                    <div class="column">
                        <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/sector.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/sector.png)"
                       class="areas__icon"></span>
            </span>
                            <span class="areas__title">Гос.сектор</span>
                        </a>
                    </div>
                    <div class="column">
                        <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/tele.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/tele.png)" class="areas__icon"></span>
            </span>
                            <span class="areas__title">Телеком</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>