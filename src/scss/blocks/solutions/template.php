<section class="solutions">
    <div class="row small-up-1 medium-up-2">
        <div class="column">
            <div class="solutions__card">
                <div style="background-image: url(dist/images/solutions/solutions__card/solutions-1.jpg)"
                     class="solutions__card-photo">
                    <div class="solutions__card-icon arrow-icon"></div>
                </div>
                <div class="solutions__card-wrapper">
                    <div class="solutions__card-title">
                        <span>Решения <br> для бизнеса</span>
                    </div>
                    <div class="solutions__card-desc">
                        <ul>
                            <li><a href="#">Виртуальный офис</a></li>

                            <li><a href="#">Office 365</a></li>

                            <li><a href="#">G Suite (ex. Google Apps)</a></li>

                            <li><a href="#">Корпоративная почта</a></li>

                            <li><a href="#">Виртуальный рабочий стол</a></li>

                            <li><a href="#">Непрерывность бизнеса</a></li>

                            <li><a href="#">Сапр в облаке</a></li>

                            <li><a href="#">Видеоконференцсвязь Cisco WebEx</a></li>
                        </ul>
                    </div>
                    <a href="#" class="solutions__card-button">Подробнее</a>
                </div>
            </div>
        </div>
    </div>
</section>