<? include 'partials/header.php'; ?>
<main>
    <section class="course-modules-section">
        <div class="row">
            <div class="column small-12">
                <div class="row">
                    <div class="column small-12 medium-9">
                        <div class="b-title flex-container">
                            <h1 class="h4">Developing Dynamic Web Applications Using Angular</h1>
                        </div>
                    </div>
                    <div class="column small-12 medium-3">
                        <div class="flex-container align-right medium-valign-center">
                            <a href="#" class="button small small-expanded">Открыть курс</a>
                        </div>
                    </div>
                    <div class="column small-12">
                        <hr>
                    </div>
                </div>
            </div>
            <div class="column small-12 medium-8">
                <div class="course-module">
                    <div class="course-module__title">
                        <span>Welcome</span>
                    </div>
                    <ul class="course-module__list">
                        <li>
                            <a href="#" class="last">Welcome
                                <span class="course-module__back">
                                    Вернуться к курсу
                                    <svg class="icon right-arrow-circular">
                                        <use xlink:href="#images--svg--right-arrow-circular"></use>
                                    </svg>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">About this Course</a>
                        </li>
                        <li>
                            <a href="#">Getting Started</a>
                        </li>
                    </ul>
                </div>

                <div class="course-module">
                    <div class="course-module__title">
                        <span>Getting Started with Angular</span>
                    </div>
                    <ul class="course-module__list">
                        <li>
                            <a href="#">History</a>
                        </li>
                        <li>
                            <a href="#">Node Package Manager</a>
                        </li>
                        <li>
                            <a href="#">Components</a>
                        </li>
                    </ul>
                </div>

                <div class="course-module">
                    <div class="course-module__title">
                        <span>Building Dynamic Applications using Angular</span>
                    </div>
                    <ul class="course-module__list">
                        <li>
                            <a href="#">Application Architecture</a>
                        </li>
                        <li>
                            <a href="#">Template Binding</a>
                        </li>
                        <li>
                            <a href="#">HTTP Service</a>
                        </li>
                    </ul>
                </div>

                <div class="course-module">
                    <div class="course-module__title">
                        <span>Advanced Angular Features</span>
                    </div>
                    <ul class="course-module__list">
                        <li>
                            <a href="#">Angular Lifecycle</a>
                        </li>
                        <li>
                            <a href="#">Dependency Injection
                            </a>
                        </li>
                        <li>
                            <a href="#">Application Modularity</a>
                        </li>

                    </ul>
                </div>

                <div class="course-module">
                    <div class="course-module__title">
                        <span>TypeScript Features in Angular</span>
                    </div>
                    <ul class="course-module__list">
                        <li>
                            <a href="#">Compiler</a>
                        </li>
                        <li>
                            <a href="#">Modules & Namespaces</a>
                        </li>
                        <li>
                            <a href="#">TypeScript Definition Files</a>
                        </li>
                    </ul>
                </div>

                <div class="course-module">
                    <div class="course-module__title">
                        <span>Get Microsoft Course Completion Certificate</span>
                    </div>
                    <ul class="course-module__list">
                        <li>
                            <a href="#">Instructions</a>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="column small-12 medium-4">
                <aside class="sidebar">

                    <div class="section section-tools">
                        <h5 class="hd-6">Course Tools</h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#">
                                    <svg class="icon bookmark"><use xlink:href="#images--svg--bookmark"></use></svg>
                                    Закладки
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="section section-dates">
                        <h5>Important Course Dates</h5>

                        <div class="date-summary-container">
                            <div class="date-summary date-summary-todays-date">
                                <span class="h6 localized-datetime">Сегодня 24 нояб. 2017 г. 12:47 +07</span>
                            </div>
                        </div>
                        <div class="date-summary-container">
                            <div class="date-summary date-summary-end-date">
                                <span class="h6">Курс завершится</span>
                                <p class="hd hd-6 date localized-datetime" data-format="shortDate" data-datetime="2017-12-31 00:00:00+00:00" data-timezone="None" data-language="ru" data-string="в 1 месяц - {date}">
                                    в 1 месяц - 31 дек. 2017 г.</p>
                                <p class="description">После этой даты содержание курса будет перенесено в архив.</p>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>
</main>
<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
</body>
</html>

