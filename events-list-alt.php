<? include 'partials/header-cloud.php'; ?>

<main>
    <section style="padding-bottom: 30px" class="course-header  no-border-btm">
        <div class="row">
            <div class="column small-12">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Меропирятия</a></li>
                    <li><a href="#">Семинары и вебинары</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="column small-12">
                <div class="course-header__wrapper course-header__wrapper--column">
                    <div class="course-header__title">
                        <h1>Ближайшие бесплатные <br class="show-for-large"> IT-мероприятия Softline</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="column small-12 large-8">

                <div class="event-filter">
                    <p>Фильтр по событиям:</p>
                    <form>
                        <label class="b-select">
                            <select class="b-select__container">
                                <option value="">Все направления</option>
                                <option value="">Все направления</option>
                                <option value="">Все направления</option>

                            </select>
                        </label>
                        <label class="b-select">
                            <select class="b-select__container">
                                <option value="">Все специальности</option>
                                <option value="">Все специальности</option>
                                <option value="">Все специальности</option>

                            </select>
                        </label>
                        <label class="b-select">
                            <select class="b-select__container">
                                <option value="">Все вендоры</option>
                                <option value="">Все вендоры</option>
                                <option value="">Все вендоры</option>
                            </select>
                        </label>
                    </form>
                </div>
            </div>
            <div class="column small-12 large-4">
                <div class="event-filter">
                    <p class="text-center">Даты (от – до):</p>
                    <form>
                        <label class="date-input">
                            <input value="24.08.12" type="text">
                        </label>
                        <label class="date-input">
                            <input value="24.08.12" type="text">
                        </label>
                    </form>
                    <div style="justify-content: center">
                        <a href="#">2 недели</a>
                        <a href="#">Месяц</a>
                        <a href="#">2 месяца</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="column small-12 large-3">
                <aside>
                    <div class="aside-widget">
                        <div class="aside-widget__title">Участвуйте</div>
                        <div class="aside-widget__content">
                            <a href="#">Мероприятия и вебинары</a>
                            <a href="#">О мероприятиях</a>
                            <a href="#">О вебинарах</a>
                            <a href="#">Архив</a>
                        </div>
                    </div>
                    <div class="aside-widget">

                        <div class="aside-widget__content">
                            <hr>
                            <p>Подпишитесь на рассылку новостей Softline</p>
                            <form action="#">
                                <label for="">
                                    <input type="text" placeholder="Ваш e-mail">
                                </label>
                                <label for="">
                                    <input type="text" placeholder="Ваше имя">
                                </label>
                                <button class="button small expanded">Подписаться</button>
                            </form>
                        </div>
                    </div>
                </aside>
            </div>
            <div style="padding-bottom: 8rem" class="column small-12 large-9">
                <div class="offer-loop">
                    <div class="flex-container">
                        <!--div class="offer-loop__image">
                            <a href="#">
                                <img src="content/offers/item.jpg" alt="">
                            </a>
                        </div-->
                        <div class="offer-loop__content">
                            <h5 class="offer-loop__title"><a href="#">Защита персональных данных в облаке в соответствии
                                    с ФЗ-152</a></h5>
                            <div class="offer-loop__description">
                                <p>компания Softline приглашает Вас принять участие в Online-семинаре: «Защита
                                    персональных данных в облаке в соответствии с ФЗ-152».</p>
                            </div>
                        </div>
                    </div>
                    <a class="offer-loop__duration" href="#">
                        <svg class="icon calendar">
                            <use xlink:href="#images--svg--calendar"></use>
                        </svg>
                        5 сентябрая 2018, 11:00 — 12:00
                        <svg class="icon arrow">
                            <use xlink:href="#images--svg--arrow"></use>
                        </svg>
                    </a>
                </div>
                <div class="offer-loop">
                    <div class="flex-container">
                        <!--div class="offer-loop__image">
                             <a href="#">
                                 <img src="content/offers/item.jpg" alt="">
                             </a>
                         </div-->
                        <div class="offer-loop__content">
                            <h5 class="offer-loop__title"><a href="#">Office на диване: продуктивность труда и
                                    лояльность сотрудников</a></h5>
                            <div class="offer-loop__description">
                                <p>компания Softline приглашает Вас принять участие в Online-семинаре: «Office на
                                    диване: продуктивность труда и лояльность сотрудников».</p>
                            </div>
                        </div>
                    </div>
                    <a class="offer-loop__duration" href="#">
                        <svg class="icon calendar">
                            <use xlink:href="#images--svg--calendar"></use>
                        </svg>
                        25 января 2018, 11:00
                        <svg class="icon arrow">
                            <use xlink:href="#images--svg--arrow"></use>
                        </svg>
                    </a>
                </div>
                <div class="offer-loop">
                    <div class="flex-container">
                        <!--div class="offer-loop__image">
                             <a href="#">
                                 <img src="content/offers/item.jpg" alt="">
                             </a>
                         </div-->
                        <div class="offer-loop__content">
                            <h5 class="offer-loop__title"><a href="#">Бизнес-завтрак: Цифровая трансформация от
                                    Google</a></h5>
                            <div class="offer-loop__description">
                                <p>Начните утро правильно со свежесваренного кофе и полного толковых идей воркшопа по
                                    цифровой трансформации от Google. Разберемся на практике, что такое digital
                                    workplace, digital culture, как можно всей командой одновременно работать в одном
                                    документе и решать сложные задачи.</p>
                            </div>
                        </div>
                    </div>
                    <a class="offer-loop__duration" href="#">
                        <svg class="icon calendar">
                            <use xlink:href="#images--svg--calendar"></use>
                        </svg>
                        30 января 2018, 09:30 — 11:00
                        <svg class="icon arrow">
                            <use xlink:href="#images--svg--arrow"></use>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section style="padding-top: 30px" class="btn-next-page">
        <a href="#">Показать больше мероприятий</a>
    </section>
    <section class="partners">
        <div class="row">
            <div class="column small-12">
                <h2>Наши партнёры</h2>
            </div>
        </div>
        <div class="partners__card-wrapper row small-up-1 medium-up-3 large-up-4">
            <div class="column">
                <div class="flex-container">
                    <a href="http://edu.softline.ru/vendors/microsoft" class="b-card">
                        <img src="dist/images/partners/partners__card/microsoft.png" alt="">
                    </a>
                </div>
            </div>
            <div class="column">
                <div class="flex-container">
                    <a href="http://edu.softline.ru/vendors/cisco-systems" class="b-card">
                        <img src="content/partners/cisco.png" alt="">
                    </a>
                </div>
            </div>
            <div class="column">
                <div class="flex-container">
                    <a href="http://edu.softline.ru/vendors/veeam-vmce" class="b-card">
                        <img src="content/partners/veeam.png" alt="">
                    </a>
                </div>
            </div>
            <div class="column">
                <div class="flex-container">
                    <a href="http://edu.softline.ru/vendors/vmware" class="b-card">
                        <img src="content/partners/vmware.png" alt="">
                    </a>
                </div>
            </div>
            <div class="column">
                <div class="flex-container">
                    <a href="http://edu.softline.ru/vendors/kaspersky" class="b-card">
                        <img src="content/partners/kaspersky.jpg" alt="">
                    </a>
                </div>
            </div>
            <div class="column">
                <div class="flex-container">
                    <a href="http://edu.softline.ru/vendors/oracle" class="b-card">
                        <img src="content/partners/oracle.png" alt="">
                    </a>
                </div>
            </div>
            <div class="column">
                <div class="flex-container">
                    <a href="http://edu.softline.ru/vendors/ibm" class="b-card">
                        <img src="content/partners/ibm.png" alt="">
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="small-12 column">
                <hr>
            </div>
        </div>
    </section>
    <section class="subscribe-banner">
        <div class="subscribe-banner__wrapper">
            <div class="row">
                <div class="column small-12 large-9 large-offset-3">
                    <div class="subscribe-banner__title">
                        <span>Будьте в курсе</span>
                    </div>
                    <form class="subscribe-banner__form">
                        <div class="subscribe-banner__input subscribe-banner__input--name">
                            <!--доп модификатор: error-->
                            <input type="text" placeholder="Ваше имя">
                        </div>
                        <div class="subscribe-banner__input subscribe-banner__input--email">
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="subscribe-banner__button">
                            <button class="button">Подписаться</button>
                        </div>
                    </form>
                    <div class="subscribe-banner__desc">
                        <span>Подпишитесь на информацию о новинках, скидках и акциях. <br> Уже более 36 000 подписчиков!</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<footer class="footer">
    <div class="row footer__middle">
        <div class="column small-12 large-3">
            <div class="footer__contacts">
                <div class="row">
                    <div class="column small-12 medium-6 large-12">
                        <div class="footer__contacts-item">
                            <span><a href="mailto:services@softlinegroup.com">services@softlinegroup.com</a></span>
                        </div>
                        <div class="footer__contacts-item">
                            <span><a href="tel:+74952320023">+7(495) 232-0023</a></span> <br>
                            <span><a href="tel:88002320023">8 800 2320023</a> </span>
                        </div>
                    </div>
                    <div class="column small-12 medium-6 large-12">
                        <div class="footer__contacts-item footer__contacts-item--address">
                            <span>115114, Москва, <br class="show-for-medium">Дербеневская наб., <br class="show-for-medium"> дом 7, стр. 8</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column small-12 medium-2">
            <ul class="footer__link">
                <li><a href="#">Программное обеспечение</a></li>
                <li><a href="#">Услуги и решения</a></li>
                <li><a href="#">IT-обучение</a></li>
                <li><a href="#">Специальные предложения</a></li>
                <li><a href="#">IT-события</a></li>
            </ul>
        </div>
        <div class="column small-12 medium-2">
            <ul class="footer__link">
                <li><a href="#">Новости компании </a></li>
                <li><a href="#">Мобильные <br class="show-for-medium"> приложения</a></li>
                <li><a href="#">Печатный <br class="show-for-medium"> каталог ПО</a></li>
                <li><a href="#">Облачные решения</a></li>
                <li><a href="#">Качество <br class="show-for-medium"> и ISO 9001</a></li>
            </ul>
        </div>
        <div class="column small-12 medium-2">
            <ul class="footer__link">
                <li><a href="#">Техподдержка </a></li>
                <li><a href="#">О компании</a></li>
                <li><a href="#">Вакансии</a></li>
                <li><a href="#">Контакты</a></li>
                <li><a href="#">Приемная Softline</a></li>
                <li><a href="#">Группа компаний Softline</a></li>
            </ul>
        </div>
        <div class="column small-12 medium-6 large-2">

            <div class="footer__social">
                <p style="margin: 0 -30px 0 0">Свяжитесь с нами:</p>
            </div>
            <div class="footer__social">
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon facebook">
                            <use xlink:href="#images--svg--facebook"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon twitter">
                            <use xlink:href="#images--svg--twitter"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon vk">
                            <use xlink:href="#images--svg--vk"></use>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="footer__social">
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon youtube">
                            <use xlink:href="#images--svg--youtube"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon linkedin">
                            <use xlink:href="#images--svg--linkedin"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon habr">
                            <use xlink:href="#images--svg--habr"></use>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="footer__social">
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon google-plus">
                            <use xlink:href="#images--svg--google+"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon instagram">
                            <use xlink:href="#images--svg--instagram"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon telegram">
                            <use xlink:href="#images--svg--telegram"></use>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="column small-12 show-for-medium">
            <hr class="footer__divider">
        </div>
        <div class="column small-12 medium-3">
            <ul class="nav-area show-for-medium">
                <li>
                    <button>RU</button>
                </li>
            </ul>
        </div>
        <div class="column small-12 medium-2">
            <div class="footer__company">© 1993—2017 Softline</div>
        </div>
        <div class="column small-12 medium-2">
            <div class="footer__terms"><a href="#">Условия использования</a></div>
        </div>
        <div class="column small-12 medium-2"><span class="footer__age-limit">14+</span></div>
    </div>
</footer>
<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        $('.select2').select2();
        $('.select2-selection__arrow').html('<svg class="icon arrow"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#images--svg--arrow"></use></svg>');

        var $offers = $('.offer-loop');

        $('.change-offer').on('change', function (e) {
            if (this.value == 'all') {
                $offers.stop().show()
            } else {
                $offers.filter('.' + this.value).stop().show();
                $offers.not('.' + this.value).stop().hide();
            }
        });

        $('.b-select__container').select2();
    })


</script>
</script>
<
/body>
< /html>

