<? include 'partials/header-cloud.php'; ?>

<main>
    <section  style="margin-bottom: 60px" class="cloud-header">
        <div class="breadcrumbs-wrapper">
            <ul class="breadcrumbs">
                <li><a href="#">Главная</a></li>
                <li><a href="#">Облачные решения</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="column small-12 text-center">
                <div class="cloud-header__title">
                    <span>Защита персональных данных <br class="show-for-large"> в облаке в соответствии с ФЗ-152</span>
                </div>
                <div class="cloud-header__desc">
                    <p></p>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="column small-12 large-3">
                <aside>
                    <div class="aside-widget">
                        <div style="padding-bottom: 21px" class="aside-widget__title">Участвуйте</div>
                        <div class="aside-widget__content">
                            <a href="#">Мероприятия и вебинары</a>
                            <a href="#">О мероприятиях</a>
                            <a href="#">О вебинарах</a>
                            <a href="#">Архив</a>
                        </div>
                    </div>
                    <div class="aside-widget">

                        <div class="aside-widget__content">
                            <hr>
                            <p>Подпишитесь на рассылку новостей Softline</p>
                            <form action="#">
                                <label for="">
                                    <input type="text" placeholder="Ваш e-mail">
                                </label>
                                <label for="">
                                    <input type="text" placeholder="Ваше имя">
                                </label>
                                <button class="button small expanded">Подписаться</button>
                            </form>
                        </div>
                    </div>
                </aside>
            </div>
            <div style="padding-bottom: 8rem" class="column small-12 large-9">

                <div style="padding-bottom: 25px;" class="flex-container">
                    <ul style="margin-left: 0; width: 100%" class="b-tabs">
                        <li class="is-active"><span>О вебинаре</span></li>
                    </ul>
                </div>

                <div class="b-editor">
                    <p>
                        31 января 2018 года в 11:00 (Мск) компания Softline приглашает Вас принять участие в
                        Online-семинаре: «Защита персональных данных в облаке в соответствии с ФЗ-152».
                    </p>
                    Облачные технологии стали неотъемлемой частью успешного современного бизнеса, а вопрос хранения
                    и защиты персональных данных сегодня, в разрезе последних изменений в законодательстве,
                    актуализировался.
                    <p>
                        Для обеспечения безопасности персональных данных при их обработке требуется ряд серьезных мер
                        как организационного, так и технического плана.
                    </p>
                    <p>
                        Мы предлагаем вам обсудить актуальные вопросы защиты персональных данных с использованием
                        облачных технологий, рассмотреть аттестованное в соответствии с требованиями ФЗ-152 решение и
                        научиться ориентироваться в облаках для ИСПДн.
                    <p>
                    </p>
                    Online-семинар проводит: Динар Гарипов, руководитель направления развития бизнеса Департамента
                    облачных технологий Softline
                    <p>
                    </p>
                    Softline —лидирующий международный поставщик IT-решений и сервисов, работающий на рынках России,
                    СНГ, Латинской Америки, Индии и Юго-Восточной Азии. Мы предлагаем комплексные технологические IT
                    - решения, лицензирование программного обеспечения, аппаратное обеспечение и сопутствующие
                    услуги. Собственная облачная платформа Softline обеспечивает клиентов доступом к публичным,
                    частным и гибридным облачным решениям. Компания представлена в 79 городах 27 стран мира.
                    Softline — это клиентоориентированная компания: мы всегда находимся на стороне клиента и
                    предлагаем решения, наилучшим образом решающие eго задачи, вне зависимости от бренда.
                    </p>

                    <h5><strong>Программа</strong></h5>

                    <ul>
                        <li> Что такое персональные данные и как государство требует их защищать?</li>

                        <li>Как построить информационную систему персональных данных (ИСПДн) на

                        <li>базе «Защищенного облака Softline».</li>

                        <li>Схема работы и зоны ответственности.</li>

                        <li>Как выбрать правильное облако для ИСПДн – нюансы законодательства.</li>
                    </ul>
                    <hr>
                </div>
                <div>
                    <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                    <script src="//yastatic.net/share2/share.js"></script>
                    <div class="ya-share2" data-services="facebook,gplus,twitter" data-counter=""></div>
                </div>

                <div class="register-wrap">

                    <div style="padding-bottom: 25px;" class="flex-container">
                        <ul style="margin-left: 0; width: 100%" class="b-tabs">
                            <li class="is-active"><span>Регистрация</span></li>
                        </ul>
                    </div>
                    <form action="">
                        <div class="flex-container">
                            <div class="register-label" style="">
                                <p>E-mail:</p>
                            </div>
                            <div class="register-input"><label for="">
                                    <input placeholder="E-mail" type="text">
                                </label>
                            </div>
                        </div>
                        <div class="flex-container">
                            <div class="register-label" style="">
                                <p>Имя:</p>
                            </div>
                            <div class="register-input"><label for="">
                                    <input placeholder="Представьтесь, пожалуйста" type="text">
                                </label>
                            </div>
                        </div>
                        <div class="flex-container">
                            <div class="register-label" style="">
                                <p>Фамилия:</p>
                            </div>
                            <div class="register-input"><label for="">
                                    <input placeholder="Введите фамилию" type="text">
                                </label>
                            </div>
                        </div>
                        <div class="flex-container">
                            <div class="register-label" style="">
                                <p>Должность:</p>
                            </div>
                            <div class="register-input"><label for="">
                                    <input placeholder="Название должности" type="text">
                                </label>
                            </div>
                        </div>
                        <div class="flex-container">
                            <div class="register-label" style="">
                                <p>Компания:</p>
                            </div>
                            <div class="register-input"><label for="">
                                    <input placeholder="Название компании" type="text">
                                </label>
                            </div>
                        </div>
                        <div class="flex-container">
                            <div class="register-label" style="">
                                <p>Страна:</p>
                            </div>
                            <div class="register-input"><label for="">
                                    <select name="" id="">
                                        <option value="ru">Россия</option>
                                        <option value="ru">Чехия</option>
                                        <option value="ru">Прага</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                        <div class="flex-container">
                            <div class="register-label" style="">
                                <p>Город:</p>
                            </div>
                            <div class="register-input"><label for="">
                                    <select name="" id="">
                                        <option value="ru">Москва</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                        <div class="flex-container">
                            <div class="register-label" style="">
                                <p>Телефон:</p>
                            </div>
                            <div class="register-input"><label for="">
                                    <input placeholder="+7 ХХХ ХХХ-ХХ-ХХ" type="text">
                                </label>
                            </div>
                        </div>
                        <div class="flex-container">
                            <div class="register-label" style="">
                                <p></p>
                            </div>
                            <div style="padding-top: 20px" class="register-input">
                                <p >
                                    Настоящим в соответствии с Федеральным законом № 152-ФЗ «О персональных данных»
                                    от 27.07.2006, отправляя данную форму, вы подтверждаете свое согласие на обработку
                                    персональных данных. Мы, ЗАО «СофтЛайн Интернейшнл» и аффилированные к нему лица,
                                    гарантируем конфиденциальность получаемой нами информации. Обработка персональных
                                    данных осуществляется в целях эффективного исполнения заказов, договоров и пр. в
                                    соответствии с «Политикой конфиденциальности персональных данных».
                                </p>
                                <div style="padding: 30px 0;">
                                    <button class="button">Зарегистророваться</button>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <section class="subscribe-banner">
        <div class="subscribe-banner__wrapper">
            <div class="row">
                <div class="column small-12 large-9 large-offset-3">
                    <div class="subscribe-banner__title">
                        <span>Будьте в курсе</span>
                    </div>
                    <form class="subscribe-banner__form">
                        <div class="subscribe-banner__input subscribe-banner__input--name">
                            <input type="text" placeholder="Ваше имя">
                        </div>
                        <div class="subscribe-banner__input subscribe-banner__input--email">
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="subscribe-banner__button">
                            <button class="button">Подписаться</button>
                        </div>
                    </form>
                    <div class="subscribe-banner__desc">
                        <span>Подпишитесь на информацию о новинках, скидках и акциях. <br> Уже более 36 000 подписчиков!</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<footer class="footer">
    <div class="row footer__middle">
        <div class="column small-12 large-3">
            <div class="footer__contacts">
                <div class="row">
                    <div class="column small-12 medium-6 large-12">
                        <div class="footer__contacts-item">
                            <span><a href="mailto:services@softlinegroup.com">services@softlinegroup.com</a></span>
                        </div>
                        <div class="footer__contacts-item">
                            <span><a href="tel:+74952320023">+7(495) 232-0023</a></span> <br>
                            <span><a href="tel:88002320023">8 800 2320023</a> </span>
                        </div>
                    </div>
                    <div class="column small-12 medium-6 large-12">
                        <div class="footer__contacts-item footer__contacts-item--address">
                            <span>115114, Москва, <br class="show-for-medium">Дербеневская наб., <br class="show-for-medium"> дом 7, стр. 8</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column small-12 medium-2">
            <ul class="footer__link">
                <li><a href="#">Программное обеспечение</a></li>
                <li><a href="#">Услуги и решения</a></li>
                <li><a href="#">IT-обучение</a></li>
                <li><a href="#">Специальные предложения</a></li>
                <li><a href="#">IT-события</a></li>
            </ul>
        </div>
        <div class="column small-12 medium-2">
            <ul class="footer__link">
                <li><a href="#">Новости компании </a></li>
                <li><a href="#">Мобильные <br class="show-for-medium"> приложения</a></li>
                <li><a href="#">Печатный <br class="show-for-medium"> каталог ПО</a></li>
                <li><a href="#">Облачные решения</a></li>
                <li><a href="#">Качество <br class="show-for-medium"> и ISO 9001</a></li>
            </ul>
        </div>
        <div class="column small-12 medium-2">
            <ul class="footer__link">
                <li><a href="#">Техподдержка </a></li>
                <li><a href="#">О компании</a></li>
                <li><a href="#">Вакансии</a></li>
                <li><a href="#">Контакты</a></li>
                <li><a href="#">Приемная Softline</a></li>
                <li><a href="#">Группа компаний Softline</a></li>
            </ul>
        </div>
        <div class="column small-12 medium-6 large-2">

            <div class="footer__social">
                <p style="margin: 0 -30px 0 0">Свяжитесь с нами:</p>
            </div>
            <div class="footer__social">
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon facebook">
                            <use xlink:href="#images--svg--facebook"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon twitter">
                            <use xlink:href="#images--svg--twitter"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon vk">
                            <use xlink:href="#images--svg--vk"></use>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="footer__social">
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon youtube">
                            <use xlink:href="#images--svg--youtube"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon linkedin">
                            <use xlink:href="#images--svg--linkedin"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon habr">
                            <use xlink:href="#images--svg--habr"></use>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="footer__social">
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon google-plus">
                            <use xlink:href="#images--svg--google+"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon instagram">
                            <use xlink:href="#images--svg--instagram"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon telegram">
                            <use xlink:href="#images--svg--telegram"></use>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="column small-12 show-for-medium">
            <hr class="footer__divider">
        </div>
        <div class="column small-12 medium-3">
            <ul class="nav-area show-for-medium">
                <li>
                    <button>RU</button>
                </li>
            </ul>
        </div>
        <div class="column small-12 medium-2">
            <div class="footer__company">© 1993—2017 Softline</div>
        </div>
        <div class="column small-12 medium-2">
            <div class="footer__terms"><a href="#">Условия использования</a></div>
        </div>
        <div class="column small-12 medium-2"><span class="footer__age-limit">14+</span></div>
    </div>
</footer>
<script src="dist/javascript/bundle.js"></script>
</body>
</html>

