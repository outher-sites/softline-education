<?php
$date = new DateTime();
$cacheReset = $date->getTimestamp();
?><!doctype html>
<html lang="ru-RU" id="nojs">

<head>
    <meta charset="UTF-8"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <meta http-equiv="imagetoolbar" content="no"/>
    <meta http-equiv="keywords" content=""/>
    <meta http-equiv="description" content=""/>
    <meta name="description" content=""/>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>

    <link rel="stylesheet" href="dist/css/template_styles.css?<?= $cacheReset; ?>">
    <title>Заголовок страницы</title>
    <script>document.documentElement.id = 'id';</script>
</head>

<body id="page">

<div style="display: none">
    <? include 'dist/images/icons/symbol/svg/sprite.symbol.svg'; ?>
</div>

<header class="header" id="header">
    <div class="header-top">
        <div class="row">
            <div class="column small-6 medium-4 large-3">
                <div class="header-top__wrapper">
                    <ul class="nav-area">
                        <li>
                            <button class="header-top__country">
                                <span class="header-top__flag" style="background-image:url(dist/images/flags/ru.png);"></span>
                                <span>Россия</span>
                            </button>
                        </li>
                        <li>
                            <button>Москва</button>
                        </li>
                        <li>
                            <button>RU</button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="column large-5 show-for-large">
                <ul class="header-top__nav">
                    <li><a href="#">Интернет-магазин</a></li>
                    <li><a href="#">Учебный центр</a></li>
                </ul>
            </div>
            <div class="column small-6 medium-8 large-4 text-right">
                <div class="authorization">
                    <div class="authorization__log-in">
                        <a href="#">
                            <svg class="icon user">
                                <use xlink:href="#images--svg--user"></use>
                            </svg>
                            <span>Зарегистрироваться</span>
                        </a>
                    </div>
                    <div class="authorization__log-in user-name">
                        <a href="#"><span>Войти</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row header__general">
        <div class="column small-6 large-5 xlarge-3">

            <div class="header__logos">
                <a href="#" class="header__logo">
                    <svg class="icon logo">
                        <use xlink:href="#images--svg--softline-logo"></use>
                    </svg>
                </a>
            </div>

        </div>

        <div class="column xlarge-6 show-for-xlarge" style="display: none;">
            <ul class="header__nav">
                <li><a href="#">О компании</a></li>
                <li><a href="#">Реализованные проекты</a></li>
                <li><a href="#">Мероприятия</a></li>
                <li><a href="#">Пресса о нас</a></li>
                <li><a href="#">Спецпредложения</a></li>
                <li><a href="#">Контакты</a></li>
            </ul>
        </div>
        <div class="column small-6 medium-6 large-7 xlarge-3 xlarge-offset-6">
            <div class="contacts contacts--flex-end">
                <button class="contacts__icon show-for-large">
                    <svg class="icon phone">
                        <use xlink:href="#images--svg--phone"></use>
                    </svg>
                </button>
                <div class="contacts__data show-for-medium">
                    <span>8 (800) 200-41-36</span>
                    <a href="#">edusales@softline.ru</a>
                </div>
                <button class="header-bottom__hamburger hide-for-medium" id="header-bottom__hamburger">
                    <i class="icon hamburger"></i>
                    <svg class="icon close" style="display: none;">
                        <use xlink:href="#images--svg--close"></use>
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <div class="header-bottom show-for-medium">
        <div class="row">
            <div class="column small-12">
                <ul class="header__nav header__nav--bottom">
                    <li><a href="http://academy4cloud.com/">На главную</a></li>
                    <li><a href="/courses">Список MOOCs</a></li>
                    <li><a href="http://edu.softline.ru/special-offers">Акции</a></li>
                    <li><a href="http://edu.softline.ru/reviews/microsoft-options">Дополнительные опции</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="mobile-nav" id="mobile-nav" style="display: none;">
        <div class="mobile-nav__phones">
            <a href="#">8 (800) 232-00-23</a>
            <a href="#" class="text-right">+7 (495) 232-00-23</a>
        </div>

        <div class="mobile-nav__email">
            <a href="#">info@softline.ru</a>
        </div>
    </div>
</header>
