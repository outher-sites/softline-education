<?php
$date = new DateTime();
$cacheReset = $date->getTimestamp();
?><!doctype html>
<html lang="ru-RU" id="nojs">

<head>
    <meta charset="UTF-8"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <meta http-equiv="imagetoolbar" content="no"/>
    <meta http-equiv="keywords" content=""/>
    <meta http-equiv="description" content=""/>
    <meta name="description" content=""/>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>

    <link rel="stylesheet" href="dist/css/template_styles.css?<?= $cacheReset; ?>">
    <title>Заголовок страницы</title>
    <script>document.documentElement.id = 'id';</script>
</head>

<body id="page">

<div style="display: none">
    <? include 'dist/images/icons/symbol/svg/sprite.symbol.svg'; ?>
</div>

<header class="header" id="header">
    <div class="header-top">
        <div class="row">
            <div class="column small-6 hide-for-medium">
                <div class="header-top__wrapper">
                    <ul class="nav-area">
                        <li>
                            <a href="http://edu.softline.ru">Учебный центр</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="column small-12 medium-5 show-for-medium">
                <ul class="header-top__nav">
                    <li style="display: none;"><a href="#">Интернет-магазин</a></li>
                    <li><a href="http://edu.softline.ru">Учебный центр</a></li>
                </ul>
            </div>
            <div class="column small-6 medium-offset-3 medium-4 text-right">
                <div class="authorization">
                    <div class="authorization__log-in">
                        <a href="http://online.academy4cloud.com/register">
                            <svg class="icon user">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#images--svg--user"></use>
                            </svg>
                            <span>Зарегистрироваться</span>
                        </a>
                    </div>
                    <div class="authorization__log-in user-name">
                        <a href="http://online.academy4cloud.com/login"><span>Войти</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row header__general">
        <div class="column small-6 medium-8 large-6">

            <div class="header__logos">
                <a href="http://edu.softline.ru" class="header__logo">
                    <svg class="icon logo">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#images--svg--softline-logo"></use>
                    </svg>
                </a>

                <div class="show-for-medium">
                    <img src="dist/images/microsoft.png">
                </div>
            </div>

        </div>

        <div class="column medium-6 large-7 xlarge-6 show-for-large" style="display: none;">
            <ul class="header__nav">
                <li><a href="#">О компании</a></li>
                <li><a href="#">Карьера</a></li>
                <li><a href="#">Блог</a></li>
                <li><a href="#">Мероприятия</a></li>
                <li><a href="#">Акции</a></li>
                <li><a href="#">Контакты</a></li>
            </ul>
        </div>
        <div class="column small-6 medium-4 medium-offset-0 large-3 large-offset-3">
            <div class="contacts contacts--flex-end">
                <a class="contacts__icon show-for-large" href="tel:88005050507">
                    <svg class="icon phone">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#images--svg--phone"></use>
                    </svg>
                </a>
                <div class="contacts__data show-for-medium">
                    <span><a href="tel:88005050507">8 (800) 505-05-07</a></span>
                    <a href="mailto:edusales@softline.ru" target="_blank">edusales@softline.ru</a>
                </div>
                <button class="header-bottom__hamburger hide-for-medium" id="header-bottom__hamburger">
                    <i class="icon hamburger"></i>
                    <svg class="icon close" style="display: none;">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#images--svg--close"></use>
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <div class="mobile-nav" id="mobile-nav" style="display: none;">
        <div class="mobile-nav__phones">
            <a href="tel:88005050507">8 (800) 505-05-07</a>
            <a href="tel:+74952320023" class="text-right">+7 (495) 232-00-23</a>
        </div>

        <div class="mobile-nav__email">
            <a href="mailto:info@softline.ru">info@softline.ru</a>
        </div>
    </div>
</header>