<footer class="footer">
    <div class="row show-for-medium">
        <div class="column small-12 large-3">
            <div class="footer__bank-cards">
                <span><img src="/dist/images/footer/mir.png" alt=""></span>
                <span><img src="/dist/images/footer/visa.png" alt=""></span>
                <span><img src="/dist/images/footer/mastercard.png" alt=""></span>
            </div>
        </div>
        <div class="column small-12 large-9">
            <div class="footer__desc">
                <span>Работаем с юридическими и физическими лицами. Оплата по счёту или картой</span>
            </div>
        </div>

    </div>
    <div class="row footer__middle">
        <div class="column small-12 large-3">
            <div class="footer__contacts">
                <div class="row">
                    <div class="column small-12 medium-6 large-12">
                        <div class="footer__contacts-item">
                            <span><a href="mailto:edusales@softline.com">edusales@softline.com</a></span>
                        </div>
                        <div class="footer__contacts-item">
                            <span><a href="tel:88005050507">8 (800) 505-05-07</a> </span>
                        </div>
                    </div>
                    <div class="column small-12 medium-6 large-12">
                        <div class="footer__contacts-item footer__contacts-item--address">
                            <span>115114, Москва, <br class="show-for-medium">Дербеневская наб., <br
                                        class="show-for-medium"> дом 7, стр. 8</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column small-12 medium-2">
            <ul class="footer__link">
                <li><a href="#">Программное обеспечение</a></li>
                <li><a href="#">Услуги и решения</a></li>
                <li><a href="#">IT-обучение</a></li>
                <li><a href="#">Специальные предложения</a></li>
                <li><a href="#">IT-события</a></li>
            </ul>
        </div>
        <div class="column small-12 medium-2">
            <ul class="footer__link">
                <li><a href="#">Новости компании </a></li>
                <li><a href="#">Мобильные <br class="show-for-medium"> приложения</a></li>
                <li><a href="#">Печатный <br class="show-for-medium"> каталог ПО</a></li>
                <li><a href="#">Облачные решения</a></li>
                <li><a href="#">Качество <br class="show-for-medium"> и ISO 9001</a></li>
            </ul>
        </div>
        <div class="column small-12 medium-2">
            <ul class="footer__link">
                <li><a href="#">Техподдержка </a></li>
                <li><a href="#">О компании</a></li>
                <li><a href="#">Вакансии</a></li>
                <li><a href="#">Контакты</a></li>
                <li><a href="#">Приемная Softline</a></li>
                <li><a href="#">Группа компаний Softline</a></li>
            </ul>
        </div>
        <div class="column small-12 medium-6 large-2">


            <div class="footer__social">
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon facebook">
                            <use xlink:href="#images--svg--facebook"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon twitter">
                            <use xlink:href="#images--svg--twitter"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon vk">
                            <use xlink:href="#images--svg--vk"></use>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="footer__social">
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon youtube">
                            <use xlink:href="#images--svg--youtube"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon linkedin">
                            <use xlink:href="#images--svg--linkedin"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon habr">
                            <use xlink:href="#images--svg--habr"></use>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="footer__social">
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon google-plus">
                            <use xlink:href="#images--svg--google+"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon instagram">
                            <use xlink:href="#images--svg--instagram"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon telegram">
                            <use xlink:href="#images--svg--telegram"></use>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="column small-12 show-for-medium">
            <hr class="footer__divider">
        </div>
        <div class="column small-12 medium-3">
            <ul class="nav-area show-for-medium">
                <li>
                    <button>RU</button>
                </li>
            </ul>
        </div>
        <div class="column small-12 medium-2">
            <div class="footer__company">© 1993—2017 Softline</div>
        </div>
        <div class="column small-12 medium-2">
            <div class="footer__terms"><a href="#">Условия использования</a></div>
        </div>
        <div class="column small-12 medium-2"><span class="footer__age-limit">14+</span></div>
    </div>
</footer>
<div id="back-top" style="display: none;" class="back-top">
    <svg class="icon arrow">
        <use xlink:href="#images--svg--arrow"></use>
    </svg>
</div>

