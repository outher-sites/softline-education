<? include 'partials/header.php'; ?>

<main>
    <section class="course-header">
        <div class="row">
            <div class="column small-12 medium-7 large-8">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Учебный центр</a></li>
                    <li><a href="#">Сертификация и экзамены</a></li>
                </ul>
                <div class="course-header__wrapper">
                    <div class="course-header__title">
                        <h1>Сертификация и экзамены</h1>
                    </div>
                </div>
            </div>
            <div class="column small-12 medium-5 large-3 large-offset-1">
                <div class="feedback-header">
                    <div class="feedback-header__name">
                        <span>Светлана Жученко</span>
                    </div>
                    <div class="feedback-header__photo">
                        <img src="dist/images/feedback__header/feedback-header__photo/photo-1.png" alt="">
                    </div>
                    <div class="feedback-header__position">
                        <span>менеджер</span>
                        <span>интернет-магазина</span>
                    </div>
                    <div class="feedback-header__contacts">
                        <span>8 (800) 200-08-60 доб. 6011</span>
                        <a href="Svetlana.Zhuchenko@softlinegroup.com">Svetlana.Zhuchenko@softlinegroup.com</a>
                    </div>

                    <a href="#" class="button expanded">Связаться сейчас</a>
                </div>
            </div>
        </div>
    </section>
    <section class="course-specification">
        <div class="row">
            <div class="column small-12 large-8">
                <div class="row">
                    <div class="column small-12 medium-8">
                        <div class="course-specification__desc b-editor">
                            <p>
                                3-х дневый учебный курс под руководством инструктора для программистов среднего уровня.
                                Получение знаний и практических навыков для разработки распределенных приложений на
                                основе Microsoft Windows Communication Foundation (WCF) 4 с помощью Visual Studio 2010.
                            </p>
                        </div>
                    </div>
                    <div class="column small-12 medium-4">
                        <div class="course-specification__img">
                            <img src="dist/images/course-specification/course-specification__img/msvs.png" alt="">
                        </div>
                    </div>
                </div>
                <div style="padding-top: 5.5rem" class="row">
                    <div class="column small-12 medium-5">
                        <div class="course-specification__download">
                            <a href="#">
                                <span>Скачать программу курса pdf, 309.03 кб</span>
                            </a>
                        </div>
                    </div>
                    <div class="column small-12 medium-7 medium-flex-container">
                        <div class="course-specification__level">
                            <p>Средний</p>
                            <span>Уровень</span>
                        </div>
                        <div class="course-specification__level">
                            <p>24 ак. часа</p>
                            <span>Продолжительность</span>
                        </div>
                    </div>
                    <div class="column small-12">
                        <div class="training-type">
                            <div class="training-type__label">
                                <span class="icon"><img src="dist/images/course-specification/sun.png" alt=""></span>
                                <span>Очное (дневное обучение)</span>
                            </div>
                            <div class="training-type__label">
                                <span class="icon"><img src="dist/images/course-specification/moon.png" alt=""></span>
                                <span>Очное (вечернее обучение)</span>
                            </div>
                            <div class="training-type__label">
                                <span class="icon"><img src="dist/images/course-specification/comp.png" alt=""></span>
                                <span>Удаленное обучение</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="padding-top: 5rem" class="row">
                    <div class="column small-12 medium-6">
                        <div class="course-info course-info--course-page">
                            <div class="course-info__cell">
                                <div class="course-info__name"><span>Производитель:</span></div>
                                <div class="course-info__value"><a href="#">Microsoft Corporation</a></div>
                            </div>
                            <div class="course-info__cell">
                                <div class="course-info__name"><span>Направление:</span></div>
                                <div class="course-info__value"><a href="#">Программирование</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="column small-12 medium-6">
                        <div class="course-statistics course-statistics--align-right">
                            <div class="course-statistics__cell">
                                <div class="course-statistics__number"><span>50</span></div>
                                <div class="course-statistics__desc">
                                    <span>Курс успешно закончило более 50 человек.</span></div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="accordion-large-wrap">

                    <ul class="accordion-large" data-accordion>

                        <li class="accordion-large__item" data-accordion-item>
                            <a href="" class="accordion-large__title"><span>О курсе</span>
                                <svg class="icon plus">
                                    <use xlink:href="#images--svg--plus"></use>
                                </svg>
                                <svg class="icon miuns">
                                    <use xlink:href="#images--svg--minus"></use>
                                </svg>
                            </a>
                            <div class="accordion-large__content accordion-content" data-tab-content>
                                <p>В рамках спецификации современных стандартов, действия представителей оппозиции
                                    представлены в исключительно положительном свете. Внезапно, диаграммы связей,
                                    инициированные исключительно синтетически, ассоциативно распределены по
                                    отраслям.</p>
                            </div>
                        </li>

                        <li class="accordion-large__item" data-accordion-item>
                            <a href="" class="accordion-large__title"><span>Расписание и цены</span>
                                <svg class="icon plus">
                                    <use xlink:href="#images--svg--plus"></use>
                                </svg>
                                <svg class="icon miuns">
                                    <use xlink:href="#images--svg--minus"></use>
                                </svg>
                            </a>
                            <div class="accordion-large__content accordion-content" data-tab-content>
                                <table width="100%" cellpadding="0" cellspacing="0" class="course-table prices">
                                    <tbody>
                                    <tr>
                                        <td class="course-table__place">Место обучения</td>
                                        <td class="course-table__format">Формат</td>
                                        <td class="course-table__date"><span>Дата и время</span></td>
                                        <td class="course-table__price"><span>Стоимость</span></td>
                                        <td class="course-table__basket" width="120"></td>
                                    </tr>
                                    <tr>
                                        <td class="course-table__place">
                                            <span>Дистанционно</span>
                                        </td>
                                        <td class="course-table__format">
                                            <span>Очный</span>
                                            <span class="add-note">с применением дистанционных технологий</span>

                                        </td>
                                        <td class="course-table__date">
                                            <span>25 ноября — 3 декабря 2017</span>
                                        </td>
                                        <td class="course-table__price">
                                            <span>17 375 руб.</span>
                                            <span class="old-price">19 305 руб.</span>
                                            <span class="add-discount">скидка 10%</span>
                                        </td>
                                        <td class="course-table__basket" width="120">
                                            <a href="#">В корзину</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="course-table__place">
                                            <span>Москва, Москва, 2-ой Южнопортовый проезд, дом 31, стр. 1</span>
                                            <a href="#">Схема проезда и фото</a>
                                        </td>
                                        <td class="course-table__format">
                                            <span>Очный</span>
                                            <span class="add-note">дневной, в аудитории</span>

                                        </td>
                                        <td class="course-table__date">
                                            <span>25 — 27 сентября 2017</span>
                                        </td>
                                        <td class="course-table__price">
                                            <span>19 305 руб.</span>
                                        </td>
                                        <td class="course-table__basket" width="120">
                                            <a href="#">В корзину</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="course-table__place">
                                            <span>Москва, Москва, 2-ой Южнопортовый проезд, дом 31, стр. 1</span>
                                            <a href="#">Схема проезда и фото</a>
                                        </td>
                                        <td class="course-table__format">
                                            <span>Очный</span>
                                            <span class="add-note">дневной, в аудитории</span>

                                        </td>
                                        <td class="course-table__date">
                                            <span>25 — 27 сентября 2017</span>
                                        </td>
                                        <td class="course-table__price">
                                            <span>19 305 руб.</span>
                                        </td>
                                        <td class="course-table__basket" width="120">
                                            <a href="#">В корзину</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>

                        <li class="accordion-large__item" data-accordion-item>
                            <a href="" class="accordion-large__title"><span>Отзывы</span>
                                <svg class="icon plus">
                                    <use xlink:href="#images--svg--plus"></use>
                                </svg>
                                <svg class="icon miuns">
                                    <use xlink:href="#images--svg--minus"></use>
                                </svg>
                            </a>
                            <div class="accordion-large__content accordion-content" data-tab-content>
                                <p>В рамках спецификации современных стандартов, действия представителей оппозиции
                                    представлены в исключительно положительном свете. Внезапно, диаграммы связей,
                                    инициированные исключительно синтетически, ассоциативно распределены по
                                    отраслям.</p>
                            </div>
                        </li>

                        <li class="accordion-large__item" data-accordion-item>
                            <a href="" class="accordion-large__title"><span>Похожие курсы</span>
                                <svg class="icon plus">
                                    <use xlink:href="#images--svg--plus"></use>
                                </svg>
                                <svg class="icon miuns">
                                    <use xlink:href="#images--svg--minus"></use>
                                </svg>
                            </a>
                            <div class="accordion-large__content accordion-content" data-tab-content>
                                <p>В рамках спецификации современных стандартов, действия представителей оппозиции
                                    представлены в исключительно положительном свете. Внезапно, диаграммы связей,
                                    инициированные исключительно синтетически, ассоциативно распределены по
                                    отраслям.</p>
                            </div>
                        </li>

                    </ul>
                </div>

                <div style="padding-top: 3rem">
                    <div class="b-editor">
                        <p><strong>Предварительная подготовка:</strong></p>
                        <ul>

                            <li>Понимание проблемно-ориентированных методов и технологий разработки программного обеспечения.  </li>

                            <li>Понимание целей, функций и возможностей платформы .NET Framework.  </li>

                            <li>Практический опыт объектно-ориентированного проектирования и разработки с использованием языка программирования Visual C#.  </li>

                            <li>Опыт проектирования и разработки распределенного многоуровневого приложения.  </li>

                        </ul>
                        <p><strong>Программа курса:</strong></p>
                        <ul>

                            <li>Модуль 1. Сервис-ориентированной архитектуры.</li>

                            <li>Модуль 2. Начало программирования на основе Microsoft Windows Communication Foundation.</li>

                            <li>Модуль 3. Развертывание служб Microsoft Windows Communication Foundation.</li>

                            <li>Модуль 4. Определение и реализация контрактов Windows Communication Foundation.</li>

                            <li>Модуль 5. Конечные точки и поведение.</li>

                            <li> Модуль 6. Тестирование и устранение WCF сервисов.</li>

                            <li>Модуль 7. Безопасность.

                            <li>Модуль 8. Введение в расширенное программирование WCF.</li>

                        </ul>

                        <p><strong>Документы об окончании:</strong></p>

                        <a href="#"><img src="dist/images/course-specification/doc-tem.jpg" alt=""></a>
                    </div>
                </div>

                <div style="padding-top: 13rem">

                    <div class="course-card">
                        <div class="course-card__container">
                            <div class="course-card__photo">
                                <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                            </div>
                            <div class="course-card__cell">
                                <div class="course-card__top">
                                    <div class="course-card__title">
                                            <span>
                                               Программирование на HTML5 с <br class="show-for-large"> использованием JavaScript и CSS3
                                            </span>
                                    </div>
                                </div>
                                <div class="course-card__wrap">
                                    <div class="course-card__details">
                                        <div class="course-card__column">
                                            <ul>
                                                <li>
                                                    <span>Производитель:</span>
                                                    <span>Направление:</span>
                                                    <span>Формат обучения:</span>

                                                </li>

                                                <li>
                                                    <span>Java</span>
                                                    <span><a href="#">Программирование</a></span>
                                                    <span>
                                                                    <img src="dist/images/course-card/icon-1.png" alt="">
                                                                    <img src="dist/images/course-card/icon-2.png" alt="">
                                                                    <img src="dist/images/course-card/icon-3.png" alt="">
                                                     </span>

                                                </li>
                                            </ul>
                                        </div>
                                        <div class="course-card__column">
                                            <ul>
                                                <li><span>Уровень:</span><span>Цена (от):</span></li>

                                                <li><span>начальный</span><span>35 100,00 руб.</span></li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="course-card">
                        <div class="course-card__container">
                            <div class="course-card__photo">
                                <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                            </div>
                            <div class="course-card__cell">
                                <div class="course-card__top">
                                    <div class="course-card__title">
                                            <span>
                                               Программирование на HTML5 с <br class="show-for-large"> использованием JavaScript и CSS3
                                            </span>
                                    </div>
                                </div>
                                <div class="course-card__wrap">
                                    <div class="course-card__details">
                                        <div class="course-card__column">
                                            <ul>
                                                <li>
                                                    <span>Производитель:</span>
                                                    <span>Направление:</span>
                                                    <span>Формат обучения:</span>

                                                </li>

                                                <li>
                                                    <span>Java</span>
                                                    <span><a href="#">Программирование</a></span>
                                                    <span>
                                                                    <img src="dist/images/course-card/icon-1.png" alt="">
                                                                    <img src="dist/images/course-card/icon-2.png" alt="">
                                                                    <img src="dist/images/course-card/icon-3.png" alt="">
                                                     </span>

                                                </li>
                                            </ul>
                                        </div>
                                        <div class="course-card__column">
                                            <ul>
                                                <li><span>Уровень:</span><span>Цена (от):</span></li>

                                                <li><span>начальный</span><span>35 100,00 руб.</span></li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="course-button-wrap">
                        <a class="course-button">
                            <svg class="icon plus">
                                <use xlink:href="#images--svg--plus"></use>
                            </svg>
                            <span>Добавить в план обучения</span>
                        </a>
                    </div>


                </div>
            </div>
            <div class="column small-12 large-offset-1 large-3">
                <aside>
                    <div class="aside-widget">
                        <div class="aside-widget__title">О центре</div>
                        <div class="aside-widget__content">
                            <ul class="aside-menu">


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/mission">

                                        <span class="b-link__text">Программирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/training-conditions">

                                        <span class="b-link__text">Системное администрирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class=""
                                       href="http://edu.softline.ru/about/svedeniya-ob-obrazovatelnoy-organizatsii">

                                        <span class="b-link__text">Пользовательское ПО</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/accreditation">

                                        <span class="b-link__text">Операционные системы (ОС)</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/status">

                                        <span class="b-link__text">Сетевые технологии</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-klientov">

                                        <span class="b-link__text">Информационная безопасность</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-partnerov">

                                        <span class="b-link__text">Виртуализация</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/razrabotka-kursov">

                                        <span class="b-link__text">Базы данных и СУБД</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/delivery-info">

                                        <span class="b-link__text">Моделирование и САПР</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/requisites">

                                        <span class="b-link__text">Коммуникации</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/tech-support">

                                        <span class="b-link__text">Резервное копирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">CRM</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Бизнес-аналитика</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">IT-сервис менеджмент</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Импортозамещение ПО</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Бизнес-тренинги</span>
                                    </a>

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__title">Будьте в курсе</div>
                        <div class="aside-widget__content">
                            <p>Подпишитесь на рассылку новостей Softline</p>
                            <form action="#">
                                <label for="">
                                    <input type="text" placeholder="Ваш e-mail">
                                </label>
                                <label for="">
                                    <input type="text" placeholder="Ваше имя">
                                </label>
                                <button class="button small expanded">Подписаться</button>
                            </form>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__content">
                            <a href="#">
                                <img src="content/banners/kasp.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>
    <section class="subscribe-banner">
        <div class="subscribe-banner__wrapper">
            <div class="row">
                <div class="column small-12 large-9 large-offset-3">
                    <div class="subscribe-banner__title">
                        <span>Будьте в курсе</span>
                    </div>
                    <form class="subscribe-banner__form">
                        <div class="subscribe-banner__input subscribe-banner__input--name">
                            <!--доп модификатор: error-->
                            <input type="text" placeholder="Ваше имя">
                        </div>
                        <div class="subscribe-banner__input subscribe-banner__input--email">
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="subscribe-banner__button">
                            <button class="button">Подписаться</button>
                        </div>
                    </form>
                    <div class="subscribe-banner__desc">
                        <span>Подпишитесь на информацию о новинках, скидках и акциях. <br> Уже более 36 000 подписчиков!</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        $(document).foundation();
    });
</script>

</body>
</html>
