## Softline Education

### Страницы
* [Главная](http://edu.htmlmak.ru/)
* [Страница курса (до регистрации)](http://edu.htmlmak.ru/course.php)
* [Страница курса. Список модулей (после регистрации)](http://edu.htmlmak.ru/course-modules.php)

### Сборка статики. 

`npm instal` или `yarn install`

`npm run build` для production сборки
 
`npm run development` для development сборки 

### Готовый билд

Или можно скачать готовый билд. Архив будет обвновляться будет по мере готовности страниц
[edu.htmlmak.ru/dist/static_build_latest.zip](http://edu.htmlmak.ru/dist/static_build_latest.zip)