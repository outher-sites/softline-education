<? include 'partials/header.php'; ?>

<section class="course-header">
    <div class="row">
        <div class="column small-12 medium-7 large-8">
            <ul class="breadcrumbs">
                <li><a href="#">Главная</a></li>
                <li><a href="#">Учебный центр</a></li>
                <li><a href="#">Сертификация и экзамены</a></li>
            </ul>
            <div class="course-header__wrapper">
                <div class="course-header__title">
                    <h1>Учебный центр Softline</h1>
                </div>
            </div>
        </div>
        <div class="column small-12 medium-5 large-3 large-offset-1">
            <div class="feedback-header">
                <div class="feedback-header__name">
                    <span>Светлана Жученко</span>
                </div>
                <div class="feedback-header__photo">
                    <img src="dist/images/feedback__header/feedback-header__photo/photo-1.png" alt="">
                </div>
                <div class="feedback-header__position">
                    <span>менеджер</span>
                    <span>интернет-магазина</span>
                </div>
                <div class="feedback-header__contacts">
                    <span>8 (800) 200-08-60 доб. 6011</span>
                    <a href="mailto:Svetlana.Zhuchenko@softlinegroup.com">Svetlana.Zhuchenko@softlinegroup.com</a>
                </div>

                <a href="#" class="button expanded">Связаться сейчас</a>
            </div>
        </div>
    </div>
</section>


<section>
    <div class="row">
        <div class="column small-12 medium-8">
            <ul class="section-nav">
                <li>
                    <a href="#">Москва</a>
                    <a href="#">Санкт-Петербург</a>
                    <a href="#">Владивосток</a>
                </li>
                <li>
                    <a href="#">Екатеринбург</a>
                    <a href="#">Казань</a>
                    <a href="#">Красноярск</a>
                </li>
                <li>
                    <a href="#">Нижний Новгород</a>
                    <a href="#">Новосибирск</a>
                    <a href="#">ОМСК</a>
                </li>
                <li>
                    <a href="#">Ростов-на-Дону</a>
                    <a href="#">Самара</a>
                    <a href="#">Уфа</a>
                </li>
                <li>
                    <a href="#">Хабаровск</a>
                </li>
            </ul>
            <div class="section-contact">
                <div class="section-contact__content">
                    <div class="section-contact__title">
                        <span>Москва</span>
                    </div>
                    <div class="section-contact__data">
                        <table cellspacing="0" cellpadding="0" class="section-contact__table">
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Адрес:</span>
                                </td>
                                <td>
                                    <span>ул. Авроры, д. 63, 5 эт., офис. 504</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Тел./факс:</span>
                                </td>
                                <td>
                                    <span>+7 (3812) 91-8811</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>E-mail:</span>
                                </td>
                                <td>
                                    <a href="mailto:edusales@softline.ru">edusales@softline.ru</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="section-contact__photo">
                    <div class="section-contact__photo-item">
                        <img src="content/section-contact/photo-1.jpg" alt="">
                        <button onclick="$('#contact-map').slideDown();" class="button small">Показать на карте</button>
                    </div>
                </div>
                <div style="display: none" id="contact-map" class="section-contact__map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2248.0142316758543!2d37.689038216255376!3d55.706124380540885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ab4dba9fa4bd9%3A0x81ca3f0e10fdb914!2z0L_RgC4gMi3QuSDQrtC20L3QvtC_0L7RgNGC0L7QstGL0LksIDMxLCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTE1MDg4!5e0!3m2!1sru!2sua!4v1518788639975"
                            width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="section-contact">
                <div class="section-contact__content">
                    <div class="section-contact__title">
                        <span>Санкт-Петербург</span>
                    </div>
                    <div class="section-contact__data">
                        <table cellspacing="0" cellpadding="0" class="section-contact__table">
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Адрес:</span>
                                </td>
                                <td>
                                    <span>Площадь Карла Фаберже, д. 8Б, БЦ «Золотая долина», оф. 306</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Тел./факс:</span>
                                </td>
                                <td>
                                    <span>+7 (812) 777-4446</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>E-mail:</span>
                                </td>
                                <td>
                                    <a href="mailto:edusales@softline.ru">edusales@softline.ru</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="section-contact__photo">
                    <div class="section-contact__photo-item">
                        <img src="content/section-contact/photo-1.jpg" alt="">
                        <button onclick="$('#contact-map-1').slideDown();" class="button small">Показать на карте</button>
                    </div>
                </div>
                <div style="display: none" id="contact-map-1" class="section-contact__map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2248.0142316758543!2d37.689038216255376!3d55.706124380540885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ab4dba9fa4bd9%3A0x81ca3f0e10fdb914!2z0L_RgC4gMi3QuSDQrtC20L3QvtC_0L7RgNGC0L7QstGL0LksIDMxLCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTE1MDg4!5e0!3m2!1sru!2sua!4v1518788639975"
                            width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="section-contact">
                <div class="section-contact__content">
                    <div class="section-contact__title">
                        <span>Владивосток</span>
                    </div>
                    <div class="section-contact__data">
                        <table cellspacing="0" cellpadding="0" class="section-contact__table">
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Адрес:</span>
                                </td>
                                <td>
                                    <span>ул. Пушкинская, д. 109, офис 306 (3-й этаж)</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Тел./факс:</span>
                                </td>
                                <td>
                                    <span>+7 (423) 260-0010</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>E-mail:</span>
                                </td>
                                <td>
                                    <a href="mailto:edusales@softline.ru">edusales@softline.ru</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="section-contact__photo">
                    <div class="section-contact__photo-item">
                        <img src="content/section-contact/photo-1.jpg" alt="">
                        <button onclick="$('#contact-map-2').slideDown();" class="button small">Показать на карте</button>
                    </div>
                </div>
                <div style="display: none" id="contact-map-2" class="section-contact__map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2248.0142316758543!2d37.689038216255376!3d55.706124380540885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ab4dba9fa4bd9%3A0x81ca3f0e10fdb914!2z0L_RgC4gMi3QuSDQrtC20L3QvtC_0L7RgNGC0L7QstGL0LksIDMxLCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTE1MDg4!5e0!3m2!1sru!2sua!4v1518788639975"
                            width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="section-contact">
                <div class="section-contact__content">
                    <div class="section-contact__title">
                        <span>Екатеринбург</span>
                    </div>
                    <div class="section-contact__data">
                        <table cellspacing="0" cellpadding="0" class="section-contact__table">
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Адрес:</span>
                                </td>
                                <td>
                                    <span>ул. 8 Марта, д. 49, БЦ «Арена», 8 эт.</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Тел./факс:</span>
                                </td>
                                <td>
                                    <span>+7 (343) 278-53-35</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>E-mail:</span>
                                </td>
                                <td>
                                    <a href="mailto:edusales@softline.ru">edusales@softline.ru</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="section-contact__photo">
                    <div class="section-contact__photo-item">
                        <img src="content/section-contact/photo-1.jpg" alt="">
                        <button onclick="$('#contact-map-3').slideDown();" class="button small">Показать на карте</button>
                    </div>
                </div>
                <div style="display: none" id="contact-map-3" class="section-contact__map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2248.0142316758543!2d37.689038216255376!3d55.706124380540885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ab4dba9fa4bd9%3A0x81ca3f0e10fdb914!2z0L_RgC4gMi3QuSDQrtC20L3QvtC_0L7RgNGC0L7QstGL0LksIDMxLCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTE1MDg4!5e0!3m2!1sru!2sua!4v1518788639975"
                            width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="section-contact">
                <div class="section-contact__content">
                    <div class="section-contact__title">
                        <span>Красноярск</span>
                    </div>
                    <div class="section-contact__data">
                        <table cellspacing="0" cellpadding="0" class="section-contact__table">
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Адрес:</span>
                                </td>
                                <td>
                                    <span>ул. Партизана Железняка, д. 17, ТОЦ «Ньютон», 5 эт.</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Тел./факс:</span>
                                </td>
                                <td>
                                    <span>+7 (391) 25-25-991, 25-25-993</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>E-mail:</span>
                                </td>
                                <td>
                                    <a href="mailto:edusales@softline.ru">edusales@softline.ru</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="section-contact__photo">
                    <div class="section-contact__photo-item">
                        <img src="content/section-contact/photo-1.jpg" alt="">
                        <button onclick="$('#contact-map-4').slideDown();" class="button small">Показать на карте</button>
                    </div>
                </div>
                <div style="display: none" id="contact-map-4" class="section-contact__map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2248.0142316758543!2d37.689038216255376!3d55.706124380540885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ab4dba9fa4bd9%3A0x81ca3f0e10fdb914!2z0L_RgC4gMi3QuSDQrtC20L3QvtC_0L7RgNGC0L7QstGL0LksIDMxLCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTE1MDg4!5e0!3m2!1sru!2sua!4v1518788639975"
                            width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="section-contact">
                <div class="section-contact__content">
                    <div class="section-contact__title">
                        <span>Нижний Новгород</span>
                    </div>
                    <div class="section-contact__data">
                        <table cellspacing="0" cellpadding="0" class="section-contact__table">
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Адрес:</span>
                                </td>
                                <td>
                                    <span>ул. Новая, д. 28, 1 подъезд, 4 эт.</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Тел./факс:</span>
                                </td>
                                <td>
                                    <span>7 (831) 220-0046</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>E-mail:</span>
                                </td>
                                <td>
                                    <a href="mailto:edusales@softline.ru">edusales@softline.ru</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="section-contact__photo">
                    <div class="section-contact__photo-item">
                        <img src="content/section-contact/photo-1.jpg" alt="">
                        <button onclick="$('#contact-map-5').slideDown();" class="button small">Показать на карте</button>
                    </div>
                </div>
                <div style="display: none" id="contact-map-5" class="section-contact__map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2248.0142316758543!2d37.689038216255376!3d55.706124380540885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ab4dba9fa4bd9%3A0x81ca3f0e10fdb914!2z0L_RgC4gMi3QuSDQrtC20L3QvtC_0L7RgNGC0L7QstGL0LksIDMxLCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTE1MDg4!5e0!3m2!1sru!2sua!4v1518788639975"
                            width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="section-contact">
                <div class="section-contact__content">
                    <div class="section-contact__title">
                        <span>Новосибирск</span>
                    </div>
                    <div class="section-contact__data">
                        <table cellspacing="0" cellpadding="0" class="section-contact__table">
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Адрес:</span>
                                </td>
                                <td>
                                    <span>ул. Новая, д. 28, 1 подъезд, 4 эт.</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Тел./факс:</span>
                                </td>
                                <td>
                                    <span>7 (831) 220-0046</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>E-mail:</span>
                                </td>
                                <td>
                                    <a href="mailto:edusales@softline.ru">edusales@softline.ru</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="section-contact__photo">
                    <div class="section-contact__photo-item">
                        <img src="content/section-contact/photo-1.jpg" alt="">
                        <button onclick="$('#contact-map-6').slideDown();" class="button small">Показать на карте</button>
                    </div>
                </div>
                <div style="display: none" id="contact-map-6" class="section-contact__map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2248.0142316758543!2d37.689038216255376!3d55.706124380540885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ab4dba9fa4bd9%3A0x81ca3f0e10fdb914!2z0L_RgC4gMi3QuSDQrtC20L3QvtC_0L7RgNGC0L7QstGL0LksIDMxLCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTE1MDg4!5e0!3m2!1sru!2sua!4v1518788639975"
                            width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="section-contact">
                <div class="section-contact__content">
                    <div class="section-contact__title">
                        <span>Омск</span>
                    </div>
                    <div class="section-contact__data">
                        <table cellspacing="0" cellpadding="0" class="section-contact__table">
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Адрес:</span>
                                </td>
                                <td>
                                    <span>ул. Новая, д. 28, 1 подъезд, 4 эт.</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Тел./факс:</span>
                                </td>
                                <td>
                                    <span>7 (831) 220-0046</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>E-mail:</span>
                                </td>
                                <td>
                                    <a href="mailto:edusales@softline.ru">edusales@softline.ru</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="section-contact__photo">
                    <div class="section-contact__photo-item">
                        <img src="content/section-contact/photo-1.jpg" alt="">
                        <button onclick="$('#contact-map-7').slideDown();" class="button small">Показать на карте</button>
                    </div>
                </div>
                <div style="display: none" id="contact-map-7" class="section-contact__map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2248.0142316758543!2d37.689038216255376!3d55.706124380540885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ab4dba9fa4bd9%3A0x81ca3f0e10fdb914!2z0L_RgC4gMi3QuSDQrtC20L3QvtC_0L7RgNGC0L7QstGL0LksIDMxLCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTE1MDg4!5e0!3m2!1sru!2sua!4v1518788639975"
                            width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="section-contact">
                <div class="section-contact__content">
                    <div class="section-contact__title">
                        <span>Ростов-на-Дону</span>
                    </div>
                    <div class="section-contact__data">
                        <table cellspacing="0" cellpadding="0" class="section-contact__table">
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Адрес:</span>
                                </td>
                                <td>
                                    <span>ул. Новая, д. 28, 1 подъезд, 4 эт.</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Тел./факс:</span>
                                </td>
                                <td>
                                    <span>7 (831) 220-0046</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>E-mail:</span>
                                </td>
                                <td>
                                    <a href="mailto:edusales@softline.ru">edusales@softline.ru</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="section-contact__photo">
                    <div class="section-contact__photo-item">
                        <img src="content/section-contact/photo-1.jpg" alt="">
                        <button onclick="$('#contact-map-8').slideDown();" class="button small">Показать на карте</button>
                    </div>
                </div>
                <div style="display: none" id="contact-map-8" class="section-contact__map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2248.0142316758543!2d37.689038216255376!3d55.706124380540885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ab4dba9fa4bd9%3A0x81ca3f0e10fdb914!2z0L_RgC4gMi3QuSDQrtC20L3QvtC_0L7RgNGC0L7QstGL0LksIDMxLCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTE1MDg4!5e0!3m2!1sru!2sua!4v1518788639975"
                            width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="section-contact">
                <div class="section-contact__content">
                    <div class="section-contact__title">
                        <span>Самара</span>
                    </div>
                    <div class="section-contact__data">
                        <table cellspacing="0" cellpadding="0" class="section-contact__table">
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Адрес:</span>
                                </td>
                                <td>
                                    <span>ул. Новая, д. 28, 1 подъезд, 4 эт.</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Тел./факс:</span>
                                </td>
                                <td>
                                    <span>7 (831) 220-0046</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>E-mail:</span>
                                </td>
                                <td>
                                    <a href="mailto:edusales@softline.ru">edusales@softline.ru</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="section-contact__photo">
                    <div class="section-contact__photo-item">
                        <img src="content/section-contact/photo-1.jpg" alt="">
                        <button onclick="$('#contact-map-9').slideDown();" class="button small">Показать на карте</button>
                    </div>
                </div>
                <div style="display: none" id="contact-map-9" class="section-contact__map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2248.0142316758543!2d37.689038216255376!3d55.706124380540885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ab4dba9fa4bd9%3A0x81ca3f0e10fdb914!2z0L_RgC4gMi3QuSDQrtC20L3QvtC_0L7RgNGC0L7QstGL0LksIDMxLCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTE1MDg4!5e0!3m2!1sru!2sua!4v1518788639975"
                            width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="section-contact">
                <div class="section-contact__content">
                    <div class="section-contact__title">
                        <span>Уфа</span>
                    </div>
                    <div class="section-contact__data">
                        <table cellspacing="0" cellpadding="0" class="section-contact__table">
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Адрес:</span>
                                </td>
                                <td>
                                    <span>ул. Новая, д. 28, 1 подъезд, 4 эт.</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Тел./факс:</span>
                                </td>
                                <td>
                                    <span>7 (831) 220-0046</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>E-mail:</span>
                                </td>
                                <td>
                                    <a href="mailto:edusales@softline.ru">edusales@softline.ru</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="section-contact__photo">
                    <div class="section-contact__photo-item">
                        <img src="content/section-contact/photo-1.jpg" alt="">
                        <button onclick="$('#contact-map-11').slideDown();" class="button small">Показать на карте</button>
                    </div>
                </div>
                <div style="display: none" id="contact-map-11" class="section-contact__map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2248.0142316758543!2d37.689038216255376!3d55.706124380540885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ab4dba9fa4bd9%3A0x81ca3f0e10fdb914!2z0L_RgC4gMi3QuSDQrtC20L3QvtC_0L7RgNGC0L7QstGL0LksIDMxLCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTE1MDg4!5e0!3m2!1sru!2sua!4v1518788639975"
                            width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="section-contact">
                <div class="section-contact__content">
                    <div class="section-contact__title">
                        <span>Хабаровск</span>
                    </div>
                    <div class="section-contact__data">
                        <table cellspacing="0" cellpadding="0" class="section-contact__table">
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Адрес:</span>
                                </td>
                                <td>
                                    <span>ул. Новая, д. 28, 1 подъезд, 4 эт.</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>Тел./факс:</span>
                                </td>
                                <td>
                                    <span>7 (831) 220-0046</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-contact__table-name">
                                    <span>E-mail:</span>
                                </td>
                                <td>
                                    <a href="mailto:edusales@softline.ru">edusales@softline.ru</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="section-contact__photo">
                    <div class="section-contact__photo-item">
                        <img src="content/section-contact/photo-1.jpg" alt="">
                        <button onclick="$('#contact-map-12').slideDown();" class="button small">Показать на карте</button>
                    </div>
                </div>
                <div style="display: none" id="contact-map-12" class="section-contact__map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2248.0142316758543!2d37.689038216255376!3d55.706124380540885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ab4dba9fa4bd9%3A0x81ca3f0e10fdb914!2z0L_RgC4gMi3QuSDQrtC20L3QvtC_0L7RgNGC0L7QstGL0LksIDMxLCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTE1MDg4!5e0!3m2!1sru!2sua!4v1518788639975"
                            width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
    </div>
    <div class="column small-12 large-offset-1 large-3">
        <aside>
            <div class="aside-widget">
                <div class="aside-widget__title">О центре</div>
                <div class="aside-widget__content">
                    <ul class="aside-menu">


                        <li class="">

                            <a class="" href="http://edu.softline.ru/about/mission">

                                <span class="b-link__text">Программирование</span>
                            </a>

                        </li>


                        <li class="">

                            <a class="" href="http://edu.softline.ru/about/training-conditions">

                                <span class="b-link__text">Системное администрирование</span>
                            </a>

                        </li>


                        <li class="">

                            <a class=""
                               href="http://edu.softline.ru/about/svedeniya-ob-obrazovatelnoy-organizatsii">

                                <span class="b-link__text">Пользовательское ПО</span>
                            </a>

                        </li>


                        <li class="">

                            <a class="" href="http://edu.softline.ru/about/accreditation">

                                <span class="b-link__text">Операционные системы (ОС)</span>
                            </a>

                        </li>


                        <li class="">

                            <a class="" href="http://edu.softline.ru/about/status">

                                <span class="b-link__text">Сетевые технологии</span>
                            </a>

                        </li>


                        <li class="">

                            <a class="" href="http://edu.softline.ru/about/otzyivyi-klientov">

                                <span class="b-link__text">Информационная безопасность</span>
                            </a>

                        </li>


                        <li class="">

                            <a class="" href="http://edu.softline.ru/about/otzyivyi-partnerov">

                                <span class="b-link__text">Виртуализация</span>
                            </a>

                        </li>


                        <li class="">

                            <a class="" href="http://edu.softline.ru/about/razrabotka-kursov">

                                <span class="b-link__text">Базы данных и СУБД</span>
                            </a>

                        </li>


                        <li class="">

                            <a class="" href="http://edu.softline.ru/about/delivery-info">

                                <span class="b-link__text">Моделирование и САПР</span>
                            </a>

                        </li>


                        <li class="">

                            <a class="" href="http://edu.softline.ru/about/requisites">

                                <span class="b-link__text">Коммуникации</span>
                            </a>

                        </li>


                        <li class="">

                            <a class="" href="http://edu.softline.ru/tech-support">

                                <span class="b-link__text">Резервное копирование</span>
                            </a>

                        </li>


                        <li class="">

                            <a class="" href="http://edu.softline.ru/about/contact">

                                <span class="b-link__text">CRM</span>
                            </a>

                        </li>
                        <li class="">

                            <a class="" href="http://edu.softline.ru/about/contact">

                                <span class="b-link__text">Бизнес-аналитика</span>
                            </a>

                        </li>
                        <li class="">

                            <a class="" href="http://edu.softline.ru/about/contact">

                                <span class="b-link__text">IT-сервис менеджмент</span>
                            </a>

                        </li>
                        <li class="">

                            <a class="" href="http://edu.softline.ru/about/contact">

                                <span class="b-link__text">Импортозамещение ПО</span>
                            </a>

                        </li>
                        <li class="">

                            <a class="" href="http://edu.softline.ru/about/contact">

                                <span class="b-link__text">Бизнес-тренинги</span>
                            </a>

                        </li>
                    </ul>
                </div>
            </div>
            <div class="aside-widget">
                <div class="aside-widget__title">Подбор курса</div>
                <div class="aside-widget__content">
                    <form action="#">
                        <div class="filter__input">
                            <div class="filter__label">
                                <span>Направление:</span>
                            </div>
                            <label class="b-select">
                                <select class="b-select__container">
                                    <option value="">Все направления</option>
                                    <option value="">Все направления 2</option>
                                    <option value="">Все направления 3</option>
                                </select>
                            </label>
                        </div>
                        <div class="filter__input">
                            <div class="filter__label">
                                <span>Специальность:</span>
                            </div>
                            <label class="b-select">
                                <select class="b-select__container">
                                    <option value="">Все специальности</option>
                                    <option value="">Все специальности 2</option>
                                    <option value="">Все специальности 3</option>
                                </select>
                            </label>
                        </div>
                        <div class="filter__input">
                            <div class="filter__label">
                                <span>Вендор:</span>
                            </div>
                            <label class="b-select">
                                <select class="b-select__container">
                                    <option value="">Все Вендоры</option>
                                    <option value="">Все Вендоры 2</option>
                                    <option value="">Все Вендоры 3</option>
                                </select>
                            </label>
                        </div>
                        <div class="filter__input">
                            <div class="filter__label">
                                <span>Вендор:</span>
                            </div>
                            <label class="b-select">
                                <select class="b-select__container">
                                    <option value="">Все Вендоры</option>
                                    <option value="">Все Вендоры 2</option>
                                    <option value="">Все Вендоры 3</option>
                                </select>
                            </label>
                        </div>
                        <div class="radio-box">
                            <label class="b-radiobox">
                                <input name="event-filter" type="radio">
                                <span>Очно (в аудитории)</span>
                            </label>
                            <label class="b-radiobox">
                                <input name="event-filter" type="radio">
                                <span>Дистанционно</span>
                            </label>
                        </div>
                        <div class="filter__input">
                            <div class="filter__label">
                                <span>Место проведения:</span>
                            </div>
                            <label class="b-select">
                                <select class="b-select__container">
                                    <option value="">Москва</option>
                                    <option value="">Москва 2</option>
                                    <option value="">Москва 3</option>
                                </select>
                            </label>
                        </div>
                        <button class="button small expanded">Подобрать курс</button>
                    </form>
                </div>
            </div>
            <div class="aside-widget">
                <div class="aside-widget__title">Будьте в курсе</div>
                <div class="aside-widget__content">
                    <p>Подпишитесь на рассылку новостей Softline</p>
                    <form action="#">
                        <label for="">
                            <input type="text" placeholder="Ваш e-mail">
                        </label>
                        <label for="">
                            <input type="text" placeholder="Ваше имя">
                        </label>
                        <button class="button small expanded">Подписаться</button>
                    </form>
                </div>
            </div>
            <div class="aside-widget">
                <div class="aside-widget__content">
                    <a href="#">
                        <img src="content/banners/kasp.jpg" alt="">
                    </a>
                </div>
            </div>
        </aside>
    </div>
    </div>

</section>
<section class="subscribe-banner">
    <div class="subscribe-banner__wrapper">
        <div class="row">
            <div class="column small-12 large-9 large-offset-3">
                <div class="subscribe-banner__title">
                    <span>Будьте в курсе</span>
                </div>
                <form class="subscribe-banner__form">
                    <div class="subscribe-banner__input subscribe-banner__input--name">
                        <!--доп модификатор: error-->
                        <input type="text" placeholder="Ваше имя">
                    </div>
                    <div class="subscribe-banner__input subscribe-banner__input--email">
                        <input type="text" placeholder="Ваш e-mail">
                    </div>
                    <div class="subscribe-banner__button">
                        <button class="button">Подписаться</button>
                    </div>
                </form>
                <div class="subscribe-banner__desc">
                    <span>Подпишитесь на информацию о новинках, скидках и акциях. <br> Уже более 36 000 подписчиков!</span>
                </div>
            </div>
        </div>
    </div>
</section>
<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        $('.b-select__container').select2();
        $(document).foundation();
    });
</script>
</body>
</html>
