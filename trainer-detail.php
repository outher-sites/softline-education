<? include 'partials/header-cloud.php'; ?>

<main>
    <section class="course-header">
        <div class="row">
            <div class="column small-12 medium-7 large-8">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Учебный центр</a></li>
                    <li><a href="#">О нас</a></li>
                </ul>
                <div class="course-header__wrapper">
                    <div class="course-header__title">
                        <h1>Учебный центр Softline</h1>
                    </div>
                </div>
                <div class="course-header__download">
                    <a href="#" class="button">Скачать презентацию <span>PDF, 2,7 Мб</span></a>
                </div>
            </div>
            <div class="column small-12 medium-5 large-3 large-offset-1">
                <div class="feedback-header">
                    <div class="feedback-header__name">
                        <span>Светлана Жученко</span>
                    </div>
                    <div class="feedback-header__photo">
                        <img src="dist/images/feedback__header/feedback-header__photo/photo-1.png" alt="">
                    </div>
                    <div class="feedback-header__position">
                        <span>менеджер</span>
                        <span>интернет-магазина</span>
                    </div>
                    <div class="feedback-header__contacts">
                        <span>8 (800) 200-08-60 доб. 6011</span>
                        <a href="Svetlana.Zhuchenko@softlinegroup.com">Svetlana.Zhuchenko@softlinegroup.com</a>
                    </div>

                    <a href="#" class="button expanded">Связаться сейчас</a>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div style="padding-bottom: 8rem" class="column small-12 large-8">
                <div class="row">
                    <div class="column small-12 medium-6">
                        <div class="trainer-detail">
                            <h1 class="trainer-detail__title h3">Фамилиев <br>Имен<br> Отчествович</h1>

                            <div class="b-editor">
                                <p>
                                    Российский учёный, педагог и общественный деятель, известный своими трудами и
                                    выступлениями в области использования в деятельности предприятий и государства
                                    информационных технологий, систем управления знаниями и коллективного интеллекта.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="column small-12 medium-6">
                        <div class="trainer-detail">
                            <div class="trainer-detail__photo">
                                <img src="content/trainer/photo.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-box">
                    <ul class="tabs" data-tabs id="example-tabs">
                        <li class="tabs-title is-active"><a href="#panel1" aria-selected="true">Курсы</a></li>
                        <li class="tabs-title"><a data-tabs-target="panel2" href="#panel2">Информация</a></li>
                    </ul>

                    <div class="tabs-content" data-tabs-content="example-tabs">
                        <div class="tabs-panel is-active" id="panel1">
                            <div class="course-card">
                                <div class="course-card__container">
                                    <div class="course-card__photo">
                                        <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                                    </div>
                                    <div class="course-card__cell">
                                        <div class="course-card__top">
                                            <div class="course-card__title">
                                                <span>
                                                    Java Standard Edition (Java SE).
                                                   <br> Программирование. Базовые технологии
                                                </span>
                                            </div>
                                            <div class="course-card__tags">
                                                <div class="course-card__tag pink">Топ 50</div>
                                                <div class="course-card__tag green">Скидка 10%</div>
                                            </div>
                                        </div>
                                        <div class="course-card__wrap">
                                            <div class="course-card__details">
                                                <div class="course-card__column">
                                                    <ul>
                                                        <li>
                                                            <span>Производитель:</span><span>Формат обучения:</span></li>

                                                        <li><span>Java</span>
                                                            <span>
                                                                    <img src="dist/images/course-card/icon-1.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-2.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-3.png"
                                                                         alt="">
                                                                </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="course-card__column">
                                                    <ul>
                                                        <li><span>Уровень:</span><span>Цена (от):</span></li>

                                                        <li>
                                                            <span>начальный</span><span>35 100,00 руб.<span class="old-price">39 000 руб.</span> </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="course-card__desc">
                                                <p>
                                                    В предлагаемой программе даётся обзор платформ: Java Standard
                                                    Edition
                                                    (J2SE/Java SE 8) и Java Enterprise Edition (J2EE / Java EE 7)
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="course-card__button">
                                    <span>Расписание и цены</span>

                                    <svg>
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </a>
                            </div>
                            <div class="course-card">
                                <div class="course-card__container">
                                    <div class="course-card__photo">
                                        <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                                    </div>
                                    <div class="course-card__cell">
                                        <div class="course-card__top">
                                            <div class="course-card__title">
                                            <span>
                                                Java Standard Edition (Java SE).
                                               <br> Программирование. Базовые технологии
                                            </span>
                                            </div>
                                        </div>
                                        <div class="course-card__wrap">
                                            <div class="course-card__details">
                                                <div class="course-card__column">
                                                    <ul>
                                                        <li><span>Производитель:</span><span>Формат обучения:</span>
                                                        </li>

                                                        <li><span>Java</span>
                                                            <span>
                                                                    <img src="dist/images/course-card/icon-1.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-2.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-3.png"
                                                                         alt="">
                                                                </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="course-card__column">
                                                    <ul>
                                                        <li><span>Уровень:</span><span>Цена (от):</span></li>

                                                        <li><span>начальный</span><span>35 100,00 руб.</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="course-card__desc">
                                                <p>
                                                    В предлагаемой программе даётся обзор платформ: Java Standard
                                                    Edition
                                                    (J2SE/Java SE 8) и Java Enterprise Edition (J2EE / Java EE 7)
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="course-card__button">
                                    <span>Расписание и цены</span>

                                    <svg>
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </a>
                            </div>
                            <div class="course-card">
                                <div class="course-card__container">
                                    <div class="course-card__photo">
                                        <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                                    </div>
                                    <div class="course-card__cell">
                                        <div class="course-card__top">
                                            <div class="course-card__title">
                                            <span>
                                                Java Standard Edition (Java SE).
                                               <br> Программирование. Базовые технологии
                                            </span>
                                            </div>
                                        </div>
                                        <div class="course-card__wrap">
                                            <div class="course-card__details">
                                                <div class="course-card__column">
                                                    <ul>
                                                        <li><span>Производитель:</span><span>Формат обучения:</span>
                                                        </li>

                                                        <li><span>Java</span>
                                                            <span>
                                                                    <img src="dist/images/course-card/icon-1.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-2.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-3.png"
                                                                         alt="">
                                                                </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="course-card__column">
                                                    <ul>
                                                        <li><span>Уровень:</span><span>Цена (от):</span></li>

                                                        <li><span>начальный</span><span>35 100,00 руб.</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="course-card__desc">
                                                <p>
                                                    В предлагаемой программе даётся обзор платформ: Java Standard
                                                    Edition
                                                    (J2SE/Java SE 8) и Java Enterprise Edition (J2EE / Java EE 7)
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="course-card__button">
                                    <span>Расписание и цены</span>

                                    <svg>
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                        <div class="tabs-panel" id="panel2">
                            <div class="b-editor">
                                <p>Более 20 лет в сфере ИТ и интернет-технологий.</p>

                                <p>Более 15 лет опыта работы ИТ-директором в различных сферах экономики</p>

                                <p>Стаж преподавательской деятельности более 20 лет</p>

                                <p>Преподает на кафедре бизнес-информатики Финансового университета при Правительстве
                                    РФ, Школе Бизнес-Информатики при НИУ «Высшая школа экономики», в Высшей Школе
                                    Бизнеса при МГУ им. Ломоносова.</p>

                                <p>Один из создателей межрегиональной общественной организации Союз ИТ-директоров
                                    России.</p>

                                <p>Член Координационного Совета СДС «ИТ-Стандарт».</p>

                                <p>Член редколлегии журнала БИТ.</p>

                                <p>Член экспертного совета при Комитете по образованию и науке Госдумы РФ.</p>

                                <p>Автор многочисленных публикаций в области ИТ и инноваций.</p>

                                <p>Участие в рабочей группе по разработке программы по развитию цифровой экономики
                                    России.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="column small-12 large-offset-1 large-3">
                <aside>
                    <div class="aside-widget">
                        <div class="aside-widget__title">О центре</div>
                        <div class="aside-widget__content">
                            <ul class="aside-menu">


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/mission">

                                        <span class="b-link__text">Программирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/training-conditions">

                                        <span class="b-link__text">Системное администрирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class=""
                                       href="http://edu.softline.ru/about/svedeniya-ob-obrazovatelnoy-organizatsii">

                                        <span class="b-link__text">Пользовательское ПО</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/accreditation">

                                        <span class="b-link__text">Операционные системы (ОС)</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/status">

                                        <span class="b-link__text">Сетевые технологии</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-klientov">

                                        <span class="b-link__text">Информационная безопасность</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-partnerov">

                                        <span class="b-link__text">Виртуализация</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/razrabotka-kursov">

                                        <span class="b-link__text">Базы данных и СУБД</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/delivery-info">

                                        <span class="b-link__text">Моделирование и САПР</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/requisites">

                                        <span class="b-link__text">Коммуникации</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/tech-support">

                                        <span class="b-link__text">Резервное копирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">CRM</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Бизнес-аналитика</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">IT-сервис менеджмент</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Импортозамещение ПО</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Бизнес-тренинги</span>
                                    </a>

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__title">Подбор курса</div>
                        <div class="aside-widget__content">
                            <form action="#">
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Направление:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все направления</option>
                                            <option value="">Все направления 2</option>
                                            <option value="">Все направления 3</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Специальность:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все специальности</option>
                                            <option value="">Все специальности 2</option>
                                            <option value="">Все специальности 3</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Вендор:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все Вендоры</option>
                                            <option value="">Все Вендоры 2</option>
                                            <option value="">Все Вендоры 3</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Вендор:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все Вендоры</option>
                                            <option value="">Все Вендоры 2</option>
                                            <option value="">Все Вендоры 3</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="radio-box">
                                    <label class="b-radiobox">
                                        <input name="event-filter" type="radio">
                                        <span>Очно (в аудитории)</span>
                                    </label>
                                    <label class="b-radiobox">
                                        <input name="event-filter" type="radio">
                                        <span>Дистанционно</span>
                                    </label>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Место проведения:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Москва</option>
                                            <option value="">Москва 2</option>
                                            <option value="">Москва 3</option>
                                        </select>
                                    </label>
                                </div>
                                <button class="button small expanded">Подобрать курс</button>
                            </form>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__title">Будьте в курсе</div>
                        <div class="aside-widget__content">
                            <p>Подпишитесь на рассылку новостей Softline</p>
                            <form action="#">
                                <label for="">
                                    <input type="text" placeholder="Ваш e-mail">
                                </label>
                                <label for="">
                                    <input type="text" placeholder="Ваше имя">
                                </label>
                                <button class="button small expanded">Подписаться</button>
                            </form>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>
    <section class="subscribe-banner">
        <div class="subscribe-banner__wrapper">
            <div class="row">
                <div class="column small-12 large-9 large-offset-3">
                    <div class="subscribe-banner__title">
                        <span>Будьте в курсе</span>
                    </div>
                    <form class="subscribe-banner__form">
                        <div class="subscribe-banner__input subscribe-banner__input--name">
                            <input type="text" placeholder="Ваше имя">
                        </div>
                        <div class="subscribe-banner__input subscribe-banner__input--email">
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="subscribe-banner__button">
                            <button class="button">Подписаться</button>
                        </div>
                    </form>
                    <div class="subscribe-banner__desc">
                        <span>Подпишитесь на информацию о новинках, скидках и акциях. <br> Уже более 36 000 подписчиков!</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<footer class="footer">
    <div class="row footer__middle">
        <div class="column small-12 large-3">
            <div class="footer__contacts">
                <div class="row">
                    <div class="column small-12 medium-6 large-12">
                        <div class="footer__contacts-item">
                            <span><a href="mailto:services@softlinegroup.com">services@softlinegroup.com</a></span>
                        </div>
                        <div class="footer__contacts-item">
                            <span><a href="tel:+74952320023">+7(495) 232-0023</a></span> <br>
                            <span><a href="tel:88002320023">8 800 2320023</a> </span>
                        </div>
                    </div>
                    <div class="column small-12 medium-6 large-12">
                        <div class="footer__contacts-item footer__contacts-item--address">
                            <span>115114, Москва, <br class="show-for-medium">Дербеневская наб., <br class="show-for-medium"> дом 7, стр. 8</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column small-12 medium-2">
            <ul class="footer__link">
                <li><a href="#">Программное обеспечение</a></li>
                <li><a href="#">Услуги и решения</a></li>
                <li><a href="#">IT-обучение</a></li>
                <li><a href="#">Специальные предложения</a></li>
                <li><a href="#">IT-события</a></li>
            </ul>
        </div>
        <div class="column small-12 medium-2">
            <ul class="footer__link">
                <li><a href="#">Новости компании </a></li>
                <li><a href="#">Мобильные <br class="show-for-medium"> приложения</a></li>
                <li><a href="#">Печатный <br class="show-for-medium"> каталог ПО</a></li>
                <li><a href="#">Облачные решения</a></li>
                <li><a href="#">Качество <br class="show-for-medium"> и ISO 9001</a></li>
            </ul>
        </div>
        <div class="column small-12 medium-2">
            <ul class="footer__link">
                <li><a href="#">Техподдержка </a></li>
                <li><a href="#">О компании</a></li>
                <li><a href="#">Вакансии</a></li>
                <li><a href="#">Контакты</a></li>
                <li><a href="#">Приемная Softline</a></li>
                <li><a href="#">Группа компаний Softline</a></li>
            </ul>
        </div>
        <div class="column small-12 medium-6 large-2">

            <div class="footer__social">
                <p style="margin: 0 -30px 0 0">Свяжитесь с нами:</p>
            </div>
            <div class="footer__social">
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon facebook">
                            <use xlink:href="#images--svg--facebook"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon twitter">
                            <use xlink:href="#images--svg--twitter"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon vk">
                            <use xlink:href="#images--svg--vk"></use>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="footer__social">
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon youtube">
                            <use xlink:href="#images--svg--youtube"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon linkedin">
                            <use xlink:href="#images--svg--linkedin"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon habr">
                            <use xlink:href="#images--svg--habr"></use>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="footer__social">
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon google-plus">
                            <use xlink:href="#images--svg--google+"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon instagram">
                            <use xlink:href="#images--svg--instagram"></use>
                        </svg>
                    </a>
                </div>
                <div class="footer__social-icon">
                    <a href="#">
                        <svg class="icon telegram">
                            <use xlink:href="#images--svg--telegram"></use>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="column small-12 show-for-medium">
            <hr class="footer__divider">
        </div>
        <div class="column small-12 medium-3">
            <ul class="nav-area show-for-medium">
                <li>
                    <button>RU</button>
                </li>
            </ul>
        </div>
        <div class="column small-12 medium-2">
            <div class="footer__company">© 1993—2017 Softline</div>
        </div>
        <div class="column small-12 medium-2">
            <div class="footer__terms"><a href="#">Условия использования</a></div>
        </div>
        <div class="column small-12 medium-2"><span class="footer__age-limit">14+</span></div>
    </div>
</footer>

<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        $(document).foundation();
        //$('.b-select').select2();
    });
</script>
</body>
</html>

