<? include 'partials/header.php'; ?>

<main>
    <section class="course-header">
        <div class="row">
            <div class="column small-12 medium-7 large-8">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Учебный центр</a></li>
                    <li><a href="#">Вендоры</a></li>
                    <li><a href="#">Microsoft Corporation</a></li>
                </ul>
                <div class="course-header__wrapper">
                    <div class="course-header__title">
                        <h1>Специальные предложения</h1>
                    </div>
                </div>
                <div class="course-header__download">
                    <a href="#" class="button">Скачать презентацию <span>PDF, 2,7 Мб</span></a>
                </div>
            </div>
            <div class="column small-12 medium-5 large-3 large-offset-1">
                <div class="feedback-header">
                    <div class="feedback-header__name">
                        <span>Светлана Жученко</span>
                    </div>
                    <div class="feedback-header__photo">
                        <img src="dist/images/feedback__header/feedback-header__photo/photo-1.png" alt="">
                    </div>
                    <div class="feedback-header__position">
                        <span>менеджер</span>
                        <span>интернет-магазина</span>
                    </div>
                    <div class="feedback-header__contacts">
                        <span>8 (800) 200-08-60 доб. 6011</span>
                        <a href="Svetlana.Zhuchenko@softlinegroup.com">Svetlana.Zhuchenko@softlinegroup.com</a>
                    </div>

                    <a href="#" class="button expanded">Связаться сейчас</a>
                </div>
            </div>
        </div>
    </section>
    <section style="padding-bottom: 8rem;">
        <div class="row">
            <div class="column small-12 large-8">

                <div class="about-us__list">
                    <div class="b-editor">
                        <div class="row">

                            <ul class="row small-12 medium-6 small-up-1 column" style="padding-top: 0;">
                                <li class="column">
                                    Лидер на рынке IT-обучения.
                                </li>
                                <li class="column">
                                    Более <strong>16 лет</strong> опыта работы.
                                </li>
                                <li class="column">
                                    Широкая сеть из <strong>25</strong> представительств в крупнейших городах РФ и
                                    СНГ
                                </li>
                                <li class="column">
                                    Международные сертификаты для IT-специалистов и пользователей в Центрах
                                    тестирования.
                                </li>
                                <li class="column">
                                    Более <strong>250</strong> тысяч подготовленных IT-специалистов.
                                </li>
                            </ul>

                            <ul class="row small-12 medium-6 small-up-1 column" style="padding-top: 0;">
                                <li class="column">
                                    Высокотехнологическое оборудование
                                </li>
                                <li class="column">
                                    Лицензия на образовательную деятельность.
                                </li>
                                <li class="column">
                                    Сертифицированные тренеры
                                    с богатым практическим опытом работы.
                                </li>
                                <li class="column">
                                    Авторизации от мировых производителей ПО.
                                </li>
                                <li class="column">
                                    Гибкий индивидуальный подход в обучении, скидки, акции и «горящие» курсы.
                                </li>
                            </ul>


                        </div>
                    </div>
                </div>

                <div class="spacer" style="padding-top: 48px;padding-bottom: 41px;">
                    <hr>
                </div>

                <div class="row">
                    <div class="column small-12 medium-6">
                        <div class="red-title">1500+</div>
                        <p>
                            Курсов различной тематики <br>
                            и уровня сложности
                        </p>
                    </div>
                    <div class="column small-12 medium-6">
                        <div class="red-title">200+</div>
                        <p>
                            Вендоров<br>
                            мирового уровня
                        </p>
                    </div>
                </div>

                <br>
                <br>

                <div class="row">
                    <div class="column small-12 medium-6">
                        <div class="red-title">13 городов</div>
                    </div>
                    <div class="column small-12 medium-6">
                        <div class="red-title">4 страны</div>
                    </div>
                    <div class="column small-12">
                        <p>
                            Удобство сдачи экзаменов: центры тестирования расположены в 13 городах России и 4 странах
                            СНГ.
                        </p>
                    </div>
                </div>

                <br>
                <br>

                <div class="row">
                    <div class="column small-12 medium-6">
                        <div class="red-title">
                            <img src="dist/images/about-us/payment.png" alt="">
                        </div>
                        <p>
                            Удобная форма оплаты:<br>
                            по счету или картой
                        </p>
                    </div>
                    <div class="column small-12 medium-6">
                        <div class="red-title">
                            <img src="dist/images/about-us/certificat.png" alt="">
                        </div>
                        <p>
                            Признанный во всем мире стандарт IT-сертификации
                        </p>
                    </div>
                </div>

                <div class="spacer" style="padding-bottom: 66px;"></div>

                <div class="row">
                    <div class="column small-12 medium-6">
                        <div class="b-editor check">
                            <h4>Кого мы учим?</h4>
                            <ul>
                                <li>IT-специалисты.</li>
                                <li>IT-пользователи.</li>
                                <li>IT-тренеры.</li>
                                <li>Менеджеры и руководители.</li>
                                <li>Физические лица.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="column small-12 medium-6">
                        <div class="b-editor check">
                            <h4>Где это проходит?</h4>
                            <ul>
                                <li>Классы учебного центра Softline.</li>
                                <li>Территория заказчика.</li>
                                <li>Арендованные классы.</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <br>
                <br>

                <div class="row">
                    <div class="column small-12">
                        <div class="b-editor check">
                            <h4>Курсы по направлениям</h4>
                            <div class="row">
                                <div class="column small-12 medium-6">
                                    <ul>
                                        <li>Программирование.</li>
                                        <li>Администрирование.</li>
                                        <li>Операционные системы.</li>
                                        <li>Виртуализация.</li>
                                    </ul>
                                </div>
                                <div class="column small-12 medium-6">
                                    <ul>
                                        <li>Сетевое оборудование.</li>
                                        <li>Безопасность.</li>
                                        <li>Пользовательское ПО.</li>
                                        <li>Базы данных и СУБД.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <br>

                <div class="row">
                    <div class="column small-12">
                        <div class="b-editor check">
                            <h4>Дополнительные услуги</h4>
                            <div class="row">
                                <div class="column small-12 medium-6">
                                    <ul>
                                        <li>Формирование плана обучения под конкретную организацию.</li>
                                        <li>Тестирование до и/или после обучения.</li>
                                        <li>Помощь в бронировании номеров в гостинице.</li>
                                        <li>Предварительное оповещение о дополнениях и изменениях в расписании
                                            мероприятий.
                                        </li>
                                    </ul>
                                </div>
                                <div class="column small-12 medium-6">
                                    <ul>
                                        <li>Запись дистанционных курсов.</li>
                                        <li>Модификация контента курса.</li>
                                        <li>Сертификационные экзамены.</li>
                                        <li>Дистанционные лабораторные работы.</li>
                                        <li>Консультации после курсов (услуга «Личный тренер»).</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <section style=" padding-top: 6.5rem">
                    <div class="b-title">
                        <h3>Условия обучения</h3>
                    </div>

                    <div class="row small-up-1 medium-up-2 large-up-3">
                        <div class="column">
                            <div class="card-box">
                                <div class="card-box__top">
                                    <div class="card-box__title">
                                        <span><a href="#">Дневные курсы</a></span>
                                    </div>
                                    <div class="card-box__img">
                                        <span class="circle">
                                            <img src="dist/images/card-box/icon-1.png" alt="">
                                        </span>
                                    </div>
                                </div>
                                <div class="b-editor card-box__desc">
                                    <p>Время проведения: с 10.00 до 17.00.
                                        В стоимость дневных курсов входят: учебные занятия, методические материалы,
                                        питание.</p>
                                </div>
                            </div>
                        </div>
                        <div class="column">
                            <div class="card-box">
                                <div class="card-box__top">
                                    <div class="card-box__title">
                                        <span><a href="#">Вечерние курсы</a></span>
                                    </div>
                                    <div class="card-box__img">
                                        <span class="circle">
                                            <img src="dist/images/card-box/icon-2.png" alt="">
                                        </span>
                                    </div>
                                </div>
                                <div class="b-editor card-box__desc">
                                    <p>Время проведения: с 18.30 до 21.30.
                                        В стоимость вечерних курсов входят: учебные занятия, методические материалы,
                                        кофе-брейки.</p>
                                </div>
                            </div>
                        </div>
                        <div class="column">
                            <div class="card-box">
                                <div class="card-box__top">
                                    <div class="card-box__title">
                                        <span><a href="#">Удаленное обучение</a></span>
                                    </div>
                                    <div class="card-box__img">
                                        <span class="circle">
                                            <img src="dist/images/card-box/icon-3.png" alt="">
                                        </span>
                                    </div>
                                </div>
                                <div class="b-editor card-box__desc">
                                    <p>Пара слов про удаленное обучение. Пара слов про удаленное обучение. Пара слов про
                                        удаленное обучение. Пара слов про удаленное обучение. </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div style="padding-top: 6rem" class="row">
                        <div class="column small-12 large-4">
                            <div class="classrooms">
                                <div class="b-editor">
                                    <p><strong>
                                            Учебные классы
                                        </strong></p>
                                    <p>
                                        Все занятия в УЦ Softline проводятся в комфортабельных классах по схеме 1 и cхеме 2,
                                        оснащенных по последним технологиям. Место проведения занятий Вы можете узнать у
                                        менеджеров Учебного центра Softline.
                                    </p>
                                </div>


                                <a href="#" class="classrooms__button button success">Больше фото</a>
                            </div>

                        </div>
                        <div class="column small-12 large-8">
                            <div class="photo-card">
                                <a class="photo-card__item" href="#"><img src="content/about-edu/edu-img-1.jpg" alt=""></a>

                                <a class="photo-card__item" href="#"><img src="content/about-edu/edu-img-2.jpg" alt=""></a>

                                <a class="photo-card__item" href="#"><img src="content/about-edu/edu-img-3.jpg" alt=""></a>

                                <a class="photo-card__item" href="#"><img src="content/about-edu/edu-img-4.jpg" alt=""></a>
                            </div>
                        </div>

                        <div class="column small-12">
                            <div style="padding-top: 4rem" class="b-editor">
                                <hr>
                                <p><strong>Индивидуальное обучение</strong></p>
                                <p>Вы можете прослушать любой курс в порядке индивидуального обучения. Стоимость
                                    обучения, даты и программы курсов в этом случае предварительно согласовываются с
                                    менеджерами Учебного центра.</p>
                                <hr>
                                <p><strong>Обучение на английском языке</strong></p>
                                <p>Курсы на английском языке организуются вне расписания и формируются в рамках
                                    индивидуального обучения или корпоративного обучения.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="column small-12 large-offset-1 large-3">
                <aside>
                    <div class="aside-widget">
                        <div class="aside-widget__title">О центре</div>
                        <div class="aside-widget__content">
                            <ul class="aside-menu">


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/mission">

                                        <span class="b-link__text">Миссия учебного центра</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/training-conditions">

                                        <span class="b-link__text">Условия обучения</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class=""
                                       href="http://edu.softline.ru/about/svedeniya-ob-obrazovatelnoy-organizatsii">

                                        <span class="b-link__text">Сведения об образовательной организации</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/accreditation">

                                        <span class="b-link__text">Наши лицензии</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/status">

                                        <span class="b-link__text">Наши статусы</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-klientov">

                                        <span class="b-link__text">Отзывы клиентов</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-partnerov">

                                        <span class="b-link__text">Отзывы вендоров</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/razrabotka-kursov">

                                        <span class="b-link__text">Дистанционные курсы как способ передачи корпоративных знаний</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/delivery-info">

                                        <span class="b-link__text">FAQ для клиентов учебного центра Softline</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/requisites">

                                        <span class="b-link__text">Реквизиты компании</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/tech-support">

                                        <span class="b-link__text">Базовая бесплатная техподдержка</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Контакты</span>
                                    </a>

                                </li>

                            </ul>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__title">Будьте в курсе</div>
                        <div class="aside-widget__content">
                            <p>Подпишитесь на рассылку новостей Softline</p>
                            <form action="#">
                                <label for="">
                                    <input type="text" placeholder="Ваш e-mail">
                                </label>
                                <label for="">
                                    <input type="text" placeholder="Ваше имя">
                                </label>
                                <button class="button small expanded">Подписаться</button>
                            </form>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__content">
                            <a href="#">
                                <img src="content/banners/kasp.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>
    <section class="subscribe-banner">
        <div class="subscribe-banner__wrapper">
            <div class="row">
                <div class="column small-12 large-9 large-offset-3">
                    <div class="subscribe-banner__title">
                        <span>Будьте в курсе</span>
                    </div>
                    <form class="subscribe-banner__form">
                        <div class="subscribe-banner__input subscribe-banner__input--name">
                            <!--доп модификатор: error-->
                            <input type="text" placeholder="Ваше имя">
                        </div>
                        <div class="subscribe-banner__input subscribe-banner__input--email">
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="subscribe-banner__button">
                            <button class="button">Подписаться</button>
                        </div>
                    </form>
                    <div class="subscribe-banner__desc">
                        <span>Подпишитесь на информацию о новинках, скидках и акциях. <br> Уже более 36 000 подписчиков!</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        $('.select2').select2();
        $('.select2-selection__arrow').html('<svg class="icon arrow"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#images--svg--arrow"></use></svg>');

        var $offers = $('.offer-loop');

        $('.change-offer').on('change', function (e) {
            if (this.value == 'all') {
                $offers.stop().show()
            } else {
                $offers.filter('.' + this.value).stop().show();
                $offers.not('.' + this.value).stop().hide();
            }
        });
    })
</script>
</body>
</html>

