<? include 'partials/header.php'; ?>

<main>


    <section class="course-header course-header--gradient-bg">
        <div class="row">
            <div class="column small-12">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Учебный центр</a></li>
                    <li><a href="#">Вендоры</a></li>
                    <li><a href="#">Microsoft Corporation</a></li>
                </ul>
            </div>
        </div>

        <div class="slider-top">

            <div class="slider-top__carousel owl-carousel">
                <div class="slider-top__item">
                    <div class="row small-collapse medium-uncollapse">
                        <div class="column small-12">
                            <div class="course-header__wrapper">
                                <div class="course-header__content">
                                    <div class="course-header__title no-padding">
                                        <h1>Новое предложение – <br>
                                            Microsoft Exam Replay </h1>
                                    </div>
                                    <div class="b-editor course-header__desc">
                                        <p>
                                            Exam Replay (экзамен + пересдача экзамена) и Exam Replay with <br
                                                    class="show-for-large">
                                            Practice Test (экзамен + пересдача экзамена + практический тест)
                                        </p>
                                    </div>
                                    <div class="course-header__details show-for-medium">
                                        <a href="#">Подробнее</a>
                                    </div>
                                    <div class="course-header__download-price show-for-medium">
                                        <a href="#">
                                            Скачать прайс-лист
                                            на все курсы
                                        </a>
                                    </div>
                                </div>
                                <div class="course-header__picture">
                                    <img src="dist/images/course-header/image-1.png" alt="">
                                </div>
                                <div class="course-header__search show-for-medium">
                                    <form action="">
                                        <label><input placeholder="Поиск по сайту" type="text">
                                            <button>найти</button>
                                        </label>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="slider-top__item">
                    <div class="row small-collapse medium-uncollapse">
                        <div class="column small-12">
                            <div class="course-header__wrapper">
                                <div class="course-header__content">
                                    <div class="course-header__title no-padding">
                                        <h1>Новое предложение – <br>
                                            Microsoft Exam Replay </h1>
                                    </div>
                                    <div class="b-editor course-header__desc">
                                        <p>
                                            Exam Replay (экзамен + пересдача экзамена) и Exam Replay with <br
                                                    class="show-for-large">
                                            Practice Test (экзамен + пересдача экзамена + практический тест)
                                        </p>
                                    </div>
                                    <div class="course-header__details show-for-medium">
                                        <a href="#">Подробнее</a>
                                    </div>
                                    <div class="course-header__download-price show-for-medium">
                                        <a href="#">
                                            Скачать прайс-лист
                                            на все курсы
                                        </a>
                                    </div>
                                </div>
                                <div class="course-header__picture">
                                    <img src="dist/images/course-header/image-1.png" alt="">
                                </div>
                                <div class="course-header__search show-for-medium">
                                    <form action="">
                                        <label><input placeholder="Поиск по сайту" type="text">
                                            <button>найти</button>
                                        </label>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="slider-top__item">
                    <div class="row small-collapse medium-uncollapse">
                        <div class="column small-12">
                            <div class="course-header__wrapper">
                                <div class="course-header__content">
                                    <div class="course-header__title no-padding">
                                        <h1>Новое предложение – <br>
                                            Microsoft Exam Replay </h1>
                                    </div>
                                    <div class="b-editor course-header__desc">
                                        <p>
                                            Exam Replay (экзамен + пересдача экзамена) и Exam Replay with <br
                                                    class="show-for-large">
                                            Practice Test (экзамен + пересдача экзамена + практический тест)
                                        </p>
                                    </div>
                                    <div class="course-header__details show-for-medium">
                                        <a href="#">Подробнее</a>
                                    </div>
                                    <div class="course-header__download-price show-for-medium">
                                        <a href="#">
                                            Скачать прайс-лист
                                            на все курсы
                                        </a>
                                    </div>
                                </div>
                                <div class="course-header__picture">
                                    <img src="dist/images/course-header/image-1.png" alt="">
                                </div>
                                <div class="course-header__search show-for-medium">
                                    <form action="">
                                        <label><input placeholder="Поиск по сайту" type="text">
                                            <button>найти</button>
                                        </label>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="slider-top__nav position-absolute">
                <div class="slider-top__dots"></div>
            </div>
        </div>

    </section>

    <section>
        <div class="row">
            <div class="column small-12 show-for-large large-offset-1 large-3">
                <aside>

                    <div class="aside-widget">
                        <div class="aside-widget__content">
                            <div class="filter expanded">
                                <div class="filter__title">
                                    <h3>Подбор курса:</h3>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Направления:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все направления</option>
                                            <option value="">Все направления</option>
                                            <option value="">Все направления</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Cпециальность:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все специальности</option>
                                            <option value="">Все специальности</option>
                                            <option value="">Все специальности</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Вендор:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все вендоры</option>
                                            <option value="">Все вендоры</option>
                                            <option value="">Все вендоры</option>
                                        </select>
                                    </label>
                                    <label class="b-radiobox">
                                        <input name="example-1" type="radio">
                                        <span>Очно (в аудитории)</span>
                                    </label>
                                    <label class="b-radiobox">
                                        <input name="example-1" type="radio">
                                        <span>Дистанционно</span>
                                    </label>

                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Место проведения:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Москва</option>
                                            <option value="">Москва</option>
                                            <option value="">Москва</option>
                                        </select>
                                    </label>
                                </div>

                            </div>
                            <button class="button small expanded">Подобрать курс</button>
                        </div>
                    </div>
                </aside>
            </div>

            <div class="column small-12 large-8">
                <div class="found-list">
                    <div class="found-list__top">
                        <div class="found-list__title">
                            <h3>
                                Найдено курсов: <span>3</span>
                            </h3>
                        </div>

                        <button class="found-list__close">
                            <span class="show-for-large">Сбросит фильтр</span>
                            <svg class="icon close">
                                <use xlink:href="#images--svg--close"></use>
                            </svg>
                        </button>

                    </div>
                    <div class="found-list__container">
                        <div class="product-card">
                            <div class="product-card__top">
                                <div class="product-card__image">
                                    <img src="/dist/images/product-card/logo-1.png" alt="">
                                </div>
                                <div class="product-card__text">
                                    <div class="product-card__title">
                                        <a href="#">Администрирование АПКШ "Континент" Версия 3.7. Расширенный курс</a>
                                    </div>
                                    <div class="product-card__desc"><p>АПКШ-2 Москва, Код Безопасноти</p></div>
                                </div>

                                <div class="product-card__price">
                                    <span>17 375 руб.</span>
                                    <span class="old-price">19 305 руб.</span>
                                    <span class="discount"><span class="show-for-medium">скидка</span> 10%</span>
                                </div>
                            </div>
                            <div class="product-card__bottom">
                                <div class="product-card__icons">
                                    <span><img src="/dist/images/product-card/icon-1.png" alt=""></span>
                                    <span><img src="/dist/images/product-card/icon-2.png" alt=""></span>
                                </div>
                                <div class="product-card__note date"><span>25 ноября — 3 декабря 2017</span></div>
                                <div class="product-card__note time"><span>16 ак. часов</span></div>
                                <div class="product-card__note place"><span>Москва</span></div>
                                <div class="product-card__note type"><span>Дистанционно</span></div>
                                <a class="product-card__button">
                                     <span class="product-btn-price">
                                        <span>17 375 руб.</span>
                                        <span class="old-price">19 305 руб.</span>
                                    </span>
                                    <span class="product-btn-icon">
                                        <span class="product-btn-discount">10%</span>
                                    </span>
                                    <span class="product-btn-text">В корзину</span>
                                </a>
                            </div>
                        </div>
                        <div class="product-card">
                            <div class="product-card__top">
                                <div class="product-card__image">
                                    <img src="/dist/images/product-card/logo-1.png" alt="">
                                </div>
                                <div class="product-card__text">
                                    <div class="product-card__title">
                                        <a href="#">Администрирование АПКШ "Континент" Версия 3.7. Расширенный курс</a>
                                    </div>
                                    <div class="product-card__desc"><p>АПКШ-2 Москва, Код Безопасноти</p></div>
                                </div>

                                <div class="product-card__price">
                                    <span>17 375 руб.</span>
                                    <span class="old-price">19 305 руб.</span>
                                    <span class="discount"><span class="show-for-medium">скидка</span> 10%</span>
                                </div>
                            </div>
                            <div class="product-card__bottom">
                                <div class="product-card__icons">
                                    <span><img src="/dist/images/product-card/icon-1.png" alt=""></span>
                                    <span><img src="/dist/images/product-card/icon-2.png" alt=""></span>
                                </div>
                                <div class="product-card__note date"><span>25 ноября — 3 декабря 2017</span></div>
                                <div class="product-card__note time"><span>16 ак. часов</span></div>
                                <div class="product-card__note place"><span>Москва</span></div>
                                <div class="product-card__note type"><span>Дистанционно</span></div>
                                <a class="product-card__button">
                                     <span class="product-btn-price">
                                        <span>17 375 руб.</span>
                                        <span class="old-price">19 305 руб.</span>
                                    </span>
                                    <span class="product-btn-icon">
                                        <span class="product-btn-discount">10%</span>
                                    </span>
                                    <span class="product-btn-text">В корзину</span>
                                </a>
                            </div>
                        </div>
                        <div class="product-card">
                            <div class="product-card__top">
                                <div class="product-card__image">
                                    <img src="/dist/images/product-card/logo-1.png" alt="">
                                </div>
                                <div class="product-card__text">
                                    <div class="product-card__title">
                                        <a href="#">Администрирование АПКШ "Континент" Версия 3.7. Расширенный курс</a>
                                    </div>
                                    <div class="product-card__desc"><p>АПКШ-2 Москва, Код Безопасноти</p></div>
                                </div>

                                <div class="product-card__price">
                                    <span>17 375 руб.</span>
                                    <span class="old-price">19 305 руб.</span>
                                    <span class="discount"><span class="show-for-medium">скидка</span> 10%</span>
                                </div>
                            </div>
                            <div class="product-card__bottom">
                                <div class="product-card__icons">
                                    <span><img src="/dist/images/product-card/icon-1.png" alt=""></span>
                                    <span><img src="/dist/images/product-card/icon-2.png" alt=""></span>
                                </div>
                                <div class="product-card__note date"><span>25 ноября — 3 декабря 2017</span></div>
                                <div class="product-card__note time"><span>16 ак. часов</span></div>
                                <div class="product-card__note place"><span>Москва</span></div>
                                <div class="product-card__note type"><span>Дистанционно</span></div>
                                <a class="product-card__button">
                                     <span class="product-btn-price">
                                        <span>17 375 руб.</span>
                                        <span class="old-price">19 305 руб.</span>
                                    </span>
                                    <span class="product-btn-icon">
                                        <span class="product-btn-discount">10%</span>
                                    </span>
                                    <span class="product-btn-text">В корзину</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="btn-next-page">
        <a href="#">Загрузить все курсы</a>
    </section>

    <section class="leave-order-banner">
        <div class="leave-order-banner__title">
            <span>НЕ НАШЛИ ПОДХОДЯЩЕГО КУРСА?</span>
        </div>
        <div class="leave-order-banner__desc">
            <p>Оставить заявку на обучение для вашей организации</p>
        </div>
        <div class="leave-order-banner__button">
            <a href="#">Свяжитесь с нами</a>
        </div>
    </section>

    <section style="padding-top: 9rem">
        <div class="row">
            <div class="column small-12">
                <div class="tab-box">
                    <ul class="tabs tabs--small-inline" data-tabs id="example-tabs">
                        <li class="tabs-title is-active"><a href="#panel1" aria-selected="true"><span class="show-for-large">Курсы</span> по
                                направлениям</a></li>
                        <li class="tabs-title"><a data-tabs-target="panel2" href="#panel2"><span class="show-for-large">Курсы</span> по вендерам</a></li>
                    </ul>

                    <div class="tabs-content" data-tabs-content="example-tabs">
                        <div class="tabs-panel is-active" id="panel1">
                            <div class="slider-mobile">
                                <div class="solutions small-padding">
                                    <div class="row small-up-1 medium-up-2 large-up-3 slider-mobile__carousel">
                                        <div class="column">
                                            <div class="solutions__card">
                                                <div style="background-image: url(dist/images/solutions/solutions__card/solutions-1.jpg)"
                                                     class="solutions__card-photo">
                                                    <div class="solutions__card-icon arrow-icon"></div>
                                                </div>
                                                <div class="solutions__card-wrapper">
                                                    <div class="solutions__card-title">
                                                        <span>Виртуальная инфраструктура (IaaS)</span>
                                                    </div>
                                                    <div class="solutions__card-desc">
                                                        <ul>
                                                            <li><a href="#">Softline Cloud (IaaS/VMware)</a></li>

                                                            <li><a href="#">Softline Cloud (IaaS/Hyper-V)</a></li>

                                                            <li><a href="#">Microsoft Azure</a></li>

                                                            <li><a href="http://aws.softline.ru/">Amazon</a></li>

                                                            <li><a href="http://promo.softline.ru/ibm-cloud">IBM Cloud</a>
                                                            </li>

                                                            <li><a href="#">Google Cloud Platform</a></li>

                                                        </ul>
                                                    </div>
                                                    <a href="#" class="solutions__card-button">Подробнее</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column">
                                            <div class="solutions__card">
                                                <div style="background-image: url(dist/images/solutions/solutions__card/solutions-2.jpg)"
                                                     class="solutions__card-photo">
                                                    <div class="solutions__card-icon virtual-icon"></div>
                                                </div>
                                                <div class="solutions__card-wrapper">
                                                    <div class="solutions__card-title">
                                                        <span> Облачные сервисы</span>
                                                    </div>
                                                    <div class="solutions__card-desc">
                                                        <ul>
                                                            <li><a href="#">Защищенное облако (152-ФЗ)</a></li>

                                                            <li><a href="#">Частное облако</a></li>

                                                            <li><a href="#">Резервное копирование в облако</a></li>

                                                            <li><a href="#">Аварийное восстановление в облаке</a></li>

                                                            <li><a href="#">SAP-хостинг</a></li>

                                                        </ul>
                                                    </div>
                                                    <a href="#" class="solutions__card-button">Подробнее</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column">
                                            <div class="solutions__card">
                                                <div style="background-image: url(dist/images/solutions/solutions__card/solutions-3.jpg)"
                                                     class="solutions__card-photo">
                                                    <div class="solutions__card-icon cloud-icon"></div>
                                                </div>
                                                <div class="solutions__card-wrapper">
                                                    <div class="solutions__card-title">
                                                        <span>Решения для бизнеса</span>
                                                    </div>
                                                    <div class="solutions__card-desc">
                                                        <ul>

                                                            <li><a href="#">VDI</a></li>

                                                            <li><a href="#"> Microsoft Office 365</a></li>

                                                            <li><a href="#">G Suite</a></li>

                                                            <li><a href="#">Виртуальный офис</a></li>

                                                        </ul>
                                                    </div>
                                                    <a href="#" class="solutions__card-button">Подробнее</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column">
                                            <div class="solutions__card">
                                                <div style="background-image: url(dist/images/solutions/solutions__card/solutions-4.jpg)"
                                                     class="solutions__card-photo">
                                                    <div class="solutions__card-icon server-icon"></div>
                                                </div>
                                                <div class="solutions__card-wrapper">
                                                    <div class="solutions__card-title">
                                                        <span>Аппаратные сервисы</span>
                                                    </div>
                                                    <div class="solutions__card-desc">
                                                        <ul>
                                                            <li><a href="#">Размещение оборудования (Colocation)</a></li>

                                                            <li><a href="#">Аренда серверного оборудования</a></li>
                                                        </ul>
                                                    </div>
                                                    <a href="#" class="solutions__card-button">Подробнее</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tabs-panel" id="panel2">

                        </div>
                    </div>
                </div>
            </div>
    </section>

    <section>
        <div class="row">
            <div class="column small-12 large-8">
                <div class="b-title">
                    <h3>Акции и спецпредложения</h3>
                </div>
                <div class="slider-mobile">
                    <div class="slider-mobile__carousel">
                        <div>
                            <div class="offer-loop-mobile hide-for-medium">
                                <div class="offer-loop-mobile__top">
                                    <div class="offer-loop__image">
                                        <a href="#">
                                            <img src="content/offers/item.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="offer-loop-mobile__duration">
                                    <svg class="icon calendar">
                                        <use xlink:href="#images--svg--calendar"></use>
                                    </svg>
                                    <span>02.08.2016  — 31.05.2017</span>
                                </div>
                                <div class="offer-loop-mobile__content">
                                    <h5 class="offer-loop__title"><a href="#">ESET: 50% скидка для учебных заведений и
                                            библиотек</a></h5>
                                    <div class="offer-loop__description">
                                        <p>Компания ESET предоставляет возможность приобрести все продукты ESET NOD32
                                            для
                                            учебных заведений и библиотек со скидкой 50%.</p>
                                    </div>
                                </div>
                                <svg class="icon arrow">
                                    <use xlink:href="#images--svg--arrow"></use>
                                </svg>
                            </div>
                            <div class="offer-loop show-for-medium">
                                <div class="flex-container">
                                    <div class="offer-loop__image">
                                        <a href="#">
                                            <img src="content/offers/item.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="offer-loop__content">
                                        <h5 class="offer-loop__title"><a href="#">ESET: 50% скидка для учебных заведений
                                                и
                                                библиотек</a></h5>
                                        <div class="offer-loop__description">
                                            <p>Компания ESET предоставляет возможность приобрести все продукты ESET
                                                NOD32 для
                                                учебных заведений и библиотек со скидкой 50%.</p>
                                        </div>
                                    </div>
                                </div>
                                <a class="offer-loop__duration" href="#">
                                    <svg class="icon calendar">
                                        <use xlink:href="#images--svg--calendar"></use>
                                    </svg>
                                    Акция действует до: 30 сентября
                                    <svg class="icon arrow">
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="offer-loop-mobile hide-for-medium">
                                <div class="offer-loop-mobile__top">
                                    <div class="offer-loop__image">
                                        <a href="#">
                                            <img src="content/offers/item.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="offer-loop-mobile__duration">
                                    <svg class="icon calendar">
                                        <use xlink:href="#images--svg--calendar"></use>
                                    </svg>
                                    <span>02.08.2016  — 31.05.2017</span>
                                </div>
                                <div class="offer-loop-mobile__content">
                                    <h5 class="offer-loop__title"><a href="#">ESET: 50% скидка для учебных заведений и
                                            библиотек</a></h5>
                                    <div class="offer-loop__description">
                                        <p>Компания ESET предоставляет возможность приобрести все продукты ESET NOD32
                                            для
                                            учебных заведений и библиотек со скидкой 50%.</p>
                                    </div>
                                </div>
                                <svg class="icon arrow">
                                    <use xlink:href="#images--svg--arrow"></use>
                                </svg>
                            </div>
                            <div class="offer-loop show-for-medium">
                                <div class="flex-container">
                                    <div class="offer-loop__image">
                                        <a href="#">
                                            <img src="content/offers/item.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="offer-loop__content">
                                        <h5 class="offer-loop__title"><a href="#">ESET: 50% скидка для учебных заведений
                                                и
                                                библиотек</a></h5>
                                        <div class="offer-loop__description">
                                            <p>Компания ESET предоставляет возможность приобрести все продукты ESET
                                                NOD32 для
                                                учебных заведений и библиотек со скидкой 50%.</p>
                                        </div>
                                    </div>
                                </div>
                                <a class="offer-loop__duration" href="#">
                                    <svg class="icon calendar">
                                        <use xlink:href="#images--svg--calendar"></use>
                                    </svg>
                                    Акция действует до: 30 сентября
                                    <svg class="icon arrow">
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="offer-loop-mobile hide-for-medium">
                                <div class="offer-loop-mobile__top">
                                    <div class="offer-loop__image">
                                        <a href="#">
                                            <img src="content/offers/item.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="offer-loop-mobile__duration">
                                    <svg class="icon calendar">
                                        <use xlink:href="#images--svg--calendar"></use>
                                    </svg>
                                    <span>02.08.2016  — 31.05.2017</span>
                                </div>
                                <div class="offer-loop-mobile__content">
                                    <h5 class="offer-loop__title"><a href="#">ESET: 50% скидка для учебных заведений и
                                            библиотек</a></h5>
                                    <div class="offer-loop__description">
                                        <p>Компания ESET предоставляет возможность приобрести все продукты ESET NOD32
                                            для
                                            учебных заведений и библиотек со скидкой 50%.</p>
                                    </div>
                                </div>
                                <svg class="icon arrow">
                                    <use xlink:href="#images--svg--arrow"></use>
                                </svg>
                            </div>
                            <div class="offer-loop show-for-medium">
                                <div class="flex-container">
                                    <div class="offer-loop__image">
                                        <a href="#">
                                            <img src="content/offers/item.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="offer-loop__content">
                                        <h5 class="offer-loop__title"><a href="#">ESET: 50% скидка для учебных заведений
                                                и
                                                библиотек</a></h5>
                                        <div class="offer-loop__description">
                                            <p>Компания ESET предоставляет возможность приобрести все продукты ESET
                                                NOD32 для
                                                учебных заведений и библиотек со скидкой 50%.</p>
                                        </div>
                                    </div>
                                </div>
                                <a class="offer-loop__duration" href="#">
                                    <svg class="icon calendar">
                                        <use xlink:href="#images--svg--calendar"></use>
                                    </svg>
                                    Акция действует до: 30 сентября
                                    <svg class="icon arrow">
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column small-12 large-3 large-offset-1">
                <div class="b-title">
                    <h3>Новости</h3>
                </div>
                <div class="b-news">
                    <div class="b-news__top">
                        <span>24.05.2017</span>
                        <span><a href="#">Конференции, выставки</a></span>
                    </div>
                    <a href="#">Компания Softline примет участие в Неделе Российского Ритейла</a>
                </div>
                <div class="b-news">
                    <div class="b-news__top">
                        <span>24.05.2017</span>
                        <span><a href="#">Конференции, выставки</a></span>
                    </div>
                    <a href="#">Softline проанализировала IТ-инфраструктуру компании ManpowerGroup Russia & CIS</a>
                </div>
                <div class="b-news">
                    <div class="b-news__top">
                        <span>24.05.2017</span>
                        <span><a href="#">Конференции, выставки</a></span>
                    </div>
                    <a href="#">Компания Softline примет участие в Неделе Российского Ритейла</a>
                </div>
                <div class="b-news">
                    <div class="b-news__top">
                        <span>24.05.2017</span>
                        <span><a href="#">Конференции, выставки</a></span>
                    </div>
                    <a href="#">Softline проанализировала IТ-инфраструктуру компании ManpowerGroup Russia & CIS</a>
                </div>
            </div>
        </div>
    </section>

    <section style="padding-top: 30px" class="btn-next-page">
        <a href="#">Все акции и спецпредложения</a>
    </section>

    <section class="customers">
        <div class="row collapse">
            <div class="column small-12">
                <div class="text-center">
                    <div class="h1 customers__wrap">
                        Нам доверяют
                        <br class="show-for-large"> наши заказчики и партнеры
                    </div>
                </div>
            </div>
            <div class="column small-12 large-6">
                <div class="customers__editor">
                    <p>
                        <strong>
                            Открытие при университете Центра прототипирования и аддитивных технологий позволит нам
                            повысить
                            конкурентоспособность на рынке образовательных услуг, создавая все условия, необходимые для
                            подготовки высококвалифицированных технических специалистов.
                        </strong>
                    </p>
                    <hr style="margin-bottom: 40px; margin-top: 49px">
                    <p>
                        <span>
                            Александр Шендриков, начальник Управления информатизации Сибирского государственного
                        индустриального университета
                        </span>
                    </p>
                </div>
            </div>
            <div class="column small-12 large-6">
                <div class="customers__cards">
                    <div class="customers__item">
                        <a href="#"><img src="/dist/images/customers/lukoil.png" alt=""></a>
                    </div>
                    <div class="customers__item">
                        <a href="#"><img src="/dist/images/customers/coca.png" alt=""></a>
                    </div>
                    <div class="customers__item">
                        <a href="#"><img src="/dist/images/customers/siemens.png" alt=""></a>
                    </div>
                    <div class="customers__item">
                        <a href="#"><img src="/dist/images/customers/samsung.png" alt=""></a>
                    </div>
                    <div class="customers__item">
                        <a href="#"><img src="/dist/images/customers/unb.png" alt=""></a>
                    </div>
                    <div class="customers__item">
                        <img src="/dist/images/customers/bat.png" alt=""></a>
                    </div>
                    <div class="customers__item">
                        <a href="#"><img src="/dist/images/customers/dhl.png" alt=""></a>
                    </div>
                    <div class="customers__item">
                        <a href="#"><img src="/dist/images/customers/volvo.png" alt=""></a>
                    </div>
                    <div class="customers__item">
                        <a href="#"><img src="/dist/images/customers/nec.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="areas">
        <div class="row">
            <div class="column small-12">
                <div class="text-center  h1">
                   <span>Предлагаем программы <br class="show-for-large">
                        для специалистов по направлениям:
                   </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column small-12">
                <div class="areas__wrap">
                    <div class="row small-up-1 medium-up-3 large-up-6 medium-collapse">
                        <div class="column">
                            <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/finance.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/finance.png)"
                       class="areas__icon"></span>
            </span>
                                <span class="areas__title">Финансы</span>
                            </a>
                        </div>
                        <div class="column">
                            <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/retail.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/retail.png)"
                       class="areas__icon"></span>
            </span>
                                <span class="areas__title">Ритейл</span>
                            </a>
                        </div>
                        <div class="column">
                            <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/tek.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/tek.png)" class="areas__icon"></span>
            </span>
                                <span class="areas__title">ТЭК</span>
                            </a>
                        </div>
                        <div class="column">
                            <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/industry.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/industry.png)"
                       class="areas__icon"></span>
            </span>
                                <span class="areas__title">Промышленность</span>
                            </a>
                        </div>
                        <div class="column">
                            <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/sector.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/sector.png)"
                       class="areas__icon"></span>
            </span>
                                <span class="areas__title">Гос.сектор</span>
                            </a>
                        </div>
                        <div class="column">
                            <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/tele.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/tele.png)" class="areas__icon"></span>
            </span>
                                <span class="areas__title">Телеком</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="subscribe-banner">
        <div class="subscribe-banner__wrapper">
            <div class="row">
                <div class="column small-12 large-9 large-offset-3">
                    <div class="subscribe-banner__title">
                        <span>Будьте в курсе</span>
                    </div>
                    <form class="subscribe-banner__form">
                        <div class="subscribe-banner__input subscribe-banner__input--name">
                            <!--доп модификатор: error-->
                            <input type="text" placeholder="Ваше имя">
                        </div>
                        <div class="subscribe-banner__input subscribe-banner__input--email">
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="subscribe-banner__button">
                            <button class="button">Подписаться</button>
                        </div>
                    </form>
                    <div class="subscribe-banner__desc">
                        <span>Подпишитесь на информацию о новинках, скидках и акциях. <br> Уже более 36 000 подписчиков!</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        $('.b-select__container').select2();
    });

    $('.slider-top__carousel').owlCarousel({
        nav: true,
        items: 1,
        navText: ['', ''],
        navContainer: '.slider-top__nav',
        dotsContainer: '.slider-top__dots'
    });

    $(document).foundation();

</script>
<script>
    var initedSlider = false;

    function SliderMobile() {
        if (window.innerWidth > 425 && initedSlider) {
            $('.slider-mobile__carousel').owlCarousel('destroy');
            $('.slider-mobile__carousel').removeClass('owl-carousel');
            $('.slider-mobile__carousel').find('.owl-stage-outer').children().unwrap();
            $('.slider-mobile__carousel').removeData();
            initedSlider = false;
        }
        if (window.innerWidth <= 425 && !initedSlider) {
            $('.slider-mobile__carousel').addClass('owl-carousel');
            $('.slider-mobile__carousel').owlCarousel({
                nav: true,
                items: 1,
                dots: true,
                navText: ['<svg class="slider-mobile__arrow next"><use xlink:href="#images--svg--arrow"></use> </svg>', '<svg class="slider-mobile__arrow prev"><use xlink:href="#images--svg--arrow"></use></svg>'],
            });

            initedSlider = true;
        }
    }

    $(window).on('resize', function () {
        SliderMobile();
    });

    SliderMobile();

</script>
</body>
</html>
