<? include 'partials/header.php'; ?>
<main>
    <section class="course-modules-section">
        <div class="row">
            <div class="column small-12">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Учебный центр</a></li>
                    <li><a href="#">Вендоры</a></li>
                    <li><a href="#">Microsoft Corporation</a></li>
                </ul>
                <hr>
            </div>
            <div class="column small-12">

                <div class="course-wrapper">
                    <div class="course-content">
                        <div class="xblock xblock-student_view xblock-student_view-sequential xmodule_display xmodule_SequenceModule xblock-initialized" data-runtime-class="LmsRuntime" data-init="XBlockToXModuleShim" data-block-type="sequential" data-request-token="3f4881b8d0e311e7b4890242ac120003" data-runtime-version="1" data-usage-id="block-v1:Microsoft+DEV216x+course+type@sequential+block@fc29f78e-1c64-4fb9-9436-b255d400f3fc" data-type="Sequence" data-course-id="course-v1:Microsoft+DEV216x+course">

                            <div id="sequence_fc29f78e-1c64-4fb9-9436-b255d400f3fc" class="sequence" data-id="block-v1:Microsoft+DEV216x+course+type@sequential+block@fc29f78e-1c64-4fb9-9436-b255d400f3fc" data-position="1" data-ajax-url="/courses/course-v1:Microsoft+DEV216x+course/xblock/block-v1:Microsoft+DEV216x+course+type@sequential+block@fc29f78e-1c64-4fb9-9436-b255d400f3fc/handler/xmodule_handler" data-next-url="/courses/course-v1:Microsoft+DEV216x+course/courseware/083e1ae3-93c7-1f72-6306-1765a78799e4/22249251-86aa-59e6-ace8-cb4a45a33a89/?child=first" data-prev-url="/courses/course-v1:Microsoft+DEV216x+course/courseware/083e1ae3-93c7-1f72-6306-1765a78799e4/9858b654-cac7-48aa-978b-232e1bfe9cd0/?child=last">
                                <div class="sequence-nav">
                                    <button class="sequence-nav-button button-previous" aria-hidden="true">
                                        <span class="icon fa fa-chevron-prev" aria-hidden="true"></span>
                                        <span>Предыдущее</span>
                                    </button>
                                    <nav class="sequence-list-wrapper" aria-label="Sequence">
                                        <ol id="sequence-list" role="tablist">
                                            <li>
                                                <button class="seq_other nav-item tab active" role="tab" tabindex="0" aria-selected="true" aria-expanded="true" aria-controls="seq_content" data-index="0" data-id="block-v1:Microsoft+DEV216x+course+type@vertical+block@da19f3d3-fbc5-22ce-4658-82c419a9a3dd" data-element="1" data-page-title="Getting Around This Course" data-path="Welcome > About this Course > Getting Around This Course" id="tab_0">
                                            <span class="icon fa seq_other" aria-hidden="true">
                                                <svg class="book">
                                                    <use xlink:href="#images--svg--book"></use>
                                                </svg>
                                            </span>
                                                    <span class="fa fa-fw fa-bookmark bookmark-icon is-hidden" aria-hidden="true"></span>
                                                    <div class="sequence-tooltip sr"><span class="sr">other&nbsp;</span>Getting
                                                        Around This
                                                        Course<span class="sr bookmark-icon-sr">&nbsp;</span>
                                                    </div>
                                                </button>
                                            </li>
                                            <li>
                                                <button class="seq_video nav-item tab visited" role="tab" tabindex="-1" aria-selected="false" aria-expanded="false" aria-controls="seq_content" data-index="1" data-id="block-v1:Microsoft+DEV216x+course+type@vertical+block@e5a08905-a05e-7d46-7261-876f39b5c9d6" data-element="2" data-page-title="Course Staff" data-path="Welcome > About this Course > Course Staff" id="tab_1">
                                            <span class="icon fa seq_video" aria-hidden="true">
                                                <svg class="film">
                                                    <use xlink:href="#images--svg--film"></use>
                                                </svg>
                                            </span>
                                                    <span class="fa fa-fw fa-bookmark bookmark-icon is-hidden" aria-hidden="true"></span>
                                                    <div class="sequence-tooltip sr"><span class="sr">video&nbsp;</span>Course
                                                        Staff<span class="sr bookmark-icon-sr">&nbsp;</span></div>
                                                </button>
                                            </li>
                                            <li>
                                                <button class="seq_other nav-item tab visited" role="tab" tabindex="-1" aria-selected="false" aria-expanded="false" aria-controls="seq_content" data-index="2" data-id="block-v1:Microsoft+DEV216x+course+type@vertical+block@72c9dc5e-4911-eb90-a580-f2abc9a39cfb" data-element="3" data-page-title="Grading" data-path="Welcome > About this Course > Grading" id="tab_2">
                                            <span class="icon fa seq_other" aria-hidden="true">
                                                <svg class="book">
                                                    <use xlink:href="#images--svg--book"></use>
                                                </svg>
                                            </span>
                                                    <span class="fa fa-fw fa-bookmark bookmark-icon is-hidden" aria-hidden="true"></span>
                                                    <div class="sequence-tooltip sr">
                                                        <span class="sr">other&nbsp;</span>Grading<span class="sr bookmark-icon-sr">&nbsp;</span>
                                                    </div>
                                                </button>
                                            </li>
                                            <li>
                                                <button class="seq_other nav-item tab visited" role="tab" tabindex="-1" aria-selected="false" aria-expanded="false" aria-controls="seq_content" data-index="3" data-id="block-v1:Microsoft+DEV216x+course+type@vertical+block@7515bcb8-4ad9-93a3-2514-463ca1350421" data-element="4" data-page-title="Prerequisites" data-path="Welcome > About this Course > Prerequisites" id="tab_3">
                                            <span class="icon fa seq_other" aria-hidden="true">
                                                <svg class="book">
                                                    <use xlink:href="#images--svg--book"></use>
                                                </svg>
                                            </span>
                                                    <span class="fa fa-fw fa-bookmark bookmark-icon is-hidden" aria-hidden="true"></span>
                                                    <div class="sequence-tooltip sr"><span class="sr">other&nbsp;</span>Prerequisites<span class="sr bookmark-icon-sr">&nbsp;</span>
                                                    </div>
                                                </button>
                                            </li>
                                            <li>
                                                <button class="seq_other nav-item tab visited" role="tab" tabindex="-1" aria-selected="false" aria-expanded="false" aria-controls="seq_content" data-index="4" data-id="block-v1:Microsoft+DEV216x+course+type@vertical+block@891b6bdb-58b1-85dc-65aa-afde6163ad07" data-element="5" data-page-title="Pre-course Survey" data-path="Welcome > About this Course > Pre-course Survey" id="tab_4">
                                            <span class="icon fa seq_other" aria-hidden="true">
                                                <svg class="book">
                                                    <use xlink:href="#images--svg--book"></use>
                                                </svg>
                                            </span>
                                                    <span class="fa fa-fw fa-bookmark bookmark-icon is-hidden" aria-hidden="true"></span>
                                                    <div class="sequence-tooltip sr"><span class="sr">other&nbsp;</span>Pre-course
                                                        Survey<span class="sr bookmark-icon-sr">&nbsp;</span></div>
                                                </button>
                                            </li>
                                        </ol>
                                    </nav>
                                    <button class="sequence-nav-button button-next" aria-hidden="true">
                                        <span>Следующее</span>
                                        <span class="icon fa fa-chevron-next" aria-hidden="true"></span>
                                    </button>
                                </div>
                                <div class="sr-is-focusable" tabindex="-1"></div>
                                <div id="seq_contents_0" aria-labelledby="tab_0" aria-hidden="true" class="seq_contents tex2jax_ignore asciimath2jax_ignore">
                                    &lt;div class="xblock xblock-student_view xblock-student_view-vertical"
                                    data-runtime-class="LmsRuntime" data-init="VerticalStudentView"
                                    data-course-id="course-v1:Microsoft+DEV216x+course"
                                    data-request-token="3f4881b8d0e311e7b4890242ac120003" data-runtime-version="1"
                                    data-usage-id="block-v1:Microsoft+DEV216x+course+type@vertical+block@da19f3d3-fbc5-22ce-4658-82c419a9a3dd"
                                    data-block-type="vertical"&gt;


                                    &lt;h2 class="hd hd-2 unit-title"&gt;Getting Around This Course&lt;/h2&gt;


                                    &lt;div class="bookmark-button-wrapper"&gt;
                                    &lt;button class="btn btn-link bookmark-button "
                                    aria-pressed="false"
                                    data-bookmark-id="htmlmak_2,block-v1:Microsoft+DEV216x+course+type@vertical+block@da19f3d3-fbc5-22ce-4658-82c419a9a3dd"
                                    data-bookmarks-api-url="/api/bookmarks/v1/bookmarks/"&gt;
                                    &lt;span class="bookmark-text"&gt;Bookmark this page&lt;/span&gt;
                                    &lt;/button&gt;
                                    &lt;/div&gt;


                                    &lt;div class="vert-mod"&gt;
                                    &lt;div class="vert vert-0"
                                    data-id="block-v1:Microsoft+DEV216x+course+type@html+block@f6114fea-f15e-9c52-e0e7-2095e1a8a716"&gt;
                                    &lt;div class="xblock xblock-student_view xblock-student_view-html xmodule_display
                                    xmodule_HtmlModule" data-runtime-class="LmsRuntime" data-init="XBlockToXModuleShim"
                                    data-block-type="html" data-request-token="3f4881b8d0e311e7b4890242ac120003"
                                    data-runtime-version="1"
                                    data-usage-id="block-v1:Microsoft+DEV216x+course+type@html+block@f6114fea-f15e-9c52-e0e7-2095e1a8a716"
                                    data-type="HTMLModule" data-course-id="course-v1:Microsoft+DEV216x+course"&gt;
                                    &lt;script type="json/xblock-args" class="xblock-json-init-args"&gt;
                                    {"xmodule-type": "HTMLModule"}
                                    &lt;/script&gt;
                                    &lt;p&gt;If you're new to edX, here is some information to get you started. Even if
                                    you've
                                    used edX for other courses, the information here will help you understand how this
                                    course is
                                    organized.&lt;/p&gt;
                                    &lt;p&gt;This course is structured into modules, sections,&nbsp;and units. Each
                                    module
                                    contains a combination of&nbsp;written documentation (reference documents)&nbsp;and
                                    lab
                                    activities that cover the&nbsp;topic areas&nbsp;for that module. In this course,
                                    there
                                    are
                                    more&nbsp;lab&nbsp;sections&nbsp;than there are&nbsp;documentation sections, so you
                                    will&nbsp;be
                                    spending more time&nbsp;working through the&nbsp;hands-on activities than reading
                                    about
                                    the
                                    technologies. Each module contains&nbsp;all of the material that you'll need to
                                    learn
                                    the
                                    included&nbsp;topics, and we've included links to additional sources of information
                                    if
                                    you
                                    are interested in digging even deeper into the technologies.&lt;/p&gt;
                                    &lt;p&gt;To navigate through the sections of a module, use the navigation bar at the
                                    top
                                    of
                                    the display window. You can use the left and right arrows to move back-and-forth
                                    through&nbsp;the
                                    units, and you can hover your mouse pointer over unit icon to see the unit title.&lt;/p&gt;
                                    &lt;p&gt;As noted above, each&nbsp;module in this course will have some combination
                                    of
                                    reading material, labs, and video content. The&nbsp;modules and the labs are&nbsp;meant
                                    to
                                    be taken in order, but you can skip around in the resource document&nbsp;sections,
                                    or
                                    even
                                    skip over a topic&nbsp;if you have already mastered the material in a particular
                                    unit.
                                    In
                                    order to&nbsp;get the most out of the lab assignments, you&nbsp;should make sure
                                    that
                                    you&nbsp;understand
                                    the material&nbsp;in the resource documents.&lt;/p&gt;
                                    &lt;h3 id="instructor-feedback"&gt;INSTRUCTOR FEEDBACK&lt;/h3&gt;
                                    &lt;p&gt;Given the nature of this MOOC-style course and the large number of students
                                    enrolled, it's not possible for instructors to provide feedback to students.
                                    However, we
                                    encourage you to use the discussion boards to seek help and feedback from your
                                    fellow
                                    students. This is a great way to help each other and enrich your learning
                                    experience.&lt;/p&gt;
                                    &lt;h3 id="other-resources"&gt;OTHER RESOURCES&lt;/h3&gt;
                                    &lt;p&gt;&lt;a href="https://www.edx.org/about/student-faq" target="_blank"&gt;edX
                                    Student
                                    FAQ&lt;/a&gt;: This FAQ answers many questions about the edX student experience.
                                    Read
                                    the
                                    questions, so if you encounter any issues during the course, you can refer back to
                                    this
                                    document for help.&lt;/p&gt;

                                    &lt;/div&gt;

                                    &lt;/div&gt;
                                    &lt;/div&gt;

                                    &lt;/div&gt;

                                </div>
                                <div id="seq_contents_1" aria-labelledby="tab_1" aria-hidden="true" class="seq_contents tex2jax_ignore asciimath2jax_ignore">
                                    &lt;div class="xblock xblock-student_view xblock-student_view-vertical"
                                    data-runtime-class="LmsRuntime" data-init="VerticalStudentView"
                                    data-course-id="course-v1:Microsoft+DEV216x+course"
                                    data-request-token="3f4881b8d0e311e7b4890242ac120003" data-runtime-version="1"
                                    data-usage-id="block-v1:Microsoft+DEV216x+course+type@vertical+block@e5a08905-a05e-7d46-7261-876f39b5c9d6"
                                    data-block-type="vertical"&gt;


                                    &lt;h2 class="hd hd-2 unit-title"&gt;Course Staff&lt;/h2&gt;


                                    &lt;div class="bookmark-button-wrapper"&gt;
                                    &lt;button class="btn btn-link bookmark-button "
                                    aria-pressed="false"
                                    data-bookmark-id="htmlmak_2,block-v1:Microsoft+DEV216x+course+type@vertical+block@e5a08905-a05e-7d46-7261-876f39b5c9d6"
                                    data-bookmarks-api-url="/api/bookmarks/v1/bookmarks/"&gt;
                                    &lt;span class="bookmark-text"&gt;Bookmark this page&lt;/span&gt;
                                    &lt;/button&gt;
                                    &lt;/div&gt;


                                    &lt;div class="vert-mod"&gt;
                                    &lt;div class="vert vert-0"
                                    data-id="block-v1:Microsoft+DEV216x+course+type@video+block@e6c494a7-68cb-9243-1790-9700c6f8bde9"&gt;
                                    &lt;div class="xblock xblock-student_view xblock-student_view-video xmodule_display
                                    xmodule_VideoModule" data-runtime-class="LmsRuntime" data-init="XBlockToXModuleShim"
                                    data-block-type="video" data-request-token="3f4881b8d0e311e7b4890242ac120003"
                                    data-runtime-version="1"
                                    data-usage-id="block-v1:Microsoft+DEV216x+course+type@video+block@e6c494a7-68cb-9243-1790-9700c6f8bde9"
                                    data-type="Video" data-course-id="course-v1:Microsoft+DEV216x+course"&gt;
                                    &lt;script type="json/xblock-args" class="xblock-json-init-args"&gt;
                                    {"xmodule-type": "Video"}
                                    &lt;/script&gt;


                                    &lt;h3 class="hd hd-2"&gt;Sidney Andrews&lt;/h3&gt;

                                    &lt;div
                                    id="video_e6c494a7-68cb-9243-1790-9700c6f8bde9"
                                    class="video closed"
                                    data-metadata='{&amp;#34;savedVideoPosition&amp;#34;: 0.0, &amp;#34;ytMetadataUrl&amp;#34;:
                                    &amp;#34;https://www.googleapis.com/youtube/v3/videos/&amp;#34;, &amp;#34;sources&amp;#34;:
                                    [&amp;#34;https://d2f1egay8yehza.cloudfront.net/MSXDDCAS2017-V003100_DTH.mp4&amp;#34;],
                                    &amp;#34;speed&amp;#34;: null, &amp;#34;generalSpeed&amp;#34;: 1.0, &amp;#34;end&amp;#34;:
                                    0.0, &amp;#34;sub&amp;#34;: &amp;#34;oCNmelsoe2A&amp;#34;, &amp;#34;ytTestTimeout&amp;#34;:
                                    1500, &amp;#34;transcriptLanguages&amp;#34;: {&amp;#34;en&amp;#34;: &amp;#34;English&amp;#34;},
                                    &amp;#34;start&amp;#34;: 0.0, &amp;#34;ytKey&amp;#34;: null, &amp;#34;recordedYoutubeIsAvailable&amp;#34;:
                                    true, &amp;#34;ytApiUrl&amp;#34;: &amp;#34;https://www.youtube.com/iframe_api&amp;#34;,
                                    &amp;#34;saveStateUrl&amp;#34;: &amp;#34;/courses/course-v1:Microsoft+DEV216x+course/xblock/block-v1:Microsoft+DEV216x+course+type@video+block@e6c494a7-68cb-9243-1790-9700c6f8bde9/handler/xmodule_handler/save_user_state&amp;#34;,
                                    &amp;#34;transcriptAvailableTranslationsUrl&amp;#34;: &amp;#34;/courses/course-v1:Microsoft+DEV216x+course/xblock/block-v1:Microsoft+DEV216x+course+type@video+block@e6c494a7-68cb-9243-1790-9700c6f8bde9/handler/transcript/available_translations&amp;#34;,
                                    &amp;#34;transcriptLanguage&amp;#34;: &amp;#34;en&amp;#34;, &amp;#34;showCaptions&amp;#34;:
                                    &amp;#34;true&amp;#34;, &amp;#34;autohideHtml5&amp;#34;: false, &amp;#34;streams&amp;#34;:
                                    &amp;#34;1.00:oCNmelsoe2A&amp;#34;, &amp;#34;transcriptTranslationUrl&amp;#34;:
                                    &amp;#34;/courses/course-v1:Microsoft+DEV216x+course/xblock/block-v1:Microsoft+DEV216x+course+type@video+block@e6c494a7-68cb-9243-1790-9700c6f8bde9/handler/transcript/translation/__lang__&amp;#34;,
                                    &amp;#34;captionDataDir&amp;#34;: null, &amp;#34;autoplay&amp;#34;: false}'
                                    data-bumper-metadata='null'
                                    data-poster='null'
                                    tabindex="-1"
                                    &gt;
                                    &lt;div class="focus_grabber first"&gt;&lt;/div&gt;

                                    &lt;div class="tc-wrapper"&gt;
                                    &lt;div class="video-wrapper"&gt;
                                    &lt;span tabindex="0" class="spinner" aria-hidden="false" aria-label="Загрузка
                                    видеоплеера"&gt;&lt;/span&gt;
                                    &lt;span tabindex="-1" class="btn-play fa fa-youtube-play fa-2x is-hidden"
                                    aria-hidden="true" aria-label="Просмотреть видео"&gt;&lt;/span&gt;
                                    &lt;div class="video-player-pre"&gt;&lt;/div&gt;
                                    &lt;div class="video-player"&gt;
                                    &lt;div id="e6c494a7-68cb-9243-1790-9700c6f8bde9"&gt;&lt;/div&gt;
                                    &lt;h4 class="hd hd-4 video-error is-hidden"&gt;Воспроизводимых видеоматериалов не
                                    найдено.&lt;/h4&gt;
                                    &lt;h4 class="hd hd-4 video-hls-error is-hidden"&gt;
                                    Your browser does not support this video format. Try using a different browser.
                                    &lt;/h4&gt;
                                    &lt;/div&gt;
                                    &lt;div class="video-player-post"&gt;&lt;/div&gt;
                                    &lt;div class="closed-captions"&gt;&lt;/div&gt;
                                    &lt;div class="video-controls is-hidden"&gt;
                                    &lt;div&gt;
                                    &lt;div class="vcr"&gt;&lt;div class="vidtime"&gt;0:00 / 0:00&lt;/div&gt;&lt;/div&gt;
                                    &lt;div class="secondary-controls"&gt;&lt;/div&gt;
                                    &lt;/div&gt;
                                    &lt;/div&gt;
                                    &lt;/div&gt;
                                    &lt;/div&gt;

                                    &lt;div class="focus_grabber last"&gt;&lt;/div&gt;

                                    &lt;h3 class="hd hd-4 downloads-heading sr"
                                    id="video-download-transcripts_e6c494a7-68cb-9243-1790-9700c6f8bde9"&gt;Downloads
                                    and
                                    transcripts&lt;/h3&gt;
                                    &lt;div class="wrapper-downloads" role="region"
                                    aria-labelledby="video-download-transcripts_e6c494a7-68cb-9243-1790-9700c6f8bde9"&gt;
                                    &lt;div class="wrapper-download-video"&gt;
                                    &lt;h4 class="hd hd-5"&gt;Видео&lt;/h4&gt;
                                    &lt;a class="btn-link video-sources video-download-button"
                                    href="https://d2f1egay8yehza.cloudfront.net/MSXDDCAS2017-V003100_DTH.mp4"&gt;
                                    Download video file
                                    &lt;/a&gt;
                                    &lt;/div&gt;
                                    &lt;div class="wrapper-download-transcripts"&gt;
                                    &lt;h4 class="hd hd-5"&gt;Transcripts&lt;/h4&gt;
                                    &lt;ul class="list-download-transcripts"&gt;
                                    &lt;li class="transcript-option"&gt;

                                    &lt;a class="btn btn-link"
                                    href="/courses/course-v1:Microsoft+DEV216x+course/xblock/block-v1:Microsoft+DEV216x+course+type@video+block@e6c494a7-68cb-9243-1790-9700c6f8bde9/handler/transcript/download"
                                    data-value="srt"&gt;Download SubRip (.srt) file&lt;/a&gt;
                                    &lt;/li&gt;
                                    &lt;li class="transcript-option"&gt;

                                    &lt;a class="btn btn-link"
                                    href="/courses/course-v1:Microsoft+DEV216x+course/xblock/block-v1:Microsoft+DEV216x+course+type@video+block@e6c494a7-68cb-9243-1790-9700c6f8bde9/handler/transcript/download"
                                    data-value="txt"&gt;Download Text (.txt) file&lt;/a&gt;
                                    &lt;/li&gt;
                                    &lt;/ul&gt;
                                    &lt;/div&gt;
                                    &lt;/div&gt;
                                    &lt;/div&gt;

                                    &lt;/div&gt;

                                    &lt;/div&gt;
                                    &lt;/div&gt;

                                    &lt;/div&gt;

                                </div>
                                <div id="seq_contents_2" aria-labelledby="tab_2" aria-hidden="true" class="seq_contents tex2jax_ignore asciimath2jax_ignore">
                                    &lt;div class="xblock xblock-student_view xblock-student_view-vertical"
                                    data-runtime-class="LmsRuntime" data-init="VerticalStudentView"
                                    data-course-id="course-v1:Microsoft+DEV216x+course"
                                    data-request-token="3f4881b8d0e311e7b4890242ac120003" data-runtime-version="1"
                                    data-usage-id="block-v1:Microsoft+DEV216x+course+type@vertical+block@72c9dc5e-4911-eb90-a580-f2abc9a39cfb"
                                    data-block-type="vertical"&gt;


                                    &lt;h2 class="hd hd-2 unit-title"&gt;Grading&lt;/h2&gt;


                                    &lt;div class="bookmark-button-wrapper"&gt;
                                    &lt;button class="btn btn-link bookmark-button "
                                    aria-pressed="false"
                                    data-bookmark-id="htmlmak_2,block-v1:Microsoft+DEV216x+course+type@vertical+block@72c9dc5e-4911-eb90-a580-f2abc9a39cfb"
                                    data-bookmarks-api-url="/api/bookmarks/v1/bookmarks/"&gt;
                                    &lt;span class="bookmark-text"&gt;Bookmark this page&lt;/span&gt;
                                    &lt;/button&gt;
                                    &lt;/div&gt;


                                    &lt;div class="vert-mod"&gt;
                                    &lt;div class="vert vert-0"
                                    data-id="block-v1:Microsoft+DEV216x+course+type@html+block@9afcb4ef-8c0e-d8bf-a0c3-95d9f6e09d43"&gt;
                                    &lt;div class="xblock xblock-student_view xblock-student_view-html xmodule_display
                                    xmodule_HtmlModule" data-runtime-class="LmsRuntime" data-init="XBlockToXModuleShim"
                                    data-block-type="html" data-request-token="3f4881b8d0e311e7b4890242ac120003"
                                    data-runtime-version="1"
                                    data-usage-id="block-v1:Microsoft+DEV216x+course+type@html+block@9afcb4ef-8c0e-d8bf-a0c3-95d9f6e09d43"
                                    data-type="HTMLModule" data-course-id="course-v1:Microsoft+DEV216x+course"&gt;
                                    &lt;script type="json/xblock-args" class="xblock-json-init-args"&gt;
                                    {"xmodule-type": "HTMLModule"}
                                    &lt;/script&gt;
                                    &lt;p&gt;A passing score in this course can be obtained by earning &lt;strong&gt;70%&lt;/strong&gt;
                                    of the possible points. Many times this is referred to as earning a grade of &lt;strong&gt;70/100&lt;/strong&gt;.&lt;/p&gt;
                                    &lt;p&gt;&lt;img
                                    src="/assets/courseware/v1/c4821512995dd06bbb2746b7c5952e2f/asset-v1:Microsoft+DEV216x+course+type@asset+block/00_grading.png"
                                    alt="" /&gt;&lt;/p&gt;
                                    &lt;p&gt;This course was designed so that all activities summed together will earn
                                    you a
                                    score of &lt;strong&gt;1000&lt;/strong&gt;. There are two main types of activities
                                    in
                                    this
                                    course:&lt;/p&gt;
                                    &lt;ul&gt;
                                    &lt;li&gt;&lt;p&gt;&lt;em&gt;Modules 1 - 4&lt;/em&gt; have a lab assignment each
                                    that
                                    make
                                    up 4 lab assignments total. Each lab assignment is weighted at &lt;strong&gt;150&lt;/strong&gt;
                                    points. In total, lab assignments are worth &lt;strong&gt;600&lt;/strong&gt; points.&lt;/p&gt;
                                    &lt;/li&gt;
                                    &lt;li&gt;&lt;p&gt;This course contains a final exam that is worth &lt;strong&gt;400&lt;/strong&gt;
                                    points. The exam is composed of &lt;strong&gt;20&lt;/strong&gt; questions valued at
                                    &lt;strong&gt;20&lt;/strong&gt;
                                    points each.&lt;/p&gt;
                                    &lt;/li&gt;
                                    &lt;/ul&gt;
                                    &lt;p&gt;&lt;img
                                    src="/assets/courseware/v1/9f1817296ab04c8993bfeb2c89b52f0c/asset-v1:Microsoft+DEV216x+course+type@asset+block/00_grading_breakdown.png"
                                    alt="" /&gt;&lt;/p&gt;
                                    &lt;blockquote&gt;
                                    &lt;p&gt;&lt;strong&gt;Reminder&lt;/strong&gt;: It is not possible to achieve a
                                    passing
                                    score in this course if you skip either the Final Exam or the Lab Assignments. Each
                                    portion
                                    of the course offers less than the &lt;strong&gt;700 points&lt;/strong&gt; needed to
                                    pass
                                    the course.&lt;/p&gt;
                                    &lt;/blockquote&gt;

                                    &lt;/div&gt;

                                    &lt;/div&gt;
                                    &lt;/div&gt;

                                    &lt;/div&gt;

                                </div>
                                <div id="seq_contents_3" aria-labelledby="tab_3" aria-hidden="true" class="seq_contents tex2jax_ignore asciimath2jax_ignore">
                                    &lt;div class="xblock xblock-student_view xblock-student_view-vertical"
                                    data-runtime-class="LmsRuntime" data-init="VerticalStudentView"
                                    data-course-id="course-v1:Microsoft+DEV216x+course"
                                    data-request-token="3f4881b8d0e311e7b4890242ac120003" data-runtime-version="1"
                                    data-usage-id="block-v1:Microsoft+DEV216x+course+type@vertical+block@7515bcb8-4ad9-93a3-2514-463ca1350421"
                                    data-block-type="vertical"&gt;


                                    &lt;h2 class="hd hd-2 unit-title"&gt;Prerequisites&lt;/h2&gt;


                                    &lt;div class="bookmark-button-wrapper"&gt;
                                    &lt;button class="btn btn-link bookmark-button "
                                    aria-pressed="false"
                                    data-bookmark-id="htmlmak_2,block-v1:Microsoft+DEV216x+course+type@vertical+block@7515bcb8-4ad9-93a3-2514-463ca1350421"
                                    data-bookmarks-api-url="/api/bookmarks/v1/bookmarks/"&gt;
                                    &lt;span class="bookmark-text"&gt;Bookmark this page&lt;/span&gt;
                                    &lt;/button&gt;
                                    &lt;/div&gt;


                                    &lt;div class="vert-mod"&gt;
                                    &lt;div class="vert vert-0"
                                    data-id="block-v1:Microsoft+DEV216x+course+type@html+block@f79d3f2d-edb3-5979-5695-0d147ba2ca84"&gt;
                                    &lt;div class="xblock xblock-student_view xblock-student_view-html xmodule_display
                                    xmodule_HtmlModule" data-runtime-class="LmsRuntime" data-init="XBlockToXModuleShim"
                                    data-block-type="html" data-request-token="3f4881b8d0e311e7b4890242ac120003"
                                    data-runtime-version="1"
                                    data-usage-id="block-v1:Microsoft+DEV216x+course+type@html+block@f79d3f2d-edb3-5979-5695-0d147ba2ca84"
                                    data-type="HTMLModule" data-course-id="course-v1:Microsoft+DEV216x+course"&gt;
                                    &lt;script type="json/xblock-args" class="xblock-json-init-args"&gt;
                                    {"xmodule-type": "HTMLModule"}
                                    &lt;/script&gt;
                                    &lt;p&gt;This is course focused on the Angular 2 framework. Development in Angular 2
                                    requires a developer to have experience in a few technical areas:&lt;/p&gt;
                                    &lt;h3 id="html-javascript"&gt;HTML &amp;amp; JavaScript&lt;/h3&gt;
                                    &lt;p&gt;In this course, you will create Angular 2 templates using HTML. You will
                                    also
                                    need
                                    to configure features (such as module loaders) using JavaScript.&lt;br /&gt;
                                    We recommend that you complete or have equivalent experience to the &lt;a
                                    href="https://www.edx.org/course/javascript-html-css-web-development-microsoft-dev211-1x"
                                    target="_blank"&gt;DEV211x - JavaScript, HTML and CSS Web Development&lt;/a&gt;
                                    course
                                    on
                                    edX.&lt;/p&gt;
                                    &lt;h3 id="typescript"&gt;TypeScript&lt;/h3&gt;
                                    &lt;p&gt;In this course, we are going to use the TypeScript language to create our
                                    Angular 2
                                    applications. While TypeScript is not strictly required (you can write Angular 2
                                    applications using JavaScript), TypeScript provides a lot of useful advantages. If
                                    you
                                    do
                                    not have experience with TypeScript, we recommend that you complete or have
                                    equivalent
                                    experience to the [DEV211x - JavaScript, HTML and CSS Web Development][DEV201x -
                                    Introduction to TypeScript(&lt;a
                                    href="https://www.edx.org/course/introduction-typescript-microsoft-dev201x-1"
                                    target="_blank"&gt;https://www.edx.org/course/introduction-typescript-microsoft-dev201x-1&lt;/a&gt;)
                                    course on edX.&lt;/p&gt;
                                    &lt;h3 id="node"&gt;Node&lt;/h3&gt;
                                    &lt;p&gt;In this course, we are going to use Node and Node Package Manager (NPM)
                                    extensively
                                    to bootstrap and host our Angular 2 web applications. You do not need Node
                                    experience to
                                    complete this course as all concepts will be explained in full in the first module.&lt;/p&gt;
                                    &lt;h3 id="bootstrap"&gt;Bootstrap&lt;/h3&gt;
                                    &lt;p&gt;While Bootstrap is not required to use Angular 2, we will use Bootstrap in
                                    this
                                    course to improve the appearance of our applications. You do not need Bootstrap
                                    experience
                                    to complete this course as all concepts will be explained in full in the last
                                    module. If
                                    you
                                    wish to learn more about Bootstrap, you can take the &lt;a
                                    href="https://www.edx.org/course/introduction-bootstrap-tutorial-microsoft-dev203x-0"
                                    target="_blank"&gt;DEV203x - Introduction to Bootstrap&lt;/a&gt; course on edX.&lt;/p&gt;

                                    &lt;/div&gt;

                                    &lt;/div&gt;
                                    &lt;/div&gt;

                                    &lt;/div&gt;

                                </div>
                                <div id="seq_contents_4" aria-labelledby="tab_4" aria-hidden="true" class="seq_contents tex2jax_ignore asciimath2jax_ignore">
                                    &lt;div class="xblock xblock-student_view xblock-student_view-vertical"
                                    data-runtime-class="LmsRuntime" data-init="VerticalStudentView"
                                    data-course-id="course-v1:Microsoft+DEV216x+course"
                                    data-request-token="3f4881b8d0e311e7b4890242ac120003" data-runtime-version="1"
                                    data-usage-id="block-v1:Microsoft+DEV216x+course+type@vertical+block@891b6bdb-58b1-85dc-65aa-afde6163ad07"
                                    data-block-type="vertical"&gt;


                                    &lt;h2 class="hd hd-2 unit-title"&gt;Pre-course Survey&lt;/h2&gt;


                                    &lt;div class="bookmark-button-wrapper"&gt;
                                    &lt;button class="btn btn-link bookmark-button "
                                    aria-pressed="false"
                                    data-bookmark-id="htmlmak_2,block-v1:Microsoft+DEV216x+course+type@vertical+block@891b6bdb-58b1-85dc-65aa-afde6163ad07"
                                    data-bookmarks-api-url="/api/bookmarks/v1/bookmarks/"&gt;
                                    &lt;span class="bookmark-text"&gt;Bookmark this page&lt;/span&gt;
                                    &lt;/button&gt;
                                    &lt;/div&gt;


                                    &lt;div class="vert-mod"&gt;
                                    &lt;div class="vert vert-0"
                                    data-id="block-v1:Microsoft+DEV216x+course+type@html+block@1a852bd3-eb8e-5239-a33e-4a431802230e"&gt;
                                    &lt;div class="xblock xblock-student_view xblock-student_view-html xmodule_display
                                    xmodule_HtmlModule" data-runtime-class="LmsRuntime" data-init="XBlockToXModuleShim"
                                    data-block-type="html" data-request-token="3f4881b8d0e311e7b4890242ac120003"
                                    data-runtime-version="1"
                                    data-usage-id="block-v1:Microsoft+DEV216x+course+type@html+block@1a852bd3-eb8e-5239-a33e-4a431802230e"
                                    data-type="HTMLModule" data-course-id="course-v1:Microsoft+DEV216x+course"&gt;
                                    &lt;script type="json/xblock-args" class="xblock-json-init-args"&gt;
                                    {"xmodule-type": "HTMLModule"}
                                    &lt;/script&gt;
                                    &lt;h2 id="pre-course-survey"&gt;Pre-course Survey&lt;/h2&gt;
                                    &lt;p&gt;Please complete the survey below before starting the course. If the survey
                                    is
                                    not
                                    displayed in your browser, click this &lt;a
                                    href="https://mslexsurveys.azurewebsites.net/survey/519038" target="_blank"&gt;here&lt;/a&gt;
                                    to open it in a separate pane.&lt;/p&gt;

                                    &lt;/div&gt;

                                    &lt;/div&gt;
                                    &lt;div class="vert vert-1"
                                    data-id="block-v1:Microsoft+DEV216x+course+type@html+block@9e6b3eae-bf30-bc09-2577-1fc30ff1a49c"&gt;
                                    &lt;div class="xblock xblock-student_view xblock-student_view-html xmodule_display
                                    xmodule_HtmlModule" data-runtime-class="LmsRuntime" data-init="XBlockToXModuleShim"
                                    data-block-type="html" data-request-token="3f4881b8d0e311e7b4890242ac120003"
                                    data-runtime-version="1"
                                    data-usage-id="block-v1:Microsoft+DEV216x+course+type@html+block@9e6b3eae-bf30-bc09-2577-1fc30ff1a49c"
                                    data-type="HTMLModule" data-course-id="course-v1:Microsoft+DEV216x+course"&gt;
                                    &lt;script type="json/xblock-args" class="xblock-json-init-args"&gt;
                                    {"xmodule-type": "HTMLModule"}
                                    &lt;/script&gt;
                                    &lt;h2 id="survey"&gt;Survey&lt;/h2&gt;
                                    &lt;p&gt;&lt;iframe width="100%" height="1000" title="Pre-course survey"
                                    src="https://mslexsurveys.azurewebsites.net/survey/519038/0aaca904989119e6c1f3697c6a2010ee"
                                    frameborder="0" marginwidth="0" marginheight="0" scrolling="yes"&gt;
                                    Your browser does not support IFrames.
                                    &lt;/iframe&gt;&lt;/p&gt;

                                    &lt;/div&gt;

                                    &lt;/div&gt;
                                    &lt;/div&gt;

                                    &lt;/div&gt;

                                </div>
                                <div id="seq_content" role="tabpanel" aria-labelledby="tab_0">
                                    <div class="xblock xblock-student_view xblock-student_view-vertical xblock-initialized" data-runtime-class="LmsRuntime" data-init="VerticalStudentView" data-course-id="course-v1:Microsoft+DEV216x+course" data-request-token="3f4881b8d0e311e7b4890242ac120003" data-runtime-version="1" data-usage-id="block-v1:Microsoft+DEV216x+course+type@vertical+block@da19f3d3-fbc5-22ce-4658-82c419a9a3dd" data-block-type="vertical">


                                        <h2 class="hd hd-2 unit-title">Getting Around This Course</h2>


                                        <div class="bookmark-button-wrapper">
                                            <button class="btn btn-link bookmark-button" aria-pressed="false" data-bookmark-id="htmlmak_2,block-v1:Microsoft+DEV216x+course+type@vertical+block@da19f3d3-fbc5-22ce-4658-82c419a9a3dd" data-bookmarks-api-url="/api/bookmarks/v1/bookmarks/">
                                                <span class="bookmark-text">Bookmark this page</span>
                                            </button>
                                        </div>


                                        <div class="vert-mod">
                                            <div class="vert vert-0" data-id="block-v1:Microsoft+DEV216x+course+type@html+block@f6114fea-f15e-9c52-e0e7-2095e1a8a716">
                                                <div class="xblock xblock-student_view xblock-student_view-html xmodule_display xmodule_HtmlModule xblock-initialized" data-runtime-class="LmsRuntime" data-init="XBlockToXModuleShim" data-block-type="html" data-request-token="3f4881b8d0e311e7b4890242ac120003" data-runtime-version="1" data-usage-id="block-v1:Microsoft+DEV216x+course+type@html+block@f6114fea-f15e-9c52-e0e7-2095e1a8a716" data-type="HTMLModule" data-course-id="course-v1:Microsoft+DEV216x+course">

                                                    <p>If you're new to edX, here is some information to get you
                                                        started.
                                                        Even
                                                        if you've used edX for other courses, the information here will
                                                        help
                                                        you
                                                        understand how this course is organized.</p>
                                                    <p>This course is structured into modules, sections,&nbsp;and units.
                                                        Each
                                                        module contains a combination of&nbsp;written documentation
                                                        (reference
                                                        documents)&nbsp;and lab activities that cover the&nbsp;topic
                                                        areas&nbsp;for
                                                        that module. In this course, there are more&nbsp;lab&nbsp;sections&nbsp;than
                                                        there are&nbsp;documentation sections, so you will&nbsp;be
                                                        spending
                                                        more
                                                        time&nbsp;working through the&nbsp;hands-on activities than
                                                        reading
                                                        about the technologies. Each module contains&nbsp;all of the
                                                        material
                                                        that you'll need to learn the included&nbsp;topics, and we've
                                                        included
                                                        links to additional sources of information if you are interested
                                                        in
                                                        digging even deeper into the technologies.</p>
                                                    <p>To navigate through the sections of a module, use the navigation
                                                        bar
                                                        at
                                                        the top of the display window. You can use the left and right
                                                        arrows
                                                        to
                                                        move back-and-forth through&nbsp;the units, and you can hover
                                                        your
                                                        mouse
                                                        pointer over unit icon to see the unit title.</p>
                                                    <p>As noted above, each&nbsp;module in this course will have some
                                                        combination of reading material, labs, and video content. The&nbsp;modules
                                                        and the labs are&nbsp;meant to be taken in order, but you can
                                                        skip
                                                        around in the resource document&nbsp;sections, or even skip over
                                                        a
                                                        topic&nbsp;if
                                                        you have already mastered the material in a particular unit. In
                                                        order to&nbsp;get
                                                        the most out of the lab assignments, you&nbsp;should make sure
                                                        that
                                                        you&nbsp;understand
                                                        the material&nbsp;in the resource documents.</p>
                                                    <h3 id="instructor-feedback">INSTRUCTOR FEEDBACK</h3>
                                                    <p>Given the nature of this MOOC-style course and the large number
                                                        of
                                                        students enrolled, it's not possible for instructors to provide
                                                        feedback
                                                        to students. However, we encourage you to use the discussion
                                                        boards
                                                        to
                                                        seek help and feedback from your fellow students. This is a
                                                        great
                                                        way to
                                                        help each other and enrich your learning experience.</p>
                                                    <h3 id="other-resources">OTHER RESOURCES</h3>
                                                    <p><a href="https://www.edx.org/about/student-faq" target="_blank">edX
                                                            Student FAQ</a>: This FAQ answers many questions about the
                                                        edX
                                                        student experience. Read the questions, so if you encounter any
                                                        issues
                                                        during the course, you can refer back to this document for help.
                                                    </p>

                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <nav class="sequence-bottom" aria-label="Раздел">
                                    <button class="sequence-nav-button button-previous">
                                        <span class="icon fa fa-chevron-prev" aria-hidden="true"></span>
                                        <span>Предыдущее</span>
                                    </button>
                                    <button class="sequence-nav-button button-next">
                                        <span>Следующее</span>
                                        <span class="icon fa fa-chevron-next" aria-hidden="true"></span>
                                    </button>
                                </nav>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
</main>
<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
<script>
    (function () {
            "use strict";
            this.Sequence = function () {
                function Sequence(element) {
                    var self = this;
                    this.removeBookmarkIconFromActiveNavItem = function (event) {
                        return Sequence.prototype.removeBookmarkIconFromActiveNavItem.apply(self, [event])
                    }
                    ;
                    this.addBookmarkIconToActiveNavItem = function (event) {
                        return Sequence.prototype.addBookmarkIconToActiveNavItem.apply(self, [event])
                    }
                    ;
                    this._change_sequential = function (direction, event) {
                        return Sequence.prototype._change_sequential.apply(self, [direction, event])
                    }
                    ;
                    this.selectPrevious = function (event) {
                        return Sequence.prototype.selectPrevious.apply(self, [event])
                    }
                    ;
                    this.selectNext = function (event) {
                        return Sequence.prototype.selectNext.apply(self, [event])
                    }
                    ;
                    this.goto = function (event) {
                        return Sequence.prototype.goto.apply(self, [event])
                    }
                    ;
                    this.toggleArrows = function () {
                        return Sequence.prototype.toggleArrows.apply(self)
                    }
                    ;
                    this.addToUpdatedProblems = function (problemId, newContentState, newState) {
                        return Sequence.prototype.addToUpdatedProblems.apply(self, [problemId, newContentState, newState])
                    }
                    ;
                    this.hideTabTooltip = function (event) {
                        return Sequence.prototype.hideTabTooltip.apply(self, [event])
                    }
                    ;
                    this.displayTabTooltip = function (event) {
                        return Sequence.prototype.displayTabTooltip.apply(self, [event])
                    }
                    ;
                    this.arrowKeys = {
                        LEFT: 37,
                        UP: 38,
                        RIGHT: 39,
                        DOWN: 40
                    };
                    this.updatedProblems = {};
                    this.requestToken = $(element).data("request-token");
                    this.el = $(element).find(".sequence");
                    this.path = $(".path");
                    this.contents = this.$(".seq_contents");
                    this.content_container = this.$("#seq_content");
                    this.sr_container = this.$(".sr-is-focusable");
                    this.num_contents = this.contents.length;
                    this.id = this.el.data("id");
                    this.ajaxUrl = this.el.data("ajax-url");
                    this.nextUrl = this.el.data("next-url");
                    this.prevUrl = this.el.data("prev-url");
                    this.keydownHandler($(element).find("#sequence-list .tab"));
                    this.base_page_title = ($("title").data("base-title") || "").trim();
                    this.bind();
                    this.render(parseInt(this.el.data("position"), 10))
                }

                Sequence.prototype.$ = function (selector) {
                    return $(selector, this.el)
                }
                ;
                Sequence.prototype.bind = function () {
                    this.$("#sequence-list .nav-item").click(this.goto);
                    this.$("#sequence-list .nav-item").keypress(this.keyDownHandler);
                    this.el.on("bookmark:add", this.addBookmarkIconToActiveNavItem);
                    this.el.on("bookmark:remove", this.removeBookmarkIconFromActiveNavItem);
                    this.$("#sequence-list .nav-item").on("focus mouseenter", this.displayTabTooltip);
                    this.$("#sequence-list .nav-item").on("blur mouseleave", this.hideTabTooltip)
                }
                ;
                Sequence.prototype.previousNav = function (focused, index) {
                    var $navItemList, $sequenceList = $(focused).parent().parent();
                    if (index === 0) {
                        $navItemList = $sequenceList.find("li").last()
                    } else {
                        $navItemList = $sequenceList.find("li:eq(" + index + ")").prev()
                    }
                    $sequenceList.find(".tab").removeClass("visited").removeClass("focused");
                    $navItemList.find(".tab").addClass("focused").focus()
                }
                ;
                Sequence.prototype.nextNav = function (focused, index, total) {
                    var $navItemList, $sequenceList = $(focused).parent().parent();
                    if (index === total) {
                        $navItemList = $sequenceList.find("li").first()
                    } else {
                        $navItemList = $sequenceList.find("li:eq(" + index + ")").next()
                    }
                    $sequenceList.find(".tab").removeClass("visited").removeClass("focused");
                    $navItemList.find(".tab").addClass("focused").focus()
                }
                ;
                Sequence.prototype.keydownHandler = function (element) {
                    var self = this;
                    element.keydown(function (event) {
                        var key = event.keyCode
                            , $focused = $(event.currentTarget)
                            , $sequenceList = $focused.parent().parent()
                            , index = $sequenceList.find("li").index($focused.parent())
                            , total = $sequenceList.find("li").size() - 1;
                        switch (key) {
                            case self.arrowKeys.LEFT:
                                event.preventDefault();
                                self.previousNav($focused, index);
                                break;
                            case self.arrowKeys.RIGHT:
                                event.preventDefault();
                                self.nextNav($focused, index, total);
                                break
                        }
                    })
                }
                ;
                Sequence.prototype.displayTabTooltip = function (event) {
                    $(event.currentTarget).find(".sequence-tooltip").removeClass("sr")
                }
                ;
                Sequence.prototype.hideTabTooltip = function (event) {
                    $(event.currentTarget).find(".sequence-tooltip").addClass("sr")
                }
                ;
                Sequence.prototype.updatePageTitle = function () {
                    var currentUnitTitle, newPageTitle, positionLink = this.link_for(this.position);
                    if (positionLink && positionLink.data("page-title")) {
                        currentUnitTitle = positionLink.data("page-title");
                        newPageTitle = currentUnitTitle + " | " + this.base_page_title;
                        if (newPageTitle !== document.title) {
                            document.title = newPageTitle
                        }
                        $(".nav-item-sequence").text(currentUnitTitle)
                    }
                }
                ;
                Sequence.prototype.hookUpContentStateChangeEvent = function () {
                    var self = this;
                    return $(".problems-wrapper").bind("contentChanged", function (event, problemId, newContentState, newState) {
                        return self.addToUpdatedProblems(problemId, newContentState, newState)
                    })
                }
                ;
                Sequence.prototype.addToUpdatedProblems = function (problemId, newContentState, newState) {
                    if (!this.anyUpdatedProblems(this.position)) {
                        this.updatedProblems[this.position] = {}
                    }
                    this.updatedProblems[this.position][problemId] = [newContentState, newState]
                }
                ;
                Sequence.prototype.anyUpdatedProblems = function (position) {
                    return typeof this.updatedProblems[position] !== "undefined"
                }
                ;
                Sequence.prototype.enableButton = function (buttonClass, buttonAction) {
                    this.$(buttonClass).removeClass("disabled").removeAttr("disabled").click(buttonAction)
                }
                ;
                Sequence.prototype.disableButton = function (buttonClass) {
                    this.$(buttonClass).addClass("disabled").attr("disabled", true)
                }
                ;
                Sequence.prototype.updateButtonState = function (buttonClass, buttonAction, isAtBoundary, boundaryUrl) {
                    if (isAtBoundary && boundaryUrl === "None") {
                        this.disableButton(buttonClass)
                    } else {
                        this.enableButton(buttonClass, buttonAction)
                    }
                }
                ;
                Sequence.prototype.toggleArrows = function () {
                    var isFirstTab, isLastTab, nextButtonClass, previousButtonClass;
                    this.$(".sequence-nav-button").unbind("click");
                    isFirstTab = this.position === 1;
                    previousButtonClass = ".sequence-nav-button.button-previous";
                    this.updateButtonState(previousButtonClass, this.selectPrevious, isFirstTab, this.prevUrl);
                    isLastTab = this.position >= this.contents.length;
                    nextButtonClass = ".sequence-nav-button.button-next";
                    this.updateButtonState(nextButtonClass, this.selectNext, isLastTab, this.nextUrl)
                }
                ;
                Sequence.prototype.render = function (newPosition) {
                    var bookmarked, currentTab, modxFullUrl, sequenceLinks, self = this;
                    if (this.position !== newPosition) {
                        if (this.position) {
                            this.mark_visited(this.position);
                            modxFullUrl = "" + this.ajaxUrl + "/goto_position";
                            $.postWithPrefix(modxFullUrl, {
                                position: newPosition
                            })
                        }
                        this.el.trigger("sequence:change");
                        this.mark_active(newPosition);
                        currentTab = this.contents.eq(newPosition - 1);
                        bookmarked = this.el.find(".active .bookmark-icon").hasClass("bookmarked");
                        this.content_container.html(currentTab.text()).attr("aria-labelledby", currentTab.attr("aria-labelledby")).data("bookmarked", bookmarked);
                        if (this.anyUpdatedProblems(newPosition)) {
                            $.each(this.updatedProblems[newPosition], function (problemId, latestData) {
                                var latestContent, latestResponse;
                                latestContent = latestData[0];
                                latestResponse = latestData[1];
                                self.content_container.find("[data-problem-id='" + problemId + "']").data("content", latestContent).data("problem-score", latestResponse.current_score).data("problem-total-possible", latestResponse.total_possible).data("attempts-used", latestResponse.attempts_used)
                            })
                        }
                        XBlock.initializeBlocks(this.content_container, this.requestToken);
                        window.update_schematics();
                        this.position = newPosition;
                        this.toggleArrows();
                        this.hookUpContentStateChangeEvent();
                        this.updatePageTitle();
                        sequenceLinks = this.content_container.find("a.seqnav");
                        sequenceLinks.click(this.goto);
                        this.sr_container.focus()
                    }
                }
                ;
                Sequence.prototype.goto = function (event) {
                    var alertTemplate, alertText, isBottomNav, newPosition, widgetPlacement;
                    event.preventDefault();
                    if ($(event.currentTarget).hasClass("seqnav")) {
                        newPosition = $(event.currentTarget).attr("href")
                    } else {
                        newPosition = $(event.currentTarget).data("element")
                    }
                    if (newPosition >= 1 && newPosition <= this.num_contents) {
                        isBottomNav = $(event.target).closest('nav[class="sequence-bottom"]').length > 0;
                        if (isBottomNav) {
                            widgetPlacement = "bottom"
                        } else {
                            widgetPlacement = "top"
                        }
                        Logger.log("edx.ui.lms.sequence.tab_selected", {
                            current_tab: this.position,
                            target_tab: newPosition,
                            tab_count: this.num_contents,
                            id: this.id,
                            widget_placement: widgetPlacement
                        });
                        if (window.queuePollerID) {
                            window.clearTimeout(window.queuePollerID);
                            delete window.queuePollerID
                        }
                        this.render(newPosition)
                    } else {
                        alertTemplate = gettext("Sequence error! Cannot navigate to %(tab_name)s in the current SequenceModule. Please contact the course staff.");
                        alertText = interpolate(alertTemplate, {
                            tab_name: newPosition
                        }, true);
                        alert(alertText)
                    }
                }
                ;
                Sequence.prototype.selectNext = function (event) {
                    this._change_sequential("next", event)
                }
                ;
                Sequence.prototype.selectPrevious = function (event) {
                    this._change_sequential("previous", event)
                }
                ;
                Sequence.prototype._change_sequential = function (direction, event) {
                    var analyticsEventName, isBottomNav, newPosition, offset, targetUrl, widgetPlacement;
                    if (direction !== "previous" && direction !== "next") {
                        return
                    }
                    event.preventDefault();
                    analyticsEventName = "edx.ui.lms.sequence." + direction + "_selected";
                    isBottomNav = $(event.target).closest('nav[class="sequence-bottom"]').length > 0;
                    if (isBottomNav) {
                        widgetPlacement = "bottom"
                    } else {
                        widgetPlacement = "top"
                    }
                    if (direction === "next" && this.position >= this.contents.length) {
                        targetUrl = this.nextUrl
                    } else if (direction === "previous" && this.position === 1) {
                        targetUrl = this.prevUrl
                    }
                    Logger.log(analyticsEventName, {
                        id: this.id,
                        current_tab: this.position,
                        tab_count: this.num_contents,
                        widget_placement: widgetPlacement
                    }).always(function () {
                        if (targetUrl) {
                            window.location.href = targetUrl
                        }
                    });
                    if (!targetUrl) {
                        if (isBottomNav) {
                            $.scrollTo(0, 150)
                        }
                        offset = {
                            next: 1,
                            previous: -1
                        };
                        newPosition = this.position + offset[direction];
                        this.render(newPosition)
                    }
                }
                ;
                Sequence.prototype.link_for = function (position) {
                    return this.$("#sequence-list .nav-item[data-element=" + position + "]")
                }
                ;
                Sequence.prototype.mark_visited = function (position) {
                    var element = this.link_for(position);
                    element.attr({
                        tabindex: "-1",
                        "aria-selected": "false",
                        "aria-expanded": "false"
                    }).removeClass("inactive").removeClass("active").removeClass("focused").addClass("visited")
                }
                ;
                Sequence.prototype.mark_active = function (position) {
                    var element = this.link_for(position);
                    element.attr({
                        tabindex: "0",
                        "aria-selected": "true",
                        "aria-expanded": "true"
                    }).removeClass("inactive").removeClass("visited").removeClass("focused").addClass("active");
                    this.$(".sequence-list-wrapper").focus()
                }
                ;
                Sequence.prototype.addBookmarkIconToActiveNavItem = function (event) {
                    event.preventDefault();
                    this.el.find(".nav-item.active .bookmark-icon").removeClass("is-hidden").addClass("bookmarked");
                    this.el.find(".nav-item.active .bookmark-icon-sr").text(gettext("Bookmarked"))
                }
                ;
                Sequence.prototype.removeBookmarkIconFromActiveNavItem = function (event) {
                    event.preventDefault();
                    this.el.find(".nav-item.active .bookmark-icon").removeClass("bookmarked").addClass("is-hidden");
                    this.el.find(".nav-item.active .bookmark-icon-sr").text("")
                }
                ;
                return Sequence
            }()
        }
    ).call(this);
</script>

</body>
</html>
