<? include 'partials/header.php'; ?>

<main>
    <section class="course-header">
        <div class="row">
            <div class="column small-12 medium-7 large-8">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Учебный центр</a></li>
                    <li><a href="#">Вендоры</a></li>
                    <li><a href="#">Microsoft Corporation</a></li>
                </ul>
                <div class="course-header__wrapper">
                    <div class="course-header__title">
                        <h1>Курсы <br class="show-for-large"> программирования</h1>
                    </div>
                </div>
            </div>
            <div class="column small-12 medium-5 large-3 large-offset-1">
                <div class="feedback-header">
                    <div class="feedback-header__name">
                        <span>Светлана Жученко</span>
                    </div>
                    <div class="feedback-header__photo">
                        <img src="dist/images/feedback__header/feedback-header__photo/photo-1.png" alt="">
                    </div>
                    <div class="feedback-header__position">
                        <span>менеджер</span>
                        <span>интернет-магазина</span>
                    </div>
                    <div class="feedback-header__contacts">
                        <span>8 (800) 200-08-60 доб. 6011</span>
                        <a href="mailto:Svetlana.Zhuchenko@softlinegroup.com">Svetlana.Zhuchenko@softlinegroup.com</a>
                    </div>

                    <a href="#" class="button expanded">Связаться сейчас</a>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="row">
            <div class="column small-12 large-3">
                <div class="filter">
                    <div class="filter__title">
                        <h3>Фильтр:</h3>
                    </div>
                    <div class="filter__input">
                        <div class="filter__label">
                            <span>Вендор:</span>
                        </div>
                        <label class="b-select">
                            <select class="b-select__container">
                                <option value="">Все вендоры</option>
                                <option value="">Все вендоры</option>
                                <option value="">Все вендоры</option>
                            </select>
                        </label>
                    </div>
                    <div class="filter__input">
                        <div class="filter__label">
                            <span>Продукты:</span>
                        </div>
                        <label class="b-select">
                            <select class="b-select__container">
                                <option value="">Все продукты</option>
                                <option value="">Все продукты</option>
                                <option value="">Все продукты</option>
                            </select>
                        </label>
                    </div>
                    <hr>
                    <div class="filter__input">
                        <div class="filter__label">
                            <span>Подпишитесь на рассылку новостей Softline</span>
                        </div>
                        <label>
                            <input placeholder="Ваш e-mail" type="text">
                        </label>
                        <label>
                            <input placeholder="Ваше имя" type="text">
                        </label>
                    </div>

                    <button class="button">Подписаться</button>
                </div>
            </div>

            <div class="column small-12 medium-9">
                <div class="found-list">
                    <div class="found-list__top">
                        <div class="found-list__title">
                            <h3>
                                Найдено курсов: <span>3</span>
                            </h3>
                        </div>
                        <div class="found-list__sorting">
                            <span>Вид:</span>
                            <div>
                                <a class="sorting-btn">
                                    <span><img src="dist/images/found-list/tiel.png" alt=""></span>
                                    <span>Плитка</span>
                                </a>
                                <a class="sorting-btn">
                                    <span><img src="dist/images/found-list/bread.png" alt=""></span>
                                    <span>Список</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="found-list__container card-layout">
                        <div class="row small-up-1 large-up-2">
                            <div class="column">
                                <div class="course-card">
                                    <div class="course-card__container">
                                        <div class="course-card__photo">
                                            <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                                        </div>
                                        <div class="course-card__cell">
                                            <div class="course-card__top">
                                                <div class="course-card__title">
                                            <span>
                                                Java Standard Edition (Java SE).
                                               <br> Программирование. Базовые технологии
                                            </span>
                                                </div>
                                            </div>
                                            <div class="course-card__wrap">
                                                <div class="course-card__details">
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Производитель:</span><span>Формат обучения:</span>
                                                            </li>

                                                            <li><span>Java</span>
                                                                <span>
                                                                    <img src="dist/images/course-card/icon-1.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-2.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-3.png"
                                                                         alt="">
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Уровень:</span><span>Цена (от):</span></li>

                                                            <li><span>начальный</span><span>35 100,00 руб.</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="course-card__desc">
                                                    <p>
                                                        В предлагаемой программе даётся обзор платформ: Java Standard
                                                        Edition
                                                        (J2SE/Java SE 8) и Java Enterprise Edition (J2EE / Java EE 7)
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="course-card__button">
                                        <span>Расписание и цены</span>

                                        <svg>
                                            <use xlink:href="#images--svg--arrow"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                            <div class="column">
                                <div class="course-card">
                                    <div class="course-card__container">
                                        <div class="course-card__photo">
                                            <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                                        </div>
                                        <div class="course-card__cell">
                                            <div class="course-card__top">
                                                <div class="course-card__title">
                                            <span>
                                                Java Standard Edition (Java SE).
                                               <br> Программирование. Базовые технологии
                                            </span>
                                                </div>
                                            </div>
                                            <div class="course-card__wrap">
                                                <div class="course-card__details">
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Производитель:</span><span>Формат обучения:</span>
                                                            </li>

                                                            <li><span>Java</span>
                                                                <span>
                                                                    <img src="dist/images/course-card/icon-1.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-2.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-3.png"
                                                                         alt="">
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Уровень:</span><span>Цена (от):</span></li>

                                                            <li><span>начальный</span><span>35 100,00 руб.</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="course-card__desc">
                                                    <p>
                                                        В предлагаемой программе даётся обзор платформ: Java Standard
                                                        Edition
                                                        (J2SE/Java SE 8) и Java Enterprise Edition (J2EE / Java EE 7)
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="course-card__button">
                                        <span>Расписание и цены</span>

                                        <svg>
                                            <use xlink:href="#images--svg--arrow"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                            <div class="column">
                                <div class="course-card">
                                    <div class="course-card__container">
                                        <div class="course-card__photo">
                                            <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                                        </div>
                                        <div class="course-card__cell">
                                            <div class="course-card__top">
                                                <div class="course-card__title">
                                            <span>
                                                Java Standard Edition (Java SE).
                                               <br> Программирование. Базовые технологии
                                            </span>
                                                </div>
                                            </div>
                                            <div class="course-card__wrap">
                                                <div class="course-card__details">
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Производитель:</span><span>Формат обучения:</span>
                                                            </li>

                                                            <li><span>Java</span>
                                                                <span>
                                                                    <img src="dist/images/course-card/icon-1.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-2.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-3.png"
                                                                         alt="">
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Уровень:</span><span>Цена (от):</span></li>

                                                            <li><span>начальный</span><span>35 100,00 руб.</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="course-card__desc">
                                                    <p>
                                                        В предлагаемой программе даётся обзор платформ: Java Standard
                                                        Edition
                                                        (J2SE/Java SE 8) и Java Enterprise Edition (J2EE / Java EE 7)
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="course-card__button">
                                        <span>Расписание и цены</span>

                                        <svg>
                                            <use xlink:href="#images--svg--arrow"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                            <div class="column">
                                <div class="course-card">
                                    <div class="course-card__container">
                                        <div class="course-card__photo">
                                            <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                                        </div>
                                        <div class="course-card__cell">
                                            <div class="course-card__top">
                                                <div class="course-card__title">
                                            <span>
                                                Java Standard Edition (Java SE).
                                               <br> Программирование. Базовые технологии
                                            </span>
                                                </div>
                                            </div>
                                            <div class="course-card__wrap">
                                                <div class="course-card__details">
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Производитель:</span><span>Формат обучения:</span>
                                                            </li>

                                                            <li><span>Java</span>
                                                                <span>
                                                                    <img src="dist/images/course-card/icon-1.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-2.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-3.png"
                                                                         alt="">
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Уровень:</span><span>Цена (от):</span></li>

                                                            <li><span>начальный</span><span>35 100,00 руб.</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="course-card__desc">
                                                    <p>
                                                        В предлагаемой программе даётся обзор платформ: Java Standard
                                                        Edition
                                                        (J2SE/Java SE 8) и Java Enterprise Edition (J2EE / Java EE 7)
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="course-card__button">
                                        <span>Расписание и цены</span>

                                        <svg>
                                            <use xlink:href="#images--svg--arrow"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="pagination-wrapper">
                        <ul class="pagination" role="navigation" aria-label="Pagination">
                            <li class="pagination-previous disabled"></li>
                            <li class="current"><span class="show-for-sr">You're on page</span> 1</li>
                            <li><a href="#" aria-label="Page 2">2</a></li>
                            <li><a href="#" aria-label="Page 3">3</a></li>
                            <li><a href="#" aria-label="Page 4">4</a></li>
                            <li class="ellipsis" aria-hidden="true"></li>
                            <li><a href="#" aria-label="Page 12">12</a></li>
                            <li><a href="#" aria-label="Page 13">13</a></li>
                            <li class="pagination-next"><a href="#" aria-label="Next page"></a></li>
                        </ul>

                        <div class="pagination-select show-for-large">
                            <span>Элем. на стр.</span>
                            <label class="b-select">
                                <select class="b-select__container">
                                    <option value="24">24</option>
                                    <option value="50">50</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>


                <div style="padding-top: 50px" class="found-list">
                    <div class="found-list__top">
                        <div class="found-list__title">
                            <h3>
                                Найдено курсов: <span>3</span>
                            </h3>
                        </div>
                        <div class="found-list__sorting">
                            <span>Вид:</span>
                            <div>
                                <a class="sorting-btn">
                                    <span><img src="dist/images/found-list/tiel.png" alt=""></span>
                                    <span>Плитка</span>
                                </a>
                                <a class="sorting-btn">
                                    <span><img src="dist/images/found-list/bread.png" alt=""></span>
                                    <span>Список</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="found-list__container">
                        <div class="row small-up-1">
                            <div class="column">
                                <div class="course-card">
                                    <div class="course-card__container">
                                        <div class="course-card__photo">
                                            <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                                        </div>
                                        <div class="course-card__cell">
                                            <div class="course-card__top">
                                                <div class="course-card__title">
                                            <span>
                                                Java Standard Edition (Java SE).
                                               <br> Программирование. Базовые технологии
                                            </span>
                                                </div>
                                            </div>
                                            <div class="course-card__wrap">
                                                <div class="course-card__details">
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Производитель:</span><span>Формат обучения:</span>
                                                            </li>

                                                            <li><span>Java</span>
                                                                <span>
                                                                    <img src="dist/images/course-card/icon-1.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-2.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-3.png"
                                                                         alt="">
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Уровень:</span><span>Цена (от):</span></li>

                                                            <li><span>начальный</span><span>35 100,00 руб.</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="course-card__desc">
                                                    <p>
                                                        В предлагаемой программе даётся обзор платформ: Java Standard
                                                        Edition
                                                        (J2SE/Java SE 8) и Java Enterprise Edition (J2EE / Java EE 7)
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="course-card__button">
                                        <span>Расписание и цены</span>

                                        <svg>
                                            <use xlink:href="#images--svg--arrow"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                            <div class="column">
                                <div class="course-card">
                                    <div class="course-card__container">
                                        <div class="course-card__photo">
                                            <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                                        </div>
                                        <div class="course-card__cell">
                                            <div class="course-card__top">
                                                <div class="course-card__title">
                                            <span>
                                                Java Standard Edition (Java SE).
                                               <br> Программирование. Базовые технологии
                                            </span>
                                                </div>
                                            </div>
                                            <div class="course-card__wrap">
                                                <div class="course-card__details">
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Производитель:</span><span>Формат обучения:</span>
                                                            </li>

                                                            <li><span>Java</span>
                                                                <span>
                                                                    <img src="dist/images/course-card/icon-1.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-2.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-3.png"
                                                                         alt="">
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Уровень:</span><span>Цена (от):</span></li>

                                                            <li><span>начальный</span><span>35 100,00 руб.</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="course-card__desc">
                                                    <p>
                                                        В предлагаемой программе даётся обзор платформ: Java Standard
                                                        Edition
                                                        (J2SE/Java SE 8) и Java Enterprise Edition (J2EE / Java EE 7)
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="course-card__button">
                                        <span>Расписание и цены</span>

                                        <svg>
                                            <use xlink:href="#images--svg--arrow"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>

                            <div class="column">
                                <div class="course-card">
                                    <div class="course-card__container">
                                        <div class="course-card__photo">
                                            <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                                        </div>
                                        <div class="course-card__cell">
                                            <div class="course-card__top">
                                                <div class="course-card__title">
                                            <span>
                                                Java Standard Edition (Java SE).
                                               <br> Программирование. Базовые технологии
                                            </span>
                                                </div>
                                            </div>
                                            <div class="course-card__wrap">
                                                <div class="course-card__details">
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Производитель:</span><span>Формат обучения:</span>
                                                            </li>

                                                            <li><span>Java</span>
                                                                <span>
                                                                    <img src="dist/images/course-card/icon-1.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-2.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-3.png"
                                                                         alt="">
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Уровень:</span><span>Цена (от):</span></li>

                                                            <li><span>начальный</span><span>35 100,00 руб.</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="course-card__desc">
                                                    <p>
                                                        В предлагаемой программе даётся обзор платформ: Java Standard
                                                        Edition
                                                        (J2SE/Java SE 8) и Java Enterprise Edition (J2EE / Java EE 7)
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="course-card__button">
                                        <span>Расписание и цены</span>

                                        <svg>
                                            <use xlink:href="#images--svg--arrow"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>

                            <div class="column">
                                <div class="course-card">
                                    <div class="course-card__container">
                                        <div class="course-card__photo">
                                            <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                                        </div>
                                        <div class="course-card__cell">
                                            <div class="course-card__top">
                                                <div class="course-card__title">
                                            <span>
                                                Java Standard Edition (Java SE).
                                               <br> Программирование. Базовые технологии
                                            </span>
                                                </div>
                                            </div>
                                            <div class="course-card__wrap">
                                                <div class="course-card__details">
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Производитель:</span><span>Формат обучения:</span>
                                                            </li>

                                                            <li><span>Java</span>
                                                                <span>
                                                                    <img src="dist/images/course-card/icon-1.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-2.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-3.png"
                                                                         alt="">
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Уровень:</span><span>Цена (от):</span></li>

                                                            <li><span>начальный</span><span>35 100,00 руб.</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="course-card__desc">
                                                    <p>
                                                        В предлагаемой программе даётся обзор платформ: Java Standard
                                                        Edition
                                                        (J2SE/Java SE 8) и Java Enterprise Edition (J2EE / Java EE 7)
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="course-card__button">
                                        <span>Расписание и цены</span>

                                        <svg>
                                            <use xlink:href="#images--svg--arrow"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="pagination-wrapper">
                        <ul class="pagination" role="navigation" aria-label="Pagination">
                            <li class="pagination-previous disabled"></li>
                            <li class="current"><span class="show-for-sr">You're on page</span> 1</li>
                            <li><a href="#" aria-label="Page 2">2</a></li>
                            <li><a href="#" aria-label="Page 3">3</a></li>
                            <li><a href="#" aria-label="Page 4">4</a></li>
                            <li class="ellipsis" aria-hidden="true"></li>
                            <li><a href="#" aria-label="Page 12">12</a></li>
                            <li><a href="#" aria-label="Page 13">13</a></li>
                            <li class="pagination-next"><a href="#" aria-label="Next page"></a></li>
                        </ul>

                        <div class="pagination-select show-for-large">
                            <span>Элем. на стр.</span>
                            <label class="b-select">
                                <select class="b-select__container">
                                    <option value="24">24</option>
                                    <option value="50">50</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="subscribe-banner">
        <div class="subscribe-banner__wrapper">
            <div class="row">
                <div class="column small-12 large-9 large-offset-3">
                    <div class="subscribe-banner__title">
                        <span>Будьте в курсе</span>
                    </div>
                    <form class="subscribe-banner__form">
                        <div class="subscribe-banner__input subscribe-banner__input--name">
                            <input type="text" placeholder="Ваше имя">
                        </div>
                        <div class="subscribe-banner__input subscribe-banner__input--email">
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="subscribe-banner__button">
                            <button class="button">Подписаться</button>
                        </div>
                    </form>
                    <div class="subscribe-banner__desc">
                        <span>Подпишитесь на информацию о новинках, скидках и акциях. <br> Уже более 36 000 подписчиков!</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        $('.b-select__container').select2();
    });
</script>
</body>
</html>
