<? include 'partials/header.php'; ?>

<main>
    <section class="cloud-header">
        <div class="breadcrumbs-wrapper">
            <ul class="breadcrumbs">
                <li><a href="#">Главная</a></li>
                <li><a href="#">Облачные решения</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="column small-12 text-center">
                <div class="cloud-header__title">
                    <span>Облачные решения</span>
                </div>
                <div class="cloud-header__desc">
                    <p>Our deep relationships with cloud providers and our robust services portfolio
                        make us your gateway to solutions tailored to your growth.</p>
                </div>
            </div>
        </div>
    </section>
    <div class="solutions-container">
        <div class="cloud-nav">
            <ul class="cloud-nav__menu">

                <li><a href="#" id=""> Решения для бизнеса </a></li>

                <li><a href="#" id="">Аренда виртуальных мощностей </a></li>

                <li><a href="#" id="">Облачные платформы </a></li>

                <li><a href="#" id="">Аренда оборудования </a></li>
            </ul>
        </div>
        <section class="row">
            <div class="column small-12 large-offset-4 large-8 xlarge-offset-3 xlarge-9">
                <section class="solutions">
                    <div class="row small-up-1 medium-up-2">
                        <div class="column">
                            <div class="solutions__card">
                                <div style="background-image: url(dist/images/solutions/solutions__card/solutions-1.jpg)"
                                     class="solutions__card-photo">
                                    <div class="solutions__card-icon arrow-icon"></div>
                                </div>
                                <div class="solutions__card-wrapper">
                                    <div class="solutions__card-title">
                                        <span>Виртуальная инфраструктура (IaaS)</span>
                                    </div>
                                    <div class="solutions__card-desc">
                                        <ul>
                                            <li><a href="#">Softline Cloud (IaaS/VMware)</a></li>

                                            <li><a href="#">Softline Cloud (IaaS/Hyper-V)</a></li>

                                            <li><a href="#">Microsoft Azure</a></li>

                                            <li><a href="http://aws.softline.ru/">Amazon</a></li>

                                            <li><a href="http://promo.softline.ru/ibm-cloud">IBM Cloud</a></li>

                                            <li><a href="#">Google Cloud Platform</a></li>

                                        </ul>
                                    </div>
                                    <a href="#" class="solutions__card-button">Подробнее</a>
                                </div>
                            </div>
                        </div>
                        <div class="column">
                            <div class="solutions__card">
                                <div style="background-image: url(dist/images/solutions/solutions__card/solutions-2.jpg)"
                                     class="solutions__card-photo">
                                    <div class="solutions__card-icon virtual-icon"></div>
                                </div>
                                <div class="solutions__card-wrapper">
                                    <div class="solutions__card-title">
                                        <span> Облачные сервисы</span>
                                    </div>
                                    <div class="solutions__card-desc">
                                        <ul>
                                            <li><a href="#">Защищенное облако (152-ФЗ)</a></li>

                                            <li><a href="#">Частное облако</a></li>

                                            <li><a href="#">Резервное копирование в облако</a></li>

                                            <li><a href="#">Аварийное восстановление в облаке</a></li>

                                            <li><a href="#">SAP-хостинг</a></li>

                                        </ul>
                                    </div>
                                    <a href="#" class="solutions__card-button">Подробнее</a>
                                </div>
                            </div>
                        </div>
                        <div class="column">
                            <div class="solutions__card">
                                <div style="background-image: url(dist/images/solutions/solutions__card/solutions-3.jpg)"
                                     class="solutions__card-photo">
                                    <div class="solutions__card-icon cloud-icon"></div>
                                </div>
                                <div class="solutions__card-wrapper">
                                    <div class="solutions__card-title">
                                        <span>Решения для бизнеса</span>
                                    </div>
                                    <div class="solutions__card-desc">
                                        <ul>

                                            <li><a href="#">VDI</a></li>

                                            <li><a href="#"> Microsoft Office 365</a></li>

                                            <li><a href="#">G Suite</a></li>

                                            <li><a href="#">Виртуальный офис</a></li>

                                        </ul>
                                    </div>
                                    <a href="#" class="solutions__card-button">Подробнее</a>
                                </div>
                            </div>
                        </div>
                        <div class="column">
                            <div class="solutions__card">
                                <div style="background-image: url(dist/images/solutions/solutions__card/solutions-4.jpg)"
                                     class="solutions__card-photo">
                                    <div class="solutions__card-icon server-icon"></div>
                                </div>
                                <div class="solutions__card-wrapper">
                                    <div class="solutions__card-title">
                                        <span>Аппаратные сервисы</span>
                                    </div>
                                    <div class="solutions__card-desc">
                                        <ul>
                                            <li><a href="#">Размещение оборудования (Colocation)</a></li>

                                            <li><a href="#">Аренда серверного оборудования</a></li>
                                        </ul>
                                    </div>
                                    <a href="#" class="solutions__card-button">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </section>
    </div>
    <section class="advantages">
        <div class="row small-up-1 medium-up-3">
            <div class="column">
                <div class="advantages__card">
                    <div class="advantages__icon"></div>
                    <div class="advantages__desc">
                        <span>ЦОДы уровня <br> Tier-3</span>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="advantages__card">
                    <div class="advantages__icon"></div>
                    <div class="advantages__desc">
                        <span>SLA с финансовыми гарантиями</span>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="advantages__card">
                    <div class="advantages__icon"></div>
                    <div class="advantages__desc">
                        <span>Шифрованные каналы связи</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="expertise">
        <div class="row">
            <div class="column small-12 medium-8">
                <div class="expertise__title h1 text-center medium-text-left">
                    <span>Наша <br class="show-for-large"> экспертиза</span>
                </div>
            </div>
            <div class="column small-12 medium-4 show-for-medium">
                <div class="catalog-slider__nav">
                    <div class="catalog-slider__prev">
                        <svg>
                            <use xlink:href="#images--svg--nav-arrow" href="#images--svg--nav-arrow"></use>
                        </svg>
                    </div>
                    <div class="catalog-slider__next">
                        <svg>
                            <use xlink:href="#images--svg--nav-arrow" href="#images--svg--nav-arrow"></use>
                        </svg>
                    </div>
                </div>
            </div>
            <div class="column small-12 large-3">
                <div class="expertise__desc">
                    <p>
                        Мы выполнили более <br class="show-for-large">
                        2000 проектов, из которых
                        560 в сфере информационной безопасности
                    </p>
                </div>
            </div>
            <div class="column small-12 large-9">
                <div class="expertise__wrapper owl-carousel">
                    <a href="#" class="expertise__card">
                        <div class="expertise__card-img">
                            <img src="/dist/images/expertise/expertise__card-img/miel.jpg" alt="">
                        </div>
                        <div class="expertise__card-desc">
                            <p>
                                Softline поставила отраслевую CRM-систему риелторской компании «Миэль»
                            </p>
                        </div>
                    </a>
                    <a href="#" class="expertise__card">
                        <div class="expertise__card-img">
                            <img src="/dist/images/expertise/expertise__card-img/logo-2.jpg" alt="">
                        </div>
                        <div class="expertise__card-desc">
                            <p>
                                Softline переоборудовала диспетчерскую Сибирского регионального центра МЧС России.
                            </p>
                        </div>
                    </a>
                    <a href="#" class="expertise__card">
                        <div class="expertise__card-img">
                            <img src="/dist/images/expertise/expertise__card-img/logo-3.png" alt="">
                        </div>
                        <div class="expertise__card-desc">
                            <p>
                                Softline оборудовала лабораторию в Сибирском университете.
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="column small-12">
                <a href="#" class="expertise__button">Показать все выполненные проекты</a>
            </div>
        </div>

    </section>
    <section class="details">
        <div class="row">
            <div class="column medium-offset-6 medium-6 large-offset-7 large-5  small-12 ">
                <div class="details__wrapper">
                    <div class="details__title text-center medium-text-left h1"><span>Нам можно доверять</span></div>
                    <div class="details__desc">
                        <p>
                            Мы выполнили более 2000 проектов. От серверного оборудования до бытовых мелочей: мы
                            поставляем
                            широчайший список IT-оборудования для нужд бизнеса
                            и госкомпаний.
                        </p>
                    </div>
                    <div class="details__button">
                        <a href="#" class="button">Подробнее о Softline</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="partners">
        <div class="row">
            <div class="column small-12 medium-8 large-9 text-center medium-text-left">
                <h2>Наши партнёры</h2>
            </div>
            <div class="column small-12 medium-4 large-3 show-for-medium">
                <button class="partners__button">показать всех</button>
            </div>
        </div>
        <div class="partners__card-wrapper row small-up-1 medium-up-3 large-up-4">
            <div class="column">
                <div class="flex-container">
                    <a href="http://edu.softline.ru/vendors/microsoft" class="b-card">
                        <img src="dist/images/partners/partners__card/microsoft.png" alt="">
                    </a>
                </div>
            </div>
            <div class="column">
                <div class="flex-container">
                    <a href="http://edu.softline.ru/vendors/cisco-systems" class="b-card">
                        <img src="content/partners/cisco.png" alt="">
                    </a>
                </div>
            </div>
            <div class="column">
                <div class="flex-container">
                    <a href="http://edu.softline.ru/vendors/veeam-vmce" class="b-card">
                        <img src="content/partners/veeam.png" alt="">
                    </a>
                </div>
            </div>
            <div class="column">
                <div class="flex-container">
                    <a href="http://edu.softline.ru/vendors/vmware" class="b-card">
                        <img src="content/partners/vmware.png" alt="">
                    </a>
                </div>
            </div>
            <div class="column">
                <div class="flex-container">
                    <a href="http://edu.softline.ru/vendors/kaspersky" class="b-card">
                        <img src="content/partners/kaspersky.jpg" alt="">
                    </a>
                </div>
            </div>
            <div class="column">
                <div class="flex-container">
                    <a href="http://edu.softline.ru/vendors/oracle" class="b-card">
                        <img src="content/partners/oracle.png" alt="">
                    </a>
                </div>
            </div>
            <div class="column">
                <div class="flex-container">
                    <a href="http://edu.softline.ru/vendors/ibm" class="b-card">
                        <img src="content/partners/ibm.png" alt="">
                    </a>
                </div>
            </div>
            <div class="column">
                <div class="flex-container">
                    <a href="http://edu.softline.ru/vendors/amazon" class="b-card">
                        <img src="content/partners/amazon.png" alt="">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="areas">
        <div class="row">
            <div class="column small-12">
                <div class="text-center  h1">
                   <span>Предлагаем программы <br class="show-for-large">
                        для специалистов по направлениям:
                   </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column small-12">
                <div class="areas__wrap">
                    <div class="row small-up-1 medium-up-3 large-up-6 medium-collapse">
                        <div class="column">
                            <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/finance.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/finance.png)"
                       class="areas__icon"></span>
            </span>
                                <span class="areas__title">Финансы</span>
                            </a>
                        </div>
                        <div class="column">
                            <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/retail.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/retail.png)"
                       class="areas__icon"></span>
            </span>
                                <span class="areas__title">Ритейл</span>
                            </a>
                        </div>
                        <div class="column">
                            <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/tek.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/tek.png)" class="areas__icon"></span>
            </span>
                                <span class="areas__title">ТЭК</span>
                            </a>
                        </div>
                        <div class="column">
                            <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/industry.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/industry.png)" class="areas__icon"></span>
            </span>
                                <span class="areas__title">Промышленность</span>
                            </a>
                        </div>
                        <div class="column">
                            <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/sector.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/sector.png)"
                       class="areas__icon"></span>
            </span>
                                <span class="areas__title">Гос.сектор</span>
                            </a>
                        </div>
                        <div class="column">
                            <a href="#" class="areas__card">
            <span class="areas__img">
<span class="photo"><span style="background-image: url(dist/images/areas/areas__img/tele.jpg)"></span></span>
                 <span style="background-image: url(dist/images/areas/areas__icon/tele.png)" class="areas__icon"></span>
            </span>
                                <span class="areas__title">Телеком</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="subscribe-banner">
        <div class="subscribe-banner__wrapper">
            <div class="row">
                <div class="column small-12">
                    <div class="subscribe-banner__title medium-text-center">
                        <span>Узнавайте первыми!</span>
                    </div>
                    <form class="subscribe-banner__form subscribe-banner__form--solutions">
                        <div class="subscribe-banner__input">
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="subscribe-banner__button">
                            <button class="button">Подписаться</button>
                        </div>
                    </form>
                    <div class="subscribe-banner__desc medium-text-center">
                        <span>Уникальные IT-практики, кейсы, обзоры <br
                                    class="show-for-medium"> и экспертные мнения.</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        // Слайдеры
        $('.expertise__wrapper').owlCarousel({
            loop: true,
            nav: true,
            dots: false,
            margin: 20,
            navText: [$('.expertise').find('.catalog-slider__prev'), $('.expertise').find('.catalog-slider__next')],
            navContainer: $('.expertise').find('.catalog-slider__nav'),
            responsive: {
                0: {
                    items: 1,
                },
                500: {
                    items: 2
                },
                768: {
                    items: 3,
                    margin: 40
                },
                1000: {
                    items: 3,
                    margin: 40
                }
            }
        });
    })
</script>
</body>
</html>

