<? include 'partials/header.php'; ?>

<main>
    <section class="course-header">
        <div class="row">
            <div class="column small-12 medium-7 large-8">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Учебный центр</a></li>
                    <li><a href="#">Сертификация и экзамены</a></li>
                </ul>
                <div class="course-header__wrapper">
                    <div class="course-header__title">
                        <h1>Учебный центр Softline</h1>
                    </div>
                </div>
            </div>
            <div class="column small-12 medium-5 large-3 large-offset-1">
                <div class="feedback-header">
                    <div class="feedback-header__name">
                        <span>Светлана Жученко</span>
                    </div>
                    <div class="feedback-header__photo">
                        <img src="dist/images/feedback__header/feedback-header__photo/photo-1.png" alt="">
                    </div>
                    <div class="feedback-header__position">
                        <span>менеджер</span>
                        <span>интернет-магазина</span>
                    </div>
                    <div class="feedback-header__contacts">
                        <span>8 (800) 200-08-60 доб. 6011</span>
                        <a href="mailto:Svetlana.Zhuchenko@softlinegroup.com">Svetlana.Zhuchenko@softlinegroup.com</a>
                    </div>

                    <a href="#" class="button expanded">Связаться сейчас</a>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="row">
            <div class="column small-12 large-8">
                <div class="b-editor">
                    <p><strong>Как оформить заказ?</strong></p>
                    <p>Учебный центр Softline обслуживает заказы как от юридических так и от физических лиц. Если вы
                        планируете пройти обучение на одном из курсов, пожалуйста, перейдите на главную страницу
                        учебного центра, где вы сможете выбрать желаемый курс и оформить на него заказ.
                        Клиентам учебного центра Softline доступны два основных варианта оформления заказа:</p>
                    <p>
                        Оформление заказа с помощью корзины пользователя.
                        Чтобы оформить заказ, выберите нужный(е) курс(ы), нажмите "добавить в корзину" и либо перейдите
                        в корзину, либо нажмите кнопку "оформить заказ" появившуюся на месте кнопки "добавить в
                        корзину".
                    </p>
                    <p>
                        Оформление заказа с помощью менеджера учебного центра
                        Оформить заказ с помощью менеджера вы можете несколькими способами:
                        позвонив по телефону: +7 (800) 200-41-36.</p>
                </div>
                <div class="accordion-large-wrap">

                    <ul class="accordion-large accordion-large--alt" data-accordion>

                        <li class="accordion-large__item" data-accordion-item>
                            <a href="" class="accordion-large__title"><span>Как оплатить заказ?</span>
                                <svg class="icon plus">
                                    <use xlink:href="#images--svg--plus"></use>
                                </svg>
                                <svg class="icon miuns">
                                    <use xlink:href="#images--svg--minus"></use>
                                </svg>
                            </a>
                            <div class="accordion-large__content accordion-content" data-tab-content>
                                <p>Предварительные выводы неутешительны: начало повседневной работы по формированию
                                    позиции представляет собой интересный эксперимент проверки прогресса
                                    профессионального сообщества. Как уже неоднократно упомянуто, акционеры крупнейших
                                    компаний могут быть описаны максимально подробно.</p>
                            </div>
                        </li>

                        <li class="accordion-large__item" data-accordion-item>
                            <a href="" class="accordion-large__title"><span>Как получить бухгалтерские документы по заказу?</span>
                                <svg class="icon plus">
                                    <use xlink:href="#images--svg--plus"></use>
                                </svg>
                                <svg class="icon miuns">
                                    <use xlink:href="#images--svg--minus"></use>
                                </svg>
                            </a>
                            <div class="accordion-large__content accordion-content" data-tab-content>
                                <p>Предварительные выводы неутешительны: начало повседневной работы по формированию
                                    позиции представляет собой интересный эксперимент проверки прогресса
                                    профессионального сообщества. Как уже неоднократно упомянуто, акционеры крупнейших
                                    компаний могут быть описаны максимально подробно.</p>
                            </div>
                        </li>

                        <li class="accordion-large__item" data-accordion-item>
                            <a href="" class="accordion-large__title"><span>Вы работаете с бюджетными организациями, которым законодательно запрещена предоплата в размере более 30%?</span>
                                <svg class="icon plus">
                                    <use xlink:href="#images--svg--plus"></use>
                                </svg>
                                <svg class="icon miuns">
                                    <use xlink:href="#images--svg--minus"></use>
                                </svg>
                            </a>
                            <div class="accordion-large__content accordion-content" data-tab-content>
                                <p>Предварительные выводы неутешительны: начало повседневной работы по формированию
                                    позиции представляет собой интересный эксперимент проверки прогресса
                                    профессионального сообщества. Как уже неоднократно упомянуто, акционеры крупнейших
                                    компаний могут быть описаны максимально подробно.</p>
                            </div>
                        </li>

                        <li class="accordion-large__item" data-accordion-item>
                            <a href=""
                               class="accordion-large__title"><span>График работы учебного центра Softline</span>
                                <svg class="icon plus">
                                    <use xlink:href="#images--svg--plus"></use>
                                </svg>
                                <svg class="icon miuns">
                                    <use xlink:href="#images--svg--minus"></use>
                                </svg>
                            </a>
                            <div class="accordion-large__content accordion-content" data-tab-content>
                                <p>Предварительные выводы неутешительны: начало повседневной работы по формированию
                                    позиции представляет собой интересный эксперимент проверки прогресса
                                    профессионального сообщества. Как уже неоднократно упомянуто, акционеры крупнейших
                                    компаний могут быть описаны максимально подробно.</p>
                            </div>
                        </li>

                    </ul>
                </div>
                <div class="b-title">
                    <h3>Доставка</h3>
                </div>
                <div class="b-editor">
                    <p>
                        Учебный центр Softline проводит курсы как в своих филиалах в России и СНГ, так и на территории
                        заказчика (по договоренности). Также доступен дистанционный формат обучения.
                    </p>
                </div>

                <div class="b-title">
                    <h3>Гарантии и ответственность учебного центра:</h3>
                </div>

                <ul class="accordion-large accordion-large--alt" data-accordion>

                    <li class="accordion-large__item" data-accordion-item>
                        <a href=""
                           class="accordion-large__title"><span>Условия возврата денежных средств за услуги</span>
                            <svg class="icon plus">
                                <use xlink:href="#images--svg--plus"></use>
                            </svg>
                            <svg class="icon miuns">
                                <use xlink:href="#images--svg--minus"></use>
                            </svg>
                        </a>
                        <div class="accordion-large__content accordion-content" data-tab-content>
                            <p>Предварительные выводы неутешительны: начало повседневной работы по формированию позиции
                                представляет собой интересный эксперимент проверки прогресса профессионального
                                сообщества. Как уже неоднократно упомянуто, акционеры крупнейших компаний могут быть
                                описаны максимально подробно.</p>
                        </div>
                    </li>

                    <li class="accordion-large__item" data-accordion-item>
                        <a href="" class="accordion-large__title"><span>Наши реквизиты</span>
                            <svg class="icon plus">
                                <use xlink:href="#images--svg--plus"></use>
                            </svg>
                            <svg class="icon miuns">
                                <use xlink:href="#images--svg--minus"></use>
                            </svg>
                        </a>
                        <div class="accordion-large__content accordion-content" data-tab-content>
                            <p>Предварительные выводы неутешительны: начало повседневной работы по формированию позиции
                                представляет собой интересный эксперимент проверки прогресса профессионального
                                сообщества. Как уже неоднократно упомянуто, акционеры крупнейших компаний могут быть
                                описаны максимально подробно.</p>
                        </div>
                    </li>
                </ul>

            </div>
            <div class="column small-12 large-offset-1 large-3">
                <aside>
                    <div class="aside-widget">
                        <div class="aside-widget__title">О центре</div>
                        <div class="aside-widget__content">
                            <ul class="aside-menu">


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/mission">

                                        <span class="b-link__text">Программирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/training-conditions">

                                        <span class="b-link__text">Системное администрирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class=""
                                       href="http://edu.softline.ru/about/svedeniya-ob-obrazovatelnoy-organizatsii">

                                        <span class="b-link__text">Пользовательское ПО</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/accreditation">

                                        <span class="b-link__text">Операционные системы (ОС)</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/status">

                                        <span class="b-link__text">Сетевые технологии</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-klientov">

                                        <span class="b-link__text">Информационная безопасность</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-partnerov">

                                        <span class="b-link__text">Виртуализация</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/razrabotka-kursov">

                                        <span class="b-link__text">Базы данных и СУБД</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/delivery-info">

                                        <span class="b-link__text">Моделирование и САПР</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/requisites">

                                        <span class="b-link__text">Коммуникации</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/tech-support">

                                        <span class="b-link__text">Резервное копирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">CRM</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Бизнес-аналитика</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">IT-сервис менеджмент</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Импортозамещение ПО</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Бизнес-тренинги</span>
                                    </a>

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__title">Подбор курса</div>
                        <div class="aside-widget__content">
                            <form action="#">
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Направление:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все направления</option>
                                            <option value="">Все направления 2</option>
                                            <option value="">Все направления 3</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Специальность:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все специальности</option>
                                            <option value="">Все специальности 2</option>
                                            <option value="">Все специальности 3</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Вендор:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все Вендоры</option>
                                            <option value="">Все Вендоры 2</option>
                                            <option value="">Все Вендоры 3</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Вендор:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все Вендоры</option>
                                            <option value="">Все Вендоры 2</option>
                                            <option value="">Все Вендоры 3</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="radio-box">
                                    <label class="b-radiobox">
                                        <input name="event-filter" type="radio">
                                        <span>Очно (в аудитории)</span>
                                    </label>
                                    <label class="b-radiobox">
                                        <input name="event-filter" type="radio">
                                        <span>Дистанционно</span>
                                    </label>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Место проведения:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Москва</option>
                                            <option value="">Москва 2</option>
                                            <option value="">Москва 3</option>
                                        </select>
                                    </label>
                                </div>
                                <button class="button small expanded">Подобрать курс</button>
                            </form>
                        </div>
                    </div>
                </aside>
            </div>
        </div>

    </section>
    <section class="subscribe-banner">
        <div class="subscribe-banner__wrapper">
            <div class="row">
                <div class="column small-12 large-9 large-offset-3">
                    <div class="subscribe-banner__title">
                        <span>Будьте в курсе</span>
                    </div>
                    <form class="subscribe-banner__form">
                        <div class="subscribe-banner__input subscribe-banner__input--name">
                            <!--доп модификатор: error-->
                            <input type="text" placeholder="Ваше имя">
                        </div>
                        <div class="subscribe-banner__input subscribe-banner__input--email">
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="subscribe-banner__button">
                            <button class="button">Подписаться</button>
                        </div>
                    </form>
                    <div class="subscribe-banner__desc">
                        <span>Подпишитесь на информацию о новинках, скидках и акциях. <br> Уже более 36 000 подписчиков!</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        $('.b-select__container').select2();
        $(document).foundation();
    });
</script>
</body>
</html>

