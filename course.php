<? include 'partials/header.php'; ?>

<main>
    <section class="course-header">
        <div class="row">
            <div class="column small-12 medium-7 large-8">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Учебный центр</a></li>
                    <li><a href="#">Вендоры</a></li>
                    <li><a href="#">Microsoft Corporation</a></li>
                </ul>
                <div class="course-header__wrapper">
                    <div class="course-header__title">
                        <h1>Разработка приложений WCF с помощью Visual Studio 2010</h1>
                    </div>
                </div>
                <div class="course-header__download">
                    <a href="#" class="button">Посмотреть курс</a>
                </div>
            </div>
            <div class="column small-12 medium-5 large-3 large-offset-1">
                <div class="feedback-header">
                    <div class="feedback-header__name">
                        <span>Светлана Жученко</span>
                    </div>
                    <div class="feedback-header__photo">
                        <img src="dist/images/feedback__header/feedback-header__photo/photo-1.png" alt="">
                    </div>
                    <div class="feedback-header__position">
                        <span>менеджер</span>
                        <span>интернет-магазина</span>
                    </div>
                    <div class="feedback-header__contacts">
                        <span>8 (800) 200-08-60 доб. 6011</span>
                        <a href="Svetlana.Zhuchenko@softlinegroup.com">Svetlana.Zhuchenko@softlinegroup.com</a>
                    </div>

                    <a href="#" class="button expanded">Связаться сейчас</a>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="column small-12 medium-7 large-8">
                <div class="b-editor">
                    <h5><strong>About this course</strong></h5>
                    <p>This course is part of the Microsoft Professional Program Certificate in Big Data.</p>
                    <p>
                        As a data pro, you know that some scenarios—particularly those involving real-time analytics,
                        site
                        personalization, IoT, and mobile apps—are better addressed with NoSQL storage than they are with
                        relational databases. Azure Cosmos DB has several NoSQL (or “Not Only SQL”) non-relational data
                        storage options to choose from. Azure Cosmos DB is the distributed multi-model database from
                        Microsoft that transparently replicates your data wherever your users are. It has broad,
                        global-scale features and capabilities. If you have a data model that is constantly evolving and
                        you
                        want to move fast, that’s what these storage options are about. In this practical course,
                        complete
                        with labs, assessments, and a final exam, join the experts to learn NoSQL basics, and how NoSQL
                        has
                        evolved over time. Explore Azure Cosmos DB, and see how to use its storage options in your
                        applications. You’ll take a look the Tables API, DocumentDB API, MongoDB API, and more. Learn
                        about
                        the “three Vs”—variety (schemas or scenarios that evolve quickly), volume (scale in terms of
                        data
                        storage), and velocity (throughput needs to support a large user base). Take this opportunity to
                        explore the NoSQL options in Azure.
                    </p>
                    <p>
                        In this practical course, complete with labs, assessments, and a final exam. Explore the basics
                        of
                        NoSQL and the storage options in Azure Cosmos DB, and see how to use them in your applications.
                        Find
                        out how to create, store, manage, and access data in these different storage options. Get an
                        in-depth look at the various APIs in Azure Cosmos DB. Learn about the “three Vs”—variety
                        (schemas or
                        scenarios that evolve quickly), volume (scale in terms of data storage), and velocity
                        (throughput
                        needs to support a large user base). Take this opportunity to get started with Azure Cosmos DB.
                    </p>

                    <h5><strong>What you'll learn</strong></h5>
                    <ul>
                        <li>NoSQL fundamentals</li>
                        <li>Overview of NoSQL options in Azure Cosmos DB</li>
                        <li>Fundamental techniques for using the DocumentDB API, Tables API, and MongoDB API</li>
                        <li>Other techniques for accessing and improving performance of your NoSQL storage</li>
                    </ul>
                </div>
            </div>
            <div class="column small-12 medium-5 large-3 large-offset-1">
                <div class="course-info">
                    <div class="course-info__cell">
                        <div class="course-info__name">
                            <svg class="icon clock">
                                <use xlink:href="#images--svg--clock"></use>
                            </svg>
                            <span>Length:</span>
                        </div>
                        <div class="course-info__value">
                            <span>3 weeks</span>
                        </div>
                    </div>
                    <div class="course-info__cell">
                        <div class="course-info__name">
                            <svg class="icon effort">
                                <use xlink:href="#images--svg--effort"></use>
                            </svg>
                            <span>Effort:</span>
                        </div>
                        <div class="course-info__value">
                            <span>2-3 hours per week</span>
                        </div>
                    </div>
                    <div class="course-info__cell">
                        <div class="course-info__name">
                            <svg class="icon effort">
                                <use xlink:href="#images--svg--price"></use>
                            </svg>
                            <span>Price:</span>
                        </div>
                        <div class="course-info__value">
                            <span>FREE <br> Add a Verified <br> Certificate for <br> $99 USD</span>
                        </div>
                    </div>
                    <div class="course-info__cell">
                        <div class="course-info__name">
                            <svg class="icon subject">
                                <use xlink:href="#images--svg--subject"></use>
                            </svg>
                            <span>Subject:</span>
                        </div>
                        <div class="course-info__value">
                            <a href="#">Computer Science</a>
                        </div>
                    </div>
                    <div class="course-info__cell">
                        <div class="course-info__name">
                            <svg class="icon institution">
                                <use xlink:href="#images--svg--institution"></use>
                            </svg>
                            <span>Institution:</span>
                        </div>
                        <div class="course-info__value">
                            <a href="#">Microsoft</a>
                        </div>
                    </div>
                    <div class="course-info__cell">
                        <div class="course-info__name">
                            <svg class="icon level">
                                <use xlink:href="#images--svg--level"></use>
                            </svg>
                            <span>Level:</span>
                        </div>
                        <div class="course-info__value">
                            <span>Intermediate</span>
                        </div>
                    </div>
                    <div class="course-info__cell">
                        <div class="course-info__name">
                            <svg class="icon chat">
                                <use xlink:href="#images--svg--chat"></use>
                            </svg>
                            <span>Language:</span>
                        </div>
                        <div class="course-info__value">
                            <span>English</span>
                        </div>
                    </div>
                    <div class="course-info__cell">
                        <div class="course-info__name">
                            <svg class="icon video">
                                <use xlink:href="#images--svg--video"></use>
                            </svg>
                            <span>Video Transcripts:</span>
                        </div>
                        <div class="course-info__value">
                            <span>English</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="instructors">
        <div class="row">
            <div class="column small-12 large-8">
                <div class="b-title">
                    <h3>Meet the instructors</h3>
                </div>
            </div>
            <div class="column small-12 large-8">
                <div class="row small-up-2 medium-up-3 large-up-4">
                    <div class="column">
                        <div class="instructors__card">
                            <div class="instructors__photo">
                                <a href="#"><img src="dist/images/instructors/instructors__photo/person-1.png"
                                                 alt=""></a>
                            </div>
                            <div class="instructors__name">
                                <a href="#">Любовь Дмитриевна Зубова</a>
                            </div>
                            <div class="instructors__branch">
                                <span>Тренер по CISCO</span>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="instructors__card">
                            <div class="instructors__photo">
                                <a href="#"><img src="dist/images/instructors/instructors__photo/person-2.png"
                                                 alt=""></a>
                            </div>
                            <div class="instructors__name">
                                <a href="#">Любовь Дмитриевна Зубова</a>
                            </div>
                            <div class="instructors__branch">
                                <span>Инструктор по продуктам IBM</span>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="instructors__card">
                            <div class="instructors__photo">
                                <a href="#"><img src="dist/images/instructors/instructors__photo/person-3.png"
                                                 alt=""></a>
                            </div>
                            <div class="instructors__name">
                                <a href="#">Любовь Дмитриевна Зубова</a>
                            </div>
                            <div class="instructors__branch">
                                <span>Учитель Java</span>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="instructors__card">
                            <div class="instructors__photo">
                                <a href="#"><img src="dist/images/instructors/instructors__photo/person-4.png"
                                                 alt=""></a>
                            </div>
                            <div class="instructors__name">
                                <a href="#">Любовь Дмитриевна Зубова</a>
                            </div>
                            <div class="instructors__branch">
                                <span>Преподаватель общего профиля</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
</body>
</html>
