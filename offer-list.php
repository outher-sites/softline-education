<? include 'partials/header.php'; ?>

<main>
    <section class="course-header">
        <div class="row">
            <div class="column small-12 medium-7 large-8">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Учебный центр</a></li>
                    <li><a href="#">Вендоры</a></li>
                    <li><a href="#">Microsoft Corporation</a></li>
                </ul>
                <div class="course-header__wrapper">
                    <div class="course-header__title">
                        <h1>Специальные предложения</h1>
                    </div>
                </div>
            </div>
            <div class="column small-12 medium-5 large-3 large-offset-1">
                <div class="feedback-header">
                    <div class="feedback-header__name">
                        <span>Светлана Жученко</span>
                    </div>
                    <div class="feedback-header__photo">
                        <img src="dist/images/feedback__header/feedback-header__photo/photo-1.png" alt="">
                    </div>
                    <div class="feedback-header__position">
                        <span>менеджер</span>
                        <span>интернет-магазина</span>
                    </div>
                    <div class="feedback-header__contacts">
                        <span>8 (800) 200-08-60 доб. 6011</span>
                        <a href="Svetlana.Zhuchenko@softlinegroup.com">Svetlana.Zhuchenko@softlinegroup.com</a>
                    </div>

                    <a href="#" class="button expanded">Связаться сейчас</a>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div style="padding-bottom: 8rem" class="column small-12 large-9">
                <div class="offer-loop">
                    <div class="flex-container">
                        <div class="offer-loop__image">
                            <a href="#">
                                <img src="content/offers/item.jpg" alt="">
                            </a>
                        </div>
                        <div class="offer-loop__content">
                            <h5 class="offer-loop__title"><a href="#">ESET: 50% скидка для учебных заведений и
                                    библиотек</a></h5>
                            <div class="offer-loop__description">
                                <p>Компания ESET предоставляет возможность приобрести все продукты ESET NOD32 для
                                    учебных заведений и библиотек со скидкой 50%.</p>
                            </div>
                        </div>
                    </div>
                    <a class="offer-loop__duration" href="#">
                        <svg class="icon calendar">
                            <use xlink:href="#images--svg--calendar"></use>
                        </svg>
                        Акция действует до: 30 сентября
                        <svg class="icon arrow">
                            <use xlink:href="#images--svg--arrow"></use>
                        </svg>
                    </a>
                </div>
                <div class="offer-loop micr">
                    <div class="flex-container">
                        <div class="offer-loop__image">
                            <a href="#">
                                <img src="content/offers/item.jpg" alt="">
                            </a>
                        </div>
                        <div class="offer-loop__content">
                            <h5 class="offer-loop__title"><a href="#">ESET: 50% скидка для учебных заведений и
                                    библиотек</a></h5>
                            <div class="offer-loop__description">
                                <p>Компания ESET предоставляет возможность приобрести все продукты ESET NOD32 для
                                    учебных заведений и библиотек со скидкой 50%.</p>
                            </div>
                        </div>
                    </div>
                    <a class="offer-loop__duration" href="#">
                        <svg class="icon calendar">
                            <use xlink:href="#images--svg--calendar"></use>
                        </svg>
                        Акция действует до: 30 сентября
                        <svg class="icon arrow">
                            <use xlink:href="#images--svg--arrow"></use>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="column small-12 large-3">
                <aside>
                    <div class="aside-widget">
                        <div class="aside-widget__content">
                            <form action="#">
                                <label for="">Производитель</label>
                                <select class="select2 change-offer">
                                    <option value="all">Все вендоры</option>
                                    <option value="micr">Microsoft Corporation</option>
                                </select>
                            </form>
                        </div>
                    </div>

                    <div class="aside-widget">
                        <div class="aside-widget__title">Будьте в курсе</div>
                        <div class="aside-widget__content">
                            <p>Подпишитесь на рассылку новостей Softline</p>
                            <form action="#">
                                <label for="">
                                    <input type="text" placeholder="Ваш e-mail">
                                </label>
                                <label for="">
                                    <input type="text" placeholder="Ваше имя">
                                </label>
                                <button class="button small expanded">Подписаться</button>
                            </form>
                        </div>
                    </div>


                    <div class="aside-widget">
                        <div class="aside-widget__content">
                            <a href="#">
                                <img src="content/banners/kasp.jpg" alt="">
                            </a>
                        </div>
                    </div>

                </aside>
            </div>
        </div>
    </section>
</main>

<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        $('.select2').select2();
        $('.select2-selection__arrow').html('<svg class="icon arrow"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#images--svg--arrow"></use></svg>');

        var $offers = $('.offer-loop');

        $('.change-offer').on('change', function (e) {
            if (this.value == 'all') {
                $offers.stop().show()
            } else {
                $offers.filter('.' + this.value).stop().show();
                $offers.not('.' + this.value).stop().hide();
            }
        });
    })
</script>
</body>
</html>

