<? include 'partials/header.php'; ?>

<main>
    <section class="course-header no-margin">
        <div class="row">
            <div class="column small-12 medium-7 large-8">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Учебный центр</a></li>
                    <li><a href="#">Сертификация и экзамены</a></li>
                </ul>
                <div class="course-header__wrapper">
                    <div class="course-header__title">
                        <h1>Ученбый центр Softline</h1>
                    </div>
                </div>
            </div>
            <div class="column small-12 medium-5 large-3 large-offset-1">
                <div class="feedback-header">
                    <div class="feedback-header__name">
                        <span>Светлана Жученко</span>
                    </div>
                    <div class="feedback-header__photo">
                        <img src="dist/images/feedback__header/feedback-header__photo/photo-1.png" alt="">
                    </div>
                    <div class="feedback-header__position">
                        <span>менеджер</span>
                        <span>интернет-магазина</span>
                    </div>
                    <div class="feedback-header__contacts">
                        <span>8 (800) 200-08-60 доб. 6011</span>
                        <a href="mailto:Svetlana.Zhuchenko@softlinegroup.com">Svetlana.Zhuchenko@softlinegroup.com</a>
                    </div>

                    <a href="#" class="button expanded">Связаться сейчас</a>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="row">
           <div class="column small-12 large-8">
               <div class="instructors">
                           <div class="row small-up-2 medium-up-3 large-up-4">
                               <div class="column">
                                   <div class="instructors__card">
                                       <div class="instructors__photo">
                                           <a href="#"><img src="dist/images/instructors/instructors__photo/person-1.png"
                                                            alt=""></a>
                                       </div>
                                       <div class="instructors__name">
                                           <a href="#">Любовь Дмитриевна Зубова</a>
                                       </div>
                                       <div class="instructors__branch">
                                           <span>Тренер по CISCO</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="column">
                                   <div class="instructors__card">
                                       <div class="instructors__photo">
                                           <a href="#"><img src="dist/images/instructors/instructors__photo/person-2.png"
                                                            alt=""></a>
                                       </div>
                                       <div class="instructors__name">
                                           <a href="#">Любовь Дмитриевна Зубова</a>
                                       </div>
                                       <div class="instructors__branch">
                                           <span>Инструктор по продуктам IBM</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="column">
                                   <div class="instructors__card">
                                       <div class="instructors__photo">
                                           <a href="#"><img src="dist/images/instructors/instructors__photo/person-3.png"
                                                            alt=""></a>
                                       </div>
                                       <div class="instructors__name">
                                           <a href="#">Любовь Дмитриевна Зубова</a>
                                       </div>
                                       <div class="instructors__branch">
                                           <span>Учитель Java</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="column">
                                   <div class="instructors__card">
                                       <div class="instructors__photo">
                                           <a href="#"><img src="dist/images/instructors/instructors__photo/person-4.png"
                                                            alt=""></a>
                                       </div>
                                       <div class="instructors__name">
                                           <a href="#">Любовь Дмитриевна Зубова</a>
                                       </div>
                                       <div class="instructors__branch">
                                           <span>Преподаватель общего профиля</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="column">
                                   <div class="instructors__card">
                                       <div class="instructors__photo">
                                           <a href="#"><img src="dist/images/instructors/instructors__photo/person-1.png"
                                                            alt=""></a>
                                       </div>
                                       <div class="instructors__name">
                                           <a href="#">Любовь Дмитриевна Зубова</a>
                                       </div>
                                       <div class="instructors__branch">
                                           <span>Тренер по CISCO</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="column">
                                   <div class="instructors__card">
                                       <div class="instructors__photo">
                                           <a href="#"><img src="dist/images/instructors/instructors__photo/person-2.png"
                                                            alt=""></a>
                                       </div>
                                       <div class="instructors__name">
                                           <a href="#">Любовь Дмитриевна Зубова</a>
                                       </div>
                                       <div class="instructors__branch">
                                           <span>Инструктор по продуктам IBM</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="column">
                                   <div class="instructors__card">
                                       <div class="instructors__photo">
                                           <a href="#"><img src="dist/images/instructors/instructors__photo/person-3.png"
                                                            alt=""></a>
                                       </div>
                                       <div class="instructors__name">
                                           <a href="#">Любовь Дмитриевна Зубова</a>
                                       </div>
                                       <div class="instructors__branch">
                                           <span>Учитель Java</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="column">
                                   <div class="instructors__card">
                                       <div class="instructors__photo">
                                           <a href="#"><img src="dist/images/instructors/instructors__photo/person-4.png"
                                                            alt=""></a>
                                       </div>
                                       <div class="instructors__name">
                                           <a href="#">Любовь Дмитриевна Зубова</a>
                                       </div>
                                       <div class="instructors__branch">
                                           <span>Преподаватель общего профиля</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="column">
                                   <div class="instructors__card">
                                       <div class="instructors__photo">
                                           <a href="#"><img src="dist/images/instructors/instructors__photo/person-1.png"
                                                            alt=""></a>
                                       </div>
                                       <div class="instructors__name">
                                           <a href="#">Любовь Дмитриевна Зубова</a>
                                       </div>
                                       <div class="instructors__branch">
                                           <span>Тренер по CISCO</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="column">
                                   <div class="instructors__card">
                                       <div class="instructors__photo">
                                           <a href="#"><img src="dist/images/instructors/instructors__photo/person-2.png"
                                                            alt=""></a>
                                       </div>
                                       <div class="instructors__name">
                                           <a href="#">Любовь Дмитриевна Зубова</a>
                                       </div>
                                       <div class="instructors__branch">
                                           <span>Инструктор по продуктам IBM</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="column">
                                   <div class="instructors__card">
                                       <div class="instructors__photo">
                                           <a href="#"><img src="dist/images/instructors/instructors__photo/person-3.png"
                                                            alt=""></a>
                                       </div>
                                       <div class="instructors__name">
                                           <a href="#">Любовь Дмитриевна Зубова</a>
                                       </div>
                                       <div class="instructors__branch">
                                           <span>Учитель Java</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="column">
                                   <div class="instructors__card">
                                       <div class="instructors__photo">
                                           <a href="#"><img src="dist/images/instructors/instructors__photo/person-4.png"
                                                            alt=""></a>
                                       </div>
                                       <div class="instructors__name">
                                           <a href="#">Любовь Дмитриевна Зубова</a>
                                       </div>
                                       <div class="instructors__branch">
                                           <span>Преподаватель общего профиля</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="column">
                                   <div class="instructors__card">
                                       <div class="instructors__photo">
                                           <a href="#"><img src="dist/images/instructors/instructors__photo/person-1.png"
                                                            alt=""></a>
                                       </div>
                                       <div class="instructors__name">
                                           <a href="#">Любовь Дмитриевна Зубова</a>
                                       </div>
                                       <div class="instructors__branch">
                                           <span>Тренер по CISCO</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="column">
                                   <div class="instructors__card">
                                       <div class="instructors__photo">
                                           <a href="#"><img src="dist/images/instructors/instructors__photo/person-2.png"
                                                            alt=""></a>
                                       </div>
                                       <div class="instructors__name">
                                           <a href="#">Любовь Дмитриевна Зубова</a>
                                       </div>
                                       <div class="instructors__branch">
                                           <span>Инструктор по продуктам IBM</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="column">
                                   <div class="instructors__card">
                                       <div class="instructors__photo">
                                           <a href="#"><img src="dist/images/instructors/instructors__photo/person-3.png"
                                                            alt=""></a>
                                       </div>
                                       <div class="instructors__name">
                                           <a href="#">Любовь Дмитриевна Зубова</a>
                                       </div>
                                       <div class="instructors__branch">
                                           <span>Учитель Java</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="column">
                                   <div class="instructors__card">
                                       <div class="instructors__photo">
                                           <a href="#"><img src="dist/images/instructors/instructors__photo/person-4.png"
                                                            alt=""></a>
                                       </div>
                                       <div class="instructors__name">
                                           <a href="#">Любовь Дмитриевна Зубова</a>
                                       </div>
                                       <div class="instructors__branch">
                                           <span>Преподаватель общего профиля</span>
                                       </div>
                                   </div>
                               </div>
                           </div>
               </div>
               <div class="pagination-wrapper">
                   <ul class="pagination" role="navigation" aria-label="Pagination">
                       <li class="pagination-previous disabled"></li>
                       <li class="current"><span class="show-for-sr">You're on page</span> 1</li>
                       <li><a href="#" aria-label="Page 2">2</a></li>
                       <li><a href="#" aria-label="Page 3">3</a></li>
                       <li><a href="#" aria-label="Page 4">4</a></li>
                       <li class="ellipsis" aria-hidden="true"></li>
                       <li><a href="#" aria-label="Page 12">12</a></li>
                       <li><a href="#" aria-label="Page 13">13</a></li>
                       <li class="pagination-next"><a href="#" aria-label="Next page"></a></li>
                   </ul>

                   <div class="pagination-select show-for-large">
                       <span>Элем. на стр.</span>
                       <label class="b-select">
                           <select class="b-select__container">
                               <option value="24">24</option>
                               <option value="50">50</option>
                           </select>
                       </label>
                   </div>
               </div>
           </div>
            <div style="padding-top: 7rem" class="column small-12 large-offset-1 large-3">
                <aside>
                    <div class="aside-widget">
                        <div class="aside-widget__title">О центре</div>
                        <div class="aside-widget__content">
                            <ul class="aside-menu">


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/mission">

                                        <span class="b-link__text">Программирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/training-conditions">

                                        <span class="b-link__text">Системное администрирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class=""
                                       href="http://edu.softline.ru/about/svedeniya-ob-obrazovatelnoy-organizatsii">

                                        <span class="b-link__text">Пользовательское ПО</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/accreditation">

                                        <span class="b-link__text">Операционные системы (ОС)</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/status">

                                        <span class="b-link__text">Сетевые технологии</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-klientov">

                                        <span class="b-link__text">Информационная безопасность</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-partnerov">

                                        <span class="b-link__text">Виртуализация</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/razrabotka-kursov">

                                        <span class="b-link__text">Базы данных и СУБД</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/delivery-info">

                                        <span class="b-link__text">Моделирование и САПР</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/requisites">

                                        <span class="b-link__text">Коммуникации</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/tech-support">

                                        <span class="b-link__text">Резервное копирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">CRM</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Бизнес-аналитика</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">IT-сервис менеджмент</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Импортозамещение ПО</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Бизнес-тренинги</span>
                                    </a>

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__content">
                        <div class="filter expanded">
                            <div class="filter__title">
                                <h3>Подбор курса:</h3>
                            </div>
                            <div class="filter__input">
                                <div class="filter__label">
                                    <span>Направления:</span>
                                </div>
                                <label class="b-select">
                                    <select class="b-select__container">
                                        <option value="">Все направления</option>
                                        <option value="">Все направления</option>
                                        <option value="">Все направления</option>
                                    </select>
                                </label>
                            </div>
                            <div class="filter__input">
                                <div class="filter__label">
                                    <span>Cпециальность:</span>
                                </div>
                                <label class="b-select">
                                    <select class="b-select__container">
                                        <option value="">Все специальности</option>
                                        <option value="">Все специальности</option>
                                        <option value="">Все специальности</option>
                                    </select>
                                </label>
                            </div>
                            <div class="filter__input">
                                <div class="filter__label">
                                    <span>Вендор:</span>
                                </div>
                                <label class="b-select">
                                    <select class="b-select__container">
                                        <option value="">Все вендоры</option>
                                        <option value="">Все вендоры</option>
                                        <option value="">Все вендоры</option>
                                    </select>
                                </label>
                                <label class="b-radiobox">
                                    <input name="example-1" type="radio">
                                    <span>Очно (в аудитории)</span>
                                </label>
                                <label class="b-radiobox">
                                    <input name="example-1" type="radio">
                                    <span>Дистанционно</span>
                                </label>

                            </div>
                            <div class="filter__input">
                                <div class="filter__label">
                                    <span>Место проведения:</span>
                                </div>
                                <label class="b-select">
                                    <select class="b-select__container">
                                        <option value="">Москва</option>
                                        <option value="">Москва</option>
                                        <option value="">Москва</option>
                                    </select>
                                </label>
                            </div>

                        </div>
                        <button class="button small expanded">Подобрать курс</button>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__title">Будьте в курсе</div>
                        <div class="aside-widget__content">
                            <p>Подпишитесь на рассылку новостей Softline</p>
                            <form action="#">
                                <label for="">
                                    <input type="text" placeholder="Ваш e-mail">
                                </label>
                                <label for="">
                                    <input type="text" placeholder="Ваше имя">
                                </label>
                                <button class="button small expanded">Подписаться</button>
                            </form>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__content">
                            <a href="#">
                                <img src="content/banners/kasp.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>

    <section class="subscribe-banner">
        <div class="subscribe-banner__wrapper">
            <div class="row">
                <div class="column small-12 large-9 large-offset-3">
                    <div class="subscribe-banner__title">
                        <span>Будьте в курсе</span>
                    </div>
                    <form class="subscribe-banner__form">
                        <div class="subscribe-banner__input subscribe-banner__input--name">
                            <!--доп модификатор: error-->
                            <input type="text" placeholder="Ваше имя">
                        </div>
                        <div class="subscribe-banner__input subscribe-banner__input--email">
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="subscribe-banner__button">
                            <button class="button">Подписаться</button>
                        </div>
                    </form>
                    <div class="subscribe-banner__desc">
                        <span>Подпишитесь на информацию о новинках, скидках и акциях. <br> Уже более 36 000 подписчиков!</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        $('.b-select__container').select2();
        $(document).foundation();
    });
</script>
</body>
</html>
