<? include 'partials/header.php'; ?>

<main>
    <section style="padding-bottom: 30px" class="course-header no-margin no-border-btm">
        <div class="row">
            <div class="column small-12">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Учебный центр</a></li>
                    <li><a href="#">Вендоры</a></li>
                    <li><a href="#">Microsoft Corporation</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="column small-12">
                <div class="course-header__wrapper course-header__wrapper--column">
                    <div class="course-header__title">
                        <h1>Выбор курса</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="column small-12 large-8">

                <div class="event-filter">
                    <p>Фильтр по событиям:</p>
                    <form>
                        <label class="b-select">
                            <select class="b-select__container">
                                <option value="">Все направления</option>
                                <option value="">Все направления</option>
                                <option value="">Все направления</option>

                            </select>
                        </label>
                        <label class="b-select">
                            <select class="b-select__container">
                                <option value="">Все специальности</option>
                                <option value="">Все специальности</option>
                                <option value="">Все специальности</option>

                            </select>
                        </label>
                        <label class="b-select">
                            <select class="b-select__container">
                                <option value="">Все вендоры</option>
                                <option value="">Все вендоры</option>
                                <option value="">Все вендоры</option>
                            </select>
                        </label>
                    </form>
                    <form>
                        <label class="b-radiobox">
                            <input name="event-filter" type="radio">
                            <span>Все</span>
                        </label>
                        <label class="b-radiobox">
                            <input name="event-filter" type="radio">
                            <span>Очно (в аудитории)</span>
                        </label>
                        <label class="b-radiobox">
                            <input name="event-filter" type="radio">
                            <span>Дистанционно</span>
                        </label>

                        <label class="b-select city">
                            <select class="b-select__container">
                                <option value="">Все города</option>
                                <option value="">Все города</option>
                                <option value="">Все города</option>
                            </select>
                        </label>
                    </form>
                </div>
            </div>
            <div class="column small-12 large-4">
                <div class="event-filter">
                    <p class="text-center">Даты (от – до):</p>
                    <form>
                        <label class="date-input">
                            <input value="24.08.12" type="text">
                        </label>
                        <label class="date-input">
                            <input value="24.08.12" type="text">
                        </label>
                    </form>
                    <div style="justify-content: center">
                        <a href="#">2 недели</a>
                        <a href="#">Месяц</a>
                        <a href="#">2 месяца</a>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="calendar">
        <div class="calendar__menu show-for-large">
            <div class="row">
                <div class="column small-12">
                    <ul class="calendar__nav">
                        <li>
                            <a href="#">
                                <span>Сентябрь</span>
                                <span>Октябрь</span>
                                <span>Ноябрь</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>Декабрь</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>Январь</span>
                                <span>Февраль</span>
                                <span>Март</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>Апрель</span>
                                <span>Май</span>
                                <span>Июнь</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>Июль</span>
                                <span>Август</span>
                                <span>Сентябрь</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="calendar__wrapper">
            <div class="row small-up-1 medium-up-2 large-up-3">
                <div class="column">
                    <div class="calendar-table">
                        <div class="calendar-table__title text-center">
                            <span>Сентябрь</span>
                        </div>
                        <div class="calendar-table__row --days">
                            <div><span>пн</span></div>
                            <div><span>вт</span></div>
                            <div><span>ср</span></div>
                            <div><span>чт</span></div>
                            <div><span>пт</span></div>
                            <div><span>сб</span></div>
                            <div><span>вс</span></div>
                        </div>
                        <div class="calendar-table__row --unactive">
                            <div><a href="#">28</a></div>
                            <div><a href="#">29</a></div>
                            <div><a href="#">30</a></div>
                            <div><a href="#">1</a></div>
                            <div><a href="#">2</a></div>
                            <div><a href="#">3</a></div>
                            <div><a href="#">4</a></div>
                            <div><a href="#">5</a></div>
                            <div><a href="#">6</a></div>
                            <div><a href="#">7</a></div>
                            <div><a href="#">8</a></div>
                            <div><a href="#">9</a></div>
                            <div><a href="#">10</a></div>
                            <div><a href="#">11</a></div>
                            <div><a href="#">12</a></div>
                            <div><a href="#">13</a></div>
                            <div><a href="#">14</a></div>
                            <div><a href="#">15</a></div>
                            <div><a href="#">16</a></div>
                            <div><a href="#">17</a></div>
                            <div><a href="#">18</a></div>
                            <div><a href="#">19</a></div>
                            <div><a href="#">20</a></div>
                            <div><a href="#">21</a></div>
                            <div><a href="#">22</a></div>
                            <div><a href="#">23</a></div>
                            <div><a href="#">24</a></div>
                            <div><a href="#">25</a></div>
                            <div><a href="#">26</a></div>
                            <div><a href="#">27</a></div>
                            <div class="text-black"><a href="#">28</a></div>
                            <div class="text-black"><a href="#">29</a></div>
                            <div class="text-red"><a href="#">30</a></div>
                            <div class="text-red"><a href="#">1</a></div>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="calendar-table">
                        <div class="calendar-table__title text-center">
                            <span>Ноябрь</span>
                        </div>
                        <div class="calendar-table__row --days">
                            <div><span>пн</span></div>
                            <div><span>вт</span></div>
                            <div><span>ср</span></div>
                            <div><span>чт</span></div>
                            <div><span>пт</span></div>
                            <div><span>сб</span></div>
                            <div><span>вс</span></div>
                        </div>
                        <div class="calendar-table__row">
                            <div><a href="#">28</a></div>
                            <div><a href="#">29</a></div>
                            <div><a href="#">30</a></div>
                            <div><a href="#">1</a></div>
                            <div><a href="#">2</a></div>
                            <div class="green  text-red"><a href="#">3</a></div>
                            <div class="green  text-red"><a href="#">4</a></div>
                            <div class="green"><a href="#">5</a></div>
                            <div class="green"><a href="#">6</a></div>
                            <div class="green"><a href="#">7</a></div>
                            <div class="green"><a href="#">8</a></div>
                            <div class="green"><a href="#">9</a></div>
                            <div class="green  text-red"><a href="#">10</a></div>
                            <div class="green  text-red"><a href="#">11</a></div>
                            <div class="green"><a href="#">12</a></div>
                            <div class="green"><a href="#">13</a></div>
                            <div class="green"><a href="#">14</a></div>
                            <div class="green"><a href="#">15</a></div>
                            <div class="green"><a href="#">16</a></div>
                            <div class="green  text-red"><a href="#">17</a></div>
                            <div class="green  text-red"><a href="#">18</a></div>
                            <div class="green"><a href="#">19</a></div>
                            <div class="green"><a href="#">20</a></div>
                            <div class="green"><a href="#">21</a></div>
                            <div class="green"><a href="#">22</a></div>
                            <div><a href="#">23</a></div>
                            <div class="text-red"><a href="#">24</a></div>
                            <div class="text-red"><a href="#">25</a></div>
                            <div><a href="#">26</a></div>
                            <div><a href="#">27</a></div>
                            <div><a href="#">28</a></div>
                            <div><a href="#">29</a></div>
                            <div><a href="#">30</a></div>
                            <div class="text-red"><a href="#">1</a></div>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="calendar-table">
                        <div class="calendar-table__title text-center">
                            <span>Декабрь</span>
                        </div>
                        <div class="calendar-table__row --days">
                            <div><span>пн</span></div>
                            <div><span>вт</span></div>
                            <div><span>ср</span></div>
                            <div><span>чт</span></div>
                            <div><span>пт</span></div>
                            <div><span>сб</span></div>
                            <div><span>вс</span></div>
                        </div>
                        <div class="calendar-table__row">
                            <div><a href="#">28</a></div>
                            <div><a href="#">29</a></div>
                            <div><a href="#">30</a></div>
                            <div><a href="#">1</a></div>
                            <div><a href="#">2</a></div>
                            <div class="text-red"><a href="#">3</a></div>
                            <div class="text-red"><a href="#">4</a></div>
                            <div><a href="#">5</a></div>
                            <div><a href="#">6</a></div>
                            <div><a href="#">7</a></div>
                            <div><a href="#">8</a></div>
                            <div><a href="#">9</a></div>
                            <div class="text-red"><a href="#">10</a></div>
                            <div class="text-red"><a href="#">11</a></div>
                            <div><a href="#">12</a></div>
                            <div><a href="#">13</a></div>
                            <div><a href="#">14</a></div>
                            <div><a href="#">15</a></div>
                            <div><a href="#">16</a></div>
                            <div class="text-red"><a href="#">17</a></div>
                            <div class="text-red"><a href="#">18</a></div>
                            <div><a href="#">19</a></div>
                            <div><a href="#">20</a></div>
                            <div><a href="#">21</a></div>
                            <div><a href="#">22</a></div>
                            <div><a href="#">23</a></div>
                            <div class="text-red"><a href="#">24</a></div>
                            <div class="text-red"><a href="#">25</a></div>
                            <div><a href="#">26</a></div>
                            <div><a href="#">27</a></div>
                            <div><a href="#">28</a></div>
                            <div><a href="#">29</a></div>
                            <div><a href="#">30</a></div>
                            <div class="text-red"><a href="#">1</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="row">
            <div style="text-transform: uppercase" class="column small-12">
                <hr style="margin-bottom: 40px">
                <h3>Курсов в этот период: 24</h3>
            </div>
            <div class="column small-12">
                <table width="100%" cellpadding="0" cellspacing="0" class="course-table">
                    <tr>
                        <td class="course-table__img"></td>
                        <td class="course-table__name"><span>Название курса</span></td>
                        <td class="course-table__place"><span>Место обучения</span></td>
                        <td class="course-table__format"><span>Формат</span></td>
                        <td class="course-table__date"><span>Дата и время</span></td>
                        <td class="course-table__price"><span>Стоимость</span></td>
                        <td class="course-table__basket"></td>
                    </tr>
                    <tr>
                        <td class="course-table__img">
                            <span>
                                <img src="dist/images/course-table/java.png" alt="">
                            </span>
                        </td>
                        <td class="course-table__name"><a href="#">Java Standard Edition (Java SE). Программирование.
                                Базовые технологии</a></td>
                        <td class="course-table__place"><p>Дистанционно</p></td>
                        <td class="course-table__format">
                            <p>Очный</p>
                            <p><span>с применением дистанционных технологий</span></p>
                        </td>
                        <td class="course-table__date">
                            <p>25 ноября — <br class="show-for-large"> 3 декабря 2017</p>
                        </td>
                        <td class="course-table__price">
                            <p>17 375 руб.</p>
                            <p><span>19 305 руб.</span></p>
                            <p style="color: #37b63f; padding-top: 10px">скидка 10%</p>
                        </td>
                        <td class="course-table__basket">
                            <a href="#">в корзину</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="course-table__img">
                            <span>
                                <img src="dist/images/course-table/java.png" alt="">
                            </span>
                        </td>
                        <td class="course-table__name"><a href="#">Java Standard Edition (Java SE). Программирование.
                                Базовые технологии</a></td>
                        <td class="course-table__place">
                            <p>Москва, Москва, 2-ой Южнопортовый проезд, дом 31, стр. 1</p>
                            <p>Схема проезда и фото</p>
                        </td>
                        <td class="course-table__format">
                            <p>Очный</p>
                            <p><span>с применением дистанционных технологий</span></p>
                        </td>
                        <td class="course-table__date">
                            <p>25 ноября — <br class="show-for-large"> 3 декабря 2017</p>
                        </td>
                        <td class="course-table__price">
                            <p>17 375 руб.</p>
                            <p><span>19 305 руб.</span></p>
                            <p style="color: #37b63f; padding-top: 10px">скидка 10%</p>
                        </td>
                        <td class="course-table__basket">
                            <a href="#">в корзину</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="course-table__img">
                            <span>
                                <img src="dist/images/course-table/java.png" alt="">
                            </span>
                        </td>
                        <td class="course-table__name"><a href="#">Java Standard Edition (Java SE). Программирование.
                                Базовые технологии</a></td>
                        <td class="course-table__place">
                            <p>Москва, Москва, 2-ой Южнопортовый проезд, дом 31, стр. 1</p>
                            <p>Схема проезда и фото</p>
                        </td>
                        <td class="course-table__format">
                            <p>Очный</p>
                            <p><span>с применением дистанционных технологий</span></p>
                        </td>
                        <td class="course-table__date">
                            <p>25 ноября — <br class="show-for-large"> 3 декабря 2017</p>
                        </td>
                        <td class="course-table__price">
                            <p>17 375 руб.</p>
                            <p><span>19 305 руб.</span></p>
                            <p style="color: #37b63f; padding-top: 10px">скидка 10%</p>
                        </td>
                        <td class="course-table__basket">
                            <a href="#">в корзину</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </section>
</main>

<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>

<script>
    $(document).ready(function () {
        $('.b-select__container').select2();
    });
</script>
</body>
</html>
