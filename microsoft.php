<? include 'partials/header.php'; ?>

<main>
    <section class="course-header">
        <div class="row">
            <div class="column small-12 medium-7 large-8">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Учебный центр</a></li>
                    <li><a href="#">Сертификация и экзамены</a></li>
                </ul>
                <div class="course-header__wrapper">
                    <div class="course-header__title">
                        <h1>Курсы <br class="show-for-large">
                            Microsoft Corporation</h1>
                    </div>
                </div>
            </div>
            <div class="column small-12 medium-5 large-3 large-offset-1">
                <div class="feedback-header">
                    <div class="feedback-header__name">
                        <span>Светлана Жученко</span>
                    </div>
                    <div class="feedback-header__photo">
                        <img src="dist/images/feedback__header/feedback-header__photo/photo-1.png" alt="">
                    </div>
                    <div class="feedback-header__position">
                        <span>менеджер</span>
                        <span>интернет-магазина</span>
                    </div>
                    <div class="feedback-header__contacts">
                        <span>8 (800) 200-08-60 доб. 6011</span>
                        <a href="Svetlana.Zhuchenko@softlinegroup.com">Svetlana.Zhuchenko@softlinegroup.com</a>
                    </div>

                    <a href="#" class="button expanded">Связаться сейчас</a>
                </div>
            </div>
        </div>
    </section>
    <section class="about-course">
        <div class="row">
            <div class="column small-12 large-2">
                <div class="about-course__image">
                    <a href="#"><img src="/content/about-course/ms-gold-logo.png" alt=""></a>
                </div>
            </div>
            <div class="column small-12 large-offset-1 large-9">
                <div class="about-course__container">
                    <div class="b-editor">
                        <p>Softline является золотым партнером Microsoft и проводит курсы в соответствии со всеми
                            официальными требованиями и рекомендациями вендора. По окончанию курсов выдается официальный
                            сертификат вендора, а также почетная грамота и медаль. Это тестовый текст, который подлежит
                            замене, поэтому я позволил себе немного разбавить его серьезность. Это поможет не забыть
                            сменить его, перед запуском сайта.</p>

                        <p><a href="#">
                                <img src="content/about-course/example-doc.jpg" alt="">
                            </a></p>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="tab-box tab-box--border-btm">
        <div class="tabs-list-wrapper">
            <div class="row">
                <div class="column small-12 large-9 large-offset-3">
                    <ul class="tabs" data-tabs id="example-tabs">
                        <li class="tabs-title is-active"><a href="#panel3" aria-selected="true">Курсы</a></li>
                        <li class="tabs-title"><a href="#panel1" aria-selected="true">Сертификация</a></li>
                        <li class="tabs-title"><a data-tabs-target="panel2" href="#panel2">Экзамены</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column small-12 large-3">
                <div style="padding-top: 50px" class="filter">
                    <div class="filter__input">
                        <div class="filter__label">
                            <span>Вендор:</span>
                        </div>
                        <label class="b-select">
                            <select class="b-select__container">
                                <option value="">Все вендоры</option>
                                <option value="">Все вендоры</option>
                                <option value="">Все вендоры</option>
                            </select>
                        </label>
                    </div>
                    <div class="filter__input">
                        <div class="filter__label">
                            <span>Продукты:</span>
                        </div>
                        <label class="b-select">
                            <select class="b-select__container">
                                <option value="">Все продукты</option>
                                <option value="">Все продукты</option>
                                <option value="">Все продукты</option>
                            </select>
                        </label>
                    </div>
                    <hr>
                    <div class="filter__input">
                        <div class="filter__label">
                            <span>Подпишитесь на рассылку новостей Softline</span>
                        </div>
                        <label>
                            <input placeholder="Ваш e-mail" type="text">
                        </label>
                        <label>
                            <input placeholder="Ваше имя" type="text">
                        </label>
                    </div>

                    <button class="button">Подписаться</button>
                </div>
            </div>
            <div class="column small-12  large-9">

                <div style="padding-top: 44px" class="tabs-content" data-tabs-content="example-tabs">
                    <div class="tabs-panel is-active" id="panel3">
                        <div class="row small-up-1 large-up-2">
                            <div class="column">
                                <div class="course-card">
                                        <div class="course-card__sticker">
                                        <div class="sticker red">
                                            <span>топ 5</span>
                                        </div>
                                        <div class="sticker green">
                                            <span>скидка 10%</span>
                                        </div>
                                    </div>
                                    <div class="course-card__container">
                                        <div class="course-card__photo">
                                            <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                                        </div>
                                        <div class="course-card__cell">
                                            <div class="course-card__top">
                                                <div class="course-card__title">
                                            <span>
                                                Java Standard Edition (Java SE).
                                               <br> Программирование. Базовые технологии
                                            </span>
                                                </div>
                                            </div>
                                            <div class="course-card__wrap">
                                                <div class="course-card__details">
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Производитель:</span><span>Формат обучения:</span>
                                                            </li>

                                                            <li><span>Java</span>
                                                                <span>
                                                                    <img src="dist/images/course-card/icon-1.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-2.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-3.png"
                                                                         alt="">
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Уровень:</span><span>Цена (от):</span></li>

                                                            <li><span>начальный</span><span>35 100,00 руб.</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="course-card__desc">
                                                    <p>
                                                        В предлагаемой программе даётся обзор платформ: Java Standard
                                                        Edition
                                                        (J2SE/Java SE 8) и Java Enterprise Edition (J2EE / Java EE 7)
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="course-card__button">
                                        <span>Расписание и цены</span>

                                        <svg>
                                            <use xlink:href="#images--svg--arrow"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                            <div class="column">
                                <div class="course-card">
                                        <div class="course-card__sticker">
                                        <div class="sticker red">
                                            <span>топ 5</span>
                                        </div>
                                        <div class="sticker green">
                                            <span>скидка 10%</span>
                                        </div>
                                    </div>
                                    <div class="course-card__container">
                                        <div class="course-card__photo">
                                            <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                                        </div>
                                        <div class="course-card__cell">
                                            <div class="course-card__top">
                                                <div class="course-card__title">
                                            <span>
                                                Java Standard Edition (Java SE).
                                               <br> Программирование. Базовые технологии
                                            </span>
                                                </div>
                                            </div>
                                            <div class="course-card__wrap">
                                                <div class="course-card__details">
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Производитель:</span><span>Формат обучения:</span>
                                                            </li>

                                                            <li><span>Java</span>
                                                                <span>
                                                                    <img src="dist/images/course-card/icon-1.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-2.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-3.png"
                                                                         alt="">
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Уровень:</span><span>Цена (от):</span></li>

                                                            <li><span>начальный</span><span>35 100,00 руб.</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="course-card__desc">
                                                    <p>
                                                        В предлагаемой программе даётся обзор платформ: Java Standard
                                                        Edition
                                                        (J2SE/Java SE 8) и Java Enterprise Edition (J2EE / Java EE 7)
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="course-card__button">
                                        <span>Расписание и цены</span>

                                        <svg>
                                            <use xlink:href="#images--svg--arrow"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>

                            <div class="column">
                                <div class="course-card">
                                        <div class="course-card__sticker">
                                        <div class="sticker red">
                                            <span>топ 5</span>
                                        </div>
                                        <div class="sticker green">
                                            <span>скидка 10%</span>
                                        </div>
                                    </div>
                                    <div class="course-card__container">
                                        <div class="course-card__photo">
                                            <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                                        </div>
                                        <div class="course-card__cell">
                                            <div class="course-card__top">
                                                <div class="course-card__title">
                                            <span>
                                                Java Standard Edition (Java SE).
                                               <br> Программирование. Базовые технологии
                                            </span>
                                                </div>
                                            </div>
                                            <div class="course-card__wrap">
                                                <div class="course-card__details">
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Производитель:</span><span>Формат обучения:</span>
                                                            </li>

                                                            <li><span>Java</span>
                                                                <span>
                                                                    <img src="dist/images/course-card/icon-1.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-2.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-3.png"
                                                                         alt="">
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Уровень:</span><span>Цена (от):</span></li>

                                                            <li><span>начальный</span><span>35 100,00 руб.</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="course-card__desc">
                                                    <p>
                                                        В предлагаемой программе даётся обзор платформ: Java Standard
                                                        Edition
                                                        (J2SE/Java SE 8) и Java Enterprise Edition (J2EE / Java EE 7)
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="course-card__button">
                                        <span>Расписание и цены</span>

                                        <svg>
                                            <use xlink:href="#images--svg--arrow"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>

                            <div class="column">
                                <div class="course-card">
                                        <div class="course-card__sticker">
                                        <div class="sticker red">
                                            <span>топ 5</span>
                                        </div>
                                        <div class="sticker green">
                                            <span>скидка 10%</span>
                                        </div>
                                    </div>
                                    <div class="course-card__container">
                                        <div class="course-card__photo">
                                            <img src="dist/images/course-card/course-card__photo/java.jpg" alt="">
                                        </div>
                                        <div class="course-card__cell">
                                            <div class="course-card__top">
                                                <div class="course-card__title">
                                            <span>
                                                Java Standard Edition (Java SE).
                                               <br> Программирование. Базовые технологии
                                            </span>
                                                </div>
                                            </div>
                                            <div class="course-card__wrap">
                                                <div class="course-card__details">
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Производитель:</span><span>Формат обучения:</span>
                                                            </li>

                                                            <li><span>Java</span>
                                                                <span>
                                                                    <img src="dist/images/course-card/icon-1.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-2.png"
                                                                         alt="">
                                                                    <img src="dist/images/course-card/icon-3.png"
                                                                         alt="">
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="course-card__column">
                                                        <ul>
                                                            <li><span>Уровень:</span><span>Цена (от):</span></li>

                                                            <li><span>начальный</span><span>35 100,00 руб.</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="course-card__desc">
                                                    <p>
                                                        В предлагаемой программе даётся обзор платформ: Java Standard
                                                        Edition
                                                        (J2SE/Java SE 8) и Java Enterprise Edition (J2EE / Java EE 7)
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="course-card__button">
                                        <span>Расписание и цены</span>

                                        <svg>
                                            <use xlink:href="#images--svg--arrow"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="pagination-wrapper">
                            <ul class="pagination" role="navigation" aria-label="Pagination">
                                <li class="pagination-previous disabled"></li>
                                <li class="current"><span class="show-for-sr">You're on page</span> 1</li>
                                <li><a href="#" aria-label="Page 2">2</a></li>
                                <li><a href="#" aria-label="Page 3">3</a></li>
                                <li><a href="#" aria-label="Page 4">4</a></li>
                                <li><a href="#" aria-label="Page 5">5</a></li>
                                <li class="pagination-next"><a href="#" aria-label="Next page"></a></li>
                            </ul>

                            <div class="pagination-select show-for-large">
                                <span>Элем. на стр.</span>
                                <label class="b-select">
                                    <select class="b-select__container">
                                        <option value="24">24</option>
                                        <option value="50">50</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="tabs-panel" id="panel1">
                        <div class="filter inline">
                            <div class="row">
                                <div class="small-12 medium-6 column">
                                    <div class="filter__input">
                                        <div class="filter__label">
                                            <span>Вендор:</span>
                                        </div>
                                        <label class="b-select">
                                            <select class="b-select__container">
                                                <option value="">Все вендоры</option>
                                                <option value="">Все вендоры</option>
                                                <option value="">Все вендоры</option>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="certificate-list">
                            <div class="certificate">
                                <div class="certificate__photo">
                                    <img src="content/sert/layer-299.png" alt="">
                                </div>
                                <div class="certificate__content">
                                    <div class="certificate__title">CCENT</div>
                                    <ul class="breadcrumbs">
                                        <li><a href="#">Cisco Systems, Inc</a></li>
                                        <li><a href="#">Центр тестирования VUE</a></li>
                                    </ul>
                                    <ul class="certificate__prices">
                                        <li class="certificate__price">
                                            <span>Общая цена подготовки (1 курс) <i class="icon icon-help">?</i></span>
                                            <span>53 500 руб</span>
                                        </li>
                                        <li class="certificate__price">
                                            <span>Общая цена сертификации (1 экзамен) <i
                                                        class="icon icon-help">?</i></span>
                                            <span>14 700 руб</span>
                                        </li>
                                    </ul>
                                    <div class="certificate__description b-editor">
                                        <p>
                                            Первый уровень в системе сертификаций Cisco — уровень entry и начинается
                                            он с Сертифицированного Техника Сетей Cisco (Сіsco Certified Entry
                                            Networking Technician) — CCENT. Это — промежуточный шаг к Associate
                                            уровню — сертификациям CCNA и CCDA.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="certificate">
                                <div class="certificate__photo">
                                    <img src="content/sert/layer-299.png" alt="">
                                </div>
                                <div class="certificate__content">
                                    <div class="certificate__title">CCENT</div>
                                    <ul class="breadcrumbs">
                                        <li><a href="#">Cisco Systems, Inc</a></li>
                                        <li><a href="#">Центр тестирования VUE</a></li>
                                    </ul>
                                    <ul class="certificate__prices">
                                        <li class="certificate__price">
                                            <span>Общая цена подготовки (1 курс) <i class="icon icon-help">?</i></span>
                                            <span>53 500 руб</span>
                                        </li>
                                        <li class="certificate__price">
                                            <span>Общая цена сертификации (1 экзамен) <i
                                                        class="icon icon-help">?</i></span>
                                            <span>14 700 руб</span>
                                        </li>
                                    </ul>
                                    <div class="certificate__description b-editor">
                                        <p>
                                            Первый уровень в системе сертификаций Cisco — уровень entry и начинается
                                            он с Сертифицированного Техника Сетей Cisco (Сіsco Certified Entry
                                            Networking Technician) — CCENT. Это — промежуточный шаг к Associate
                                            уровню — сертификациям CCNA и CCDA.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tabs-panel" id="panel2">
                        <div class="filter inline">
                            <div class="row">
                                <div class="small-12 medium-12 column">
                                    <div class="filter__input">
                                        <div class="filter__label">
                                            <span>Код экзамена:</span>
                                        </div>
                                        <label class="b-select">
                                            <input placeholder="Введите код или имя экзамена" type="text">
                                        </label>
                                    </div>
                                </div>
                                <div class="small-12 medium-6 column">
                                    <div class="filter__input">
                                        <div class="filter__label">
                                            <span>Вендор:</span>
                                        </div>
                                        <label class="b-select">
                                            <select class="b-select__container">
                                                <option value="">Все вендоры</option>
                                                <option value="">Все вендоры</option>
                                                <option value="">Все вендоры</option>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                                <div class="small-12 medium-6 column">
                                    <div class="filter__input">
                                        <div class="filter__label">
                                            <span>Место проведения:</span>
                                        </div>
                                        <label class="b-select">
                                            <select class="b-select__container">
                                                <option value="">Москва</option>
                                                <option value="">Пермь</option>
                                                <option value="">Уфа</option>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <table width="100%" cellpadding="0" cellspacing="0" class="course-table examination">
                            <tbody>
                            <tr>
                                <td class="course-table__img">Код курса</td>
                                <td class="course-table__name">Название курса</td>
                                <td class="course-table__price"><span>Стоимость</span></td>
                                <td class="course-table__basket" width="120"></td>
                            </tr>
                            <tr>
                                <td class="course-table__img">
                                    <p>RY0100</p>
                                </td>
                                <td class="course-table__name">
                                    <a href="#">
                                        Ruby Association Certified Ruby Programmer Silver
                                        <span class="add-info">
                                                <span>EN</span>
                                                <span>150 мин</span>
                                            </span>
                                    </a>
                                    <span>Ruby Association / Центр тестирования Прометрик</span>
                                </td>
                                <td class="course-table__price">
                                    <p>17 375 руб.</p>
                                    <p><span>г. Москва</span></p>
                                </td>
                                <td class="course-table__basket">
                                    <a href="#">в корзину</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="course-table__img">
                                    <p>RY0100</p>
                                </td>
                                <td class="course-table__name">
                                    <a href="#">
                                        Ruby Association Certified Ruby Programmer Silver
                                        <span class="add-info">
                                                <span>EN</span>
                                                <span>150 мин</span>
                                            </span>
                                    </a>
                                    <span>Ruby Association / Центр тестирования Прометрик</span>
                                </td>
                                <td class="course-table__price">
                                    <p>17 375 руб.</p>
                                    <p><span>г. Москва</span></p>
                                </td>
                                <td class="course-table__basket">
                                    <a href="#">в корзину</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="course-table__img">
                                    <p>RY0100</p>
                                </td>
                                <td class="course-table__name">
                                    <a href="#">
                                        Ruby Association Certified Ruby Programmer Silver
                                        <span class="add-info">
                                                <span>EN</span>
                                                <span>150 мин</span>
                                            </span>
                                    </a>
                                    <span>Ruby Association / Центр тестирования Прометрик</span>
                                </td>
                                <td class="course-table__price">
                                    <p>17 375 руб.</p>
                                    <p><span>г. Москва</span></p>
                                </td>
                                <td class="course-table__basket">
                                    <a href="#">в корзину</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <div class="pagination-wrapper">
                            <ul class="pagination" role="navigation" aria-label="Pagination">
                                <li class="pagination-previous disabled"></li>
                                <li class="current"><span class="show-for-sr">You're on page</span> 1</li>
                                <li><a href="#" aria-label="Page 2">2</a></li>
                                <li><a href="#" aria-label="Page 3">3</a></li>
                                <li><a href="#" aria-label="Page 4">4</a></li>
                                <li><a href="#" aria-label="Page 5">5</a></li>
                                <li class="pagination-next"><a href="#" aria-label="Next page"></a></li>
                            </ul>

                            <div class="pagination-select show-for-large">
                                <span>Элем. на стр.</span>
                                <label class="b-select">
                                    <select class="b-select__container">
                                        <option value="24">24</option>
                                        <option value="50">50</option>
                                    </select>
                                </label>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="about-course about-course--padding">
        <div class="row">
            <div class="column small-12 large-2">
                <div class="about-course__image">
                    <a href="#"><img src="/content/about-course/microsoft-logo.png" alt=""></a>
                </div>
            </div>
            <div class="column small-12 large-offset-1 large-9">
                <div class="about-course__container">
                    <div class="b-editor">
                        <p>Авторизованные курсы Microsoft — это гарантированные инвестиции в развитие бизнеса и карьеры.
                            В программе обучения соблюдаются все рекомендации вендора, преподавание проводят
                            сертифицированные тренеры, используются учебные материалы от разработчика,
                            дополняемые практическими наработками специалистов Softline.</p>
                        <p>
                            Для корпоративных клиентов доступно бесплатное обучение Microsoft по программе Software
                            Assurance.
                            Мы проводим курсы Microsoft по всем направлениям и продуктам в Москве, регионах РФ и странах
                            СНГ. В аудитории и удалённо, с тренером и в форме самообучения, в открытых, корпоративных
                            группах и индивидуально, днём и вечером и во всех часовых поясах, в будни и выходные.
                        </p>
                    </div>

                    <div class="list-pages">
                        <div class="list-pages__title">
                            <span>Направления:</span>
                        </div>
                        <div class="row small-up-1 medium-up-2 large-up-3">
                            <div class="column">
                                <ul class="list-pages__item">
                                    <li><a href="#">Программирование</a></li>

                                    <li><a href="#">Системное администрирование</a></li>

                                    <li><a href="#">Пользовательское ПО</a></li>

                                    <li><a href="#">Операционные системы (ОС)</a></li>

                                    <li><a href="#">Сетевые технологии</a></li>
                                </ul>
                            </div>
                            <div class="column">
                                <ul class="list-pages__item">
                                    <li><a href="#">Коммуникации </a></li>

                                    <li><a href="#"> Информационная безопасность </a></li>

                                    <li><a href="#"> Виртуализация </a></li>

                                    <li><a href="#"> Базы данных и СУБД </a></li>
                                </ul>
                            </div>
                            <div class="column">
                                <ul class="list-pages__item">
                                    <li><a href="#">CRM</a></li>

                                    <li><a href="#">Бизнес-аналитика</a></li>

                                    <li><a href="#">IT-сервис менеджмент</a></li>

                                    <li><a href="#">Бизнес-тренинги</a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="list-pages__title">
                            <span>Продукты:</span>
                        </div>
                        <div class="row small-up-1 medium-up-2 large-up-3">
                            <div class="column">
                                <ul class="list-pages__item">
                                    <li><a href="#">Dynamics</a></li>

                                    <li><a href="#">Exchange Server 2003</a></li>

                                    <li><a href="#">Exchange Server 2007</a></li>

                                    <li><a href="#"> Exchange Server 2010</a></li>

                                    <li><a href="#">Exchange Server 2013</a></li>

                                    <li><a href="#">Exchange Server 2016</a></li>

                                    <li><a href="#">Forefront</a></li>

                                    <li><a href="#">Lync Server 2010</a></li>

                                    <li><a href="#">Lync Server 2013</a></li>

                                    <li><a href="#">Lync Server 2015</a></li>

                                    <li><a href="#"> Microsoft Azure</a></li>

                                    <li><a href="#">Microsoft Project 2016</a></li>

                                </ul>
                            </div>
                            <div class="column">
                                <ul class="list-pages__item">
                                    <li><a href="#">Office 365</a></li>

                                    <li><a href="#">Project Server</a></li>

                                    <li><a href="#">Sharepoint 2007</a></li>

                                    <li><a href="#">Sharepoint 2013</a></li>

                                    <li><a href="#">Sharepoint 2016</a></li>

                                    <li><a href="#">SMS, MOM</a></li>

                                    <li><a href="#"> SQL Server 2008</a></li>

                                    <li><a href="#">SQL Server 2014</a></li>

                                    <li><a href="#"> SQL Server 2016</a></li>

                                    <li><a href="#">Systems Center 2012</a></li>

                                    <li><a href="#">Visual Studio 2005</a></li>

                                    <li><a href="#">Visual Studio 2008</a></li>

                            </div>
                            <div class="column">
                                <ul class="list-pages__item">
                                    <li><a href="#">Visual Studio 2010</a></li>

                                    <li><a href="#">Visual Studio 2012</a></li>

                                    <li><a href="#">Windows 10</a></li>

                                    <li><a href="#">Windows 7</a></li>

                                    <li><a href="#">Windows 8</a></li>

                                    <li><a href="#">Windows PowerShell</a></li>

                                    <li><a href="#"> Windows Server 2003</a></li>

                                    <li><a href="#"> Windows Server 2008</a></li>

                                    <li><a href="#"> Windows Server 2012</a></li>

                                    <li><a href="#">Windows Server 2016</a></li>

                                    <li><a href="#">Windows Vista</a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="list-pages__title">
                            <span>Обзор курсов Microsoft Windows 10</span>
                        </div>

                        <div class="video-container">
                            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/tYV3hx1oQ2o"
                                    frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="subscribe-banner">
        <div class="subscribe-banner__wrapper">
            <div class="row">
                <div class="column small-12 large-9 large-offset-3">
                    <div class="subscribe-banner__title">
                        <span>Будьте в курсе</span>
                    </div>
                    <form class="subscribe-banner__form">
                        <div class="subscribe-banner__input subscribe-banner__input--name">
                            <!--доп модификатор: error-->
                            <input type="text" placeholder="Ваше имя">
                        </div>
                        <div class="subscribe-banner__input subscribe-banner__input--email">
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="subscribe-banner__button">
                            <button class="button">Подписаться</button>
                        </div>
                    </form>
                    <div class="subscribe-banner__desc">
                        <span>Подпишитесь на информацию о новинках, скидках и акциях. <br> Уже более 36 000 подписчиков!</span>
                    </div>
                </div>
            </div>
        </div>
    </section>


</main>

<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        $('.b-select__container').select2();
        $(document).foundation();
    });
</script>
</body>
</html>
