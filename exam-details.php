<? include 'partials/header.php'; ?>

<main>
    <section class="course-header no-margin">
        <div class="row">
            <div class="column small-12 medium-7 large-8">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Учебный центр</a></li>
                    <li><a href="#">Сертификация и экзамены</a></li>
                </ul>
                <div class="course-header__wrapper">
                    <div class="course-header__title">
                        <h1>Экзамен "Ruby Association Certified
                            Ruby Programmer Silver"</h1>
                    </div>
                </div>
            </div>
            <div class="column small-12 medium-5 large-3 large-offset-1">
                <div class="feedback-header">
                    <div class="feedback-header__name">
                        <span>Светлана Жученко</span>
                    </div>
                    <div class="feedback-header__photo">
                        <img src="dist/images/feedback__header/feedback-header__photo/photo-1.png" alt="">
                    </div>
                    <div class="feedback-header__position">
                        <span>менеджер</span>
                        <span>интернет-магазина</span>
                    </div>
                    <div class="feedback-header__contacts">
                        <span>8 (800) 200-08-60 доб. 6011</span>
                        <a href="mailto:Svetlana.Zhuchenko@softlinegroup.com">Svetlana.Zhuchenko@softlinegroup.com</a>
                    </div>

                    <a href="#" class="button expanded">Связаться сейчас</a>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="row">
            <div class="column small-12 large-8">
                <div class="exam-details">
                    <div class="exam-details__column large">
                        <div class="exam-details__data">
                            <table cellpadding="0" cellspacing="0" class="table-details">
                                <tr>
                                    <td class="table-details__name">
                                        <span>Вендор:</span>
                                    </td>
                                    <td class="table-details__value"><a href="#">Ruby Association</a></td>
                                </tr>
                                <tr>
                                    <td class="table-details__name">
                                        <span>Центр тестирования:</span>
                                    </td>
                                    <td class="table-details__value"><a href="#">Прометрик</a></td>
                                </tr>
                                <tr>
                                    <td class="table-details__name">
                                        <span>Язык:</span>
                                    </td>
                                    <td class="table-details__value"><span>Английски</span></td>
                                </tr>
                                <tr>
                                    <td class="table-details__name">
                                        <span>Продолжительность:</span>
                                    </td>
                                    <td class="table-details__value"><span">150 минут</span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="exam-details__column small">
                        <div class="exam-details__price">
                            <form>
                                <span class="price">13 300,00 руб.</span>
                                <button class="button expanded">В корзину</button>
                            </form>

                            <div class="feedback-widget">
                                <form action="">
                                    <span>Есть вопросы? Закажите звонок:</span>
                                    <input placeholder="Ваш номер телефона" type="text">
                                    <span>Перезвоним очень быстро <br
                                                class="show-for-large"> и абсолютно бесплатно.</span>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="certified-exam">
                    <div class="certified-exam__desc">
                        <div class="b-editor">
                            <p>This is a basic skill-level certification of the knowledge on the background, grammar,
                                classes, objects, and standard libraries of Ruby. The logo on the right side identifies
                                people who are certified.</p>
                        </div>
                    </div>
                    <div class="certified-exam__logo">
                        <a href="#"><img src="content/about-exam/prometric.png" alt=""></a>
                    </div>
                </div>
                <div class="region-buttons">
                    <div class="row collapse small-up-1 medium-up-3">
                        <div class="column">
                            <button class="button-transparent">
                                Москва
                            </button>
                        </div>
                        <div class="column">
                            <button class="button-transparent">
                                Казань
                            </button>
                        </div>
                        <div class="column">
                            <button class="button-transparent">
                                Ростов-на-Дону
                            </button>
                        </div>
                    </div>
                </div>
                <div class="accordion-large-wrap">

                    <ul class="accordion-large" data-accordion>

                        <li class="accordion-large__item" data-accordion-item>
                            <a href="" class="accordion-large__title"><span>Об экзамене</span>
                                <svg class="icon plus">
                                    <use xlink:href="#images--svg--plus"></use>
                                </svg>
                                <svg class="icon miuns">
                                    <use xlink:href="#images--svg--minus"></use>
                                </svg>
                            </a>
                            <div class="accordion-large__content accordion-content" data-tab-content>
                                <p>В рамках спецификации современных стандартов, действия представителей оппозиции
                                    представлены в исключительно положительном свете. Внезапно, диаграммы связей,
                                    инициированные исключительно синтетически, ассоциативно распределены по
                                    отраслям.</p>
                            </div>
                        </li>

                        <li class="accordion-large__item" data-accordion-item>
                            <a href="" class="accordion-large__title"><span>Отзывы</span>
                                <svg class="icon plus">
                                    <use xlink:href="#images--svg--plus"></use>
                                </svg>
                                <svg class="icon miuns">
                                    <use xlink:href="#images--svg--minus"></use>
                                </svg>
                                <button class="review-button" onclick="$('#review-form').slideDown();">Оставить отзыв
                                </button>
                            </a>
                            <div class="accordion-large__content accordion-content" data-tab-content>
                                <div>
                                    <div class="row">
                                        <div class="column small-12 large-3">
                                            <a href="#"><img src="content/reviews/logo-1.png" alt=""></a>
                                        </div>
                                        <div class="column small-12 large-9">
                                            <div class="b-editor">
                                                <p>Волго-Уральский научно-исследовательский и проектный институт нефти и
                                                    газа выражает благодарность учебному центру Softline за организацию
                                                    обучения по программам «Старт» и «Гидросистема» для двух групп
                                                    специалистов института. В ходе занятий специалисты ознакомились с
                                                    особенностями решений, их характеристиками и возможностями,
                                                    назначением
                                                    и практическим применением выполняемых видов расчетов.
                                                    Преподавателями
                                                    учебного центра Softline была разработана программа, которая
                                                    учитывала
                                                    образовательные потребности каждого из специалистов, проведены
                                                    практические занятия и выданы сертификаты об успешном прохождении
                                                    курсов. В результате специалисты института получили необходимые
                                                    знания,
                                                    которые помогут им усовершенствовать процессы решения различных
                                                    задач в
                                                    сфере проектирования и реконструкции инженерных объектов. Мы с
                                                    уверенностью можем рекомендовать учебный центр Softline в качестве
                                                    профессионального партнера в сфере предоставления качественных
                                                    образовательных услуг.</p>
                                                <p><strong>Любовь Дмитриевна Зубова</strong> <br>
                                                    Заместитель генерального директора по ИТ, АО «Гипровостокнефть
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>

                                <div>
                                    <div class="row">
                                        <div class="column small-12 large-3">
                                            <a href="#"><img src="content/reviews/logo-2.png" alt=""></a>
                                        </div>
                                        <div class="column small-12 large-9">
                                            <div class="b-editor">
                                                <p>Волго-Уральский научно-исследовательский и проектный институт нефти и
                                                    газа выражает благодарность учебному центру Softline за организацию
                                                    обучения по программам «Старт» и «Гидросистема» для двух групп
                                                    специалистов института. В ходе занятий специалисты ознакомились с
                                                    особенностями решений, их характеристиками и возможностями,
                                                    назначением
                                                    и практическим применением выполняемых видов расчетов.
                                                    Преподавателями
                                                    учебного центра Softline была разработана программа, которая
                                                    учитывала
                                                    образовательные потребности каждого из специалистов, проведены
                                                    практические занятия и выданы сертификаты об успешном прохождении
                                                    курсов. В результате специалисты института получили необходимые
                                                    знания,
                                                    которые помогут им усовершенствовать процессы решения различных
                                                    задач в
                                                    сфере проектирования и реконструкции инженерных объектов. Мы с
                                                    уверенностью можем рекомендовать учебный центр Softline в качестве
                                                    профессионального партнера в сфере предоставления качественных
                                                    образовательных услуг.</p>
                                                <p><strong>Любовь Дмитриевна Зубова</strong> <br>
                                                    Заместитель генерального директора по ИТ, АО «Гипровостокнефть
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>


                                <div id="review-form" class="review">
                                    <div class="review__desc">
                                        <p>Ваш отзыв поможет кому-то сделать выбор. Спасибо, что делитесь опытом!</p>
                                    </div>
                                    <form>
                                        <div class="review__input">
                                            <div class="row">
                                                <div class="column small-12 large-3">
                                                    <label class="review__label"
                                                           for="review-comment"><span>Оценка:</span></label>
                                                </div>
                                                <div class="column small-12 large-9">
                                                    <div class="rating-star-wrap">
                                                        <div class="rating-star">
                                                            <svg class="icon">
                                                                <use xlink:href="#images--svg--rating-star"></use>
                                                            </svg>
                                                        </div>
                                                        <div class="rating-star">
                                                            <svg class="icon">
                                                                <use xlink:href="#images--svg--rating-star"></use>
                                                            </svg>
                                                        </div>
                                                        <div class="rating-star">
                                                            <svg class="icon">
                                                                <use xlink:href="#images--svg--rating-star"></use>
                                                            </svg>
                                                        </div>
                                                        <div class="rating-star">
                                                            <svg class="icon">
                                                                <use xlink:href="#images--svg--rating-star"></use>
                                                            </svg>
                                                        </div>
                                                        <div class="rating-star">
                                                            <svg class="icon">
                                                                <use xlink:href="#images--svg--rating-star"></use>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review__input">
                                            <div class="row">
                                                <div class="column small-12 large-3">
                                                    <label class="review__label"
                                                           for="review-name"><span>Имя</span></label>
                                                </div>
                                                <div class="column small-12 large-9">
                                                    <div class="flex-container">
                                                        <input id="review-name" placeholder="Введите имя" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review__input">
                                            <div class="row">
                                                <div class="column small-12 large-3">
                                                    <label class="review__label"
                                                           for="review-city"><span>Город:</span></label>
                                                </div>
                                                <div class="column small-12 large-9">
                                                    <div class="flex-container">
                                                        <input id="review-city" placeholder="Укажите ваш город"
                                                               type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review__input">
                                            <div class="row">
                                                <div class="column small-12 large-3">
                                                    <label class="review__label"
                                                           for="review-company"><span>Компания:</span></label>
                                                </div>
                                                <div class="column small-12 large-9">
                                                    <div class="flex-container">
                                                        <input id="review-company" placeholder="Название компании"
                                                               type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review__input">
                                            <div class="row">
                                                <div class="column small-12 large-3">
                                                    <label class="review__label"
                                                           for="review-comment"><span>Отзыв:</span></label>
                                                </div>
                                                <div class="column small-12 large-9">
                                                    <div class="flex-container">
                                                        <textarea
                                                                placeholder="Подробно опишите свой опыт взаимодействия с курсом"
                                                                name="" id="review-comment" cols="30"
                                                                rows="10"></textarea>
                                                    </div>
                                                    <div class="checkbox-wrap">
                                                        <input id="submit-yes" type="checkbox" name="remember" value="true"/>
                                                        <label for="submit-yes">Я согласен с <a href="#"> политикой
                                                                конфиденциальности</a> </label>
                                                    </div>
                                                    <button class="button">Отправить</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </li>


                    </ul>
                </div>
            </div>
            <div style="padding-top: 7rem" class="column small-12 large-offset-1 large-3">
                <aside>
                    <div class="aside-widget">
                        <div class="aside-widget__title">О центре</div>
                        <div class="aside-widget__content">
                            <ul class="aside-menu">


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/mission">

                                        <span class="b-link__text">Программирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/training-conditions">

                                        <span class="b-link__text">Системное администрирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class=""
                                       href="http://edu.softline.ru/about/svedeniya-ob-obrazovatelnoy-organizatsii">

                                        <span class="b-link__text">Пользовательское ПО</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/accreditation">

                                        <span class="b-link__text">Операционные системы (ОС)</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/status">

                                        <span class="b-link__text">Сетевые технологии</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-klientov">

                                        <span class="b-link__text">Информационная безопасность</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-partnerov">

                                        <span class="b-link__text">Виртуализация</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/razrabotka-kursov">

                                        <span class="b-link__text">Базы данных и СУБД</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/delivery-info">

                                        <span class="b-link__text">Моделирование и САПР</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/requisites">

                                        <span class="b-link__text">Коммуникации</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/tech-support">

                                        <span class="b-link__text">Резервное копирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">CRM</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Бизнес-аналитика</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">IT-сервис менеджмент</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Импортозамещение ПО</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Бизнес-тренинги</span>
                                    </a>

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__content">
                            <div class="filter expanded">
                                <div class="filter__title">
                                    <h3>Подбор курса:</h3>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Направления:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все направления</option>
                                            <option value="">Все направления</option>
                                            <option value="">Все направления</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Cпециальность:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все специальности</option>
                                            <option value="">Все специальности</option>
                                            <option value="">Все специальности</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Вендор:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Все вендоры</option>
                                            <option value="">Все вендоры</option>
                                            <option value="">Все вендоры</option>
                                        </select>
                                    </label>
                                    <label class="b-radiobox">
                                        <input name="example-1" type="radio">
                                        <span>Очно (в аудитории)</span>
                                    </label>
                                    <label class="b-radiobox">
                                        <input name="example-1" type="radio">
                                        <span>Дистанционно</span>
                                    </label>

                                </div>
                                <div class="filter__input">
                                    <div class="filter__label">
                                        <span>Место проведения:</span>
                                    </div>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="">Москва</option>
                                            <option value="">Москва</option>
                                            <option value="">Москва</option>
                                        </select>
                                    </label>
                                </div>

                            </div>
                            <button class="button small expanded">Подобрать курс</button>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__title">Будьте в курсе</div>
                        <div class="aside-widget__content">
                            <p>Подпишитесь на рассылку новостей Softline</p>
                            <form action="#">
                                <label for="">
                                    <input type="text" placeholder="Ваш e-mail">
                                </label>
                                <label for="">
                                    <input type="text" placeholder="Ваше имя">
                                </label>
                                <button class="button small expanded">Подписаться</button>
                            </form>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__content">
                            <a href="#">
                                <img src="content/banners/kasp.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>


</main>

<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        $('.b-select__container').select2();
        $(document).foundation();
    });
</script>
</body>
</html>
