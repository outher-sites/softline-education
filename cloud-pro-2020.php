<? include 'partials/header.php'; ?>

<main>
    <section class="course-header">
        <div class="row">
            <div class="column small-12 medium-7 large-8">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Учебный центр</a></li>
                    <li><a href="#">Сертификация и экзамены</a></li>
                </ul>
                <div class="course-header__wrapper">
                    <div class="course-header__title">
                        <h1>Путь сертификации</h1>
                    </div>
                </div>
            </div>
            <div class="column small-12 medium-5 large-3 large-offset-1">
                <div class="feedback-header">
                    <div class="feedback-header__name">
                        <span>Светлана Жученко</span>
                    </div>
                    <div class="feedback-header__photo">
                        <img src="dist/images/feedback__header/feedback-header__photo/photo-1.png" alt="">
                    </div>
                    <div class="feedback-header__position">
                        <span>менеджер</span>
                        <span>интернет-магазина</span>
                    </div>
                    <div class="feedback-header__contacts">
                        <span>8 (800) 200-08-60 доб. 6011</span>
                        <a href="Svetlana.Zhuchenko@softlinegroup.com">Svetlana.Zhuchenko@softlinegroup.com</a>
                    </div>

                    <a href="#" class="button expanded">Связаться сейчас</a>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="row">
            <div class="column small-12 large-8">
                <div class="b-editor exams-description">
                    <p>
                        Azure - это комплексный набор облачных служб, которые разработчики и ИТ-специалисты используют
                        для создания и развертывания приложений, а также управления ими через глобальную сеть центров
                        обработки данных. С помощью интегрированных средств, DevOps и Marketplace вы сможете создать как
                        простые мобильные приложения, так и интернет-решения.
                    </p>
                    <p>
                        Эти четыре современных роли работы IT Pro наиболее важны для успешного управления и
                        функционирования среды Azure Stack:
                    </p>
                </div>

                <div class="accordion-box">
                    <div class="accordion-box__content">
                        <ul class="accordion" data-accordion>
                            <li class="accordion-item" data-accordion-item>
                                <a href="#" class="accordion-title"><span>Оператор Azure Stack </span>
                                    <svg class="icon arrow">
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </a>

                                <div class="accordion-content" data-tab-content>
                                    <div class="b-editor b-editor--small-ul">
                                        <p>
                                            Оператор Azure Stack: отвечает за функционирование инфраструктуры Azure
                                            Stack, полноценное
                                            планирование, развертывание и интеграцию, упаковку и предоставление облачных
                                            ресурсов и
                                            требуемых сервисов в инфраструктуре.<br>
                                            Путь обучения:
                                        </p>
                                        <ol>
                                            <li>
                                                <h6>Очное обучение в классе:</h6>
                                                <ul>
                                                    <li>20533
                                                        <a href="http://edu.softline.ru/vendors/microsoft/course-20533">Применение
                                                            инфраструктурных решений Microsoft Azure</a></li>
                                                    <li>20537
                                                        <a href="http://edu.softline.ru/vendors/microsoft/course-20537">Настройка
                                                            и управление гибридным облаком с помощью Microsoft Azure
                                                            Stack</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <h6>Сдача экзамена</h6>
                                                <p>
                                                    <strong>Экзамен 70-537</strong><br>
                                                    Configuring and Operating a Hybrid Cloud with Microsoft Azure Stack
                                                    (Beta)<br>
                                                    Этот экзамен находится на этапе разработки и будет выпущен в январе
                                                    2018 г.
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Необходимые курсы MOOC:</h6>
                                                <ul>
                                                    <li>
                                                        <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE205x+course/about">Microsoft
                                                            Azure Storage</a></li>
                                                    <li>
                                                        <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE206x+course/about">Microsoft
                                                            Azure App Service</a></li>
                                                    <li>
                                                        <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE207x+course/about">Databases
                                                            in Azure</a></li>
                                                    <li>
                                                        <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE210x+course/about">Automating
                                                            Azure Workloads</a></li>
                                                </ul>
                                            </li>

                                            <li>
                                                <h6>Очные курсы</h6>
                                                <p>
                                                    20535A: Architecting Microsoft Azure Solutions
                                                    (В разработке, старт 29 января 2018)
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Сдача экзамена</h6>
                                                <p>
                                                    <strong>70-535</strong><br>
                                                    Архитектура решений Microsoft Azure (Architecting Microsoft Azure
                                                    Solutions)<br>
                                                    <strong>40522</strong><br>
                                                    Microsoft Cloud Workshop: Azure Stack (Экзамен находится в
                                                    разработке)
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Сертификат Microsoft Certified Solutions Associate (MCSA): Cloud
                                                    Platform</h6>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion-item" data-accordion-item>
                                <a href="#" class="accordion-title"><span>Архитектор решений Azure</span>
                                    <svg class="icon arrow">
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </a>

                                <div class="accordion-content" data-tab-content>
                                    <div class="b-editor b-editor--small-ul">
                                        <p>
                                            Архитектор решений Azure: контролирует стратегию облачных вычислений,
                                            включая планы
                                            внедрения, стратегию мульти-облачных и гибридных облаков, разработку
                                            приложений, управление
                                            и мониторинг.<br>
                                            Путь обучения:
                                        </p>
                                        <ol>
                                            <li>
                                                <h6>Необходимые курсы MOOC:</h6>
                                                <p>
                                                    Создание и работа с виртуальными машинами по управлению ресурсами
                                                    Azure<br>
                                                    <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE202x+course/about">Microsoft
                                                        Azure Virtual Machines</a>
                                                </p>
                                                <p>
                                                    Разработка и реализация стратегии хранения данных <br>
                                                    <a href="https://www.edx.org/course/provisioning-databases-azure-sql-server-microsoft-dat219x-2">Provisioning
                                                        SQL Server and Azure SQL Databases </a>
                                                </p>
                                                <p>
                                                    Внедрение Azure Active Directory<br>
                                                    <a href="http://online.academy4cloud.com/courses/course-v1:Microsoft+AZURE204x+course/about">Microsoft
                                                        Azure Identity</a>
                                                </p>
                                                <p>
                                                    Внедрение виртуальных сетей<br>
                                                    <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE203x+course/about">Microsoft
                                                        Azure Virtual Networks</a>
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Необходимые курсы MOOC:</h6>
                                                <ul>
                                                    <li>20533 Применение инфраструктурных решений Microsoft Azure</li>
                                                    <li>10992 Интеграция базовой локальной инфраструктуры с Microsoft
                                                        Azure
                                                    </li>
                                                    <li>40442 MOC Workshop: Architecting Microsoft Azure Solutions</li>
                                                </ul>
                                            </li>
                                            <li>
                                                <h6>Сдача экзамена</h6>
                                                <p>
                                                    <strong>70-533</strong><br>
                                                    <a href="http://edu.softline.ru/certification/implementing-microsoft-azure-infrastructure-solutions">Внедрение
                                                        инфраструктурных решений Microsoft Azure</a>
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Сертификат Microsoft Certified Professional (MCP) </h6>
                                            </li>
                                            <li>
                                                <h6>Необходимые курсы MOOC</h6>
                                                <ul>
                                                    <li>
                                                        <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE205x+course/about">Microsoft
                                                            Azure Storage</a></li>
                                                    <li>
                                                        <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE206x+course/about">Microsoft
                                                            Azure App Service</a></li>
                                                    <li>
                                                        <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE207x+course/about">Databases
                                                            in Azure</a></li>
                                                    <li>
                                                        <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE210x+course/about">Automating
                                                            Azure Workloads</a></li>
                                                </ul>
                                            </li>

                                            <li>
                                                <h6>Очные курсы</h6>
                                                <p>
                                                    <strong>20535A</strong> Architecting Microsoft Azure Solutions<br>
                                                    В разработке, старт 29 января 2018
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Сдача экзамена</h6>
                                                <p>
                                                    <strong>70-535</strong><br>
                                                    Архитектура решений Microsoft Azure (Architecting Microsoft Azure
                                                    Solutions)<br>
                                                    <strong>40522</strong><br>
                                                    Microsoft Cloud Workshop: Azure Stack (Экзамен находится в
                                                    разработке)
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Сертификат Microsoft Certified Solutions Associate (MCSA): Cloud
                                                    Platform</h6>
                                            </li>
                                            <li>
                                                <h6>Экзамен 70-539</h6>
                                                <p>
                                                    Managing Linux Workloads on Azure<br>
                                                    Экзамен находится в разработке и будет выпущен в марте 2018 года
                                                </p>
                                            </li>

                                            <li>
                                                <h6>Сертификат Microsoft Certified Solutions Expert (MCSE): Cloud
                                                    Platform and Infrastructure</h6>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion-item" data-accordion-item>
                                <a href="#" class="accordion-title"><span>Администратор Azure</span>
                                    <svg class="icon arrow">
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </a>

                                <div class="accordion-content" data-tab-content>
                                    <div class="b-editor b-editor--small-ul">
                                        <p>
                                            Администратор Azure: отвечает за управление арендуемым сегментом облака
                                            (будь то публичным, размещенным или гибридным) и предоставляет ресурсы и
                                            инструменты для соответствия требованиям своих клиентов.<br>
                                            Путь обучения:
                                        </p>
                                        <ol>
                                            <li>
                                                <h6>Необходимые курсы MOOC:</h6>
                                                <p>
                                                    Создание и работа с виртуальными машинами по управлению ресурсами
                                                    Azure<br>
                                                    <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE202x+course/about">Microsoft
                                                        Azure Virtual Machines</a>
                                                </p>
                                                <p>
                                                    Разработка и реализация стратегии хранения данных <br>
                                                    <a href="https://www.edx.org/course/provisioning-databases-azure-sql-server-microsoft-dat219x-2">Provisioning
                                                        SQL Server and Azure SQL Databases </a>
                                                </p>
                                                <p>
                                                    Внедрение Azure Active Directory<br>
                                                    <a href="http://online.academy4cloud.com/courses/course-v1:Microsoft+AZURE204x+course/about">Microsoft
                                                        Azure Identity</a>
                                                </p>
                                                <p>
                                                    Внедрение виртуальных сетей<br>
                                                    <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE203x+course/about">Microsoft
                                                        Azure Virtual Networks</a>
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Рекомендованные очные курсы</h6>
                                                <ul>
                                                    <li>20533
                                                        <a href="http://edu.softline.ru/vendors/microsoft/course-20533">Применение
                                                            инфраструктурных решений Microsoft Azure</a></li>
                                                    <li>20537
                                                        <a href="http://edu.softline.ru/vendors/microsoft/course-20537">Настройка
                                                            и управление гибридным облаком с помощью Microsoft Azure
                                                            Stack</a></li>
                                                    <li>20532
                                                        <a href="http://edu.softline.ru/vendors/microsoft/course-20532">Разработка
                                                            решений Microsoft Azure</a></li>
                                                    <li>40442
                                                        <a href="https://www.microsoft.com/en-us/learning/course.aspx?cid=40442">MOC
                                                            Workshop: Architecting Microsoft Azure Solutions</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <h6>Экзамен</h6>
                                                <p>
                                                    <strong>70-533</strong><br>
                                                    <a href="http://edu.softline.ru/certification/implementing-microsoft-azure-infrastructure-solutions">Внедрение
                                                        инфраструктурных решений Microsoft Azure</a>
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Сертификат Microsoft Certified Professional (MCP)</h6>
                                            </li>
                                            <li>
                                                <h6>Необходимые курсы MOOC</h6>
                                                <ul>
                                                    <li>
                                                        <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE205x+course/about">Microsoft
                                                            Azure Storage</a></li>
                                                    <li>
                                                        <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE206x+course/about">Microsoft
                                                            Azure App Service</a></li>
                                                    <li>
                                                        <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE207x+course/about">Databases
                                                            in Azure</a></li>
                                                    <li>
                                                        <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE210x+course/about">Automating
                                                            Azure Workloads</a></li>
                                                </ul>
                                            </li>

                                            <li>
                                                <h6>Очные курсы</h6>
                                                <p>
                                                    <strong>20535A</strong> Architecting Microsoft Azure Solutions<br>
                                                    В разработке, старт 29 января 2018
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Сдача экзамена</h6>
                                                <p>
                                                    <strong>70-535</strong><br>
                                                    Архитектура решений Microsoft Azure (Architecting Microsoft Azure
                                                    Solutions)<br>
                                                    <strong>40522</strong><br>
                                                    Microsoft Cloud Workshop: Azure Stack (Экзамен находится в
                                                    разработке)
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Сертификат Microsoft Certified Solutions Associate (MCSA): Cloud
                                                    Platform</h6>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion-item" data-accordion-item>
                                <a href="#" class="accordion-title"><span>Инженер DevOps</span>
                                    <svg class="icon arrow">
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </a>

                                <div class="accordion-content" data-tab-content>
                                    <div class="b-editor b-editor--small-ul">
                                        <p>
                                            Инженер DevOps ответственен за развитие бизнес-приложений, использующих
                                            облачные ресурсы, облачные платформы и методы DevOps - инфраструктуру как
                                            код, непрерывную интеграцию, непрерывную разработку, управление информацией
                                            и т.д.<br>
                                            Путь обучения:
                                        </p>
                                        <ol>
                                            <li>
                                                <h6>Необходимые курсы MOOC:</h6>
                                                <p>
                                                    Создание и работа с виртуальными машинами по управлению ресурсами
                                                    Azure<br>
                                                    <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE202x+course/about">Microsoft
                                                        Azure Virtual Machines</a><br>
                                                    <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE203x+course/about">Microsoft
                                                        Azure Virtual Networks</a>
                                                </p>
                                                <p>
                                                    Разработка и реализация стратегии хранения данных <br>
                                                    <a href="https://www.edx.org/course/developing-multidimensional-data-model-microsoft-dat224x-1">Developing
                                                        a Multidimensional Data Model</a><br>
                                                    <a href="https://www.edx.org/course/provisioning-databases-azure-sql-server-microsoft-dat219x-2">Provisioning
                                                        SQL Server and Azure SQL Databases </a>
                                                </p>
                                                <p>
                                                    Управление идентификационными данными, приложениями и сетевыми
                                                    службами<br>
                                                    <a href="http://online.academy4cloud.com/courses/course-v1:Microsoft+AZURE204x+course/about">Microsoft
                                                        Azure Identity</a>
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Очное обучение в классе:</h6>
                                                <ul>
                                                    <li>20532 Разработка решений Microsoft Azure</li>
                                                    <li>40533 Microsoft Cloud Workshop: OSS PaaS and DevOps (Курс
                                                        находится в разработке)
                                                    </li>
                                                    <li>40501 Microsoft Cloud Workshop: Container and DevOps (Курс
                                                        находится в разработке)
                                                    </li>
                                                    <li>40509 Microsoft Cloud Workshop: OSS DevOps (Курс находится в
                                                        разработке)
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <h6>Сдача экзамена</h6>
                                                <p>
                                                    <strong>70-532</strong><br>
                                                    <a href="http://edu.softline.ru/certification/developing-microsoft-azure-applications">Разработка
                                                        решений Microsoft Azure</a>
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Сертификат Microsoft Certified Professional (MCP)</h6>
                                            </li>
                                            <li>
                                                <h6>Необходимые курсы MOOC</h6>
                                                <ul>
                                                    <li>
                                                        <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE205x+course/about">Microsoft
                                                            Azure Storage</a></li>
                                                    <li>
                                                        <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE206x+course/about">Microsoft
                                                            Azure App Service</a></li>
                                                    <li>
                                                        <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE207x+course/about">Databases
                                                            in Azure</a></li>
                                                    <li>
                                                        <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE210x+course/about">Automating
                                                            Azure Workloads</a></li>
                                                </ul>
                                            </li>

                                            <li>
                                                <h6>Очные курсы</h6>
                                                <p>
                                                    <strong>20535A</strong> Architecting Microsoft Azure Solutions<br>
                                                    В разработке, старт 29 января 2018
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Сдача экзамена</h6>
                                                <p>
                                                    <strong>70-535</strong><br>
                                                    Архитектура решений Microsoft Azure (Architecting Microsoft Azure
                                                    Solutions)<br>
                                                    <strong>40522</strong><br>
                                                    Microsoft Cloud Workshop: Azure Stack (Экзамен находится в
                                                    разработке)
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Сертификат Microsoft Certified Solutions Associate (MCSA): Cloud
                                                    Platform</h6>
                                            </li>
                                            <li>
                                                <h6>Экзамен 70-53</h6>
                                                <p>
                                                    Implementing Microsoft Azure DevOps Solutions<br>
                                                    Экзамен находится в разработке и будет выпущен в марте 2018 года
                                                </p>
                                            </li>

                                            <li>
                                                <h6>Сертификат Microsoft Certified Solutions Expert (MCSE): Cloud
                                                    Platform and Infrastructure</h6>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="b-editor exams-description">
                    <p>
                        Используйте приобретённые знания (экзамен 70-535) для освоения технологии
                        <strong>Blockchain</strong>
                    </p>
                    <p>
                        <strong>Блокчейн</strong> — это инновационная технология, способная перенести цифровое
                        преобразование за пределы компании и усовершенствовать взаимодействие с поставщиками, клиентами
                        и партнерами. Технология блокчейн представляет собой структуру данных, которая используется для
                        создания цифрового реестра транзакций, размещенного не у одного поставщика, а в распределенной
                        сети компьютеров. Больше информации на сайте
                        <a href="https://azure.microsoft.com/ru-ru/solutions/blockchain/">azure.microsoft.com</a>
                    </p>
                    <p>40523 Microsoft Cloud Workshop: Blockchain (Экзамен находится в разработке)</p>
                </div>

                <div class="accordion-box">
                    <div class="accordion-box__content">
                        <ul class="accordion" data-accordion>
                            <li class="accordion-item" data-accordion-item>
                                <a href="#" class="accordion-title"><span>Специалист Linux on Azure</span>
                                    <svg class="icon arrow">
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </a>

                                <div class="accordion-content" data-tab-content>
                                    <div class="b-editor b-editor--small-ul">
                                        <p>
                                            Путь обучения:
                                        </p>
                                        <ol>
                                            <li>
                                                <h6>Необходимые курсы MOOC:</h6>
                                                <p>
                                                    Создание и работа с виртуальными машинами по управлению ресурсами
                                                    Azure<br>
                                                    <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE202x+course/about">Microsoft
                                                        Azure Virtual Machines</a>
                                                </p>
                                                <p>
                                                    Разработка и реализация стратегии хранения данных <br>
                                                    <a href="https://www.edx.org/course/provisioning-databases-azure-sql-server-microsoft-dat219x-2">Provisioning
                                                        SQL Server and Azure SQL Databases </a>
                                                </p>
                                                <p>
                                                    Внедрение Azure Active Directory<br>
                                                    <a href="http://online.academy4cloud.com/courses/course-v1:Microsoft+AZURE204x+course/about">Microsoft
                                                        Azure Identity</a>
                                                </p>
                                                <p>
                                                    Внедрение виртуальных сетей<br>
                                                    <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE203x+course/about">Microsoft
                                                        Azure Virtual Networks</a>
                                                </p>
                                                <p>
                                                    40524<br>
                                                    Microsoft Cloud Workshop: Linux Lift & Shift (Курс находится в разработке)
                                                </p>
                                            </li>

                                            <li>
                                                <h6>Очное обучение в классе:</h6>
                                                <p>
                                                    <strong>Курс 55187</strong><br>
                                                    Linux Foundation Certified System Administrato
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Сдача экзамена</h6>
                                                <p>
                                                    <strong>Экзамен LFCS </strong><br>
                                                    Linux Foundation Certified System<br>
                                                    <strong>Экзамен 70-533</strong><br>
                                                    Внедрение инфраструктурных решений Microsoft Azure
                                                    разработке)
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Сертификат Microsoft Certified Solutions Associate (MCSA): Linux on Azure</h6>
                                            </li>
                                            <li>
                                                <h6>Сдача экзамена</h6>
                                                <p>
                                                    <strong>Экзамен 70-539</strong><br>
                                                    Managing Linux Workloads on Azurebr (Экзамен находится в разработке)
                                            </li>
                                            <li>
                                                <h6>Сертификат Microsoft Certified Solutions Expert (MCSE): Cloud Platform and Infrastructure </h6>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion-item" data-accordion-item>
                                <a href="#" class="accordion-title"><span>Специалист Office 365</span>
                                    <svg class="icon arrow">
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </a>

                                <div class="accordion-content" data-tab-content>
                                    <div class="b-editor b-editor--small-ul">
                                        <p>
                                            Путь обучения:
                                        </p>
                                        <ol>
                                            <li>
                                                <h6>Необходимые курсы MOOC</h6>
                                                <ul>
                                                    <li><a href="https://www.edx.org/course/provisioning-office-365-services-microsoft-cld220x-3">Provisioning Office 365 Services</a></li>
                                                    <li><a href="https://www.edx.org/course/enabling-office-365-clients-microsoft-cld221x-3">Enabling Office 365 Clients</a></li>
                                                    <li><a href="https://www.edx.org/course/managing-microsoft-exchange-online-microsoft-cld222x-3">Managing Microsoft Exchange Online in Office 365</a></li>
                                                    <li><a href="https://www.edx.org/course/onboarding-messaging-office-365-microsoft-cld223x-3">Onboarding Messaging to Office 365</a></li>
                                                </ul>
                                            </li><li>
                                                <h6>Очное обучение в классе:</h6>
                                                <ul>
                                                    <li>20347: <a href="https://www.microsoft.com/ru-ru/learning/course.aspx?cid=20347">Enabling and Managing Office 365 (five days)</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <h6>Сдача экзамена</h6>
                                                <p>
                                                    <strong>Экзамен 70-346 </strong><br>
                                                    Managing Office 365 Identities and Requirements
                                                </p>
                                                <p>
                                                    <strong>Экзамен 70-347 </strong><br>
                                                    Enabling Office 365 Services
                                                </p>
                                                <p>
                                                    <strong>Экзамен 70-398 </strong><br>
                                                    Planning for and Managing Devices in the Enterprise
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Сертификат Microsoft Certified Solutions Associate (MCSA): Office 365</h6>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion-item" data-accordion-item>
                                <a href="#" class="accordion-title"><span>Специалист по BigData</span>
                                    <svg class="icon arrow">
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </a>

                                <div class="accordion-content" data-tab-content>
                                    <div class="b-editor b-editor--small-ul">
                                        <p>
                                            Путь обучения:
                                        </p>
                                        <ol>
                                            <li>
                                                <h6>Очное обучение в классе:</h6>
                                                <ul>
                                                    <li>40441 <a href="https://www.microsoft.com/ru-ru/learning/course.aspx?cid=40441">Designing and Implementing Cloud Data Platform Solutions (three days)</a></li>
                                                    <li>40502 Microsoft Cloud Workshop: Big Data & Visualization (Экзамен находится в разработке)</li>
                                                    <li>20773A <a href="https://www.microsoft.com/ru-ru/learning/course.aspx?cid=20773">Analyzing Big Data with Microsoft R</a></li>
                                                    <li>20774A <a href="https://www.microsoft.com/ru-ru/learning/course.aspx?cid=20774">Perform Cloud Data Science with Azure Machine Learning</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <h6>Очное обучение:</h6>
                                                <ul>
                                                    <li>20775A <a href="https://www.microsoft.com/ru-ru/learning/course.aspx?cid=20775">Performing Data Engineering on Microsoft HD Insight</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <h6>Сдача экзамена</h6>
                                                <p>
                                                    <strong>Экзамен 70-346</strong><br>
                                                    Managing Office 365 Identities and Requirements
                                                </p>
                                                <p>
                                                    <strong>Экзамен 70-347 </strong><br>
                                                    Enabling Office 365 Services
                                                </p>
                                                <p>
                                                    <strong>Экзамен 70-398 </strong><br>
                                                    Planning for and Managing Devices in the Enterprise
                                                </p>
                                                <p>
                                                    <strong>Экзамен 70-774 </strong><br>
                                                    Perform Cloud Data Science with Azure Machine Learning
                                                </p>
                                            </li>
                                            <li>
                                                <h6>Сертификат Microsoft Certified Solutions Associate (MCSA): Office 365</h6>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="column small-12 large-offset-1 large-3">
                <aside>
                    <div class="aside-widget">
                        <div class="aside-widget__title">О центре</div>
                        <div class="aside-widget__content">
                            <ul class="aside-menu">


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/mission">

                                        <span class="b-link__text">Программирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/training-conditions">

                                        <span class="b-link__text">Системное администрирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class=""
                                       href="http://edu.softline.ru/about/svedeniya-ob-obrazovatelnoy-organizatsii">

                                        <span class="b-link__text">Пользовательское ПО</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/accreditation">

                                        <span class="b-link__text">Операционные системы (ОС)</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/status">

                                        <span class="b-link__text">Сетевые технологии</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-klientov">

                                        <span class="b-link__text">Информационная безопасность</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-partnerov">

                                        <span class="b-link__text">Виртуализация</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/razrabotka-kursov">

                                        <span class="b-link__text">Базы данных и СУБД</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/delivery-info">

                                        <span class="b-link__text">Моделирование и САПР</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/requisites">

                                        <span class="b-link__text">Коммуникации</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/tech-support">

                                        <span class="b-link__text">Резервное копирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">CRM</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Бизнес-аналитика</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">IT-сервис менеджмент</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Импортозамещение ПО</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Бизнес-тренинги</span>
                                    </a>

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__title">Будьте в курсе</div>
                        <div class="aside-widget__content">
                            <p>Подпишитесь на рассылку новостей Softline</p>
                            <form action="#">
                                <label for="">
                                    <input type="text" placeholder="Ваш e-mail">
                                </label>
                                <label for="">
                                    <input type="text" placeholder="Ваше имя">
                                </label>
                                <button class="button small expanded">Подписаться</button>
                            </form>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__content">
                            <a href="#">
                                <img src="content/banners/kasp.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>

    <section class="subscribe-banner">
        <div class="subscribe-banner__wrapper">
            <div class="row">
                <div class="column small-12 large-9 large-offset-3">
                    <div class="subscribe-banner__title">
                        <span>Будьте в курсе</span>
                    </div>
                    <form class="subscribe-banner__form">
                        <div class="subscribe-banner__input subscribe-banner__input--name">
                            <!--доп модификатор: error-->
                            <input type="text" placeholder="Ваше имя">
                        </div>
                        <div class="subscribe-banner__input subscribe-banner__input--email">
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="subscribe-banner__button">
                            <button class="button">Подписаться</button>
                        </div>
                    </form>
                    <div class="subscribe-banner__desc">
                        <span>Подпишитесь на информацию о новинках, скидках и акциях. <br> Уже более 36 000 подписчиков!</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>

<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        $('.b-select__container').select2();
        $(document).foundation();
    });
</script>
</body>
</html>
