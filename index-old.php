<? include 'partials/header.php'; ?>

<main>
    <section class="banner">
        <div class="banner__carousel owl-carousel">
            <div class="banner__slide" style="background-image:url(dist/images/banner/home-banner.jpg);">
                <div class="row">
                    <div class="column small-12 medium-6 xlarge-7">
                        <div class="banner__title text-center medium-text-left">
                            <span class="h1">Azure Skills Initiative – обучение в ажуре!</span>
                        </div>
                        <div class="banner__desc banner__desc--padding text-center medium-text-left ">
                            <p>
                                Экзамен со второй попыткой + Practice Test от 5 400 руб.
                            </p>
                        </div>
                        <div class="banner__link">
                            <button data-open="pop-up-1" class="button ">Подробнее</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="banner__actions">
            <div class="row">
                <div class="small-12 column">
                    <div class="banner__dots text-center medium-text-right" id="banner__dots"></div>
                </div>
            </div>
        </div>
    </section>

    <section style="padding: 40px 0">
        <div class="row">
            <div class="column small-6">
                <div class="b-input input-form-error">
                    <label>
                        <span>инпут пример лейбла</span>
                        <input type="text">
                    </label>
                </div>

                <div class="b-input inline input-form-error">
                    <label>
                        <span>пример</span>
                        <input type="text">
                    </label>
                </div>

            </div>
            <div class="column small-6">
                <div class="b-input">
                    <label>
                        <span> инпут и это тоже пример лейбла</span>
                        <input type="text">
                    </label>
                </div>

                <div class="b-input inline">
                    <label>
                        <span>пример</span>
                        <input type="text">
                    </label>
                </div>
            </div>
            <div class="column small-12">
                <div class="b-input">
                    <label>
                        <span> инпут и это тоже пример лейбла</span>
                        <input type="text">
                    </label>
                </div>

                <div class="b-input inline">
                    <label>
                        <span>пример</span>
                        <input type="text">
                    </label>
                </div>
            </div>
            <div class="column small-4">
                <div class="b-input">
                    <label>
                        <span> инпут и это тоже пример лейбла</span>
                        <input type="text">
                    </label>
                </div>

                <div class="b-input inline">
                    <label>
                        <span>пример</span>
                        <input type="text">
                    </label>
                </div>
            </div>
            <div class="column small-4">
                <div class="b-input">
                    <label>
                        <span> инпут и это тоже пример лейбла</span>
                        <input type="text">
                    </label>
                </div>

                <div class="b-input inline">
                    <label>
                        <span>пример</span>
                        <input type="text">
                    </label>
                </div>
            </div>
            <div class="column small-4">
                <div class="b-input">
                    <label>
                        <span> инпут и это тоже пример лейбла</span>
                        <input type="text">
                    </label>
                </div>

                <div class="b-input inline">
                    <label>
                        <span>пример</span>
                        <input type="text">
                    </label>
                </div>
            </div>
        </div>
    </section>
    <div class="pop-up reveal" id="pop-up-1" data-reveal>
    </div>
</main>

<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).foundation();
</script>
<script>
    // Слайдеры
    $('.catalog-slider').each(function () {
        $(this).find('.catalog-slider__carousel').owlCarousel({
            loop: true,
            nav: true,
            dots: false,
            margin: 20,
            navText: [$(this).find('.catalog-slider__prev'), $(this).find('.catalog-slider__next')],
            navContainer: $(this).find('.catalog-slider__nav'),
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 3,
                    margin: 30
                },
                1000: {
                    items: 4,
                    margin: 30
                }
            }
        });
    });

    // Баннер
    var $banner = $('.banner');
    var setIndicator = function (index) {

        var timestamp = Date.now();

        $banner.find('.owl-dot').eq(index).append('<div class="radius" id="owl-dot-radius-' + timestamp + '"></div>');

        var obj = $('#owl-dot-radius-' + timestamp).radialIndicator({
            displayNumber: false,
            radius: 6.5,
            barWidth: 2,
            barBgColor: '#fff',
            barColor: '#c61143'
        }).data('radialIndicator');

        obj.animate(100);

    };

    if ($banner.find('.banner__slide').length === 1) {
        $('.banner__actions').fadeOut(0);
    }

    $banner.find('.banner__carousel')
        .on('initialized.owl.carousel', function () {
            setTimeout(function () {
                setIndicator(0);
            }, 100);

            $banner.on('change.owl.carousel', function (e) {
                $banner.find('.owl-dot .radius').fadeOut(200, function () {
                    $(this).remove()
                });
                var index = e.page.index;
                if (index > e.page.count) {
                    index = e.page.count;
                }
                setIndicator(index - 1);
            });
        })
        .owlCarousel({
            items: 1,
            dots: true,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            loop: false,
            dotsContainer: '#banner__dots'
        });


    // Меню
    $('#header-bottom__hamburger').on('click', function () {
        $('#mobile-nav').slideToggle();
        $(this).toggleClass('active');
    });

</script>

</body>
</html>
