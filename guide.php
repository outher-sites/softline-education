<? include 'partials/header-2020.php'; ?>

<main>
    <section class="course-header">
        <div class="row">
            <div class="column small-12 medium-7">
                <div class="course-header__wrapper">
                    <div class="course-header__title">
                        <h1>Мы подготовили для вас возможные
                            пути развития в облачном
                            мире ИТ-технологий</h1>
                    </div>
                </div>

            </div>
            <div class="column small-12 medium-5">
                <div class="text-left">
                    <img style="padding-top: 4rem" src="dist/images/guide/cloud.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="column small-12">
                <ul class="training-program" data-tabs id="training-program">
                    <li class="training-program__title tabs-title is-active">
                        <a class="training-program__inner" href="#t-panel-1" data-tabs-target="t-panel-1"
                           aria-selected="true">Архитектор Azure</a>
                        <i class="training-program__more-info" data-h-offset="-25"  data-allow-html="true"  data-template-classes="training-program__info-box" title="
                         <h6>Cтроитель-стратег</h6>
                            <p>Определяет стратегию облачных вычислений (включая мультиоблачные и гибридные облака),
                                разбирается в разработке приложений, их управлении и мониторинге.</p>"
                           data-tooltip aria-haspopup="true" data-trigger-class="" data-disable-hover="false" tabindex="1"  data-position="right" data-alignment="top"><span>?</span></i>
                    </li>
                    <li class="training-program__title tabs-title">
                        <a class="training-program__inner" data-tabs-target="t-panel-2" href="#t-panel-2">Архитектор
                            DevOps</a>
                        <i class="training-program__more-info" data-h-offset="-25"  data-allow-html="true"  data-template-classes="training-program__info-box" title="
                        <h6>Запускает в продакшн</h6>
                          <p>Отвечает за передачу в эксплуатацию разрабатываемых бизнес-приложений, использующих
                                облачные ресурсы, а также облачные платформы и методы DevOps.</p>"
                           data-tooltip aria-haspopup="true" data-trigger-class="" data-disable-hover="false" tabindex="1"  data-position="right" data-alignment="top"><span>?</span></i>
                    </li>
                    <li class="training-program__title tabs-title">
                        <a class="training-program__inner" data-tabs-target="t-panel-3" href="#t-panel-3">Администратор
                            Azure</a>
                        <i class="training-program__more-info" data-h-offset="-25"  data-allow-html="true"  data-template-classes="training-program__info-box" title="
                        <h6>Глобальный управленец</h6>
                            <p>Отвечает за управление арендуемым сегментом облака (публичным, размещенным или гибридным)
                                и предоставляет ресурсы и инструменты клиентам.</p>"
                           data-tooltip aria-haspopup="true" data-trigger-class="" data-disable-hover="false" tabindex="1"  data-position="right" data-alignment="top"><span>?</span></i>
                    </li>
                    <li class="training-program__title tabs-title">
                        <a class="training-program__inner" data-tabs-target="t-panel-4" href="#t-panel-4">Оператор Azure
                            Stack</a>
                        <i class="training-program__more-info" data-h-offset="-25"  data-allow-html="true"  data-template-classes="training-program__info-box" title="
                         <h6>Гуру по всем вопросам</h6>
                            <p>Отвечает за функционирование инфраструктуры Azure Stack, её полноценное планирование,
                                развертывание и интеграцию, подготовку пакетов установки и предоставление запрашиваемых
                                ресурсов и сервисов в облачной инфраструктуре.</p>"
                           data-tooltip aria-haspopup="true" data-trigger-class="" data-disable-hover="false" tabindex="1"  data-position="right" data-alignment="top"><span>?</span></i>
                    </li>
                </ul>
                <div class="training-program__content tabs-content" data-tabs-content="training-program">
                    <div id="t-panel-1" class="training-program__panel tabs-panel is-active">
                        <div class="training-exam-wrap">
                            <div class="training-exam-box">
                                <div class="training-exam is-active" data-target="70533">
                                    <div class="training-exam__inner">
                                        <div class="training-exam__number">
                                            <span>
                                                <a href="http://edu.softline.ru/certification/implementing-microsoft-azure-infrastructure-solutions">70-533</a>
                                            </span>
                                        </div>
                                        <div class="training-exam__desc">
                                            <p><strong>Microsoft Certified Professional (MCP)</strong></p>
                                            <p>Внедрение инфраструктурных решений Microsoft Azure</p>
                                        </div>
                                    </div>
                                    <div class="training-exam__inner training-exam__inner--bg">
                                        <a href="http://edu.softline.ru/certification/implementing-microsoft-azure-infrastructure-solutions" class="button">Сдача экзамена</a>
                                        <div class="training-exam__desc">
                                            <p>В 9 городах России в авторизованных Центрах тестирования Pearson VUE и
                                                Certiport
                                                вы сможете сдать экзамен (на английском языке). </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="training-exam-box">
                                <div class="training-exam" data-target="70535">
                                    <div class="training-exam__inner">
                                        <div class="training-exam__number">
                                            <span><a href="http://edu.softline.ru/certification/architecting-microsoft-azure-solutions-1">70-535</a></span>
                                        </div>
                                        <div class="training-exam__desc">
                                            <p><strong>Microsoft Certified Solutions Associate: Cloud Platform
                                                    (MCSA)</strong></p>
                                            <p>Architecting Microsoft Azure Solutions</p>
                                        </div>
                                    </div>
                                    <div class="training-exam__inner training-exam__inner--bg">
                                        <a href="http://edu.softline.ru/certification/architecting-microsoft-azure-solutions-1" class="button">Сдача экзамена</a>
                                        <div class="training-exam__desc">
                                            <p>В 9 городах России в авторизованных Центрах тестирования Pearson VUE и
                                                Certiport
                                                вы сможете сдать экзамен (на английском языке). </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div id="t-panel-2" class="training-program__panel  tabs-panel">
                        <div class="training-exam-wrap">
                            <div class="training-exam-box">
                                <div class="training-exam" data-target="70532">
                                    <div class="training-exam__inner">
                                        <div class="training-exam__number">
                                            <span><a href="http://edu.softline.ru/certification/developing-microsoft-azure-applications">70-532</a></span>
                                        </div>
                                        <div class="training-exam__desc">
                                            <p><strong>Microsoft Certified Professional (MCP)</strong></p>
                                            <p>Разработка решений Microsoft Azure</p>
                                        </div>
                                    </div>
                                    <div class="training-exam__inner training-exam__inner--bg">
                                        <a href="http://edu.softline.ru/certification/developing-microsoft-azure-applications" class="button">Сдача экзамена</a>
                                        <div class="training-exam__desc">
                                            <p>В 9 городах России в авторизованных Центрах тестирования Pearson VUE и
                                                Certiport
                                                вы сможете сдать экзамен (на английском языке). </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="training-exam-box">
                                <div class="training-exam" data-target="70535">
                                    <div class="training-exam__inner">
                                        <div class="training-exam__number">
                                            <span><a href="http://edu.softline.ru/certification/architecting-microsoft-azure-solutions-1">70-535</a></span>
                                        </div>
                                        <div class="training-exam__desc">
                                            <p><strong>Microsoft Certified Solutions Associate: Cloud Platform
                                                    (MCSA)</strong></p>
                                            <p>Architecting Microsoft Azure Solutions</p>
                                        </div>
                                    </div>
                                    <div class="training-exam__inner training-exam__inner--bg">
                                        <a href="http://edu.softline.ru/certification/architecting-microsoft-azure-solutions-1" class="button">Сдача экзамена</a>
                                        <div class="training-exam__desc">
                                            <p>В 9 городах России в авторизованных Центрах тестирования Pearson VUE и
                                                Certiport
                                                вы сможете сдать экзамен (на английском языке). </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                    <div id="t-panel-3" class="training-program__panel  tabs-panel">
                        <div class="training-exam-wrap">
                            <div class="training-exam-box">
                                <div class="training-exam is-active" data-target="70533">
                                    <div class="training-exam__inner">
                                        <div class="training-exam__number">
                                            <span>
                                                <a href="http://edu.softline.ru/certification/implementing-microsoft-azure-infrastructure-solutions">70-533</a>
                                            </span>
                                        </div>
                                        <div class="training-exam__desc">
                                            <p><strong>Microsoft Certified Professional (MCP)</strong></p>
                                            <p>Внедрение инфраструктурных решений Microsoft Azure</p>
                                        </div>
                                    </div>
                                    <div class="training-exam__inner training-exam__inner--bg">
                                        <a href="http://edu.softline.ru/certification/implementing-microsoft-azure-infrastructure-solutions" class="button">Сдача экзамена</a>
                                        <div class="training-exam__desc">
                                            <p>В 9 городах России в авторизованных Центрах тестирования Pearson VUE и
                                                Certiport
                                                вы сможете сдать экзамен (на английском языке). </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="training-exam-box">
                                <div class="training-exam" data-target="70535">
                                    <div class="training-exam__inner">
                                        <div class="training-exam__number">
                                            <span><a href="http://edu.softline.ru/certification/architecting-microsoft-azure-solutions-1">70-535</a></span>
                                        </div>
                                        <div class="training-exam__desc">
                                            <p><strong>Microsoft Certified Solutions Associate: Cloud Platform
                                                    (MCSA)</strong></p>
                                            <p>Architecting Microsoft Azure Solutions</p>
                                        </div>
                                    </div>
                                    <div class="training-exam__inner training-exam__inner--bg">
                                        <a href="http://edu.softline.ru/certification/architecting-microsoft-azure-solutions-1" class="button">Сдача экзамена</a>
                                        <div class="training-exam__desc">
                                            <p>В 9 городах России в авторизованных Центрах тестирования Pearson VUE и
                                                Certiport
                                                вы сможете сдать экзамен (на английском языке). </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                    <div id="t-panel-4" class="training-program__panel  tabs-panel">
                        <div class="training-exam-wrap">
                            <div class="training-exam-box">
                                <div class="training-exam" data-target="70537">
                                    <div class="training-exam__inner">
                                        <div class="training-exam__number">
                                            <span>70-537</span>
                                        </div>
                                        <div class="training-exam__desc">
                                            <p><strong>Microsoft Certified Professional (MCP)</strong></p>
                                            <p>Configuring and Operating a Hybrid Cloud with Microsoft Azure Stack
                                                (Beta)</p>
                                        </div>
                                    </div>
                                    <div class="training-exam__inner training-exam__inner--bg">
                                        <a href="#" class="button disabled">Сдача экзамена</a>
                                        <div class="training-exam__desc">
                                            <p>В 9 городах России в авторизованных Центрах тестирования Pearson VUE и
                                                Certiport
                                                вы сможете сдать экзамен (на английском языке). </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="training-exam-box">
                                <div class="training-exam" data-target="70535">
                                    <div class="training-exam__inner">
                                        <div class="training-exam__number">
                                            <span><a href="http://edu.softline.ru/certification/architecting-microsoft-azure-solutions-1">70-535</a></span>
                                        </div>
                                        <div class="training-exam__desc">
                                            <p><strong>Microsoft Certified Solutions Associate: Cloud Platform
                                                    (MCSA)</strong></p>
                                            <p>Architecting Microsoft Azure Solutions</p>
                                        </div>
                                    </div>
                                    <div class="training-exam__inner training-exam__inner--bg">
                                        <a href="http://edu.softline.ru/certification/architecting-microsoft-azure-solutions-1" class="button">Сдача экзамена</a>
                                        <div class="training-exam__desc">
                                            <p>В 9 городах России в авторизованных Центрах тестирования Pearson VUE и
                                                Certiport
                                                вы сможете сдать экзамен (на английском языке). </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="course-description-section">
        <div id="70533" class="">
            <div class="row">
                <div class="column small-12 large-8">
                    <div class="tab-box">
                        <ul class="tabs">
                            <li class="tabs-title"><span>обучение</span>
                            </li>
                        </ul>

                        <div class="tabs-content">
                            <div class="tabs-panel is-active">
                                <div class="content-bordered">
                                    <div class="b-editor  b-editor--small">
                                        <p style="text-transform: uppercase">
                                            <span class="h1">Бесплатные онлайн-курсы</span>
                                        </p>
                                        <p>Massive open online courses (MOOC) — настоящая находка для сотен тысяч
                                            специалистов по всему миру.
                                        </p>
                                        <p>
                                            Обучение по MOOCs очень сильно сэкономит ваше время
                                        </p>
                                        <a href=" https://online.academy4cloud.com/register" class="button">пройти регистрацию</a>
                                    </div>
                                </div>
                                <div class="content-bordered">
                                    <div class="b-editor  b-editor--small">
                                        <span><strong>Создание и работа с виртуальными машинами в Azure </strong></span>
                                        <p>
                                            <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE202x+course/about">Microsoft
                                                Azure Virtual Machines</a></p>
                                        <br>
                                        <p><strong>Внедрение Azure Active Directory</strong></p>
                                        <p>
                                            <a href="http://online.academy4cloud.com/courses/course-v1:Microsoft+AZURE204x+course/about">Microsoft
                                                Azure Identity</a></p>
                                        <br>
                                        <p><strong>Внедрение виртуальных сетей</strong></p>
                                        <p>
                                            <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE203x+course/about">Microsoft
                                                Azure Virtual Networks</a></p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-box">
                        <div class="tabs-content">
                            <div class="tabs-panel is-active">

                                <div class="content-bordered">
                                    <div class="b-editor b-editor--small">
                                        <p style="text-transform: uppercase">
                                            <span class="h1">Очное обучение в классе</span>
                                        </p>
                                        <p>Обучение на авторизованных курсах Microsoft в Учебном центре Softline в 13
                                            городах России или дистанционно — в вашем распоряжении!</p>
                                        <p>По итогам вам будут выданы сертификаты от вендора и Учебного центра
                                            Softline.</p>
                                    </div>
                                    <table width="100%" cellpadding="0" cellspacing="0"
                                           class="course-table examination">
                                        <tbody>
                                        <tr>
                                            <td class="course-table__img">Код курса</td>
                                            <td class="course-table__name">Название курса</td>
                                            <td class="course-table__price"><span>Стоимость</span></td>
                                            <td width="5"></td>
                                        </tr>
                                        <tr>
                                            <td class="course-table__img">
                                                <p>20533</p>
                                            </td>
                                            <td class="course-table__name">
                                                <a href="http://edu.softline.ru/vendors/microsoft/course-20533">
                                                    Применение инфраструктурных решений Microsoft Azure
                                                    <span class="add-info">
                                                <span>EN</span>
                                                <span>150 мин</span>
                                            </span>
                                                </a>
                                            </td>
                                            <td class="course-table__price">
                                                <p>32 200 руб.</p>
                                                <p><span>г. Москва</span></p>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="course-table__img">
                                                <p>10992</p>
                                            </td>
                                            <td class="course-table__name">
                                                <a href="https://online.academy4cloud.com/courses">
                                                    Интеграция базовой локальной инфраструктуры с Microsoft Azure
                                                    <span class="add-info">
                                                <span>EN</span>
                                                <span>150 мин</span>
                                            </span>
                                                </a>

                                            </td>
                                            <td class="course-table__price">
                                                <p>25 000 руб.</p>
                                                <p><span>г. Москва</span></p>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="course-table__img">
                                                <p>20535</p>
                                            </td>
                                            <td class="course-table__name">
                                                <a href="http://edu.softline.ru/vendors/microsoft/course-20535">
                                                    Применение инфраструктурных решений Microsoft Azure
                                                    <span class="add-info">
                                                <span>EN</span>
                                                <span>150 мин</span>
                                            </span>
                                                </a>
                                            </td>
                                            <td class="course-table__price">
                                                <p>36 000 руб.</p>
                                                <p><span>г. Москва</span></p>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column small-12 large-4">
                    <div class="aside-widget__title small">Дополнительные услуги:</div>
                    <div class="aside-widget">
                        <div class="content-bordered content-bordered--bg">
                            <div style="height: 100%;" class="flex-container direction-column space-between">
                                <div>
                                    <div class="b-editor b-editor--small">
                                        <p><img src="dist/images/guide/certificate-microsoft.png" alt="">
                                        </p>
                                        <p><strong>Сертификат от корпорации Microsoft (Certificate of
                                                Completion)</strong>
                                        </p>
                                        <p>Хотите иметь на руках подтверждение о прохождении курса? Нет
                                            ничего
                                            проще: сертификационный документ от корпорации Microsoft
                                            (Certificate of
                                            Completion) будет выдан вам Учебным центром Softline по запросу
                                            Отличное
                                            приложение к резюме!</p>
                                    </div>
                                </div>
                                <div>
                                    <div class="b-editor b-editor--small">
                                        <p><strong>Стоимость услуги: 6 000 руб.</strong></p>
                                        <a href="http://edu.softline.ru/reviews/microsoft-options" class="button small">Заказать</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="content-bordered content-bordered--bg">
                            <div style="height: 100%" class="flex-container direction-column space-between">
                                <div>
                                    <div class="b-editor b-editor--small">
                                        <p><img src="dist/images/guide/certificate-online.png" alt=""></p>
                                        <p><strong>Онлайн-сессия с сертифицированным русскоязычным тренером
                                                –
                                                задайте все оставшиеся вопросы!</strong></p>
                                        <p>Даже опытным людям бывает непросто разобраться в большом потоке
                                            нового
                                            материала. Так что, если вдруг у вас остались вопросы –
                                            запишитесь
                                            на
                                            онлайн-сессию с сертифицированным русскоязычным тренером. Он
                                            заранее
                                            подготовит максимум информации, чтобы дать вам подробный ответ.
                                            Формат –
                                            дистанционный. Количество слушателей в сессии – не более 7.
                                            Минимальная
                                            длительность сессии – 2 часа.</p>
                                    </div>
                                </div>
                                <div>
                                    <div class="b-editor b-editor--small">
                                        <p><strong>Стоимость сессии: 5 000 руб.</strong></p>
                                        <a href="http://edu.softline.ru/reviews/microsoft-options" class="button small">Заказать</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="70532" class="hide">
            <div class="row">
                <div class="column small-12 large-8">
                    <div class="tab-box">
                        <ul class="tabs">
                            <li class="tabs-title"><span>обучение</span>
                            </li>
                        </ul>

                        <div class="tabs-content">
                            <div class="tabs-panel is-active">
                                <div class="content-bordered">
                                    <div class="b-editor  b-editor--small">
                                        <p style="text-transform: uppercase">
                                            <span class="h1">Бесплатные онлайн-курсы</span>
                                        </p>
                                        <p>Massive open online courses (MOOC) — настоящая находка для сотен тысяч
                                            специалистов по всему миру.
                                        </p>
                                        <p>
                                            Обучение по MOOCs очень сильно сэкономит ваше время
                                        </p>
                                        <a href="https://online.academy4cloud.com/register" class="button">пройти регистрацию</a>
                                    </div>
                                </div>
                                <div class="content-bordered">
                                    <div class="b-editor  b-editor--small">
                                        <span><strong>Создание и работа с виртуальными машинами в Azure </strong></span>
                                        <p>
                                            <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE202x+course/about">Microsoft
                                                Azure Virtual Machines</a></p>
                                        <p>
                                            <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE203x+course/about">Microsoft
                                                Azure Virtual Networks</a></p>
                                        <br>
                                        <p><strong>Разработка и реализация стратегии хранения данных</strong></p>
                                        <p>
                                            <a href="https://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+DAT224x+course/about">Developing
                                                a Multidimensional Data Model
                                            </a></p>
                                        <br>
                                        <p><strong>Управление идентификационными данными, приложениями и сетевыми
                                                службами</strong></p>
                                        <p>
                                            <a href="http://online.academy4cloud.com/courses/course-v1:Microsoft+AZURE204x+course/about">Microsoft
                                                Azure Identity</a></p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-box">
                        <div class="tabs-content">
                            <div class="tabs-panel is-active">

                                <div class="content-bordered">
                                    <div class="b-editor b-editor--small">
                                        <p style="text-transform: uppercase">
                                            <span class="h1">Очное обучение в классе</span>
                                        </p>
                                        <p>Обучение на авторизованных курсах Microsoft в Учебном центре Softline в
                                            13
                                            городах России или дистанционно — в вашем распоряжении!</p>
                                        <p>По итогам вам будут выданы сертификаты от вендора и Учебного центра
                                            Softline.</p>
                                    </div>
                                    <table width="100%" cellpadding="0" cellspacing="0"
                                           class="course-table examination">
                                        <tbody>
                                        <tr>
                                            <td class="course-table__img">Код курса</td>
                                            <td class="course-table__name">Название курса</td>
                                            <td class="course-table__price"><span>Стоимость</span></td>
                                            <td width="5"></td>
                                        </tr>
                                        <tr>
                                            <td class="course-table__img">
                                                <p>20532</p>
                                            </td>
                                            <td class="course-table__name">
                                                <a href="http://edu.softline.ru/vendors/microsoft/course-20532">
                                                    Разработка решений Microsoft Azure
                                                    <span class="add-info">
                                                <span>EN</span>
                                                <span>150 мин</span>
                                            </span>
                                                </a>
                                            </td>
                                            <td class="course-table__price">
                                                <p>28 500 руб.</p>
                                                <p><span>г. Москва</span></p>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="course-table__img">
                                                <p>40533</p>
                                            </td>
                                            <td class="course-table__name">
                                                <a href="http://edu.softline.ru/directions">
                                                    Microsoft Cloud Workshop: OSS PaaS and DevOps (Coming soon)
                                                    <span class="add-info">
                                                <span>EN</span>
                                                <span>150 мин</span>
                                            </span>
                                                </a>

                                            </td>
                                            <td class="course-table__price">
                                                <p>25 000 руб.</p>
                                                <p><span>г. Москва</span></p>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="course-table__img">
                                                <p>40501</p>
                                            </td>
                                            <td class="course-table__name">
                                                <a href="http://edu.softline.ru/directions">
                                                    Microsoft Cloud Workshop: Container and DevOps (Coming soon)
                                                    <span class="add-info">
                                                <span>EN</span>
                                                <span>150 мин</span>
                                            </span>
                                                </a>
                                            </td>
                                            <td class="course-table__price">
                                                <p>36 000 руб.</p>
                                                <p><span>г. Москва</span></p>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="course-table__img">
                                                <p>40509</p>
                                            </td>
                                            <td class="course-table__name">
                                                <a href="http://edu.softline.ru/directions">
                                                    Microsoft Cloud Workshop: OSS DevOps (Coming soon)
                                                    <span class="add-info">
                                                <span>EN</span>
                                                <span>150 мин</span>
                                            </span>
                                                </a>
                                            </td>
                                            <td class="course-table__price">
                                                <p>36 000 руб.</p>
                                                <p><span>г. Москва</span></p>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column small-12 large-4">
                    <div class="aside-widget__title small">Дополнительные услуги:</div>
                    <div class="aside-widget">
                        <div class="content-bordered content-bordered--bg">
                            <div style="height: 100%;" class=" flex-container direction-column space-between
                                ">
                                <div>
                                    <div class="b-editor b-editor--small">
                                        <p><img src="dist/images/guide/certificate-microsoft.png" alt="">
                                        </p>
                                        <p><strong>Сертификат от корпорации Microsoft (Certificate of
                                                Completion)</strong>
                                        </p>
                                        <p>Хотите иметь на руках подтверждение о прохождении курса? Нет
                                            ничего
                                            проще: сертификационный документ от корпорации Microsoft
                                            (Certificate of
                                            Completion) будет выдан вам Учебным центром Softline по запросу
                                            Отличное
                                            приложение к резюме!</p>
                                    </div>
                                </div>
                                <div>
                                    <div class="b-editor b-editor--small">
                                        <p><strong>Стоимость услуги: 6 000 руб.</strong></p>
                                        <a href="http://edu.softline.ru/reviews/microsoft-options" class="button small">Заказать</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="content-bordered content-bordered--bg">
                            <div style="height: 100%" class="flex-container direction-column space-between">
                                <div>
                                    <div class="b-editor b-editor--small">
                                        <p><img src="dist/images/guide/certificate-online.png" alt=""></p>
                                        <p><strong>Онлайн-сессия с сертифицированным русскоязычным тренером
                                                –
                                                задайте все оставшиеся вопросы!</strong></p>
                                        <p>Даже опытным людям бывает непросто разобраться в большом потоке
                                            нового
                                            материала. Так что, если вдруг у вас остались вопросы –
                                            запишитесь
                                            на
                                            онлайн-сессию с сертифицированным русскоязычным тренером. Он
                                            заранее
                                            подготовит максимум информации, чтобы дать вам подробный ответ.
                                            Формат –
                                            дистанционный. Количество слушателей в сессии – не более 7.
                                            Минимальная
                                            длительность сессии – 2 часа.</p>
                                    </div>
                                </div>
                                <div>
                                    <div class="b-editor b-editor--small">
                                        <p><strong>Стоимость сессии: 5 000 руб.</strong></p>
                                        <a href="http://edu.softline.ru/reviews/microsoft-options" class="button small">Заказать</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="70535" class="hide">
            <div class="row">
                <div class="column small-12 large-8">
                    <div class="tab-box">
                        <ul class="tabs">
                            <li class="tabs-title"><span>обучение</span>
                            </li>
                        </ul>

                        <div class="tabs-content">
                            <div class="tabs-panel is-active">
                                <div class="content-bordered">
                                    <div class="b-editor  b-editor--small">
                                        <p style="text-transform: uppercase">
                                            <span class="h1">Бесплатные онлайн-курсы</span>
                                        </p>
                                        <p>Massive open online courses (MOOC) — настоящая находка для сотен тысяч
                                            специалистов по всему миру.
                                        </p>
                                        <p>
                                            Обучение по MOOCs очень сильно сэкономит ваше время
                                        </p>
                                        <a href="https://online.academy4cloud.com/register" class="button">пройти регистрацию</a>
                                    </div>
                                </div>
                                <div class="content-bordered">
                                    <div class="b-editor  b-editor--small">
                                        <p>
                                            <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE205x+course/about">Microsoft
                                                Azure Storage</a></p>
                                        <p>
                                            <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE206x+course/about">Microsoft
                                                Azure App Service</a></p>
                                        <p>
                                            <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE207x+course/about">Databases
                                                in Azure</a></p>
                                        <p>
                                            <a href="http://online.academy4cloud.com/courses/course-v1:PartnerFY18Q2+AZURE210x+course/about">Automating
                                                Azure Workloads</a></p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-box">
                        <div class="tabs-content">
                            <div class="tabs-panel is-active">

                                <div class="content-bordered">
                                    <div class="b-editor b-editor--small">
                                        <p style="text-transform: uppercase">
                                            <span class="h1">Очное обучение в классе</span>
                                        </p>
                                        <p>Обучение на авторизованных курсах Microsoft в Учебном центре Softline в
                                            13
                                            городах России или дистанционно — в вашем распоряжении!</p>
                                        <p>По итогам вам будут выданы сертификаты от вендора и Учебного центра
                                            Softline.</p>
                                    </div>
                                    <table width="100%" cellpadding="0" cellspacing="0"
                                           class="course-table examination">
                                        <tbody>
                                        <tr>
                                            <td class="course-table__img">Код курса</td>
                                            <td class="course-table__name">Название курса</td>
                                            <td class="course-table__price"><span>Стоимость</span></td>
                                            <td width="5"></td>
                                        </tr>
                                        <tr>
                                            <td class="course-table__img">
                                                <p>20535</p>
                                            </td>
                                            <td class="course-table__name">
                                                <a href="http://edu.softline.ru/vendors/microsoft/course-20535">
                                                    Применение инфраструктурных решений Microsoft Azure
                                                    <span class="add-info">
                                                <span>EN</span>
                                                <span>150 мин</span>
                                            </span>
                                                </a>
                                            </td>
                                            <td class="course-table__price">
                                                <p>36 000 руб.</p>
                                                <p><span>г. Москва</span></p>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="course-table__img">
                                                <p>40522</p>
                                            </td>
                                            <td class="course-table__name">
                                                <a href="https://online.academy4cloud.com/courses">
                                                    Microsoft Cloud Workshop: Azure Stack(Coming soon)
                                                    <span class="add-info">
                                                <span>EN</span>
                                                <span>150 мин</span>
                                            </span>
                                                </a>

                                            </td>
                                            <td class="course-table__price">
                                                <p>Coming soon</p>
                                                <p><span>г. Москва</span></p>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column small-12 large-4">
                    <div class="aside-widget__title small">Дополнительные услуги:</div>
                    <div class="aside-widget">
                        <div class="content-bordered content-bordered--bg">
                            <div style="height: 100%;" class=" flex-container direction-column space-between
                                ">
                                <div>
                                    <div class="b-editor b-editor--small">
                                        <p><img src="dist/images/guide/certificate-microsoft.png" alt="">
                                        </p>
                                        <p><strong>Сертификат от корпорации Microsoft (Certificate of
                                                Completion)</strong>
                                        </p>
                                        <p>Хотите иметь на руках подтверждение о прохождении курса? Нет
                                            ничего
                                            проще: сертификационный документ от корпорации Microsoft
                                            (Certificate of
                                            Completion) будет выдан вам Учебным центром Softline по запросу
                                            Отличное
                                            приложение к резюме!</p>
                                    </div>
                                </div>
                                <div>
                                    <div class="b-editor b-editor--small">
                                        <p><strong>Стоимость услуги: 6 000 руб.</strong></p>
                                        <a href="http://edu.softline.ru/reviews/microsoft-options" class="button small">Заказать</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="content-bordered content-bordered--bg">
                            <div style="height: 100%" class="flex-container direction-column space-between">
                                <div>
                                    <div class="b-editor b-editor--small">
                                        <p><img src="dist/images/guide/certificate-online.png" alt=""></p>
                                        <p><strong>Онлайн-сессия с сертифицированным русскоязычным тренером
                                                –
                                                задайте все оставшиеся вопросы!</strong></p>
                                        <p>Даже опытным людям бывает непросто разобраться в большом потоке
                                            нового
                                            материала. Так что, если вдруг у вас остались вопросы –
                                            запишитесь
                                            на
                                            онлайн-сессию с сертифицированным русскоязычным тренером. Он
                                            заранее
                                            подготовит максимум информации, чтобы дать вам подробный ответ.
                                            Формат –
                                            дистанционный. Количество слушателей в сессии – не более 7.
                                            Минимальная
                                            длительность сессии – 2 часа.</p>
                                    </div>
                                </div>
                                <div>
                                    <div class="b-editor b-editor--small">
                                        <p><strong>Стоимость сессии: 5 000 руб.</strong></p>
                                        <a href="http://edu.softline.ru/reviews/microsoft-options" class="button small">Заказать</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="70537" class="hide">
            <div class="row">
                <div class="column small-12 large-8">
                    <div class="tab-box">
                        <ul class="tabs">
                            <li class="tabs-title"><span>обучение</span>
                            </li>
                        </ul>

                        <div class="tabs-content">
                            <div class="tabs-panel is-active">
                                <div class="content-bordered">
                                    <div class="b-editor  b-editor--small">
                                        <p style="text-transform: uppercase">
                                            <span class="h1">Бесплатные онлайн-курсы</span>
                                        </p>
                                        <p>Massive open online courses (MOOC) — настоящая находка для сотен тысяч
                                            специалистов по всему миру.
                                        </p>
                                        <p>
                                            Обучение по MOOCs очень сильно сэкономит ваше время
                                        </p>
                                        <a href="https://online.academy4cloud.com/register" class="button">пройти регистрацию</a>
                                    </div>
                                </div>
                                <div class="content-bordered">
                                    <div class="b-editor  b-editor--small">
                                        <p><strong>Coming soon</strong></p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-box">
                        <div class="tabs-content">
                            <div class="tabs-panel is-active">

                                <div class="content-bordered">
                                    <div class="b-editor b-editor--small">
                                        <p style="text-transform: uppercase">
                                            <span class="h1">Очное обучение в классе</span>
                                        </p>
                                        <p>Обучение на авторизованных курсах Microsoft в Учебном центре Softline в
                                            13
                                            городах России или дистанционно — в вашем распоряжении!</p>
                                        <p>По итогам вам будут выданы сертификаты от вендора и Учебного центра
                                            Softline.</p>
                                    </div>
                                    <table width="100%" cellpadding="0" cellspacing="0"
                                           class="course-table examination">
                                        <tbody>
                                        <tr>
                                            <td class="course-table__img">Код курса</td>
                                            <td class="course-table__name">Название курса</td>
                                            <td class="course-table__price"><span>Стоимость</span></td>
                                            <td width="5"></td>
                                        </tr>
                                        <tr>
                                            <td class="course-table__img">
                                                <p>20533</p>
                                            </td>
                                            <td class="course-table__name">
                                                <a href="http://edu.softline.ru/vendors/microsoft/course-20533">
                                                    Применение инфраструктурных решений Microsoft Azure
                                                    <span class="add-info">
                                                <span>EN</span>
                                                <span>150 мин</span>
                                            </span>
                                                </a>
                                            </td>
                                            <td class="course-table__price">
                                                <p>32 200 руб.</p>
                                                <p><span>г. Москва</span></p>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="course-table__img">
                                                <p>20537</p>
                                            </td>
                                            <td class="course-table__name">
                                                <a href="http://edu.softline.ru/vendors/microsoft/course-20537">
                                                    Настройка и управление гибридным облаком с помощью Microsoft
                                                    Azure
                                                    Stack
                                                    <span class="add-info">
                                                <span>EN</span>
                                                <span>150 мин</span>
                                            </span>
                                                </a>

                                            </td>
                                            <td class="course-table__price">
                                                <p>28 980 руб.</p>
                                                <p><span>г. Москва</span></p>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column small-12 large-4">
                    <div class="aside-widget__title small">Дополнительные услуги:</div>
                    <div class="aside-widget">
                        <div class="content-bordered content-bordered--bg">
                            <div style="height: 100%;" class=" flex-container direction-column space-between
                                ">
                                <div>
                                    <div class="b-editor b-editor--small">
                                        <p><img src="dist/images/guide/certificate-microsoft.png" alt="">
                                        </p>
                                        <p><strong>Сертификат от корпорации Microsoft (Certificate of
                                                Completion)</strong>
                                        </p>
                                        <p>Хотите иметь на руках подтверждение о прохождении курса? Нет
                                            ничего
                                            проще: сертификационный документ от корпорации Microsoft
                                            (Certificate of
                                            Completion) будет выдан вам Учебным центром Softline по запросу
                                            Отличное
                                            приложение к резюме!</p>
                                    </div>
                                </div>
                                <div>
                                    <div class="b-editor b-editor--small">
                                        <p><strong>Стоимость услуги: 6 000 руб.</strong></p>
                                        <a href="http://edu.softline.ru/reviews/microsoft-options" class="button small">Заказать</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="content-bordered content-bordered--bg">
                            <div style="height: 100%" class="flex-container direction-column space-between">
                                <div>
                                    <div class="b-editor b-editor--small">
                                        <p><img src="dist/images/guide/certificate-online.png" alt=""></p>
                                        <p><strong>Онлайн-сессия с сертифицированным русскоязычным тренером
                                                –
                                                задайте все оставшиеся вопросы!</strong></p>
                                        <p>Даже опытным людям бывает непросто разобраться в большом потоке
                                            нового
                                            материала. Так что, если вдруг у вас остались вопросы –
                                            запишитесь
                                            на
                                            онлайн-сессию с сертифицированным русскоязычным тренером. Он
                                            заранее
                                            подготовит максимум информации, чтобы дать вам подробный ответ.
                                            Формат –
                                            дистанционный. Количество слушателей в сессии – не более 7.
                                            Минимальная
                                            длительность сессии – 2 часа.</p>
                                    </div>
                                </div>
                                <div>
                                    <div class="b-editor b-editor--small">
                                        <p><strong>Стоимость сессии: 5 000 руб.</strong></p>
                                        <a href="http://edu.softline.ru/reviews/microsoft-options" class="button small">Заказать</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="subscribe-banner">
        <div class="subscribe-banner__wrapper">
            <div class="row">
                <div class="column small-12 large-9 large-offset-3">
                    <div class="subscribe-banner__title">
                        <span>Будьте в курсе</span>
                    </div>
                    <form class="subscribe-banner__form">
                        <div class="subscribe-banner__input subscribe-banner__input--name">
                            <!--доп модификатор: error-->
                            <input type="text" placeholder="Ваше имя">
                        </div>
                        <div class="subscribe-banner__input subscribe-banner__input--email">
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="subscribe-banner__button">
                            <button class="button">Подписаться</button>
                        </div>
                    </form>
                    <div class="subscribe-banner__desc">
                        <span>Подпишитесь на информацию о новинках, скидках и акциях. <br> Уже более 36 000 подписчиков!</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        $('.select-role__inner').select2({
            tags: true
        });
        $(document).foundation();

        function showCourseContent() {

        }

        $('body').on('click', '.training-exam', function (e) {

            var _ = $(this);

            _.closest('.training-exam-wrap').find('.training-exam').removeClass('is-active');

            _.addClass('is-active');

            $('#course-description-section').children('div').addClass('hide').filter('[id="' + _.data('target') + '"]').removeClass('hide').foundation();
        });

        $('#training-program').on('change.zf.tabs', function (e) {

        })
    });

</script>