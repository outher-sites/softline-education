<? include 'partials/header.php'; ?>

<main>
    <section class="course-header">
        <div class="row">
            <div class="column small-12 medium-7 large-8">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Учебный центр</a></li>
                    <li><a href="#">Сертификация и экзамены</a></li>
                </ul>
                <div class="course-header__wrapper">
                    <div class="course-header__title">
                        <h1>Сертификация и экзамены</h1>
                    </div>
                </div>
            </div>
            <div class="column small-12 medium-5 large-3 large-offset-1">
                <div class="feedback-header">
                    <div class="feedback-header__name">
                        <span>Светлана Жученко</span>
                    </div>
                    <div class="feedback-header__photo">
                        <img src="dist/images/feedback__header/feedback-header__photo/photo-1.png" alt="">
                    </div>
                    <div class="feedback-header__position">
                        <span>менеджер</span>
                        <span>интернет-магазина</span>
                    </div>
                    <div class="feedback-header__contacts">
                        <span>8 (800) 200-08-60 доб. 6011</span>
                        <a href="Svetlana.Zhuchenko@softlinegroup.com">Svetlana.Zhuchenko@softlinegroup.com</a>
                    </div>

                    <a href="#" class="button expanded">Связаться сейчас</a>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="row">
            <div class="column small-12 large-8">
                <div class="b-editor exams-description">
                    <p>
                        На базе Учебного центра Softline действуют центры тестирования Prometric, Certiport, Red Hat,
                        Лаборатории Касперского и VUE. Наши центры тестирования позволяют IT-специалистам пройти
                        сертификацию и получить официальный статус сертифицированного специалиста по любым направлениям
                    </p>
                    <p>
                        Также вы можете воспользоваться системой предварительной оценки персонала на соответствие знаний
                        и навыков в области ИТ – <a href="#">Alltests</a>
                    </p>
                    <p>
                        Внимание: цены на экзамены могут меняться, просьба уточнять актуальные цены у менеджеров!
                    </p>
                    <p></p>
                    <p>
                        <span><img src="/content/exams/layer-293.png" alt=""></span>
                        <span><img src="/content/exams/layer-294.png" alt=""></span>
                        <span><img src="/content/exams/vector-smart-object.png" alt=""></span>
                        <br>
                        <span><img src="/content/exams/layer-295.png" alt=""></span>
                        <span><img src="/content/exams/layer-296.png" alt=""></span>
                    </p>
                </div>

                <div class="accordion-box">
                    <div class="accordion-box__title">
                        <h4>Информация для кандидатов Центров тестирования Учебного центра Sоftline:</h4>
                    </div>
                    <div class="accordion-box__content">
                        <ul class="accordion" data-accordion>
                            <li class="accordion-item" data-accordion-item>
                                <a href="#" class="accordion-title"><span>Правила сдачи тестирования</span>
                                    <svg class="icon arrow">
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </a>

                                <div class="accordion-content" data-tab-content>
                                    lorem ipsum
                                </div>
                            </li>
                            <li class="accordion-item" data-accordion-item>
                                <a href="#" class="accordion-title"><span>Что представляет собой процедура тестирования?</span>
                                    <svg class="icon arrow">
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </a>

                                <div class="accordion-content" data-tab-content>
                                    lorem ipsum
                                </div>
                            </li>
                            <li class="accordion-item is-active" data-accordion-item>
                                <a href="#" class="accordion-title"><span>Время и место проведения тестирования</span>
                                    <svg class="icon arrow">
                                        <use xlink:href="#images--svg--arrow"></use>
                                    </svg>
                                </a>

                                <div class="accordion-content" data-tab-content>
                                    <p>
                                        Тестирование можно пройти в одном из 22 Центров тестирования Учебного центра
                                        Softline
                                        (17 в России и 5 в странах СНГ). Тестирование проводится каждый день. Время
                                        проведения
                                        экзаменационного теста необходимо согласовывать с администратором Центра
                                        тестирования.
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="accordion-box__footer">
                        <p>
                            <a href="#">Скачать документ с полной информацией для кандидатов Центров тестирования
                                Учебного
                                центра Sоftline</a>
                        </p>
                    </div>
                </div>

                <div class="tab-box">
                    <ul class="tabs" data-tabs id="example-tabs">
                        <li class="tabs-title is-active"><a href="#panel1" aria-selected="true">Сертификация</a></li>
                        <li class="tabs-title"><a data-tabs-target="panel2" href="#panel2">Экзамены</a></li>
                    </ul>

                    <div class="tabs-content" data-tabs-content="example-tabs">
                        <div class="tabs-panel is-active" id="panel1">
                            <div class="filter inline">
                                <div class="row">
                                    <div class="small-12 medium-6 column">
                                        <div class="filter__input">
                                            <div class="filter__label">
                                                <span>Вендор:</span>
                                            </div>
                                            <label class="b-select">
                                                <select class="b-select__container">
                                                    <option value="">Все вендоры</option>
                                                    <option value="">Все вендоры</option>
                                                    <option value="">Все вендоры</option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="certificate-list">
                                <div class="certificate">
                                    <div class="certificate__photo">
                                        <img src="content/sert/layer-299.png" alt="">
                                    </div>
                                    <div class="certificate__content">
                                        <div class="certificate__title">CCENT</div>
                                        <ul class="breadcrumbs">
                                            <li><a href="#">Cisco Systems, Inc</a></li>
                                            <li><a href="#">Центр тестирования VUE</a></li>
                                        </ul>
                                        <ul class="certificate__prices">
                                            <li class="certificate__price">
                                                <span>Общая цена подготовки (1 курс) <i class="icon icon-help">?</i></span>
                                                <span>53 500 руб</span>
                                            </li>
                                            <li class="certificate__price">
                                                <span>Общая цена сертификации (1 экзамен) <i class="icon icon-help">?</i></span>
                                                <span>14 700 руб</span>
                                            </li>
                                        </ul>
                                        <div class="certificate__description b-editor">
                                            <p>
                                                Первый уровень в системе сертификаций Cisco — уровень entry и начинается
                                                он с Сертифицированного Техника Сетей Cisco (Сіsco Certified Entry
                                                Networking Technician) — CCENT. Это — промежуточный шаг к Associate
                                                уровню — сертификациям CCNA и CCDA.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="certificate">
                                    <div class="certificate__photo">
                                        <img src="content/sert/layer-299.png" alt="">
                                    </div>
                                    <div class="certificate__content">
                                        <div class="certificate__title">CCENT</div>
                                        <ul class="breadcrumbs">
                                            <li><a href="#">Cisco Systems, Inc</a></li>
                                            <li><a href="#">Центр тестирования VUE</a></li>
                                        </ul>
                                        <ul class="certificate__prices">
                                            <li class="certificate__price">
                                                <span>Общая цена подготовки (1 курс) <i class="icon icon-help">?</i></span>
                                                <span>53 500 руб</span>
                                            </li>
                                            <li class="certificate__price">
                                                <span>Общая цена сертификации (1 экзамен) <i class="icon icon-help">?</i></span>
                                                <span>14 700 руб</span>
                                            </li>
                                        </ul>
                                        <div class="certificate__description b-editor">
                                            <p>
                                                Первый уровень в системе сертификаций Cisco — уровень entry и начинается
                                                он с Сертифицированного Техника Сетей Cisco (Сіsco Certified Entry
                                                Networking Technician) — CCENT. Это — промежуточный шаг к Associate
                                                уровню — сертификациям CCNA и CCDA.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tabs-panel" id="panel2">
                            <div class="filter inline">
                                <div class="row">
                                    <div class="small-12 medium-12 column">
                                        <div class="filter__input">
                                            <div class="filter__label">
                                                <span>Код экзамена:</span>
                                            </div>
                                            <label class="b-select">
                                                <input placeholder="Введите код или имя экзамена" type="text">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="small-12 medium-6 column">
                                        <div class="filter__input">
                                            <div class="filter__label">
                                                <span>Вендор:</span>
                                            </div>
                                            <label class="b-select">
                                                <select class="b-select__container">
                                                    <option value="">Все вендоры</option>
                                                    <option value="">Все вендоры</option>
                                                    <option value="">Все вендоры</option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="small-12 medium-6 column">
                                        <div class="filter__input">
                                            <div class="filter__label">
                                                <span>Место проведения:</span>
                                            </div>
                                            <label class="b-select">
                                                <select class="b-select__container">
                                                    <option value="">Москва</option>
                                                    <option value="">Пермь</option>
                                                    <option value="">Уфа</option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <table width="100%" cellpadding="0" cellspacing="0" class="course-table examination">
                                <tbody>
                                <tr>
                                    <td class="course-table__img">Код курса</td>
                                    <td class="course-table__name">Название курса</td>
                                    <td class="course-table__price"><span>Стоимость</span></td>
                                    <td class="course-table__basket" width="120"></td>
                                </tr>
                                <tr>
                                    <td class="course-table__img">
                                        <p>RY0100</p>
                                    </td>
                                    <td class="course-table__name">
                                        <a href="#">
                                            Ruby Association Certified Ruby Programmer Silver
                                            <span class="add-info">
                                                <span>EN</span>
                                                <span>150 мин</span>
                                            </span>
                                        </a>
                                        <span>Ruby Association / Центр тестирования Прометрик</span>
                                    </td>
                                    <td class="course-table__price">
                                        <p>17 375 руб.</p>
                                        <p><span>г. Москва</span></p>
                                    </td>
                                    <td class="course-table__basket">
                                        <a href="#">в корзину</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="course-table__img">
                                        <p>RY0100</p>
                                    </td>
                                    <td class="course-table__name">
                                        <a href="#">
                                            Ruby Association Certified Ruby Programmer Silver
                                            <span class="add-info">
                                                <span>EN</span>
                                                <span>150 мин</span>
                                            </span>
                                        </a>
                                        <span>Ruby Association / Центр тестирования Прометрик</span>
                                    </td>
                                    <td class="course-table__price">
                                        <p>17 375 руб.</p>
                                        <p><span>г. Москва</span></p>
                                    </td>
                                    <td class="course-table__basket">
                                        <a href="#">в корзину</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="course-table__img">
                                        <p>RY0100</p>
                                    </td>
                                    <td class="course-table__name">
                                        <a href="#">
                                            Ruby Association Certified Ruby Programmer Silver
                                            <span class="add-info">
                                                <span>EN</span>
                                                <span>150 мин</span>
                                            </span>
                                        </a>
                                        <span>Ruby Association / Центр тестирования Прометрик</span>
                                    </td>
                                    <td class="course-table__price">
                                        <p>17 375 руб.</p>
                                        <p><span>г. Москва</span></p>
                                    </td>
                                    <td class="course-table__basket">
                                        <a href="#">в корзину</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="pagination-wrapper">
                                <ul class="pagination" role="navigation" aria-label="Pagination">
                                    <li class="pagination-previous disabled"></li>
                                    <li class="current"><span class="show-for-sr">You're on page</span> 1</li>
                                    <li><a href="#" aria-label="Page 2">2</a></li>
                                    <li><a href="#" aria-label="Page 3">3</a></li>
                                    <li><a href="#" aria-label="Page 4">4</a></li>
                                    <li><a href="#" aria-label="Page 5">5</a></li>
                                    <li class="pagination-next"><a href="#" aria-label="Next page"></a></li>
                                </ul>

                                <div class="pagination-select show-for-large">
                                    <span>Элем. на стр.</span>
                                    <label class="b-select">
                                        <select class="b-select__container">
                                            <option value="24">24</option>
                                            <option value="50">50</option>
                                        </select>
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="column small-12 large-offset-1 large-3">
                <aside>
                    <div class="aside-widget">
                        <div class="aside-widget__title">О центре</div>
                        <div class="aside-widget__content">
                            <ul class="aside-menu">


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/mission">

                                        <span class="b-link__text">Программирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/training-conditions">

                                        <span class="b-link__text">Системное администрирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class=""
                                       href="http://edu.softline.ru/about/svedeniya-ob-obrazovatelnoy-organizatsii">

                                        <span class="b-link__text">Пользовательское ПО</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/accreditation">

                                        <span class="b-link__text">Операционные системы (ОС)</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/status">

                                        <span class="b-link__text">Сетевые технологии</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-klientov">

                                        <span class="b-link__text">Информационная безопасность</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/otzyivyi-partnerov">

                                        <span class="b-link__text">Виртуализация</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/razrabotka-kursov">

                                        <span class="b-link__text">Базы данных и СУБД</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/delivery-info">

                                        <span class="b-link__text">Моделирование и САПР</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/requisites">

                                        <span class="b-link__text">Коммуникации</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/tech-support">

                                        <span class="b-link__text">Резервное копирование</span>
                                    </a>

                                </li>


                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">CRM</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Бизнес-аналитика</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">IT-сервис менеджмент</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Импортозамещение ПО</span>
                                    </a>

                                </li>
                                <li class="">

                                    <a class="" href="http://edu.softline.ru/about/contact">

                                        <span class="b-link__text">Бизнес-тренинги</span>
                                    </a>

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__title">Будьте в курсе</div>
                        <div class="aside-widget__content">
                            <p>Подпишитесь на рассылку новостей Softline</p>
                            <form action="#">
                                <label for="">
                                    <input type="text" placeholder="Ваш e-mail">
                                </label>
                                <label for="">
                                    <input type="text" placeholder="Ваше имя">
                                </label>
                                <button class="button small expanded">Подписаться</button>
                            </form>
                        </div>
                    </div>
                    <div class="aside-widget">
                        <div class="aside-widget__content">
                            <a href="#">
                                <img src="content/banners/kasp.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>

    <section class="subscribe-banner">
        <div class="subscribe-banner__wrapper">
            <div class="row">
                <div class="column small-12 large-9 large-offset-3">
                    <div class="subscribe-banner__title">
                        <span>Будьте в курсе</span>
                    </div>
                    <form class="subscribe-banner__form">
                        <div class="subscribe-banner__input subscribe-banner__input--name">
                            <!--доп модификатор: error-->
                            <input type="text" placeholder="Ваше имя">
                        </div>
                        <div class="subscribe-banner__input subscribe-banner__input--email">
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="subscribe-banner__button">
                            <button class="button">Подписаться</button>
                        </div>
                    </form>
                    <div class="subscribe-banner__desc">
                        <span>Подпишитесь на информацию о новинках, скидках и акциях. <br> Уже более 36 000 подписчиков!</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>

<? include 'partials/footer.php'; ?>
<script src="dist/javascript/bundle.js"></script>
<script>
    $(document).ready(function () {
        $('.b-select__container').select2();
        $(document).foundation();
    });
</script>
</body>
</html>
